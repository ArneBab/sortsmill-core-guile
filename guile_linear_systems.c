#include <config.h>

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <libintl.h>
#include <limits.h>
#include <string.h>
#include <sortsmill/guile/core.h>

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

#define _MAX(x, y) (((x) < (y)) ? (y) : (x))

STM_SCM_ONE_TIME_INITIALIZE (STM_ATTRIBUTE_PURE static, SCM, symbol_f32,
                             (symbol_f32__Value =
                              scm_from_latin1_symbol ("f32")));
STM_SCM_ONE_TIME_INITIALIZE (STM_ATTRIBUTE_PURE static, SCM, symbol_f64,
                             (symbol_f64__Value =
                              scm_from_latin1_symbol ("f64")));

static void
check_augmented_matrix_dimensions (const char *who, SCM A, SCM B,
                                   ssize_t m_A, ssize_t n_A,
                                   ssize_t m_B, ssize_t n_B)
{
  if (m_A != m_B)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition
        (_("the leading dimensions do not match")),
        rnrs_make_irritants_condition (scm_list_2 (A, B))));
  else if (m_A != n_A)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition (_("this matrix is not square")),
        rnrs_make_irritants_condition (scm_list_1 (A))));
  else if (INT_MAX < m_A || INT_MAX < n_A || INT_MAX < n_B
           || INT_MAX < n_A + n_B)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition (_("the array dimensions are too great")),
        rnrs_make_irritants_condition (scm_list_2 (A, B))));
}

static void
get_tridiag_argument_dimensions (const char *who,
                                 SCM subdiag, SCM diag, SCM superdiag, SCM B,
                                 scm_t_array_handle *handlep_subdiag,
                                 scm_t_array_handle *handlep_diag,
                                 scm_t_array_handle *handlep_superdiag,
                                 scm_t_array_handle *handlep_B,
                                 int *n, int *nrhs, size_t *rank_of_B,
                                 ssize_t *row_inc, ssize_t *col_inc)
{
  SCM_ASSERT_TYPE ((scm_array_handle_rank (handlep_subdiag) == 1),
                   subdiag, SCM_ARG1, who, "rank-1 array");
  SCM_ASSERT_TYPE ((scm_array_handle_rank (handlep_diag) == 1),
                   diag, SCM_ARG2, who, "rank-1 array");
  SCM_ASSERT_TYPE ((scm_array_handle_rank (handlep_superdiag) == 1),
                   superdiag, SCM_ARG3, who, "rank-1 array");

  *rank_of_B = scm_array_handle_rank (handlep_B);
  SCM_ASSERT_TYPE ((*rank_of_B == 1 || *rank_of_B == 2),
                   B, SCM_ARG4, who, "rank-1 or rank-2 array");

  const scm_t_array_dim *dims_subdiag = scm_array_handle_dims (handlep_subdiag);
  const scm_t_array_dim *dims_diag = scm_array_handle_dims (handlep_diag);
  const scm_t_array_dim *dims_superdiag =
    scm_array_handle_dims (handlep_superdiag);
  const scm_t_array_dim *dims_B = scm_array_handle_dims (handlep_B);

  const ssize_t n_subdiag = (dims_subdiag[0].ubnd - dims_subdiag[0].lbnd) + 1;
  const ssize_t n_diag = (dims_diag[0].ubnd - dims_diag[0].lbnd) + 1;
  const ssize_t n_superdiag =
    (dims_superdiag[0].ubnd - dims_superdiag[0].lbnd) + 1;
  const ssize_t n_B = (dims_B[0].ubnd - dims_B[0].lbnd) + 1;

  ssize_t nrhs_B;
  if (*rank_of_B == 1)
    {
      nrhs_B = 1;
      *row_inc = dims_B[0].inc;
      *col_inc = 0;             // Not used.
    }
  else
    {
      nrhs_B = (dims_B[1].ubnd - dims_B[1].lbnd) + 1;
      *row_inc = dims_B[0].inc;
      *col_inc = dims_B[1].inc;
    }

  if (n_subdiag != n_diag - 1 || n_superdiag != n_diag - 1 || n_B != n_diag)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (), rnrs_c_make_who_condition (who),
        // FIXME: Come up with a better error message.
        rnrs_c_make_message_condition (_("the dimensions are inconsistent")),
        rnrs_make_irritants_condition (scm_list_4 (subdiag, diag,
                                                   superdiag, B))));
  else if (INT_MAX < n_diag || INT_MAX < nrhs_B)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition (_("the array dimensions are too great")),
        rnrs_make_irritants_condition (scm_list_4 (subdiag, diag,
                                                   superdiag, B))));

  *n = n_diag;
  *nrhs = nrhs_B;
}

static void
check_cyclic_tridiag_dimensions (const char *who, int n, int nrhs,
                                 SCM subdiag, SCM diag, SCM superdiag, SCM B)
{
  if (INT_MAX - 1 < nrhs)
    // The cyclic tridiagonal solver needs workspace for an extra RHS.
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition
        (_("the number of right hand sides is too great")),
        rnrs_make_irritants_condition (scm_list_1 (B))));
  if (n < 3)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition
        (_("the system dimension must be at least 3x3")),
        rnrs_make_irritants_condition (scm_list_4 (subdiag, diag,
                                                   superdiag, B))));
}

#undef REAL
#undef SYMBOL_Fxx
#undef READONLY_ELEMENTS
#undef WRITABLE_ELEMENTS
#undef TAKE_VECTOR
#undef GAUSSELIMSOLVE
#undef SCM_GAUSSELIMSOLVE
#undef GAUSSELIMSOLVE_STR
#undef TRIDIAGSOLVE
#undef SCM_TRIDIAGSOLVE
#undef TRIDIAGSOLVE_STR
#undef CYCLICTRIDIAGSOLVE
#undef SCM_CYCLICTRIDIAGSOLVE
#undef CYCLICTRIDIAGSOLVE_STR

#define REAL float
#define SYMBOL_Fxx symbol_f32
#define READONLY_ELEMENTS scm_array_handle_f32_elements
#define WRITABLE_ELEMENTS scm_array_handle_f32_writable_elements
#define TAKE_VECTOR scm_take_f32vector
#define GAUSSELIMSOLVE sgausselimsolve
#define SCM_GAUSSELIMSOLVE scm_sgausselimsolve
#define GAUSSELIMSOLVE_STR "sgausselimsolve"
#define TRIDIAGSOLVE stridiagsolve
#define SCM_TRIDIAGSOLVE scm_stridiagsolve
#define TRIDIAGSOLVE_STR "stridiagsolve"
#define CYCLICTRIDIAGSOLVE scyclictridiagsolve
#define SCM_CYCLICTRIDIAGSOLVE scm_scyclictridiagsolve
#define CYCLICTRIDIAGSOLVE_STR "scyclictridiagsolve"

#include "guile_gausselimsolve.c"
#include "guile_tridiagsolve.c"
#include "guile_cyclictridiagsolve.c"

#undef REAL
#undef SYMBOL_Fxx
#undef READONLY_ELEMENTS
#undef WRITABLE_ELEMENTS
#undef TAKE_VECTOR
#undef GAUSSELIMSOLVE
#undef SCM_GAUSSELIMSOLVE
#undef GAUSSELIMSOLVE_STR
#undef TRIDIAGSOLVE
#undef SCM_TRIDIAGSOLVE
#undef TRIDIAGSOLVE_STR
#undef CYCLICTRIDIAGSOLVE
#undef SCM_CYCLICTRIDIAGSOLVE
#undef CYCLICTRIDIAGSOLVE_STR

#define REAL double
#define SYMBOL_Fxx symbol_f64
#define READONLY_ELEMENTS scm_array_handle_f64_elements
#define WRITABLE_ELEMENTS scm_array_handle_f64_writable_elements
#define TAKE_VECTOR scm_take_f64vector
#define GAUSSELIMSOLVE dgausselimsolve
#define SCM_GAUSSELIMSOLVE scm_dgausselimsolve
#define GAUSSELIMSOLVE_STR "dgausselimsolve"
#define TRIDIAGSOLVE dtridiagsolve
#define SCM_TRIDIAGSOLVE scm_dtridiagsolve
#define TRIDIAGSOLVE_STR "dtridiagsolve"
#define CYCLICTRIDIAGSOLVE dcyclictridiagsolve
#define SCM_CYCLICTRIDIAGSOLVE scm_dcyclictridiagsolve
#define CYCLICTRIDIAGSOLVE_STR "dcyclictridiagsolve"

#include "guile_gausselimsolve.c"
#include "guile_tridiagsolve.c"
#include "guile_cyclictridiagsolve.c"
