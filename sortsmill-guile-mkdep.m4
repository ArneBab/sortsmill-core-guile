include(`m4citrus.m4')dnl -*- scheme -*-
m4_changecom([;;])dnl
m4_include([vars.m4])dnl
#!SHELL_COMMAND
# -*- mode: scheme; coding: utf-8 -*-
GUILE_AUTO_COMPILE=0 exec GUILE_COMMAND -s "$0" "$@"
!#

;; Copyright (C) 2013 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(import (rnrs)
        (only (srfi :1) delete-duplicates)
        (only (srfi :26) cut)
        (only (srfi :31) rec)
        (srfi :37)                      ; args-fold.
        (except (guile) error)
        (ice-9 match)
        (ice-9 format)
        (ice-9 i18n))

;;;(define (read-chars-until s port)
;;;  (letrec ((read-more
;;;            (lambda (prior)
;;;              (let* ((c (read-char port))
;;;                     (new-string (string-append prior (string c))))
;;;                (if (string-suffix? s new-string)
;;;                    (string-drop-right new-string (string-length s))
;;;                    (read-more new-string))))))
;;;    (read-more "")))
;;;
;;;(define (enable-hash-guillemet-strings)
;;;  (read-hash-extend #\« (lambda (c port)
;;;                          (read-chars-until "»#" port))))

(define (_ string) (gettext string "generate-guile-deps"))

(define (program-name)
  (car (command-line)))

(define (path-spec->dir-list path-spec)
  (map (lambda (dir) (if (string=? dir "") "." dir))
       (string-split path-spec #\:)))

(define (find-file file-name vpath)
  (cond ((string=? file-name "") #f)
        ((file-exists? file-name) file-name)
        ((not vpath) #f)
        ((char=? (string-ref file-name 0) #\/) #f)
        (else
         (let ((dir-list (path-spec->dir-list vpath))
               (try-dirs
                (rec (self lst)
                     (match lst
                       (() #f)
                       ((h . t)
                        (let ((file (string-append h "/" file-name)))
                          (if (file-exists? file)
                              file
                              (self t))))))))
           (try-dirs dir-list)))))

(define (find-file-mandatorily file-name vpath)
  (let ((file (find-file file-name vpath)))
    (if file
        file
        (begin
          (format (current-error-port) (_ "~a: file `~a' not found\n")
                  (program-name) file-name)
          (abort-to-prompt 'exit 1)))))

(define (imports-via-define-module options)
  (match options
    (() '())
    ((#:use-module ((? list? module-name) . module-options) . more-options)
     (cons module-name (imports-via-define-module more-options)))
    ((#:use-module (? list? module-name) . more-options)
     (cons module-name (imports-via-define-module more-options)))
    ((#:autoload (? list? module-name) symbol-list . more-options)
     (cons module-name (imports-via-define-module more-options)))
    ((_ . more-options) (imports-via-define-module more-options))))

(define (find-imports library-name expression)
  (match expression
    ((or ('library lib-name ('export . exports-list) ('import . imports-list) . body)
         ('library lib-name ('import . imports-list) . body)) ; ← Is this pattern possible?
     (if (or (eq? library-name #t) (equal? library-name lib-name))
         (cons lib-name imports-list)
         #f))
    (('define-module lib-name . options)
     (if (or (eq? library-name #t) (equal? library-name lib-name))
         (cons lib-name (imports-via-define-module options))
         #f))
    (_ #f)))

(define find-imports-in-input
  (case-lambda
    ((library-name)
     (find-imports-in-input library-name (current-input-port)))
    ((library-name port)
     (letrec
         ((find-them
           (lambda (prior)
             (let ((expression (read port)))
               (if (eof-object? expression)
                   prior
                   (let ((imports
                          (find-imports library-name expression)))
                     (if imports
                         (find-them (append prior (list imports)))
                         (find-them prior))))))))
       (find-them '()))) ))

(define (find-imports-in-files library-name . file-names)
  (let ((find-in-one-file
         (lambda (f)
           (with-input-from-file f
             (lambda ()
               (set-port-encoding! (current-input-port) "UTF-8")
               (find-imports-in-input library-name))))))
    (fold-left (lambda (prior file-name)
                 (let ((new-imports (find-in-one-file file-name)))
                   (if new-imports
                       (append prior new-imports)
                       prior)))
               '() file-names)))

(define (filter-imports pred? imports)
  (map (match-lambda ((lib-name . imports-list)
                      (cons lib-name (filter pred? imports-list))))
       imports))

(define (peel-imports imports)
  (map (match-lambda ((lib-name . imports-list)
                      (cons lib-name (map peel-import imports-list))))
       imports))

(define (peel-import import-spec)
  (match import-spec
    ((? list-of-symbols? _) import-spec)
    ((_ *** (and (? list-of-symbols? _) inner-part)) inner-part) ))

(define (list-of-symbols? lst)
  (for-all symbol? lst))

(define (lib-name->file-name prefix-dir lib-name extension)
  (string-append
   (string-join (if prefix-dir
                    (cons prefix-dir (map symbol->string lib-name))
                    (map symbol->string lib-name))
                "/")
   extension))

(define (lib-name->go-file-name prefix-dir lib-name)
  (lib-name->file-name prefix-dir lib-name ".go"))

(define (lib-name->scm-file-name prefix-dir lib-name)
  (lib-name->file-name prefix-dir lib-name ".scm"))

(define (imports->dependencies prefix-dir imports libraries)
  (match imports
    (() '())
    (((lib-name . import-list) . t)
     (cons
      (format #f "~a: ~a ~a\n"
              (lib-name->go-file-name prefix-dir lib-name)
              (lib-name->scm-file-name prefix-dir lib-name)
              (string-join (map (cut lib-name->go-file-name prefix-dir <>)
                                import-list)))
      (imports->dependencies prefix-dir t libraries))) ))

(define (remove-extension file-name)
  (cond ((string-suffix? ".scm" file-name) (string-drop-right file-name 4))
        ((string-suffix? ".go" file-name) (string-drop-right file-name 3))
        (else file-name)))

(define (infer-library-name prefix file-name)
  (if (and prefix (string-prefix? prefix file-name))
      (infer-library-name "/" (string-drop file-name (string-length prefix)))
      (map string->symbol
           (filter (negate string-null?)
                   (string-split (remove-extension file-name) #\/)))))

(define (generate-deps prefix-dir target-file-names depend-file-names)
  (let* ((libraries (map (cut infer-library-name prefix-dir <>)
                         (delete-duplicates depend-file-names)))
         (imports (peel-imports
                   (apply find-imports-in-files #t target-file-names)))
         (filtered-imports
          ;; Filter out library names that are not mentioned in
          ;; @var{libraries}.
          (filter-imports (lambda (imp)
                            (exists (cut equal? imp <>) libraries))
                          imports)))
    (for-each display (imports->dependencies prefix-dir filtered-imports
                                             libraries))))

(define (process-args args)
  (args-fold (cdr args)
             (list (option '("help") #f #f
                           (lambda (opt name arg alist)
                             (assq-set! alist 'want-help #t)))
                   (option '("version") #f #f
                           (lambda (opt name arg alist)
                             (assq-set! alist 'want-version #t)))
                   (option '("vpath") #t #f
                           (lambda (opt name arg alist)
                             (assq-set! alist 'vpath arg)))
                   (option '("build") #t #f
                           (lambda (opt name arg alist)
                             (assq-set!
                              alist 'build
                              (cons arg (assq-ref alist 'build)))))
                   (option '("prefix") #t #f
                           (lambda (opt name arg alist)
                             (assq-set! alist 'prefix arg)))
                   (option '("no-prefix") #f #f
                           (lambda (opt name arg alist)
                             (assq-set! alist 'prefix #f)))
                   )
             (lambda (opt name arg alist)
               (acons 'bad-option name alist))
             (lambda (file-name alist)
               (assq-set! alist 'file-names
                          (append (assq-ref alist 'file-names)
                                  (list file-name))))
             '((file-names . ())
               (build . ())
               (vpath . #f)
               (prefix . #f))
             ))

(define (show-bad-option name)
  ;; FIXME: Make this better.
  (format (current-error-port)
          (_ "~a: unrecognized option '--~a'
Try '~a --help' for more information.\n")
          (program-name) name (program-name)))

(define (show-help)
  ;; FIXME: Make this better.
  (format #t "~a --help is not yet supported\n" (program-name)))

(define (show-version)
  ;; FIXME: Make this better.
  (format #t "~a --version is not yet supported\n" (program-name)))

(define (main args)
  (setlocale LC_ALL "")
  ;;(enable-hash-guillemet-strings)
  (set-port-encoding! (current-output-port) "UTF-8")
  (let ((opts (process-args args)))
    (cond
     ((assq-ref opts 'bad-option) =>
      (lambda (opt-name) (show-bad-option opt-name) 1))
     ((assq-ref opts 'want-help) (show-help) 0)
     ((assq-ref opts 'want-version) (show-version) 0)
     (else
      (call-with-prompt
       'exit
       (lambda ()
         (let ((vpath (assq-ref opts 'vpath)))
           (when (not (null? (assq-ref opts 'build)))
             (generate-deps
              (assq-ref opts 'prefix)
              (map (cut find-file-mandatorily <> vpath)
                   (assq-ref opts 'build))
              (assq-ref opts 'file-names)))
           0))
       (lambda (continuation exit-status) exit-status))) )))

(exit (main (command-line)))
