// -*- fundamental -*-
//
// .indent.pro file for GNU Indent.

--gnu-style
--no-tabs
--line-length80

-T FILE
-T _Bool
-T bool

-T int8_t
-T int16_t
-T int32_t
-T int64_t
-T int128_t
-T intptr_t
-T intmax_t
-T ssize_t

-T uint8_t
-T uint16_t
-T uint32_t
-T uint64_t
-T uint128_t
-T uintptr_t
-T uintmax_t
-T size_t

-T mpz_t
-T mpq_t
-T mpf_t

-T pthread_mutex_t
-T regoff_t
-T pcre
-T AO_t

-T scm_t_array_handle
-T scm_t_array_dim

-T rexp_buffer_t
-T rexp_t
-T rexp_match_buffer_t
-T rexp_match_t
-T rexp_interval_t

-T SCM
-T scm_print_state

-T PyCompilerFlags
-T Py_ssize_t
-T PyObject
-T PyVarObject
-T PyCFunction
-T PyCFunctionWithKeywords
-T PyNoArgsFunction
-T PyMethodDef
-T PyMemberDef
-T PyTypeObject
-T PyType_Spec
-T PyIntObject
-T PyLongObject
-T PyFloatObject
-T Py_complex
-T PyComplexObject
-T PyByteArrayObject
-T PyBytesObject
-T PyUnicodeObject
-T Py_buffer
-T PyTupleObject
-T PyListObject
-T PyDictObject
-T PyClassObject
-T PyFunctionObject
-T PyFileObject
-T PyCapsule
-T PyCellObject
-T PyGenObject
-T PySetObject
-T PyCodeObject
-T PyFrameObject
-T PyNumberMethods
-T PyMappingMethods
-T PySequenceMethods
-T PyBufferProcs
-T unaryfunc
-T binaryfunc
-T ternaryfunc
-T inquiry
-T intargfunc
-T intintargfunc
-T intobjargproc
-T intintobjargproc
-T objobjargproc
-T destructor
-T freefunc
-T printfunc
-T getattrfunc
-T getattrofunc
-T setattrfunc
-T setattrofunc
-T reprfunc
-T hashfunc

-T hobby_guide_token

-T sbrentroot_func_t
-T dbrentroot_func_t

-T polynomial_list_entry_t
-T polynomial_root_interval_t
-T exact_poly_poly_to_poly_t
-T dpoly_poly_to_poly_t
-T exact_poly_to_poly_t
-T dpoly_to_poly_t
-T exact_poly_to_bool_t
-T dpoly_to_bool_t
-T exact_poly_to_size_t_t
-T dpoly_to_size_t_t
-T mpq_bipoly_add_or_sub_t_
-T dbipoly_add_or_sub_t_
-T scmivect_t
-T u8ivect_t
-T u16ivect_t
-T u32ivect_t
-T u64ivect_t
-T s8ivect_t
-T s16ivect_t
-T s32ivect_t
-T s64ivect_t
-T f32ivect_t
-T f64ivect_t
-T c32ivect_t
-T c64ivect_t
-T scm_t_hashmap
