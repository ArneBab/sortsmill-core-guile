#include <config.h>

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <stdbool.h>
#include <libintl.h>
#include <sortsmill/core.h>
#include <sortsmill/guile/core.h>

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

static bool
unbndp_or_false (SCM x)
{
  return (SCM_UNBNDP (x) || scm_is_false (x));
}

//----------------------------------------------------------------------

static float
call_sfunc (float x, void *func_p)
{
  SCM func = *(SCM *) func_p;
  return (float) scm_to_double (scm_call_1 (func, scm_from_double (x)));
}

static double
call_dfunc (double x, void *func_p)
{
  SCM func = *(SCM *) func_p;
  return scm_to_double (scm_call_1 (func, scm_from_double (x)));
}

//----------------------------------------------------------------------

VISIBLE SCM
scm_sbrentroot (SCM t0, SCM t1, SCM f, SCM tolerance, SCM epsilon)
{
  const float t0_ = scm_to_double (t0);
  const float t1_ = scm_to_double (t1);

  const float tolerance_ =
    (unbndp_or_false (tolerance)) ? (-1) : (scm_to_double (tolerance));
  const float epsilon_ =
    (unbndp_or_false (epsilon)) ? (-1) : (scm_to_double (epsilon));

  float root;
  int info;
  sbrentroot (t0_, t1_, call_sfunc, &f, tolerance_, epsilon_, &root, &info);
  if (info == 1)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("sbrentroot"),
        rnrs_c_make_message_condition
        (_("the function does not evaluate to opposite signs at these points")),
        rnrs_make_irritants_condition (scm_list_2 (t0, t1))));
  if (info != 0)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("sbrentroot"),
        rnrs_c_make_message_condition (_("incorrect call to the C function")),
        rnrs_make_irritants_condition (scm_list_2 (t0, t1))));

  return scm_from_double (root);
}

VISIBLE SCM
scm_dbrentroot (SCM t0, SCM t1, SCM f, SCM tolerance, SCM epsilon)
{
  const double t0_ = scm_to_double (t0);
  const double t1_ = scm_to_double (t1);

  const double tolerance_ =
    (unbndp_or_false (tolerance)) ? (-1) : (scm_to_double (tolerance));
  const double epsilon_ =
    (unbndp_or_false (epsilon)) ? (-1) : (scm_to_double (epsilon));

  double root;
  int info;
  dbrentroot (t0_, t1_, call_dfunc, &f, tolerance_, epsilon_, &root, &info);
  if (info == 1)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("dbrentroot"),
        rnrs_c_make_message_condition
        (_("the function does not evaluate to opposite signs at these points")),
        rnrs_make_irritants_condition (scm_list_2 (t0, t1))));
  if (info != 0)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("dbrentroot"),
        rnrs_c_make_message_condition (_("incorrect call to the C function")),
        rnrs_make_irritants_condition (scm_list_2 (t0, t1))));

  return scm_from_double (root);
}

//----------------------------------------------------------------------
