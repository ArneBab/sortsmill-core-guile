#include <config.h>

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <assert.h>
#include <libintl.h>
#include <sortsmill/guile/core.h>

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

#define VLISTS_MODULE__ "sortsmill core vlists"

static inline bool
unbndp_or_false (SCM x)
{
  return (SCM_UNBNDP (x) || scm_is_false (x));
}

#define PRED_OR_EQUAL_P__(pred, x, y)                                   \
  (scm_is_true                                                          \
   ((SCM_UNBNDP (pred)) ?                                               \
    (scm_equal_p ((x), (y))) : (scm_call_2 ((pred), (x), (y)))))

#define VLIST_R_INVERSE__ 2
#define VLIST_START_SIZE__ 4
DECLARE_SMCORE_VLIST_DATATYPE (static STM_MAYBE_UNUSED, _scmvlst, SCM);
DEFINE_SMCORE_VLIST_DATATYPE (static STM_MAYBE_UNUSED, _scmvlst, SCM,
                              VLIST_R_INVERSE__, VLIST_START_SIZE__);

static SCM _scm_vlst_equal_p (SCM a, SCM b);
void guile_init_smcoreguile_vlsts (void);

//----------------------------------------------------------------------

volatile stm_dcl_indicator_t _scm_vlst_null_is_initialized__ = false;
SCM _scm_vlst_null__;           /* Use SCM_VLST_NULL instead of this. */

// *INDENT-OFF*

STM_SCM_ONE_TIME_INITIALIZE (static, SCM, _load_the_guile_module,
                             { // Load the module by looking up an
                               // arbitrary variable in it.
                               _load_the_guile_module__Value =
                                 scm_c_private_lookup (VLISTS_MODULE__,
                                                       "vlst-null");
                               stm_dcl_store_indicator
                                 (&_scm_vlst_null_is_initialized__, true); })

// *INDENT-ON*

VISIBLE SCM
_initialize_scm_vlst_null__ (void)
{
  (void) _load_the_guile_module ();
  return _scm_vlst_null__;
}

//----------------------------------------------------------------------

#if HAVE_SCM_MAKE_FOREIGN_OBJECT_TYPE
////////////////////////////////
// Use a ‘foreign object type’.
////////////////////////////////

static SCM vlst_type = SCM_UNDEFINED;

static inline _scmvlst_t
_scm_to_scmvlst_t (SCM obj)
{
  const _scmvlst_t p = {
    .base = scm_foreign_object_ref (obj, 0),
    .offset = scm_foreign_object_unsigned_ref (obj, 1)
  };
  return p;
}

static inline SCM
_scm_from_scmvlst_t (_scmvlst_t p)
{
  SCM obj = scm_make_foreign_object_1 (vlst_type, p.base);
  scm_foreign_object_unsigned_set_x (obj, 1, (scm_t_bits) p.offset);
  return obj;
}

static inline void
_scm_assert_vlst (SCM obj)
{
  scm_assert_foreign_object_type (vlst_type, obj);
}

#if defined SCM_IS_A_P          /* The SCM_IS_A_P macro is not documented in
                                   the public interface, so test for its
                                   presence. */
VISIBLE bool
scm_is_vlst (SCM obj)
{
  return SCM_IS_A_P (obj, vlst_type);
}

VISIBLE SCM
scm_vlst_p (SCM obj)
{
  return scm_from_bool (SCM_IS_A_P (obj, vlst_type));
}

#else // !defined SCM_IS_A_P

STM_SCM_ONE_TIME_INITIALIZE (static STM_ATTRIBUTE_PURE, SCM, _scm_is_a_p,
                             (_scm_is_a_p__Value =
                              scm_c_public_ref ("oop goops", "is-a?")));

VISIBLE SCM
scm_vlst_p (SCM obj)
{
  return scm_call_2 (_scm_is_a_p (), obj, vlst_type);
}

VISIBLE bool
scm_is_vlst (SCM obj)
{
  return scm_is_true (scm_vlst_p (obj));
}

#endif // !defined SCM_IS_A_P

VISIBLE void
guile_init_smcoreguile_vlsts (void)
{
  SCM name = scm_from_utf8_symbol ("<vlst>");
  SCM slot0 = scm_from_utf8_symbol ("base");
  SCM slot1 = scm_from_utf8_symbol ("offset");
  SCM slots = scm_list_2 (slot0, slot1);
  vlst_type = scm_make_foreign_object_type (name, slots, NULL);

  scm_c_define ("<vlst>", vlst_type);
  scm_c_define ("using-foreign-objects?", SCM_BOOL_T);
  _scm_vlst_null__ = _scm_from_scmvlst_t (_scmvlst_null ());
  scm_c_define ("vlst-null", _scm_vlst_null__);
  scm_c_define_gsubr ("vlst-equal?", 2, 0, 0, _scm_vlst_equal_p);
}

#else // !HAVE_SCM_MAKE_FOREIGN_OBJECT_TYPE
////////////////////////////////
// Use a ‘SMOB type’.
////////////////////////////////

static scm_t_bits vlst_tag;

static inline _scmvlst_t
_scm_to_scmvlst_t (SCM obj)
{
  const _scmvlst_t p = {
    .base = (void *) SCM_SMOB_DATA (obj),
    .offset = (size_t) SCM_SMOB_DATA_2 (obj)
  };
  return p;
}

static inline SCM
_scm_from_scmvlst_t (_scmvlst_t p)
{
  return scm_new_double_smob (vlst_tag, (scm_t_bits) (void *) p.base,
                              (scm_t_bits) p.offset, (scm_t_bits) 0);
}

static inline void
_scm_assert_vlst (SCM obj)
{
  scm_assert_smob_type (vlst_tag, obj);
}

VISIBLE bool
scm_is_vlst (SCM obj)
{
  return SCM_SMOB_PREDICATE (vlst_tag, obj);
}

VISIBLE SCM
scm_vlst_p (SCM obj)
{
  return scm_from_bool (scm_is_vlst (obj));
}

static int
_print_vlst_smob (SCM vlst, SCM port, scm_print_state *pstate STM_MAYBE_UNUSED)
{
  scm_simple_format (port, scm_from_utf8_string ("#<vlst ~s>"),
                     scm_list_1 (scm_vlst_to_list (vlst)));
  return 1;
}

VISIBLE void
guile_init_smcoreguile_vlsts (void)
{
  vlst_tag = scm_make_smob_type ("vlst", 0);
  scm_set_smob_print (vlst_tag, _print_vlst_smob);
  scm_set_smob_equalp (vlst_tag, _scm_vlst_equal_p);

  scm_c_define ("using-foreign-objects?", SCM_BOOL_F);
  _scm_vlst_null__ = _scm_from_scmvlst_t (_scmvlst_null ());
  scm_c_define ("vlst-null", _scm_vlst_null__);
}

#endif // !HAVE_SCM_MAKE_FOREIGN_OBJECT_TYPE

//----------------------------------------------------------------------

VISIBLE bool
scm_is_vlst_null (SCM obj)
{
  return (scm_is_vlst (obj) && _scmvlst_is_null (_scm_to_scmvlst_t (obj)));
}

VISIBLE SCM
scm_vlst_null_p (SCM obj)
{
  return scm_from_bool (scm_is_vlst_null (obj));
}

VISIBLE SCM
scm_vlst_car (SCM vlst)
{
  _scm_assert_vlst (vlst);
  _scmvlst_t p = _scm_to_scmvlst_t (vlst);
  SCM_ASSERT_TYPE (!_scmvlst_is_null (p), vlst, SCM_ARG1, "vlst-car",
                   "non-null vlst");
  return _scmvlst_car (p);
}

VISIBLE SCM
scm_vlst_cdr (SCM vlst)
{
  _scm_assert_vlst (vlst);
  _scmvlst_t p = _scm_to_scmvlst_t (vlst);
  SCM_ASSERT_TYPE (!_scmvlst_is_null (p), vlst, SCM_ARG1, "vlst-cdr",
                   "non-null vlst");
  return _scm_from_scmvlst_t (_scmvlst_cdr (p));
}

VISIBLE SCM
scm_vlst_cons (SCM obj, SCM vlst)
{
  _scm_assert_vlst (vlst);
  return _scm_from_scmvlst_t (_scmvlst_cons (obj, _scm_to_scmvlst_t (vlst)));
}

VISIBLE SCM
scm_vlst_cons_star (SCM obj, SCM rest)
{
  const char *who = "vlst-cons*";

  SCM result;
  if (scm_is_null (rest))
    {
      SCM_ASSERT_TYPE ((scm_is_vlst (obj)), obj, SCM_ARG1, who, "vlst");
      result = obj;
    }
  else
    {
      const SCM args = scm_reverse (rest);
      const SCM vl = SCM_CAR (args);
      SCM_ASSERT_TYPE ((scm_is_vlst (vl)), vl, SCM_ARGn, who, "vlst");
      _scmvlst_t p = _scm_to_scmvlst_t (vl);
      SCM elems = SCM_CDR (args);
      while (!scm_is_null (elems))
        {
          p = _scmvlst_cons (SCM_CAR (elems), p);
          elems = SCM_CDR (elems);
        }
      p = _scmvlst_cons (obj, p);
      result = _scm_from_scmvlst_t (p);
    }
  return result;
}

VISIBLE SCM
scm_vlst_acons (SCM key, SCM value, SCM vlst)
{
  _scm_assert_vlst (vlst);
  return _scm_from_scmvlst_t (_scmvlst_cons (scm_cons (key, value),
                                             _scm_to_scmvlst_t (vlst)));
}

VISIBLE size_t
scm_c_vlst_length (SCM vlst)
{
  _scm_assert_vlst (vlst);
  return _scmvlst_length (_scm_to_scmvlst_t (vlst));
}

VISIBLE SCM
scm_vlst_length (SCM vlst)
{
  return scm_from_size_t (scm_c_vlst_length (vlst));
}

VISIBLE SCM
scm_c_vlst_ref (SCM vlst, size_t i, SCM dflt)
{
  _scm_assert_vlst (vlst);
  const SCM *p = _scmvlst_ptr (_scm_to_scmvlst_t (vlst), i);
  SCM e;
  if (p != NULL)
    e = *p;
  else if (SCM_UNBNDP (dflt))
    scm_out_of_range ("vlst-ref", scm_from_size_t (i));
  else
    e = dflt;
  return e;
}

VISIBLE SCM
scm_vlst_ref (SCM vlst, SCM i, SCM dflt)
{
  return scm_c_vlst_ref (vlst, scm_to_size_t (i), dflt);
}

VISIBLE SCM
scm_vlst_copy (SCM vlst)
{
  _scm_assert_vlst (vlst);
  return _scm_from_scmvlst_t (_scmvlst_copy (_scm_to_scmvlst_t (vlst)));
}

VISIBLE SCM
scm_vlst_acopy (SCM vlst)
{
  _scm_assert_vlst (vlst);
  _scmvlst_t q = _scmvlst_null ();
  for (_scmvlst_riter_t ri = _scmvlst_riter_make (_scm_to_scmvlst_t (vlst));
       ri != NULL; ri = _scmvlst_riter_next (ri))
    {
      SCM pair = _scmvlst_riter_car (ri);
      q = _scmvlst_cons (scm_cons (scm_car (pair), scm_cdr (pair)), q);
    }
  return _scm_from_scmvlst_t (q);
}

VISIBLE SCM
scm_vlst_reverse (SCM vlst)
{
  _scm_assert_vlst (vlst);
  return _scm_from_scmvlst_t (_scmvlst_reverse (_scm_to_scmvlst_t (vlst)));
}

static void
_scm_vlst_for_each_one (SCM proc, SCM vlst)
{
  // proc is applied in left-to-right order.
  _scm_assert_vlst (vlst);
  for (_scmvlst_t p = _scm_to_scmvlst_t (vlst);
       !_scmvlst_is_null (p); p = _scmvlst_cdr (p))
    scm_call_1 (proc, _scmvlst_car (p));
}

static void
_scm_vlst_for_each_two (SCM proc, SCM vlst1, SCM vlst2)
{
  // proc is applied in left-to-right order.
  _scm_assert_vlst (vlst1);
  _scm_assert_vlst (vlst2);
  _scmvlst_t p1 = _scm_to_scmvlst_t (vlst1);
  _scmvlst_t p2 = _scm_to_scmvlst_t (vlst2);
  while (!_scmvlst_is_null (p1) && !_scmvlst_is_null (p2))
    {
      scm_call_2 (proc, _scmvlst_car (p1), _scmvlst_car (p2));
      p1 = _scmvlst_cdr (p1);
      p2 = _scmvlst_cdr (p2);
    }
}

static void
_scm_vlst_for_each_many (SCM proc, SCM vlst1, SCM more_vlsts)
{
  // proc is applied in left-to-right order.

  size_t i;
  SCM p;

  _scm_assert_vlst (vlst1);

  SCM_ASSERT_TYPE ((scm_is_true (scm_list_p (more_vlsts))), more_vlsts,
                   SCM_ARG3, "scm_vlst_for_each", "vlst or list of vlsts");
  size_t n = 0;
  for (p = more_vlsts; !scm_is_null (p); p = SCM_CDR (p))
    {
      _scm_assert_vlst (SCM_CAR (p));
      n++;
    }

  _scmvlst_t p1 = _scm_to_scmvlst_t (vlst1);
  _scmvlst_t *parray = scm_gc_malloc (n * sizeof (_scmvlst_t), "");
  i = 0;
  for (p = more_vlsts; !scm_is_null (p); p = SCM_CDR (p))
    {
      parray[i] = _scm_to_scmvlst_t (SCM_CAR (p));
      i++;
    }

  bool done;
  do
    {
      done = _scmvlst_is_null (p1);
      i = 0;
      SCM args = SCM_EOL;
      while (!done && i < n)
        {
          done = _scmvlst_is_null (parray[i]);
          if (!done)
            {
              args = scm_cons (_scmvlst_car (parray[i]), args);
              parray[i] = _scmvlst_cdr (parray[i]);
              i++;
            }
        }

      if (!done)
        {
          scm_apply_1 (proc, _scmvlst_car (p1),
                       scm_reverse_x_without_checking (args, SCM_EOL));
          p1 = _scmvlst_cdr (p1);
        }
    }
  while (!done);
}

VISIBLE SCM
scm_vlst_for_each (SCM proc, SCM vlst, SCM another_vlst_or_vlsts)
{
  if (SCM_UNBNDP (another_vlst_or_vlsts))
    _scm_vlst_for_each_one (proc, vlst);
  else if (scm_is_vlst (another_vlst_or_vlsts))
    _scm_vlst_for_each_two (proc, vlst, another_vlst_or_vlsts);
  else
    _scm_vlst_for_each_many (proc, vlst, another_vlst_or_vlsts);
  return SCM_UNSPECIFIED;
}

static SCM
_scm_vlst_map_one (SCM proc, SCM vlst)
{
  // This implementation maps the vlst entries in reverse order.

  _scm_assert_vlst (vlst);
  _scmvlst_t q = _scmvlst_null ();
  for (_scmvlst_riter_t ri = _scmvlst_riter_make (_scm_to_scmvlst_t (vlst));
       ri != NULL; ri = _scmvlst_riter_next (ri))
    q = _scmvlst_cons (scm_call_1 (proc, _scmvlst_riter_car (ri)), q);
  return _scm_from_scmvlst_t (q);
}

static SCM
_scm_vlst_map_one_in_order (SCM proc, SCM vlst)
{
  // This implementation maps the vlst entries in left-to-right order.

  _scm_assert_vlst (vlst);
  _scmvlst_t q = _scmvlst_null ();
  for (_scmvlst_t p = _scm_to_scmvlst_t (vlst);
       !_scmvlst_is_null (p); p = _scmvlst_cdr (p))
    q = _scmvlst_cons (scm_call_1 (proc, _scmvlst_car (p)), q);
  return _scm_from_scmvlst_t (_scmvlst_reverse (q));
}

static SCM
_scm_vlst_map_two (SCM proc, SCM vlst1, SCM vlst2)
{
  // This implementation maps the vlst entries in left-to-right order.

  _scm_assert_vlst (vlst1);
  _scm_assert_vlst (vlst2);
  _scmvlst_t p1 = _scm_to_scmvlst_t (vlst1);
  _scmvlst_t p2 = _scm_to_scmvlst_t (vlst2);
  _scmvlst_t q = _scmvlst_null ();
  while (!_scmvlst_is_null (p1) && !_scmvlst_is_null (p2))
    {
      const SCM obj = scm_call_2 (proc, _scmvlst_car (p1), _scmvlst_car (p2));
      q = _scmvlst_cons (obj, q);
      p1 = _scmvlst_cdr (p1);
      p2 = _scmvlst_cdr (p2);
    }
  return _scm_from_scmvlst_t (_scmvlst_reverse (q));
}

static SCM
_scm_vlst_map_many (SCM proc, SCM vlst1, SCM more_vlsts)
{
  // This implementation maps the vlst entries in left-to-right order.

  size_t i;
  SCM p;

  _scm_assert_vlst (vlst1);

  SCM_ASSERT_TYPE ((scm_is_true (scm_list_p (more_vlsts))), more_vlsts,
                   SCM_ARG3, "scm_vlst_map", "vlst or list of vlsts");
  size_t n = 0;
  for (p = more_vlsts; !scm_is_null (p); p = SCM_CDR (p))
    {
      _scm_assert_vlst (SCM_CAR (p));
      n++;
    }

  _scmvlst_t p1 = _scm_to_scmvlst_t (vlst1);
  _scmvlst_t *parray = scm_gc_malloc (n * sizeof (_scmvlst_t), "");
  i = 0;
  for (p = more_vlsts; !scm_is_null (p); p = SCM_CDR (p))
    {
      parray[i] = _scm_to_scmvlst_t (SCM_CAR (p));
      i++;
    }

  _scmvlst_t q = _scmvlst_null ();

  bool done;
  do
    {
      done = _scmvlst_is_null (p1);
      i = 0;
      SCM args = SCM_EOL;
      while (!done && i < n)
        {
          done = _scmvlst_is_null (parray[i]);
          if (!done)
            {
              args = scm_cons (_scmvlst_car (parray[i]), args);
              parray[i] = _scmvlst_cdr (parray[i]);
              i++;
            }
        }

      if (!done)
        {
          const SCM obj = scm_apply_1 (proc, _scmvlst_car (p1),
                                       scm_reverse_x_without_checking (args,
                                                                       SCM_EOL));
          q = _scmvlst_cons (obj, q);
          p1 = _scmvlst_cdr (p1);
        }
    }
  while (!done);

  return _scm_from_scmvlst_t (_scmvlst_reverse (q));
}

VISIBLE SCM
scm_vlst_map (SCM proc, SCM vlst, SCM another_vlst_or_vlsts)
{
  SCM result;
  if (SCM_UNBNDP (another_vlst_or_vlsts))
    result = _scm_vlst_map_one (proc, vlst);
  else if (scm_is_vlst (another_vlst_or_vlsts))
    result = _scm_vlst_map_two (proc, vlst, another_vlst_or_vlsts);
  else
    result = _scm_vlst_map_many (proc, vlst, another_vlst_or_vlsts);
  return result;
}

VISIBLE SCM
scm_vlst_map_in_order (SCM proc, SCM vlst, SCM another_vlst_or_vlsts)
{
  SCM result;
  if (SCM_UNBNDP (another_vlst_or_vlsts))
    result = _scm_vlst_map_one_in_order (proc, vlst);
  else if (scm_is_vlst (another_vlst_or_vlsts))
    result = _scm_vlst_map_two (proc, vlst, another_vlst_or_vlsts);
  else
    result = _scm_vlst_map_many (proc, vlst, another_vlst_or_vlsts);
  return result;
}

VISIBLE SCM
scm_vlst_reverse_map (SCM proc, SCM vlst)
{
  // This implementation maps the vlst entries in forward order, but a
  // person should not assume any particular order.
  _scm_assert_vlst (vlst);
  _scmvlst_t q = _scmvlst_null ();
  for (_scmvlst_t p = _scm_to_scmvlst_t (vlst);
       !_scmvlst_is_null (p); p = _scmvlst_cdr (p))
    q = _scmvlst_cons (scm_call_1 (proc, _scmvlst_car (p)), q);
  return _scm_from_scmvlst_t (q);
}

static SCM
_scm_vlst_append_map_one (SCM proc, SCM vlst)
{
  // This implementation maps the vlst entries in reverse order.

  _scm_assert_vlst (vlst);
  _scmvlst_t q = _scmvlst_null ();
  for (_scmvlst_riter_t ri = _scmvlst_riter_make (_scm_to_scmvlst_t (vlst));
       ri != NULL; ri = _scmvlst_riter_next (ri))
    {
      SCM vl = scm_call_1 (proc, _scmvlst_riter_car (ri));
      _scm_assert_vlst (vl);
      q = _scmvlst_append (_scm_to_scmvlst_t (vl), q);
    }
  return _scm_from_scmvlst_t (q);
}

static SCM
_scm_vlst_append_map_two (SCM proc, SCM vlst1, SCM vlst2)
{
  return scm_vlst_concatenate (_scm_vlst_map_two (proc, vlst1, vlst2));
}

static SCM
_scm_vlst_append_map_many (SCM proc, SCM vlst1, SCM more_vlsts)
{
  return scm_vlst_concatenate (_scm_vlst_map_many (proc, vlst1, more_vlsts));
}

// A simple implementation of scm_vlst_append_map(), demonstrating
// what the function does:
//
// VISIBLE SCM
// scm_vlst_append_map (SCM proc, SCM vlst, SCM another_vlst_or_vlsts)
// {
//   return scm_vlst_concatenate (scm_vlst_map (proc, vlst,
//                                              another_vlst_or_vlsts));
// }
//
VISIBLE SCM
scm_vlst_append_map (SCM proc, SCM vlst, SCM another_vlst_or_vlsts)
{
  SCM result;
  if (SCM_UNBNDP (another_vlst_or_vlsts))
    result = _scm_vlst_append_map_one (proc, vlst);
  else if (scm_is_vlst (another_vlst_or_vlsts))
    result = _scm_vlst_append_map_two (proc, vlst, another_vlst_or_vlsts);
  else
    result = _scm_vlst_append_map_many (proc, vlst, another_vlst_or_vlsts);
  return result;
}

static SCM
_scm_vlst_filter_map_one (SCM proc, SCM vlst)
{
  // This implementation maps the vlst entries in reverse order.

  _scm_assert_vlst (vlst);
  _scmvlst_t q = _scmvlst_null ();
  for (_scmvlst_riter_t ri = _scmvlst_riter_make (_scm_to_scmvlst_t (vlst));
       ri != NULL; ri = _scmvlst_riter_next (ri))
    {
      SCM obj = scm_call_1 (proc, _scmvlst_riter_car (ri));
      if (scm_is_true (obj))
        q = _scmvlst_cons (obj, q);
    }
  return _scm_from_scmvlst_t (q);
}

static SCM
_scm_vlst_filter_map_two (SCM proc, SCM vlst1, SCM vlst2)
{
  // This implementation maps the vlst entries in left-to-right order.

  _scm_assert_vlst (vlst1);
  _scm_assert_vlst (vlst2);
  _scmvlst_t p1 = _scm_to_scmvlst_t (vlst1);
  _scmvlst_t p2 = _scm_to_scmvlst_t (vlst2);
  _scmvlst_t q = _scmvlst_null ();
  while (!_scmvlst_is_null (p1) && !_scmvlst_is_null (p2))
    {
      const SCM obj = scm_call_2 (proc, _scmvlst_car (p1), _scmvlst_car (p2));
      if (scm_is_true (obj))
        q = _scmvlst_cons (obj, q);
      p1 = _scmvlst_cdr (p1);
      p2 = _scmvlst_cdr (p2);
    }
  return _scm_from_scmvlst_t (_scmvlst_reverse (q));
}

static SCM
_scm_vlst_filter_map_many (SCM proc, SCM vlst1, SCM more_vlsts)
{
  // This implementation maps the vlst entries in left-to-right order.

  size_t i;
  SCM p;

  _scm_assert_vlst (vlst1);

  SCM_ASSERT_TYPE ((scm_is_true (scm_list_p (more_vlsts))), more_vlsts,
                   SCM_ARG3, "scm_vlst_map", "vlst or list of vlsts");
  size_t n = 0;
  for (p = more_vlsts; !scm_is_null (p); p = SCM_CDR (p))
    {
      _scm_assert_vlst (SCM_CAR (p));
      n++;
    }

  _scmvlst_t p1 = _scm_to_scmvlst_t (vlst1);
  _scmvlst_t *parray = scm_gc_malloc (n * sizeof (_scmvlst_t), "");
  i = 0;
  for (p = more_vlsts; !scm_is_null (p); p = SCM_CDR (p))
    {
      parray[i] = _scm_to_scmvlst_t (SCM_CAR (p));
      i++;
    }

  _scmvlst_t q = _scmvlst_null ();

  bool done;
  do
    {
      done = _scmvlst_is_null (p1);
      i = 0;
      SCM args = SCM_EOL;
      while (!done && i < n)
        {
          done = _scmvlst_is_null (parray[i]);
          if (!done)
            {
              args = scm_cons (_scmvlst_car (parray[i]), args);
              parray[i] = _scmvlst_cdr (parray[i]);
              i++;
            }
        }

      if (!done)
        {
          const SCM obj = scm_apply_1 (proc, _scmvlst_car (p1),
                                       scm_reverse_x_without_checking (args,
                                                                       SCM_EOL));
          if (scm_is_true (obj))
            q = _scmvlst_cons (obj, q);
          p1 = _scmvlst_cdr (p1);
        }
    }
  while (!done);

  return _scm_from_scmvlst_t (_scmvlst_reverse (q));
}

VISIBLE SCM
scm_vlst_filter_map (SCM proc, SCM vlst, SCM another_vlst_or_vlsts)
{
  SCM result;
  if (SCM_UNBNDP (another_vlst_or_vlsts))
    result = _scm_vlst_filter_map_one (proc, vlst);
  else if (scm_is_vlst (another_vlst_or_vlsts))
    result = _scm_vlst_filter_map_two (proc, vlst, another_vlst_or_vlsts);
  else
    result = _scm_vlst_filter_map_many (proc, vlst, another_vlst_or_vlsts);
  return result;
}

static SCM
_scm_vlst_count_one (SCM pred, SCM vlst)
{
  // Count the vlst entries in left-to-right order.

  _scm_assert_vlst (vlst);

  size_t n = 0;
  for (_scmvlst_t p = _scm_to_scmvlst_t (vlst);
       !_scmvlst_is_null (p); p = _scmvlst_cdr (p))
    if (scm_is_true (scm_call_1 (pred, _scmvlst_car (p))))
      n++;
  return scm_from_size_t (n);
}

static SCM
_scm_vlst_count_two (SCM pred, SCM vlst1, SCM vlst2)
{
  // Count the vlst entries in left-to-right order.

  _scm_assert_vlst (vlst1);
  _scm_assert_vlst (vlst2);

  size_t n = 0;
  _scmvlst_t p1 = _scm_to_scmvlst_t (vlst1);
  _scmvlst_t p2 = _scm_to_scmvlst_t (vlst2);
  while (!_scmvlst_is_null (p1) && !_scmvlst_is_null (p2))
    {
      if (scm_is_true (scm_call_2 (pred, _scmvlst_car (p1), _scmvlst_car (p2))))
        n++;
      p1 = _scmvlst_cdr (p1);
      p2 = _scmvlst_cdr (p2);
    }
  return scm_from_size_t (n);
}

static SCM
_scm_vlst_count_many (SCM pred, SCM vlst1, SCM more_vlsts)
{
  // Count the vlst entries in left-to-right order.

  size_t i;
  SCM p;

  _scm_assert_vlst (vlst1);

  SCM_ASSERT_TYPE ((scm_is_true (scm_list_p (more_vlsts))), more_vlsts,
                   SCM_ARG3, "scm_vlst_count", "vlst or list of vlsts");
  size_t n = 0;
  for (p = more_vlsts; !scm_is_null (p); p = SCM_CDR (p))
    {
      _scm_assert_vlst (SCM_CAR (p));
      n++;
    }

  _scmvlst_t p1 = _scm_to_scmvlst_t (vlst1);
  _scmvlst_t *parray = scm_gc_malloc (n * sizeof (_scmvlst_t), "");
  i = 0;
  for (p = more_vlsts; !scm_is_null (p); p = SCM_CDR (p))
    {
      parray[i] = _scm_to_scmvlst_t (SCM_CAR (p));
      i++;
    }

  size_t count = 0;

  bool done;
  do
    {
      done = _scmvlst_is_null (p1);
      i = 0;
      SCM args = SCM_EOL;
      while (!done && i < n)
        {
          done = _scmvlst_is_null (parray[i]);
          if (!done)
            {
              args = scm_cons (_scmvlst_car (parray[i]), args);
              parray[i] = _scmvlst_cdr (parray[i]);
              i++;
            }
        }

      if (!done)
        {
          if (scm_is_true
              (scm_apply_1 (pred, _scmvlst_car (p1),
                            scm_reverse_x_without_checking (args, SCM_EOL))))
            count++;
          p1 = _scmvlst_cdr (p1);
        }
    }
  while (!done);

  return scm_from_size_t (count);
}

VISIBLE SCM
scm_vlst_count (SCM pred, SCM vlst, SCM another_vlst_or_vlsts)
{
  // scm_vlst_count() is guaranteed to apply the predicate in
  // left-to-right order.

  SCM result;
  if (SCM_UNBNDP (another_vlst_or_vlsts))
    result = _scm_vlst_count_one (pred, vlst);
  else if (scm_is_vlst (another_vlst_or_vlsts))
    result = _scm_vlst_count_two (pred, vlst, another_vlst_or_vlsts);
  else
    result = _scm_vlst_count_many (pred, vlst, another_vlst_or_vlsts);
  return result;
}

static SCM
_scm_vlst_index_one (SCM pred, SCM vlst)
{
  // Search left-to-right for an element that verifies the predicate.

  _scm_assert_vlst (vlst);

  size_t i = 0;
  _scmvlst_t p = _scm_to_scmvlst_t (vlst);
  while (!_scmvlst_is_null (p)
         && scm_is_false (scm_call_1 (pred, _scmvlst_car (p))))
    {
      i++;
      p = _scmvlst_cdr (p);
    }
  return (_scmvlst_is_null (p)) ? SCM_BOOL_F : (scm_from_size_t (i));
}

static SCM
_scm_vlst_index_two (SCM pred, SCM vlst1, SCM vlst2)
{
  // Search left-to-right for elements that verify the predicate.

  _scm_assert_vlst (vlst1);
  _scm_assert_vlst (vlst2);

  size_t i = 0;
  _scmvlst_t p1 = _scm_to_scmvlst_t (vlst1);
  _scmvlst_t p2 = _scm_to_scmvlst_t (vlst2);
  while (!_scmvlst_is_null (p1) && !_scmvlst_is_null (p2)
         && scm_is_false (scm_call_2 (pred, _scmvlst_car (p1),
                                      _scmvlst_car (p2))))
    {
      i++;
      p1 = _scmvlst_cdr (p1);
      p2 = _scmvlst_cdr (p2);
    }
  return (_scmvlst_is_null (p1) || _scmvlst_is_null (p2)) ?
    SCM_BOOL_F : (scm_from_size_t (i));
}

static SCM
_scm_vlst_index_many (SCM pred, SCM vlst1, SCM more_vlsts)
{
  // Search left-to-right for elements that verify the predicate.

  size_t i;
  SCM p;

  _scm_assert_vlst (vlst1);

  SCM_ASSERT_TYPE ((scm_is_true (scm_list_p (more_vlsts))), more_vlsts,
                   SCM_ARG3, "scm_vlst_index", "vlst or list of vlsts");
  size_t n = 0;
  for (p = more_vlsts; !scm_is_null (p); p = SCM_CDR (p))
    {
      _scm_assert_vlst (SCM_CAR (p));
      n++;
    }

  _scmvlst_t p1 = _scm_to_scmvlst_t (vlst1);
  _scmvlst_t *parray = scm_gc_malloc (n * sizeof (_scmvlst_t), "");
  i = 0;
  for (p = more_vlsts; !scm_is_null (p); p = SCM_CDR (p))
    {
      parray[i] = _scm_to_scmvlst_t (SCM_CAR (p));
      i++;
    }

  size_t index = 0;

  bool found = false;
  bool done;
  do
    {
      done = _scmvlst_is_null (p1);
      i = 0;
      SCM args = SCM_EOL;
      while (!done && i < n)
        {
          done = _scmvlst_is_null (parray[i]);
          if (!done)
            {
              args = scm_cons (_scmvlst_car (parray[i]), args);
              parray[i] = _scmvlst_cdr (parray[i]);
              i++;
            }
        }

      if (!done)
        {
          if (scm_is_true
              (scm_apply_1 (pred, _scmvlst_car (p1),
                            scm_reverse_x_without_checking (args, SCM_EOL))))
            {
              found = true;
              done = true;
            }
          else
            {
              index++;
              p1 = _scmvlst_cdr (p1);
            }
        }
    }
  while (!done);

  return (found) ? (scm_from_size_t (index)) : SCM_BOOL_F;
}

VISIBLE SCM
scm_vlst_index (SCM pred, SCM vlst, SCM another_vlst_or_vlsts)
{
  // scm_vlst_index() is guaranteed to apply the predicate in
  // left-to-right order.

  SCM result;
  if (SCM_UNBNDP (another_vlst_or_vlsts))
    result = _scm_vlst_index_one (pred, vlst);
  else if (scm_is_vlst (another_vlst_or_vlsts))
    result = _scm_vlst_index_two (pred, vlst, another_vlst_or_vlsts);
  else
    result = _scm_vlst_index_many (pred, vlst, another_vlst_or_vlsts);
  return result;
}

static SCM
_scm_remove_elements (SCM test_obj, SCM vlst, SCM (*test) (SCM, SCM),
                      bool sense)
{
  // This implementation tests the vlst entries in reverse order, but a
  // person should not assume any particular order.
  //
  // If there are no deletions, this implementation returns the
  // original list. This fact might be used to optimize other
  // functions in the implementation of vlst, although users might be
  // better off ignoring it.

  _scm_assert_vlst (vlst);

  // Find a common tail.
  _scmvlst_riter_t ri = _scmvlst_riter_make (_scm_to_scmvlst_t (vlst));
  SCM result = SCM_UNDEFINED;
  do
    {
      if (ri == NULL)
        // There were no matches. Return the original list.
        result = vlst;
      else
        {
          SCM obj = _scmvlst_riter_car (ri);
          if ((bool) scm_is_true (test (test_obj, obj)) == sense)
            {
              // A match was encountered. Build a new list upon the
              // common tail.
              _scmvlst_t p = _scmvlst_riter_cdr (ri);
              ri = _scmvlst_riter_next (ri);
              while (ri != NULL)
                {
                  obj = _scmvlst_riter_car (ri);
                  if ((bool) scm_is_true (test (test_obj, obj)) != sense)
                    p = _scmvlst_cons (obj, p);
                  ri = _scmvlst_riter_next (ri);
                }
              result = _scm_from_scmvlst_t (p);
            }
          else
            ri = _scmvlst_riter_next (ri);
        }
    }
  while (SCM_UNBNDP (result));
  return result;
}

VISIBLE SCM
scm_vlst_filter (SCM pred, SCM vlst)
{
  return _scm_remove_elements (pred, vlst, scm_call_1, 0);
}

VISIBLE SCM
scm_vlst_remp (SCM pred, SCM vlst)
{
  return _scm_remove_elements (pred, vlst, scm_call_1, 1);
}

VISIBLE SCM
scm_vlst_remove (SCM obj, SCM vlst)
{
  return _scm_remove_elements (obj, vlst, scm_equal_p, 1);
}

VISIBLE SCM
scm_vlst_remv (SCM obj, SCM vlst)
{
  return _scm_remove_elements (obj, vlst, scm_eqv_p, 1);
}

VISIBLE SCM
scm_vlst_remq (SCM obj, SCM vlst)
{
  return _scm_remove_elements (obj, vlst, scm_eq_p, 1);
}

VISIBLE SCM
scm_vlst_delete (SCM obj, SCM vlst, SCM pred)
{
  // Analogous to SRFI-1 delete.

  SCM result;

  if (SCM_UNBNDP (pred))
    result = scm_vlst_remove (obj, vlst);
  else
    {
      // This implementation tests the vlst entries in reverse order,
      // but a person should not assume any particular order.
      //
      // If there are no deletions, this implementation returns the
      // original list. This fact might be used to optimize other
      // functions in the implementation of vlst, although users might
      // be better off ignoring it.

      _scm_assert_vlst (vlst);

      // Find a common tail.
      _scmvlst_riter_t ri = _scmvlst_riter_make (_scm_to_scmvlst_t (vlst));
      result = SCM_UNDEFINED;
      do
        {
          if (ri == NULL)
            // There were no matches. Return the original list.
            result = vlst;
          else
            {
              SCM obj1 = _scmvlst_riter_car (ri);
              // WARNING: The order of arguments to pred must not be
              // changed. See SRFI-1 for an explanation why.
              if (scm_is_true (scm_call_2 (pred, obj, obj1)))
                {
                  // A match was encountered. Build a new list upon
                  // the common tail.
                  _scmvlst_t p = _scmvlst_riter_cdr (ri);
                  ri = _scmvlst_riter_next (ri);
                  while (ri != NULL)
                    {
                      obj1 = _scmvlst_riter_car (ri);
                      // WARNING: The order of arguments to pred must
                      // not be changed. See SRFI-1 for an explanation
                      // why.
                      if (scm_is_false (scm_call_2 (pred, obj, obj1)))
                        p = _scmvlst_cons (obj1, p);
                      ri = _scmvlst_riter_next (ri);
                    }
                  result = _scm_from_scmvlst_t (p);
                }
              else
                ri = _scmvlst_riter_next (ri);
            }
        }
      while (SCM_UNBNDP (result));
    }

  return result;
}

VISIBLE SCM
scm_vlst_adelete (SCM key, SCM vlst, SCM pred)
{
  // Analogous to SRFI-1 alist-delete.
  //
  // This implementation tests the vlst entries in reverse order, but a
  // person should not assume any particular order.
  //
  // If there are no deletions, this implementation returns the
  // original list. This fact might be used to optimize other
  // functions in the implementation of vlst, although users might be
  // better off ignoring it.

  _scm_assert_vlst (vlst);

  // Find a common tail.
  _scmvlst_riter_t ri = _scmvlst_riter_make (_scm_to_scmvlst_t (vlst));
  SCM result = SCM_UNDEFINED;
  do
    {
      if (ri == NULL)
        // There were no matches. Return the original list.
        result = vlst;
      else
        {
          SCM obj = _scmvlst_riter_car (ri);
          SCM_ASSERT_TYPE ((scm_is_pair (obj)), obj, SCM_ARGn, "vlst-adelete",
                           "pair");
          if (PRED_OR_EQUAL_P__ (pred, key, SCM_CAR (obj)))
            {
              // A match was encountered. Build a new list upon the
              // common tail.
              _scmvlst_t p = _scmvlst_riter_cdr (ri);
              ri = _scmvlst_riter_next (ri);
              while (ri != NULL)
                {
                  obj = _scmvlst_riter_car (ri);
                  if (!PRED_OR_EQUAL_P__ (pred, key, SCM_CAR (obj)))
                    p = _scmvlst_cons (obj, p);
                  ri = _scmvlst_riter_next (ri);
                }
              result = _scm_from_scmvlst_t (p);
            }
          else
            ri = _scmvlst_riter_next (ri);
        }
    }
  while (SCM_UNBNDP (result));
  return result;
}

static void
_scm_partition_elements (SCM pred, SCM vlst, _scmvlst_riter_t ri, bool sense,
                         SCM *val1, SCM *val2)
{
  // Find a common tail for vlst and *val1.
  do
    {
      if (ri == NULL)
        {
          // Everything ended up on the same side of the
          // partition. Return the original list and a null list.
          *val1 = vlst;
          *val2 = SCM_VLST_NULL;
        }
      else
        {
          SCM obj = _scmvlst_riter_car (ri);
          if ((bool) scm_is_true (scm_call_1 (pred, obj)) != sense)
            {
              // A switch in sense was encountered. Build a new list
              // upon the common tail, and also start building the
              // other output list.
              _scmvlst_t p = _scmvlst_riter_cdr (ri);
              _scmvlst_t q = _scmvlst_cons (obj, _scmvlst_null ());
              ri = _scmvlst_riter_next (ri);
              while (ri != NULL)
                {
                  obj = _scmvlst_riter_car (ri);
                  if ((bool) scm_is_true (scm_call_1 (pred, obj)) == sense)
                    p = _scmvlst_cons (obj, p);
                  else
                    q = _scmvlst_cons (obj, q);
                  ri = _scmvlst_riter_next (ri);
                }
              *val1 = _scm_from_scmvlst_t (p);
              *val2 = _scm_from_scmvlst_t (q);
            }
          else
            ri = _scmvlst_riter_next (ri);
        }
    }
  while (SCM_UNBNDP (*val1));
}

VISIBLE SCM
scm_vlst_partition (SCM pred, SCM vlst)
{
  // This implementation tests the vlst entries in reverse order, but a
  // person should not assume any particular order.
  //
  // If all members are one side of the partition, this implementation
  // returns the original list. This fact might be used to optimize
  // other functions in the implementation of vlst, although users
  // might be better off ignoring it.

  _scm_assert_vlst (vlst);

  SCM values[2] = { SCM_UNDEFINED, SCM_UNDEFINED };

  _scmvlst_riter_t ri = _scmvlst_riter_make (_scm_to_scmvlst_t (vlst));
  if (ri == NULL)
    {
      // The input list is null.
      values[0] = vlst;
      values[1] = vlst;
    }
  else
    {
      SCM obj = _scmvlst_riter_car (ri);
      ri = _scmvlst_riter_next (ri);
      if (scm_is_true (scm_call_1 (pred, obj)))
        // The first return value will have a tail in common with the
        // input.
        _scm_partition_elements (pred, vlst, ri, 1, &values[0], &values[1]);
      else
        // The second return value will have a tail in common with the
        // input.
        _scm_partition_elements (pred, vlst, ri, 0, &values[1], &values[0]);
    }

  return scm_c_values (values, 2);
}

// Let us keep the following implementation of
// scm_vlst_delete_duplicates(), though commented out, as a ‘reference
// implementation’ of our own. The main reason we do not use it is
// that it is non-tail recursive.
//
// VISIBLE SCM
// scm_vlst_delete_duplicates (SCM vlst, SCM pred)
// {
//   // This implementation employs the same non-tail recursive algorithm
//   // as does the SRFI-1 reference implementation of
//   // ‘delete-duplicates’. One probably should not use
//   // vlst-delete-duplicates on very long lists, anyway.
//   //
//   // We take advantage of the fact that our implementation of
//   // scm_vlst_delete() returns the original list if there are no
//   // deletions. The scm_vlst_delete_duplicates() function itself
//   // returns the original list if there are no deletions.
// 
//   if (!scm_is_vlst_null (vlst))
//     {
//       SCM car = scm_vlst_car (vlst);
//       SCM cdr = scm_vlst_cdr (vlst);
//       SCM cdr1 =
//         scm_vlst_delete_duplicates (scm_vlst_delete (car, cdr, pred), pred);
//       if (!scm_is_eq (cdr, cdr1))
//         vlst = scm_vlst_cons (car, cdr1);
//     }
//   return vlst;
// }

// The next macro tries to share a tail with the input list.
#define NEW_DELETE_DUPL_TAIL__(ri, old_tail, there_are_deletions)       \
  ((there_are_deletions) ?                                              \
   (_scmvlst_cons (_scmvlst_riter_car (ri), (old_tail))) :              \
   (_scmvlst_riter_pair (ri)))

VISIBLE SCM
scm_vlst_delete_duplicates (SCM vlst, SCM pred)
{
  // If there are no deletions, this implementation returns the
  // original list. This fact might be used to optimize other
  // functions in the implementation of vlst, although users might be
  // better off ignoring it.

  _scm_assert_vlst (vlst);

  SCM result;

  _scmvlst_t lst = _scm_to_scmvlst_t (vlst);

  if (_scmvlst_is_null (lst))
    result = vlst;
  else
    {
      bool there_are_deletions = false;
      _scmvlst_t tail = _scmvlst_null ();
      for (_scmvlst_riter_t ri = _scmvlst_riter_make (lst);
           ri != NULL; ri = _scmvlst_riter_next (ri))
        {
          SCM obj = _scmvlst_riter_car (ri);
          _scmvlst_t new_tail = _scmvlst_null ();
          _scmvlst_riter_t rj = _scmvlst_riter_make (tail);
          do
            {
              if (rj == NULL)
                // There were no _new_ deletions in this ri-iteration,
                // although there may have been deletions in earlier
                // ri-iterations.
                new_tail =
                  NEW_DELETE_DUPL_TAIL__ (ri, tail, there_are_deletions);
              else
                {
                  SCM obj1 = _scmvlst_riter_car (rj);
                  // WARNING: The order of arguments to pred must not
                  // be changed. See the SRFI-1 specification of
                  // ‘delete-duplicates’.
                  if (PRED_OR_EQUAL_P__ (pred, obj, obj1))
                    {
                      // A match was encountered.
                      there_are_deletions = true;

                      // Build a new list upon the common tail.
                      _scmvlst_t p = _scmvlst_riter_cdr (rj);
                      rj = _scmvlst_riter_next (rj);
                      while (rj != NULL)
                        {
                          obj1 = _scmvlst_riter_car (rj);
                          // WARNING: The order of arguments to pred
                          // must not be changed. See the SRFI-1
                          // specification of ‘delete-duplicates’.
                          if (!PRED_OR_EQUAL_P__ (pred, obj, obj1))
                            p = _scmvlst_cons (obj1, p);
                          rj = _scmvlst_riter_next (rj);
                        }
                      new_tail = _scmvlst_cons (obj, p);
                    }
                  else
                    rj = _scmvlst_riter_next (rj);
                }
            }
          while (_scmvlst_is_null (new_tail));
          tail = new_tail;
        }
      result = (there_are_deletions) ? _scm_from_scmvlst_t (tail) : vlst;
    }

  return result;
}

VISIBLE SCM
scm_vlst_drop (SCM vlst, SCM n, SCM allow_overflow_p)
{
  _scm_assert_vlst (vlst);
  bool overflow;
  _scmvlst_t result =
    _scmvlst_drop (_scm_to_scmvlst_t (vlst), scm_to_size_t (n), &overflow);
  if (overflow && unbndp_or_false (allow_overflow_p))
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("vlst-drop"),
        rnrs_c_make_message_condition
        (_("the vlst is not long enough to drop this many")),
        rnrs_make_irritants_condition (scm_list_1 (n))));
  return _scm_from_scmvlst_t (result);
}

VISIBLE SCM
scm_vlst_take (SCM vlst, SCM n, SCM allow_overflow_p)
{
  _scm_assert_vlst (vlst);
  bool overflow;
  _scmvlst_t result =
    _scmvlst_take (_scm_to_scmvlst_t (vlst), scm_to_size_t (n), &overflow);
  if (overflow && unbndp_or_false (allow_overflow_p))
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("vlst-take"),
        rnrs_c_make_message_condition
        (_("the vlst is not long enough to take this many")),
        rnrs_make_irritants_condition (scm_list_1 (n))));
  return _scm_from_scmvlst_t (result);
}

VISIBLE SCM
scm_vlst_drop_right (SCM vlst, SCM n, SCM allow_overflow_p)
{
  _scm_assert_vlst (vlst);
  bool overflow;
  _scmvlst_t result =
    _scmvlst_drop_right (_scm_to_scmvlst_t (vlst), scm_to_size_t (n),
                         &overflow);
  if (overflow && unbndp_or_false (allow_overflow_p))
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("vlst-drop-right"),
        rnrs_c_make_message_condition
        (_("the vlst is not long enough to drop this many")),
        rnrs_make_irritants_condition (scm_list_1 (n))));
  return _scm_from_scmvlst_t (result);
}

VISIBLE SCM
scm_vlst_take_right (SCM vlst, SCM n, SCM allow_overflow_p)
{
  _scm_assert_vlst (vlst);
  bool overflow;
  _scmvlst_t result =
    _scmvlst_take_right (_scm_to_scmvlst_t (vlst), scm_to_size_t (n),
                         &overflow);
  if (overflow && unbndp_or_false (allow_overflow_p))
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("vlst-take-right"),
        rnrs_c_make_message_condition
        (_("the vlst is not long enough to take this many")),
        rnrs_make_irritants_condition (scm_list_1 (n))));
  return _scm_from_scmvlst_t (result);
}

VISIBLE SCM
scm_vlst_split_at (SCM vlst, SCM n, SCM allow_overflow_p)
{
  _scm_assert_vlst (vlst);
  bool overflow;
  _scmvlst_t q1;
  _scmvlst_t q2;
  _scmvlst_split_at (_scm_to_scmvlst_t (vlst), scm_to_size_t (n),
                     &q1, &q2, &overflow);
  if (overflow && unbndp_or_false (allow_overflow_p))
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("vlst-split-at"),
        rnrs_c_make_message_condition
        (_("the vlst is not long enough to take this many")),
        rnrs_make_irritants_condition (scm_list_1 (n))));
  SCM values[2] = { _scm_from_scmvlst_t (q1), _scm_from_scmvlst_t (q2) };
  return scm_c_values (values, 2);
}

VISIBLE SCM
scm_vlst_last_pair (SCM vlst)
{
  // Return the last ‘pseudopair’ in the list (that is, the sublist
  // comprising the last element).
  //
  // If the length of the input list is one, this implementation
  // returns the original list. This fact might be used to optimize
  // other functions in the implementation of vlst, although users
  // might be better off ignoring it.

  _scm_assert_vlst (vlst);
  _scmvlst_t vl = _scm_to_scmvlst_t (vlst);
  SCM result;
  if (_scmvlst_is_null (vl))
    {
      rnrs_raise_condition
        (scm_list_4
         (rnrs_make_assertion_violation (),
          rnrs_c_make_who_condition ("vlst-last-pair"),
          rnrs_c_make_message_condition (_("the vlst is null")),
          rnrs_make_irritants_condition (scm_list_1 (vlst))));
      result = SCM_UNDEFINED;
    }
  else if (_scmvlst_is_null (_scmvlst_cdr (vl)))
    result = vlst;
  else
    result = _scm_from_scmvlst_t (_scmvlst_last_pair (vl));
  return result;
}

VISIBLE SCM
scm_vlst_last (SCM vlst)
{
  // Return the last element in the list.

  _scm_assert_vlst (vlst);
  _scmvlst_t vl = _scm_to_scmvlst_t (vlst);
  SCM result;
  if (_scmvlst_is_null (vl))
    {
      rnrs_raise_condition
        (scm_list_4
         (rnrs_make_assertion_violation (),
          rnrs_c_make_who_condition ("vlst-last"),
          rnrs_c_make_message_condition (_("the vlst is null")),
          rnrs_make_irritants_condition (scm_list_1 (vlst))));
      result = SCM_UNDEFINED;
    }
  else
    result = _scmvlst_car (_scmvlst_last_pair (vl));
  return result;
}

VISIBLE SCM
scm_vlst_append (SCM list_of_vlst)
{
  SCM result;
  if (scm_is_null (list_of_vlst))
    result = SCM_VLST_NULL;
  else
    {
      SCM cdr = scm_cdr (list_of_vlst);
      if (scm_is_null (cdr))
        {
          _scm_assert_vlst (SCM_CAR (list_of_vlst));
          result = SCM_CAR (list_of_vlst);
        }
      else
        {
          SCM cddr = scm_cdr (cdr);
          if (scm_is_null (cddr))
            {
              _scm_assert_vlst (SCM_CAR (list_of_vlst));
              _scm_assert_vlst (SCM_CAR (cdr));
              _scmvlst_t vl1 = _scm_to_scmvlst_t (SCM_CAR (list_of_vlst));
              _scmvlst_t vl2 = _scm_to_scmvlst_t (SCM_CAR (cdr));
              result = _scm_from_scmvlst_t (_scmvlst_append (vl1, vl2));
            }
          else
            {
              SCM lst = scm_reverse (list_of_vlst);
              _scm_assert_vlst (SCM_CAR (lst));
              _scmvlst_t p = _scm_to_scmvlst_t (SCM_CAR (lst));
              lst = SCM_CDR (lst);
              _scm_assert_vlst (SCM_CAR (lst));
              p = _scmvlst_append (_scm_to_scmvlst_t (SCM_CAR (lst)), p);
              lst = SCM_CDR (lst);
              while (!scm_is_null (lst))
                {
                  _scm_assert_vlst (SCM_CAR (lst));
                  p = _scmvlst_append (_scm_to_scmvlst_t (SCM_CAR (lst)), p);
                  lst = SCM_CDR (lst);
                }
              result = _scm_from_scmvlst_t (p);
            }
        }
    }
  return result;
}

VISIBLE SCM
scm_vlst_append_reverse (SCM vl1, SCM vl2)
{
  _scm_assert_vlst (vl1);
  _scm_assert_vlst (vl2);
  return
    _scm_from_scmvlst_t (_scmvlst_append_reverse (_scm_to_scmvlst_t (vl1),
                                                  _scm_to_scmvlst_t (vl2)));
}

VISIBLE SCM
scm_vlst_concatenate (SCM vlst_or_list_of_vlst)
{
  SCM result;
  if (scm_is_vlst (vlst_or_list_of_vlst))
    {
      _scmvlst_t concat = _scmvlst_null ();
      for (_scmvlst_riter_t ri =
           _scmvlst_riter_make (_scm_to_scmvlst_t (vlst_or_list_of_vlst));
           ri != NULL; ri = _scmvlst_riter_next (ri))
        {
          SCM vlst = _scmvlst_riter_car (ri);
          _scm_assert_vlst (vlst);
          concat = _scmvlst_append (_scm_to_scmvlst_t (vlst), concat);
        }
      result = _scm_from_scmvlst_t (concat);
    }
  else
    result = scm_vlst_append (vlst_or_list_of_vlst);
  return result;
}

static SCM
_scm_vlst_fold_left_one (SCM proc, SCM init, SCM vlst)
{
  _scm_assert_vlst (vlst);
  for (_scmvlst_t p = _scm_to_scmvlst_t (vlst);
       !_scmvlst_is_null (p); p = _scmvlst_cdr (p))
    init = scm_call_2 (proc, init, _scmvlst_car (p));
  return init;
}

static SCM
_scm_vlst_fold_left_two (SCM proc, SCM init, SCM vlst1, SCM vlst2)
{
  _scm_assert_vlst (vlst1);
  _scm_assert_vlst (vlst2);
  _scmvlst_t p1 = _scm_to_scmvlst_t (vlst1);
  _scmvlst_t p2 = _scm_to_scmvlst_t (vlst2);
  while (!_scmvlst_is_null (p1) && !_scmvlst_is_null (p2))
    {
      init = scm_call_3 (proc, init, _scmvlst_car (p1), _scmvlst_car (p2));
      p1 = _scmvlst_cdr (p1);
      p2 = _scmvlst_cdr (p2);
    }
  return init;
}

static SCM
_scm_vlst_fold_left_many (SCM proc, SCM init, SCM vlst1, SCM more_vlsts)
{
  size_t i;
  SCM p;

  _scm_assert_vlst (vlst1);

  SCM_ASSERT_TYPE ((scm_is_true (scm_list_p (more_vlsts))), more_vlsts,
                   SCM_ARG4, "scm_vlst_fold_left", "vlst or list of vlsts");
  size_t n = 0;
  for (p = more_vlsts; !scm_is_null (p); p = SCM_CDR (p))
    {
      _scm_assert_vlst (SCM_CAR (p));
      n++;
    }

  _scmvlst_t p1 = _scm_to_scmvlst_t (vlst1);
  _scmvlst_t *parray = scm_gc_malloc (n * sizeof (_scmvlst_t), "");
  i = 0;
  for (p = more_vlsts; !scm_is_null (p); p = SCM_CDR (p))
    {
      parray[i] = _scm_to_scmvlst_t (SCM_CAR (p));
      i++;
    }

  bool done;
  do
    {
      done = _scmvlst_is_null (p1);
      i = 0;
      SCM args = SCM_EOL;
      while (!done && i < n)
        {
          done = _scmvlst_is_null (parray[i]);
          if (!done)
            {
              args = scm_cons (_scmvlst_car (parray[i]), args);
              parray[i] = _scmvlst_cdr (parray[i]);
              i++;
            }
        }

      if (!done)
        {
          init = scm_apply_2 (proc, init, _scmvlst_car (p1),
                              scm_reverse_x_without_checking (args, SCM_EOL));
          p1 = _scmvlst_cdr (p1);
        }
    }
  while (!done);
  return init;
}

VISIBLE SCM
scm_vlst_fold_left (SCM proc, SCM init, SCM vlst, SCM another_vlst_or_vlsts)
{
  SCM result;
  if (SCM_UNBNDP (another_vlst_or_vlsts))
    result = _scm_vlst_fold_left_one (proc, init, vlst);
  else if (scm_is_vlst (another_vlst_or_vlsts))
    result = _scm_vlst_fold_left_two (proc, init, vlst, another_vlst_or_vlsts);
  else
    result = _scm_vlst_fold_left_many (proc, init, vlst, another_vlst_or_vlsts);
  return result;
}

static SCM
_scm_vlst_fold_right_one (SCM proc, SCM init, SCM vlst)
{
  _scm_assert_vlst (vlst);
  for (_scmvlst_riter_t ri = _scmvlst_riter_make (_scm_to_scmvlst_t (vlst));
       ri != NULL; ri = _scmvlst_riter_next (ri))
    init = scm_call_2 (proc, _scmvlst_riter_car (ri), init);
  return init;
}

static SCM
_scm_vlst_fold_right_two (SCM proc, SCM init, SCM vlst1, SCM vlst2)
{
  _scm_assert_vlst (vlst1);
  _scm_assert_vlst (vlst2);
  _scmvlst_t p1 = _scm_to_scmvlst_t (vlst1);
  _scmvlst_t p2 = _scm_to_scmvlst_t (vlst2);
  _scmvlst_t q1 = _scmvlst_null ();
  _scmvlst_t q2 = _scmvlst_null ();
  while (!_scmvlst_is_null (p1) && !_scmvlst_is_null (p2))
    {
      q1 = _scmvlst_cons (_scmvlst_car (p1), q1);
      p1 = _scmvlst_cdr (p1);
      q2 = _scmvlst_cons (_scmvlst_car (p2), q2);
      p2 = _scmvlst_cdr (p2);
    }
  while (!_scmvlst_is_null (q1) && !_scmvlst_is_null (q2))
    {
      init = scm_call_3 (proc, _scmvlst_car (q1), _scmvlst_car (q2), init);
      q1 = _scmvlst_cdr (q1);
      q2 = _scmvlst_cdr (q2);
    }
  return init;
}

static void
_fill_with_nulls (_scmvlst_t * qarray, size_t n)
{
  for (size_t i = 0; i < n; i++)
    qarray[i] = _scmvlst_null ();
}

static bool
_none_are_null (_scmvlst_t * parray, size_t n)
{
  size_t i = 0;
  while (i < n && !_scmvlst_is_null (parray[i]))
    i++;
  return (i == n);
}

static SCM
_scm_vlst_fold_right_many (SCM proc, SCM init, SCM vlst1, SCM more_vlsts)
{
  size_t i;
  SCM p;

  _scm_assert_vlst (vlst1);
  SCM_ASSERT_TYPE ((scm_is_true (scm_list_p (more_vlsts))), more_vlsts,
                   SCM_ARG4, "scm_vlst_fold_right", "vlst or list of vlsts");
  size_t n = 1;
  for (p = more_vlsts; !scm_is_null (p); p = SCM_CDR (p))
    {
      _scm_assert_vlst (SCM_CAR (p));
      n++;
    }

  _scmvlst_t *parray = scm_gc_malloc (n * sizeof (_scmvlst_t), "");
  parray[0] = _scm_to_scmvlst_t (vlst1);
  i = 1;
  for (p = more_vlsts; !scm_is_null (p); p = SCM_CDR (p))
    {
      parray[i] = _scm_to_scmvlst_t (SCM_CAR (p));
      i++;
    }

  _scmvlst_t *qarray = scm_gc_malloc (n * sizeof (_scmvlst_t), "");
  _fill_with_nulls (qarray, n);
  while (_none_are_null (parray, n))
    for (i = 0; i < n; i++)
      {
        qarray[i] = _scmvlst_cons (_scmvlst_car (parray[i]), qarray[i]);
        parray[i] = _scmvlst_cdr (parray[i]);
      }

  parray = NULL;

  bool done;
  do
    {
      done = false;
      i = 0;
      SCM args = SCM_EOL;
      while (!done && i < n)
        {
          done = _scmvlst_is_null (qarray[i]);
          if (!done)
            {
              args = scm_cons (_scmvlst_car (qarray[i]), args);
              qarray[i] = _scmvlst_cdr (qarray[i]);
              i++;
            }
        }

      if (!done)
        init =
          scm_apply_0
          (proc, scm_reverse_x_without_checking (args, scm_list_1 (init)));
    }
  while (!done);
  return init;
}

VISIBLE SCM
scm_vlst_fold_right (SCM proc, SCM init, SCM vlst, SCM another_vlst_or_vlsts)
{
  SCM result;
  if (SCM_UNBNDP (another_vlst_or_vlsts))
    result = _scm_vlst_fold_right_one (proc, init, vlst);
  else if (scm_is_vlst (another_vlst_or_vlsts))
    result = _scm_vlst_fold_right_two (proc, init, vlst, another_vlst_or_vlsts);
  else
    result =
      _scm_vlst_fold_right_many (proc, init, vlst, another_vlst_or_vlsts);
  return result;
}

static SCM
_scm_vlst_fold_one (SCM proc, SCM init, SCM vlst)
{
  _scm_assert_vlst (vlst);
  for (_scmvlst_t p = _scm_to_scmvlst_t (vlst);
       !_scmvlst_is_null (p); p = _scmvlst_cdr (p))
    init = scm_call_2 (proc, _scmvlst_car (p), init);
  return init;
}

static SCM
_scm_vlst_fold_two (SCM proc, SCM init, SCM vlst1, SCM vlst2)
{
  _scm_assert_vlst (vlst1);
  _scm_assert_vlst (vlst2);
  _scmvlst_t p1 = _scm_to_scmvlst_t (vlst1);
  _scmvlst_t p2 = _scm_to_scmvlst_t (vlst2);
  while (!_scmvlst_is_null (p1) && !_scmvlst_is_null (p2))
    {
      init = scm_call_3 (proc, _scmvlst_car (p1), _scmvlst_car (p2), init);
      p1 = _scmvlst_cdr (p1);
      p2 = _scmvlst_cdr (p2);
    }
  return init;
}

static SCM
_scm_vlst_fold_many (SCM proc, SCM init, SCM vlst1, SCM more_vlsts)
{
  size_t i;
  SCM p;

  _scm_assert_vlst (vlst1);

  SCM_ASSERT_TYPE ((scm_is_true (scm_list_p (more_vlsts))), more_vlsts,
                   SCM_ARG4, "scm_vlst_fold", "vlst or list of vlsts");
  size_t n = 0;
  for (p = more_vlsts; !scm_is_null (p); p = SCM_CDR (p))
    {
      _scm_assert_vlst (SCM_CAR (p));
      n++;
    }

  _scmvlst_t p1 = _scm_to_scmvlst_t (vlst1);
  _scmvlst_t *parray = scm_gc_malloc (n * sizeof (_scmvlst_t), "");
  i = 0;
  for (p = more_vlsts; !scm_is_null (p); p = SCM_CDR (p))
    {
      parray[i] = _scm_to_scmvlst_t (SCM_CAR (p));
      i++;
    }

  bool done;
  do
    {
      done = _scmvlst_is_null (p1);
      i = 0;
      SCM args = SCM_EOL;
      while (!done && i < n)
        {
          done = _scmvlst_is_null (parray[i]);
          if (!done)
            {
              args = scm_cons (_scmvlst_car (parray[i]), args);
              parray[i] = _scmvlst_cdr (parray[i]);
              i++;
            }
        }

      if (!done)
        {
          init =
            scm_apply_1 (proc, _scmvlst_car (p1),
                         scm_reverse_x_without_checking (args,
                                                         scm_list_1 (init)));
          p1 = _scmvlst_cdr (p1);
        }
    }
  while (!done);
  return init;
}

VISIBLE SCM
scm_vlst_fold (SCM proc, SCM init, SCM vlst, SCM another_vlst_or_vlsts)
{
  SCM result;
  if (SCM_UNBNDP (another_vlst_or_vlsts))
    result = _scm_vlst_fold_one (proc, init, vlst);
  else if (scm_is_vlst (another_vlst_or_vlsts))
    result = _scm_vlst_fold_two (proc, init, vlst, another_vlst_or_vlsts);
  else
    result = _scm_vlst_fold_many (proc, init, vlst, another_vlst_or_vlsts);
  return result;
}

static SCM
_scm_vlst_pair_fold_left_one (SCM proc, SCM init, SCM vlst)
{
  while (!scm_is_vlst_null (vlst))
    {
      const SCM tail = scm_vlst_cdr (vlst);
      init = scm_call_2 (proc, init, vlst);
      vlst = tail;
    }
  return init;
}

static SCM
_scm_vlst_pair_fold_left_two (SCM proc, SCM init, SCM vlst1, SCM vlst2)
{
  while (!scm_is_vlst_null (vlst1) && !scm_is_vlst_null (vlst2))
    {
      const SCM tail1 = scm_vlst_cdr (vlst1);
      const SCM tail2 = scm_vlst_cdr (vlst2);
      init = scm_call_3 (proc, init, vlst1, vlst2);
      vlst1 = tail1;
      vlst2 = tail2;
    }
  return init;
}

static SCM
_scm_vlst_pair_fold_left_many (SCM proc, SCM init, SCM vlst1, SCM more_vlsts)
{
  size_t i;
  SCM p;

  _scm_assert_vlst (vlst1);

  SCM_ASSERT_TYPE ((scm_is_true (scm_list_p (more_vlsts))), more_vlsts,
                   SCM_ARG4, "scm_vlst_pair_fold_left",
                   "vlst or list of vlsts");
  size_t n = 0;
  for (p = more_vlsts; !scm_is_null (p); p = SCM_CDR (p))
    {
      _scm_assert_vlst (SCM_CAR (p));
      n++;
    }

  SCM p1 = vlst1;
  SCM *parray = scm_gc_malloc (n * sizeof (SCM), "");
  i = 0;
  for (p = more_vlsts; !scm_is_null (p); p = SCM_CDR (p))
    {
      parray[i] = SCM_CAR (p);
      i++;
    }

  SCM tail1;
  SCM *tail_array = scm_gc_malloc (n * sizeof (SCM), "");

  bool done;
  do
    {
      done = scm_is_vlst_null (p1);
      i = 0;
      SCM args = SCM_EOL;
      while (!done && i < n)
        {
          done = scm_is_vlst_null (parray[i]);
          if (!done)
            {
              tail_array[i] = scm_vlst_cdr (parray[i]);
              args = scm_cons (parray[i], args);
              i++;
            }
        }

      if (!done)
        {
          tail1 = scm_vlst_cdr (p1);
          init = scm_apply_2 (proc, init, p1,
                              scm_reverse_x_without_checking (args, SCM_EOL));
          p1 = tail1;
          memcpy (parray, tail_array, n * sizeof (SCM));
        }
    }
  while (!done);
  return init;
}

VISIBLE SCM
scm_vlst_pair_fold_left (SCM proc, SCM init, SCM vlst,
                         SCM another_vlst_or_vlsts)
{
  SCM result;
  if (SCM_UNBNDP (another_vlst_or_vlsts))
    result = _scm_vlst_pair_fold_left_one (proc, init, vlst);
  else if (scm_is_vlst (another_vlst_or_vlsts))
    result =
      _scm_vlst_pair_fold_left_two (proc, init, vlst, another_vlst_or_vlsts);
  else
    result =
      _scm_vlst_pair_fold_left_many (proc, init, vlst, another_vlst_or_vlsts);
  return result;
}

static SCM
_scm_vlst_pair_fold_right_one (SCM proc, SCM init, SCM vlst)
{
  // A non-recursive implementation.
  SCM sublists = SCM_VLST_NULL;
  while (!scm_is_vlst_null (vlst))
    {
      sublists = scm_vlst_cons (vlst, sublists);
      vlst = scm_vlst_cdr (vlst);
    }
  return _scm_vlst_fold_one (proc, init, sublists);
}

static SCM
_scm_vlst_pair_fold_right_two (SCM proc, SCM init, SCM vlst1, SCM vlst2)
{
  // A non-recursive implementation.
  SCM sublists1 = SCM_VLST_NULL;
  SCM sublists2 = SCM_VLST_NULL;
  while (!scm_is_vlst_null (vlst1) && !scm_is_vlst_null (vlst2))
    {
      sublists1 = scm_vlst_cons (vlst1, sublists1);
      vlst1 = scm_vlst_cdr (vlst1);
      sublists2 = scm_vlst_cons (vlst2, sublists2);
      vlst2 = scm_vlst_cdr (vlst2);
    }
  return _scm_vlst_fold_two (proc, init, sublists1, sublists2);
}

static bool
_some_pair_fold_right_vlst_is_null (SCM vlsts)
{
  while (!scm_is_null (vlsts) && !scm_is_vlst_null (SCM_CAR (vlsts)))
    vlsts = SCM_CDR (vlsts);
  return (!scm_is_null (vlsts));
}

static SCM
_scm_vlst_pair_fold_right_many (SCM proc, SCM init, SCM vlst1, SCM more_vlsts)
{
  // A non-recursive implementation.

  SCM_ASSERT_TYPE ((scm_is_true (scm_list_p (more_vlsts))), more_vlsts,
                   SCM_ARG4, "scm_vlst_pair_fold_right",
                   "vlst or list of vlsts");

  SCM vlsts = scm_cons (vlst1, more_vlsts);

  SCM sublists = SCM_EOL;
  for (SCM p = vlsts; !scm_is_null (p); p = SCM_CDR (p))
    sublists = scm_cons (SCM_VLST_NULL, sublists);

  while (!_some_pair_fold_right_vlst_is_null (vlsts))
    {
      SCM new_sublists = SCM_EOL;
      SCM new_vlsts = SCM_EOL;
      SCM q = sublists;
      for (SCM p = vlsts; !scm_is_null (p); p = SCM_CDR (p))
        {
          new_sublists =
            scm_cons (scm_vlst_cons (SCM_CAR (p), SCM_CAR (q)), new_sublists);
          q = SCM_CDR (q);
          new_vlsts = scm_cons (scm_vlst_cdr (SCM_CAR (p)), new_vlsts);
        }
      sublists = scm_reverse_x_without_checking (new_sublists, SCM_EOL);
      vlsts = scm_reverse_x_without_checking (new_vlsts, SCM_EOL);
    }

  return _scm_vlst_fold_many (proc, init, SCM_CAR (sublists),
                              SCM_CDR (sublists));
}

VISIBLE SCM
scm_vlst_pair_fold_right (SCM proc, SCM init, SCM vlst,
                          SCM another_vlst_or_vlsts)
{
  SCM result;
  if (SCM_UNBNDP (another_vlst_or_vlsts))
    result = _scm_vlst_pair_fold_right_one (proc, init, vlst);
  else if (scm_is_vlst (another_vlst_or_vlsts))
    result =
      _scm_vlst_pair_fold_right_two (proc, init, vlst, another_vlst_or_vlsts);
  else
    result =
      _scm_vlst_pair_fold_right_many (proc, init, vlst, another_vlst_or_vlsts);
  return result;
}

static SCM
_scm_vlst_pair_fold_one (SCM proc, SCM init, SCM vlst)
{
  while (!scm_is_vlst_null (vlst))
    {
      const SCM tail = scm_vlst_cdr (vlst);
      init = scm_call_2 (proc, vlst, init);
      vlst = tail;
    }
  return init;
}

static SCM
_scm_vlst_pair_fold_two (SCM proc, SCM init, SCM vlst1, SCM vlst2)
{
  while (!scm_is_vlst_null (vlst1) && !scm_is_vlst_null (vlst2))
    {
      const SCM tail1 = scm_vlst_cdr (vlst1);
      const SCM tail2 = scm_vlst_cdr (vlst2);
      init = scm_call_3 (proc, vlst1, vlst2, init);
      vlst1 = tail1;
      vlst2 = tail2;
    }
  return init;
}

static SCM
_scm_vlst_pair_fold_many (SCM proc, SCM init, SCM vlst1, SCM more_vlsts)
{
  size_t i;
  SCM p;

  _scm_assert_vlst (vlst1);

  SCM_ASSERT_TYPE ((scm_is_true (scm_list_p (more_vlsts))), more_vlsts,
                   SCM_ARG3, "scm_vlst_pair_fold", "vlst or list of vlsts");
  size_t n = 0;
  for (p = more_vlsts; !scm_is_null (p); p = SCM_CDR (p))
    {
      _scm_assert_vlst (SCM_CAR (p));
      n++;
    }

  SCM p1 = vlst1;
  SCM *parray = scm_gc_malloc (n * sizeof (SCM), "");
  i = 0;
  for (p = more_vlsts; !scm_is_null (p); p = SCM_CDR (p))
    {
      parray[i] = SCM_CAR (p);
      i++;
    }

  SCM tail1;
  SCM *tail_array = scm_gc_malloc (n * sizeof (SCM), "");

  bool done;
  do
    {
      done = scm_is_vlst_null (p1);
      i = 0;
      SCM args = SCM_EOL;
      while (!done && i < n)
        {
          done = scm_is_vlst_null (parray[i]);
          if (!done)
            {
              tail_array[i] = scm_vlst_cdr (parray[i]);
              args = scm_cons (parray[i], args);
              i++;
            }
        }

      if (!done)
        {
          tail1 = scm_vlst_cdr (p1);

          init =
            scm_apply_1
            (proc, p1,
             scm_reverse_x_without_checking (args, scm_list_1 (init)));
          p1 = tail1;
          memcpy (parray, tail_array, n * sizeof (SCM));
        }
    }
  while (!done);
  return init;
}

VISIBLE SCM
scm_vlst_pair_fold (SCM proc, SCM init, SCM vlst, SCM another_vlst_or_vlsts)
{
  SCM result;
  if (SCM_UNBNDP (another_vlst_or_vlsts))
    result = _scm_vlst_pair_fold_one (proc, init, vlst);
  else if (scm_is_vlst (another_vlst_or_vlsts))
    result = _scm_vlst_pair_fold_two (proc, init, vlst, another_vlst_or_vlsts);
  else
    result = _scm_vlst_pair_fold_many (proc, init, vlst, another_vlst_or_vlsts);
  return result;
}

static int
_vlst_sort_compare (const void *xp, const void *yp, void *data)
{
  const SCM x = *(const SCM *) xp;
  const SCM y = *(const SCM *) yp;
  const SCM less_than = *(const SCM *) data;
  const bool x_is_less_than_y = scm_is_true (scm_call_2 (less_than, x, y));
  return (x_is_less_than_y) ? -1 : 0;
}

VISIBLE SCM
scm_vlst_sort (SCM before, SCM vlst)
{
  // A stable sort analogous to R⁶RS list-sort and vector-sort. The
  // current implementation uses smcore_stable_sort() on a temporary
  // array.

  _scm_assert_vlst (vlst);
  _scmvlst_t p = _scm_to_scmvlst_t (vlst);
  const size_t n = _scmvlst_length (p);

  _scmvlst_t q = _scmvlst_null ();
  if (n != 0)
    {
      SCM *buffer = scm_gc_malloc (n * sizeof (SCM), "");
      for (size_t i = 0; i < n; i++)
        {
          buffer[i] = _scmvlst_car (p);
          p = _scmvlst_cdr (p);
        }
      smcore_stable_sort (buffer, n, sizeof (SCM), _vlst_sort_compare, &before);
      scm_remember_upto_here_1 (before);        // FIXME: Does this do anything?
      for (size_t i = 0; i < n; i++)
        q = _scmvlst_cons (buffer[(n - 1) - i], q);
    }
  return _scm_from_scmvlst_t (q);
}

VISIBLE SCM
scm_vlst_find (SCM pred, SCM vlst)
{
  _scm_assert_vlst (vlst);
  _scmvlst_t p = _scm_to_scmvlst_t (vlst);
  SCM obj = SCM_UNDEFINED;
  do
    {
      if (_scmvlst_is_null (p))
        obj = SCM_BOOL_F;
      else if (scm_is_true (scm_call_1 (pred, _scmvlst_car (p))))
        obj = _scmvlst_car (p);
      else
        p = _scmvlst_cdr (p);
    }
  while (SCM_UNBNDP (obj));
  return obj;
}

VISIBLE SCM
scm_vlst_find_tail (SCM pred, SCM vlst)
{
  return scm_vlst_memp (pred, vlst);
}

VISIBLE SCM
scm_vlst_drop_while (SCM pred, SCM vlst)
{
  SCM obj = SCM_UNDEFINED;
  do
    {
      if (scm_is_vlst_null (vlst)
          || scm_is_false (scm_call_1 (pred, scm_vlst_car (vlst))))
        obj = vlst;
      else
        vlst = scm_vlst_cdr (vlst);
    }
  while (SCM_UNBNDP (obj));
  return obj;
}

VISIBLE SCM
scm_vlst_take_while (SCM pred, SCM vlst)
{
  SCM p = vlst;
  SCM taken = SCM_VLST_NULL;
  SCM obj = SCM_UNDEFINED;
  do
    {
      if (scm_is_vlst_null (p))
        obj = vlst;
      else
        {
          const SCM car = scm_vlst_car (p);
          if (scm_is_false (scm_call_1 (pred, car)))
            obj = scm_vlst_reverse (taken);
          else
            {
              taken = scm_vlst_cons (car, taken);
              p = scm_vlst_cdr (p);
            }
        }
    }
  while (SCM_UNBNDP (obj));
  return obj;
}

VISIBLE SCM
scm_vlst_span (SCM pred, SCM vlst)
{
  SCM p = vlst;
  SCM taken = SCM_VLST_NULL;
  SCM values[2] = { SCM_UNDEFINED };
  do
    {
      if (scm_is_vlst_null (p))
        {
          values[0] = vlst;
          values[1] = p;
        }
      else
        {
          const SCM car = scm_vlst_car (p);
          if (scm_is_false (scm_call_1 (pred, car)))
            {
              values[0] = scm_vlst_reverse (taken);
              values[1] = p;
            }
          else
            {
              taken = scm_vlst_cons (car, taken);
              p = scm_vlst_cdr (p);
            }
        }
    }
  while (SCM_UNBNDP (values[0]));
  return scm_c_values (values, 2);
}

VISIBLE SCM
scm_vlst_break (SCM pred, SCM vlst)
{
  SCM p = vlst;
  SCM taken = SCM_VLST_NULL;
  SCM values[2] = { SCM_UNDEFINED };
  do
    {
      if (scm_is_vlst_null (p))
        {
          values[0] = vlst;
          values[1] = p;
        }
      else
        {
          const SCM car = scm_vlst_car (p);
          if (scm_is_true (scm_call_1 (pred, car)))
            {
              values[0] = scm_vlst_reverse (taken);
              values[1] = p;
            }
          else
            {
              taken = scm_vlst_cons (car, taken);
              p = scm_vlst_cdr (p);
            }
        }
    }
  while (SCM_UNBNDP (values[0]));
  return scm_c_values (values, 2);
}

VISIBLE SCM
scm_vlst_memp (SCM pred, SCM vlst)
{
  _scm_assert_vlst (vlst);
  _scmvlst_t lst = _scm_to_scmvlst_t (vlst);
  _scmvlst_t p = lst;
  SCM obj = SCM_UNDEFINED;
  do
    {
      if (_scmvlst_is_null (p))
        obj = SCM_BOOL_F;
      else if (scm_is_true (scm_call_1 (pred, _scmvlst_car (p))))
        obj = (_scmvlst_is_eq (p, lst)) ? vlst : (_scm_from_scmvlst_t (p));
      else
        p = _scmvlst_cdr (p);
    }
  while (SCM_UNBNDP (obj));
  return obj;
}

VISIBLE SCM
scm_vlst_member (SCM obj, SCM vlst, SCM pred)
{
  _scm_assert_vlst (vlst);
  _scmvlst_t lst = _scm_to_scmvlst_t (vlst);
  _scmvlst_t p = lst;
  SCM obj1 = SCM_UNDEFINED;
  do
    {
      if (_scmvlst_is_null (p))
        obj1 = SCM_BOOL_F;
      else if (PRED_OR_EQUAL_P__ (pred, obj, _scmvlst_car (p)))
        obj1 = (_scmvlst_is_eq (p, lst)) ? vlst : (_scm_from_scmvlst_t (p));
      else
        p = _scmvlst_cdr (p);
    }
  while (SCM_UNBNDP (obj1));
  return obj1;
}

VISIBLE SCM
scm_vlst_memv (SCM obj, SCM vlst)
{
  _scm_assert_vlst (vlst);
  _scmvlst_t lst = _scm_to_scmvlst_t (vlst);
  _scmvlst_t p = lst;
  SCM obj1 = SCM_UNDEFINED;
  do
    {
      if (_scmvlst_is_null (p))
        obj1 = SCM_BOOL_F;
      else if (scm_is_true (scm_eqv_p (obj, _scmvlst_car (p))))
        obj1 = (_scmvlst_is_eq (p, lst)) ? vlst : (_scm_from_scmvlst_t (p));
      else
        p = _scmvlst_cdr (p);
    }
  while (SCM_UNBNDP (obj1));
  return obj1;
}

VISIBLE SCM
scm_vlst_memq (SCM obj, SCM vlst)
{
  _scm_assert_vlst (vlst);
  _scmvlst_t lst = _scm_to_scmvlst_t (vlst);
  _scmvlst_t p = lst;
  SCM obj1 = SCM_UNDEFINED;
  do
    {
      if (_scmvlst_is_null (p))
        obj1 = SCM_BOOL_F;
      else if (scm_is_eq (obj, _scmvlst_car (p)))
        obj1 = (_scmvlst_is_eq (p, lst)) ? vlst : (_scm_from_scmvlst_t (p));
      else
        p = _scmvlst_cdr (p);
    }
  while (SCM_UNBNDP (obj1));
  return obj1;
}

VISIBLE SCM
scm_vlst_assp (SCM pred, SCM vlst)
{
  _scm_assert_vlst (vlst);
  _scmvlst_t lst = _scm_to_scmvlst_t (vlst);
  _scmvlst_t p = lst;
  SCM obj = SCM_UNDEFINED;
  do
    {
      if (_scmvlst_is_null (p))
        obj = SCM_BOOL_F;
      else
        {
          SCM pair = _scmvlst_car (p);
          SCM_ASSERT_TYPE ((scm_is_pair (pair)), pair, SCM_ARGn, "vlst-assp",
                           "pair");
          if (scm_is_true (scm_call_1 (pred, SCM_CAR (pair))))
            obj = pair;
          else
            p = _scmvlst_cdr (p);
        }
    }
  while (SCM_UNBNDP (obj));
  return obj;
}

VISIBLE SCM
scm_vlst_assoc (SCM obj, SCM vlst, SCM pred)
{
  _scm_assert_vlst (vlst);
  _scmvlst_t lst = _scm_to_scmvlst_t (vlst);
  _scmvlst_t p = lst;
  SCM obj1 = SCM_UNDEFINED;
  do
    {
      if (_scmvlst_is_null (p))
        obj1 = SCM_BOOL_F;
      else
        {
          SCM pair = _scmvlst_car (p);
          SCM_ASSERT_TYPE ((scm_is_pair (pair)), pair, SCM_ARGn, "vlst-assoc",
                           "pair");
          if (PRED_OR_EQUAL_P__ (pred, obj, SCM_CAR (pair)))
            obj1 = pair;
          else
            p = _scmvlst_cdr (p);
        }
    }
  while (SCM_UNBNDP (obj1));
  return obj1;
}

VISIBLE SCM
scm_vlst_assv (SCM obj, SCM vlst)
{
  _scm_assert_vlst (vlst);
  _scmvlst_t lst = _scm_to_scmvlst_t (vlst);
  _scmvlst_t p = lst;
  SCM obj1 = SCM_UNDEFINED;
  do
    {
      if (_scmvlst_is_null (p))
        obj1 = SCM_BOOL_F;
      else
        {
          SCM pair = _scmvlst_car (p);
          SCM_ASSERT_TYPE ((scm_is_pair (pair)), pair, SCM_ARGn, "vlst-assv",
                           "pair");
          if (scm_is_true (scm_eqv_p (obj, SCM_CAR (pair))))
            obj1 = pair;
          else
            p = _scmvlst_cdr (p);
        }
    }
  while (SCM_UNBNDP (obj1));
  return obj1;
}

VISIBLE SCM
scm_vlst_assq (SCM obj, SCM vlst)
{
  _scm_assert_vlst (vlst);
  _scmvlst_t lst = _scm_to_scmvlst_t (vlst);
  _scmvlst_t p = lst;
  SCM obj1 = SCM_UNDEFINED;
  do
    {
      if (_scmvlst_is_null (p))
        obj1 = SCM_BOOL_F;
      else
        {
          SCM pair = _scmvlst_car (p);
          SCM_ASSERT_TYPE ((scm_is_pair (pair)), pair, SCM_ARGn, "vlst-assq",
                           "pair");
          if (scm_is_eq (obj, SCM_CAR (pair)))
            obj1 = pair;
          else
            p = _scmvlst_cdr (p);
        }
    }
  while (SCM_UNBNDP (obj1));
  return obj1;
}

VISIBLE SCM
scm_vlst_assoc_ref (SCM vlst, SCM key)
{
  const SCM pair = scm_vlst_assoc (key, vlst, SCM_UNDEFINED);
  return (scm_is_true (pair)) ? SCM_CDR (pair) : SCM_BOOL_F;
}

VISIBLE SCM
scm_vlst_assv_ref (SCM vlst, SCM key)
{
  const SCM pair = scm_vlst_assv (key, vlst);
  return (scm_is_true (pair)) ? SCM_CDR (pair) : SCM_BOOL_F;
}

VISIBLE SCM
scm_vlst_assq_ref (SCM vlst, SCM key)
{
  const SCM pair = scm_vlst_assq (key, vlst);
  return (scm_is_true (pair)) ? SCM_CDR (pair) : SCM_BOOL_F;
}

VISIBLE SCM
scm_vlst_eq_p (SCM pred, SCM vlsts)
{
  SCM result;
  if (scm_is_null (vlsts))
    result = SCM_BOOL_T;
  else
    {
      SCM_ASSERT_TYPE ((scm_is_true (scm_list_p (vlsts))), vlsts, SCM_ARG2,
                       "scm_vlst_eq_p", "list");
      _scm_assert_vlst (SCM_CAR (vlsts));
      _scmvlst_t vl1 = _scm_to_scmvlst_t (SCM_CAR (vlsts));
      const size_t n = _scmvlst_length (vl1);
      bool eq = true;
      SCM p = SCM_CDR (vlsts);
      while (scm_is_true (result) && !scm_is_null (p))
        {
          _scm_assert_vlst (SCM_CAR (p));
          _scmvlst_t vl = _scm_to_scmvlst_t (SCM_CAR (p));
          if (_scmvlst_length (vl) != n)
            eq = false;
          else
            {
              _scmvlst_t t = vl1;
              bool done = false;
              do
                {
                  if (_scmvlst_is_null (vl))
                    done = true;
                  else
                    {
                      SCM x = _scmvlst_car (vl);
                      SCM y = _scmvlst_car (t);
                      //
                      // The ‘eq?’ shortcut is allowed by SRFI-1 for
                      // ‘list=’, so let us allow it for ‘vlst=’.
                      if (!scm_is_eq (x, y)
                          || scm_is_false (scm_call_2 (pred, x, y)))
                        {
                          eq = false;
                          done = true;
                        }
                      else
                        {
                          vl = _scmvlst_cdr (vl);
                          t = _scmvlst_cdr (t);
                        }
                    }
                }
              while (!done);
            }
          p = SCM_CDR (p);
        }
      result = scm_from_bool (eq);
    }
  return result;
}

VISIBLE SCM
scm_make_vlst (SCM n, SCM fill)
{
  if (SCM_UNBNDP (fill))
    {
      // SRFI-1 ‘make-list’ leaves the value unspecified. Guile
      // ‘make-list’ specifies SCM_EOL. Let us follow SRFI-1’s
      // specification in a way that will tend to catch programming
      // errors.
      fill = SCM_UNDEFINED;
    }
  const size_t n_ = scm_to_size_t (n);
  _scmvlst_t p = _scmvlst_null ();
  for (size_t i = 0; i < n_; i++)
    p = _scmvlst_cons (fill, p);
  return _scm_from_scmvlst_t (p);
}

VISIBLE SCM
scm_vlst_tabulate (SCM n, SCM proc)
{
  // A vlst cannot be longer than SIZE_MAX (or even that long), so it
  // is safe to assume n <= SIZE_MAX.
  const size_t n_ = scm_to_size_t (n);
  _scmvlst_t p = _scmvlst_null ();
  for (size_t i = 0; i < n_; i++)
    p = _scmvlst_cons (scm_call_1 (proc, scm_from_size_t (n_ - 1 - i)), p);
  return _scm_from_scmvlst_t (p);
}

VISIBLE SCM
scm_vlst_to_list (SCM vlst)
{
  _scm_assert_vlst (vlst);
  SCM lst = SCM_EOL;
  for (_scmvlst_riter_t ri = _scmvlst_riter_make (_scm_to_scmvlst_t (vlst));
       ri != NULL; ri = _scmvlst_riter_next (ri))
    lst = scm_cons (_scmvlst_riter_car (ri), lst);
  return lst;
}

VISIBLE SCM
scm_list_to_vlst (SCM lst)
{
  SCM_ASSERT_TYPE ((scm_is_true (scm_list_p (lst))), lst, SCM_ARG1,
                   "list->vlst", "list");
  _scmvlst_t vlst = _scmvlst_null ();
  for (SCM p = lst; !scm_is_null (p); p = SCM_CDR (p))
    vlst = _scmvlst_cons (SCM_CAR (p), vlst);
  return _scm_from_scmvlst_t (_scmvlst_reverse (vlst));
}

VISIBLE SCM
scm_vlst_to_vector (SCM vlst)
{
  _scm_assert_vlst (vlst);
  _scmvlst_t vl = _scm_to_scmvlst_t (vlst);
  const size_t n = _scmvlst_length (vl);
  SCM vec = scm_c_make_vector (n, SCM_UNDEFINED);
  for (size_t i = 0; i < n; i++)
    {
      SCM_SIMPLE_VECTOR_SET (vec, i, _scmvlst_car (vl));
      vl = _scmvlst_cdr (vl);
    }
  return vec;
}

VISIBLE SCM
scm_vector_to_vlst (SCM vec)
{
  SCM_ASSERT_TYPE ((scm_is_vector (vec)), vec, SCM_ARG1,
                   "vector->vlst", "vector");
  _scmvlst_t vlst = _scmvlst_null ();
  scm_t_array_handle handle;
  size_t n;
  ssize_t stride;
  const SCM *elems = scm_vector_elements (vec, &handle, &n, &stride);
  for (size_t i = 0; i < n; i++)
    {
      vlst = _scmvlst_cons (*elems, vlst);
      elems += stride;
    }
  scm_array_handle_release (&handle);
  return _scm_from_scmvlst_t (_scmvlst_reverse (vlst));
}

VISIBLE SCM
scm_vlst_1 (SCM a)
{
  _scmvlst_t vl = _scmvlst_null ();
  vl = _scmvlst_cons (a, vl);
  return _scm_from_scmvlst_t (vl);
}

VISIBLE SCM
scm_vlst_2 (SCM a, SCM b)
{
  _scmvlst_t vl = _scmvlst_null ();
  vl = _scmvlst_cons (b, vl);
  vl = _scmvlst_cons (a, vl);
  return _scm_from_scmvlst_t (vl);
}

VISIBLE SCM
scm_vlst_3 (SCM a, SCM b, SCM c)
{
  _scmvlst_t vl = _scmvlst_null ();
  vl = _scmvlst_cons (c, vl);
  vl = _scmvlst_cons (b, vl);
  vl = _scmvlst_cons (a, vl);
  return _scm_from_scmvlst_t (vl);
}

VISIBLE SCM
scm_vlst_4 (SCM a, SCM b, SCM c, SCM d)
{
  _scmvlst_t vl = _scmvlst_null ();
  vl = _scmvlst_cons (d, vl);
  vl = _scmvlst_cons (c, vl);
  vl = _scmvlst_cons (b, vl);
  vl = _scmvlst_cons (a, vl);
  return _scm_from_scmvlst_t (vl);
}

VISIBLE SCM
scm_vlst_5 (SCM a, SCM b, SCM c, SCM d, SCM e)
{
  _scmvlst_t vl = _scmvlst_null ();
  vl = _scmvlst_cons (e, vl);
  vl = _scmvlst_cons (d, vl);
  vl = _scmvlst_cons (c, vl);
  vl = _scmvlst_cons (b, vl);
  vl = _scmvlst_cons (a, vl);
  return _scm_from_scmvlst_t (vl);
}

//----------------------------------------------------------------------

static SCM
_scm_vlst_equal_p (SCM a, SCM b)
{
  _scm_assert_vlst (a);
  _scm_assert_vlst (b);
  _scmvlst_t a_ = _scm_to_scmvlst_t (a);
  _scmvlst_t b_ = _scm_to_scmvlst_t (b);
  SCM equal_p;
  if (_scmvlst_is_eq (a_, b_))
    // The lists are the same.
    equal_p = SCM_BOOL_T;
  else if (_scmvlst_length (a_) == _scmvlst_length (b_))
    {
      equal_p = SCM_BOOL_T;
      while (scm_is_true (equal_p) && !_scmvlst_is_null (a_))
        {
          if (_scmvlst_is_eq (a_, b_))
            {
              // The lists share a tail. Skip the rest of the testing.
              a_ = _scmvlst_null ();
            }
          else
            {
              if (scm_is_false (scm_equal_p (_scmvlst_car (a_),
                                             _scmvlst_car (b_))))
                equal_p = SCM_BOOL_F;
              a_ = _scmvlst_cdr (a_);
              b_ = _scmvlst_cdr (b_);
            }
        }
    }
  else
    equal_p = SCM_BOOL_F;
  return equal_p;
}

//----------------------------------------------------------------------
