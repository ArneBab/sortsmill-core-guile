include(`m4citrus.m4')dnl -*- sh -*-
m4_include([vars.m4])dnl
#!SHELL_COMMAND

# Copyright (C) 2013 Khaled Hosny and Barry Schwartz
#
# This file is part of Sorts Mill Core Guile.
# 
# Sorts Mill Core Guile is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# Sorts Mill Core Guile is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


# Run whichever `sortsmill-core-pythonN.N' command is selected by the
# `SORTSMILL_PYTHON_VERSION' environment variable.

test -z "${SORTSMILL_PYTHON_VERSION}" && {
    echo "Please set the environment variable SORTSMILL_PYTHON_VERSION" >&2
    echo "   to one of \"2.6\", \"2.7\", \"3.2\", \"3.3\", etc." >&2
    exit 1
}

exec "sortsmill-core-python${SORTSMILL_PYTHON_VERSION}" ${1+"$@"}
