#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2013 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile
// 
// Sorts Mill Core Guile is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with Sorts Mill Core Guile.  If not, see
// <http://www.gnu.org/licenses/>.

#include <Python.h>
#include <libguile.h>

//-------------------------------------------------------------------------

void init_guile_pyversion (void);

VISIBLE void
init_guile_pyversion (void)
{
//////////  // The version and possible ABI letter that appear in the name of
//////////  // the Python library file, and which were specified when Sorts Mill
//////////  // Core Guile was configured. For instance, the ‘3.4m’ that appears
//////////  // in ‘libpython3.4m.so’.
//////////  scm_c_define ("python-version-and-abi",
//////////                scm_from_utf8_string (PYTHON_VERSION_AND_ABI));

  scm_c_define ("PY_VERSION_HEX",
                scm_from_uintmax ((uintmax_t) PY_VERSION_HEX));
  scm_c_define ("PY_MAJOR_VERSION",
                scm_from_uint ((unsigned int) PY_MAJOR_VERSION));
  scm_c_define ("PY_MINOR_VERSION",
                scm_from_uint ((unsigned int) PY_MINOR_VERSION));
  scm_c_define ("PY_MICRO_VERSION",
                scm_from_uint ((unsigned int) PY_MICRO_VERSION));
  scm_c_define ("PY_RELEASE_LEVEL",
                scm_from_uint ((unsigned int) PY_RELEASE_LEVEL));
  scm_c_define ("PY_RELEASE_SERIAL",
                scm_from_uint ((unsigned int) PY_RELEASE_SERIAL));
}

//-------------------------------------------------------------------------
