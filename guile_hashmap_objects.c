#include <config.h>

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <assert.h>
#include <libintl.h>
#include <sortsmill/guile/core.h>

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

#define HASHMAP_MODULE__ "sortsmill core hashmaps"

//VISIBLE SCM guile_hashmap_equal_p__ (SCM a, SCM b);
//VISIBLE SCM guile_hashmap_write__ (SCM a, SCM b);
void guile_init_smcoreguile_hashmaps (void);

//----------------------------------------------------------------------

#if HAVE_SCM_MAKE_FOREIGN_OBJECT_TYPE
////////////////////////////////
// Use a ‘foreign object type’.
////////////////////////////////

static SCM hashmap_type = SCM_UNDEFINED;
static SCM hashmap_cursor_type = SCM_UNDEFINED;

static inline void
_scm_assert_hashmap (SCM obj)
{
  scm_assert_foreign_object_type (hashmap_type, obj);
}

VISIBLE scm_t_hashmap
scm_to_scm_t_hashmap (SCM obj)
{
  _scm_assert_hashmap (obj);
  return scm_foreign_object_ref (obj, 0);
}

VISIBLE SCM
scm_from_scm_t_hashmap (scm_t_hashmap p)
{
  return scm_make_foreign_object_1 (hashmap_type, (void *) p);
}

static inline void
_scm_assert_hashmap_cursor (SCM obj)
{
  scm_assert_foreign_object_type (hashmap_cursor_type, obj);
}

VISIBLE scm_t_hashmap_cursor
scm_to_scm_t_hashmap_cursor (SCM obj)
{
  _scm_assert_hashmap_cursor (obj);
  return scm_foreign_object_ref (obj, 0);
}

VISIBLE SCM
scm_from_scm_t_hashmap_cursor (scm_t_hashmap_cursor p)
{
  return scm_make_foreign_object_1 (hashmap_cursor_type, (void *) p);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

#if defined SCM_IS_A_P          /* The SCM_IS_A_P macro is not documented in
                                   the public interface, so test for its
                                   presence. */
VISIBLE bool
scm_is_hashmap (SCM obj)
{
  return (SCM_IS_A_P (obj, hashmap_type));
}

VISIBLE SCM
scm_hashmap_p (SCM obj)
{
  return scm_from_bool (scm_is_hashmap (obj));
}

VISIBLE bool
scm_is_hashmap_cursor (SCM obj)
{
  return (SCM_IS_A_P (obj, hashmap_cursor_type));
}

VISIBLE SCM
scm_hashmap_cursor_p (SCM obj)
{
  return scm_from_bool (scm_is_hashmap_cursor (obj));
}

#else // !defined SCM_IS_A_P

STM_SCM_ONE_TIME_INITIALIZE (static STM_ATTRIBUTE_PURE, SCM, _scm_is_a_p,
                             (_scm_is_a_p__Value =
                              scm_c_public_ref ("oop goops", "is-a?")));

VISIBLE bool
scm_is_hashmap (SCM obj)
{
  return (scm_is_true (scm_call_2 (_scm_is_a_p (), obj, hashmap_type)));
}

VISIBLE SCM
scm_hashmap_p (SCM obj)
{
  return scm_from_bool (scm_is_hashmap (obj));
}

VISIBLE bool
scm_is_hashmap_cursor (SCM obj)
{
  return (scm_is_true (scm_call_2 (_scm_is_a_p (), obj, hashmap_cursor_type)));
}

VISIBLE SCM
scm_hashmap_cursor_p (SCM obj)
{
  return scm_from_bool (scm_is_hashmap_cursor (obj));
}

#endif // !defined SCM_IS_A_P

VISIBLE void
guile_init_smcoreguile_hashmaps (void)
{
  SCM slot0 = scm_from_utf8_symbol ("pointer");
  SCM slots = scm_list_1 (slot0);

  SCM name = scm_from_utf8_symbol ("<hashmap>");
  hashmap_type = scm_make_foreign_object_type (name, slots, NULL);

  name = scm_from_utf8_symbol ("<hashmap-cursor>");
  hashmap_cursor_type = scm_make_foreign_object_type (name, slots, NULL);

  scm_c_define ("<hashmap>", hashmap_type);
  scm_c_define ("<hashmap-cursor>", hashmap_cursor_type);

  scm_c_define ("using-foreign-objects?", SCM_BOOL_T);
}

#else // !HAVE_SCM_MAKE_FOREIGN_OBJECT_TYPE
////////////////////////////////
// Use a ‘SMOB type’.
////////////////////////////////

static scm_t_bits hashmap_tag;

enum {
  hashmap_subtag,
  hashmap_cursor_subtag
};

static void
_expected_hashmap (SCM obj)
{
  rnrs_raise_condition
    (scm_list_3
     (rnrs_make_assertion_violation (),
      rnrs_c_make_message_condition (_("expected a hashmap")),
      rnrs_make_irritants_condition (scm_list_1 (obj))));
}

static void
_expected_hashmap_cursor (SCM obj)
{
  rnrs_raise_condition
    (scm_list_3
     (rnrs_make_assertion_violation (),
      rnrs_c_make_message_condition (_("expected a hashmap cursor")),
      rnrs_make_irritants_condition (scm_list_1 (obj))));
}

static inline void
_scm_assert_hashmap (SCM obj)
{
  scm_assert_smob_type (hashmap_tag, obj);
  if (SCM_SMOB_DATA_2 (obj) != hashmap_subtag)
    _expected_hashmap (obj);
}

static inline void
_scm_assert_hashmap_cursor (SCM obj)
{
  scm_assert_smob_type (hashmap_tag, obj);
  if (SCM_SMOB_DATA_2 (obj) != hashmap_cursor_subtag)
    _expected_hashmap_cursor (obj);
}

VISIBLE scm_t_hashmap
scm_to_scm_t_hashmap (SCM obj)
{
  _scm_assert_hashmap (obj);
  return (void *) SCM_SMOB_DATA (obj);
}

VISIBLE scm_t_hashmap_cursor
scm_to_scm_t_hashmap_cursor (SCM obj)
{
  _scm_assert_hashmap_cursor (obj);
  return (void *) SCM_SMOB_DATA (obj);
}

VISIBLE SCM
scm_from_scm_t_hashmap (scm_t_hashmap p)
{
  return scm_new_double_smob (hashmap_tag, (scm_t_bits) (void *) p,
                              (scm_t_bits) hashmap_subtag, (scm_t_bits) 0);
}

VISIBLE SCM
scm_from_scm_t_hashmap_cursor (scm_t_hashmap_cursor p)
{
  return scm_new_double_smob (hashmap_tag, (scm_t_bits) (void *) p,
                              (scm_t_bits) hashmap_cursor_subtag,
                              (scm_t_bits) 0);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

VISIBLE bool
scm_is_hashmap (SCM obj)
{
  return (SCM_SMOB_PREDICATE (hashmap_tag, obj)
          && SCM_SMOB_DATA_2 (obj) != hashmap_subtag);
}

VISIBLE bool
scm_is_hashmap_cursor (SCM obj)
{
  return (SCM_SMOB_PREDICATE (hashmap_tag, obj)
          && SCM_SMOB_DATA_2 (obj) != hashmap_cursor_subtag);
}

VISIBLE SCM
scm_hashmap_p (SCM obj)
{
  return scm_from_bool (scm_is_hashmap (obj));
}

VISIBLE SCM
scm_hashmap_cursor_p (SCM obj)
{
  return scm_from_bool (scm_is_hashmap_cursor (obj));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

static int
_print_hashmap_smob (SCM hashmap, SCM port,
                     scm_print_state *pstate STM_MAYBE_UNUSED)
{
  //  guile_hashmap_write__ (hashmap, port); ????????????????????????????????????????????????????????????????
  return 1;
}

VISIBLE void
guile_init_smcoreguile_hashmaps (void)
{
  hashmap_tag = scm_make_smob_type ("hashmap", 0);
  //scm_set_smob_print (hashmap_tag, _print_hashmap_smob); ???????????????????????????????????????????????????????
  //scm_set_smob_equalp (hashmap_tag, guile_hashmap_equal_p__); ??????????????????????????????????????????????????

  scm_c_define ("using-foreign-objects?", SCM_BOOL_F);
}

#endif // !HAVE_SCM_MAKE_FOREIGN_OBJECT_TYPE

//----------------------------------------------------------------------

VISIBLE SCM
guile_hashmap_write__ (SCM hashmap, SCM port)
{
#if 0 // ????????????????????????????????????????????????????????????????????????????????????????????????????????
  scm_simple_format (port, scm_from_utf8_string ("#<hashmap ~s>"),
                     scm_list_1 (something_goes_here));
#endif // ????????????????????????????????????????????????????????????????????????????????????????????????????????
  return SCM_UNSPECIFIED;
}

//----------------------------------------------------------------------
