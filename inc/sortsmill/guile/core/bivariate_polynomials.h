/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Guile.
 * 
 * Sorts Mill Core Guile is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Guile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_GUILE_CORE_BIVARIATE_POLYNOMIALS_H_
#define SORTSMILL_GUILE_CORE_BIVARIATE_POLYNOMIALS_H_

#include <libguile.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

SCM scm_exact_bipoly_add (SCM a, SCM b);
SCM scm_exact_bipoly_sub (SCM a, SCM b);
SCM scm_exact_bipoly_mul (SCM a, SCM b);
SCM scm_exact_bipoly_scalarmul (SCM a, SCM r);
SCM scm_exact_bipoly_partial_deriv_wrt_x (SCM a);
SCM scm_exact_bipoly_partial_deriv_wrt_y (SCM a);

SCM scm_dbipoly_add (SCM a, SCM b);
SCM scm_dbipoly_sub (SCM a, SCM b);
SCM scm_dbipoly_mul (SCM a, SCM b);
SCM scm_dbipoly_scalarmul (SCM a, SCM r);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_GUILE_CORE_BIVARIATE_POLYNOMIALS_H_ */
