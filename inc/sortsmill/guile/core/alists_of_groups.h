/*
 * Copyright (C) 2014 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Guile.
 * 
 * Sorts Mill Core Guile is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Guile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SORTSMILL_GUILE_ALISTS_OF_GROUPS_H
#define _SORTSMILL_GUILE_ALISTS_OF_GROUPS_H

#include <libguile.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

SCM scm_alist_of_groups_make (SCM aog_or_pairs);
SCM scm_alist_of_groups_reverse (SCM aog_or_pairs);
SCM scm_alist_of_groups_ref (SCM aog, SCM key);
SCM scm_alist_of_groups_set_x (SCM aog, SCM key, SCM value);
SCM scm_alist_of_groups_cons_set_x (SCM aog, SCM key, SCM value);
SCM scm_alist_of_groups_add_x (SCM aog, SCM key, SCM elements);
SCM scm_alist_of_groups_cons_add_x (SCM aog, SCM key, SCM elements);
SCM scm_alist_of_groups_find_group (SCM aog, SCM element);
SCM scm_alist_of_groups_keys (SCM aog);
SCM scm_alist_of_groups_values (SCM aog);
SCM scm_alist_of_groups_pairs (SCM aog);
size_t scm_c_alist_of_groups_element_count (SCM aog);
SCM scm_alist_of_groups_element_count (SCM aog);
SCM scm_alist_of_groups_duplicate_elements (SCM aog);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* _SORTSMILL_GUILE_ALISTS_OF_GROUPS_H */
