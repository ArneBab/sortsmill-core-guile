/*
 * Copyright (C) 2013, 2014 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Guile.
 * 
 * Sorts Mill Core Guile is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Guile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SORTSMILL_GUILE_CORE_PYTHON_H
#define _SORTSMILL_GUILE_CORE_PYTHON_H

#include <Python.h>
#include <stdbool.h>
#include <libguile.h>
#include <sortsmill/core.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

/* Read-only access to the (PyObject*) stored within a pyobject. */
PyObject *scm_to_PyObject_ptr (SCM obj);

/* Initialization. */
SCM scm_py_initialize (SCM initsigs);
SCM scm_py_initialized_p (void);
SCM scm_py_finalize (void);
SCM scm_raise_py_uninitialized (void);
inline void scm_c_assert_py_initialized (void);
SCM scm_assert_py_initialized (void);

/* Reflection. */
SCM scm_pyeval_getbuiltins (void);
SCM scm_pyeval_getlocals (void);
SCM scm_pyeval_getglobals (void);
SCM scm_pyeval_getframe (void);
SCM scm_pyframe_getlinenumber (SCM frame);
SCM scm_pyframe_getfilename (SCM frame);
SCM scm_pyeval_getfuncname (SCM func);
SCM scm_pyeval_getfuncdesc (SCM func);

/* (py-main lst) */
SCM scm_py_main (SCM program_params);

/* (pyrun-simplestring string)
   (pyrun-simplestring string integer-flags) */
SCM scm_pyrun_simplestring (SCM command, SCM flags);

/* (pyrun-anyfile)
   (pyrun-anyfile port)
   (pyrun-anyfile port integer-flags)
   Also there is an optional keyword argument #:file-name */
SCM scm_pyrun_anyfile (SCM port, SCM flags, SCM file_name);

/* (pyrun-interactiveone)
   (pyrun-interactiveone port)
   (pyrun-interactiveone port integer-flags)
   Also there is an optional keyword argument #:file-name */
SCM scm_pyrun_interactiveone (SCM port, SCM flags, SCM file_name);

/* (pyrun-string string start globals locals)
   (pyrun-string string start globals locals flags) */
SCM scm_pyrun_string (SCM string, SCM start, SCM globals, SCM locals,
                      SCM flags);

/* (pyrun-file port start globals locals)
   (pyrun-file port start globals locals flags)
   Also there is an optional keyword argument #:file-name */
SCM scm_pyrun_file (SCM port, SCM start, SCM globals, SCM locals, SCM flags,
                    SCM file_name);

/* (py-compilestring string file-name start)
   (py-compilestring string file-name start flags)
   (py-compilestring string file-name start flags optimize) */
SCM scm_py_compilestring (SCM string, SCM file_name, SCM start, SCM flags,
                          SCM optimize);

/* (pyeval-evalcode pycode globals locals [args [keywords [defaults
   [closure]]]]) */
SCM scm_pyeval_evalcode (SCM pycode, SCM globals, SCM locals, SCM args,
                         SCM keywords, SCM defaults, SCM closure);

/* Return Guile symbols that represent Python parser start symbols. */
STM_ATTRIBUTE_PURE SCM scm_Py_eval_input (void);
STM_ATTRIBUTE_PURE SCM scm_Py_file_input (void);
STM_ATTRIBUTE_PURE SCM scm_Py_single_input (void);

/* Convert Guile symbols Py_eval_input, Py_file_input, Py_single_input
   to the corresponding C integers, and vice versa. */
int scm_to_py_start_symbol (SCM start_symbol);
SCM scm_from_py_start_symbol (int start);

void scm_dynwind_decref (PyObject *p);  /* Wraps Py_XDECREF. */
void scm_dynwind_decref_clear (PyObject **p);   /* Wraps Py_CLEAR. */

SCM scm_pyscm_type (void);
SCM scm_pybaseobject_type (void);
SCM scm_pysuper_type (void);
SCM scm_pytype_type (void);
SCM scm_pybytes_type (void);
SCM scm_pyunicode_type (void);
SCM scm_pybool_type (void);
SCM scm_pyint_type (void);      /* Supported only in Python 2. */
SCM scm_pylong_type (void);
SCM scm_pyfloat_type (void);
SCM scm_pycomplex_type (void);
SCM scm_pytuple_type (void);
SCM scm_pylist_type (void);
SCM scm_pydict_type (void);
SCM scm_pydictproxy_type (void);
SCM scm_pyset_type (void);
SCM scm_pyfrozenset_type (void);
SCM scm_pyrange_type (void);
SCM scm_pyslice_type (void);
SCM scm_pyellipsis_type (void);
SCM scm_pycode_type (void);
SCM scm_pyframe_type (void);
SCM scm_pytraceback_type (void);
SCM scm_pymethod_type (void);
SCM scm_pyinstancemethod_type (void);
SCM scm_pymodule_type (void);
SCM scm_pyproperty_type (void);
SCM scm_pycapsule_type (void);
SCM scm_pybytearray_type (void);
SCM scm_pymemoryview_type (void);
SCM scm_pycell_type (void);

SCM scm_py_incref_x (SCM obj);  /* Use with care! */
SCM scm_py_decref_x (SCM obj);  /* Use with care! */
SCM scm_py_refcount (SCM obj);

/* scm_c_make_pyobject steals a reference; scm_c_incref_make_pyobject,
   on the other hand, increments the reference count. */
SCM scm_c_make_pyobject (PyObject *p);
SCM scm_c_incref_make_pyobject (PyObject *p);
SCM scm_c_make_borrowed_pyobject (PyObject *child, SCM parent);
SCM scm_make_pyobject (SCM address);    /* (make-pyobject ptr/int) */
SCM scm_incref_make_pyobject (SCM address);
SCM scm_make_borrowed_pyobject (SCM child, SCM parent);

SCM scm_pyobject_to_pointer (SCM obj);
SCM scm_incref_pyobject_to_pointer (SCM obj);

bool scm_is_pyobject (SCM obj);
SCM scm_pyobject_p (SCM obj);
void scm_assert_pyobject (SCM pyobj);

SCM scm_py_notimplemented (void);
bool scm_is_py_notimplemented (SCM obj);
SCM scm_py_notimplemented_p (SCM obj);

bool scm_is_pynone (SCM obj);
SCM scm_pynone_p (SCM obj);
SCM scm_py_none (void);         /* (py-none) */

bool scm_is_pybytes (SCM obj);
bool scm_is_pybytes_exact (SCM obj);
SCM scm_pybytes_p (SCM obj);
SCM scm_pybytes_exact_p (SCM obj);
SCM scm_bytevector_to_pybytes (SCM bv); /* (bytevector->pybytes bv) */
SCM scm_pybytes_to_bytevector (SCM pybytes);    /* (pybytes->bytevector pybytes) */

bool scm_is_pyunicode (SCM obj);
bool scm_is_pyunicode_exact (SCM obj);
SCM scm_pyunicode_p (SCM obj);
SCM scm_pyunicode_exact_p (SCM obj);

/* (string->pyunicode str [errors]) and (pyunicode->string pyuni
   [errors]) where errors = "strict", "replace", "ignore", etc. */
SCM scm_string_to_pyunicode (SCM str, SCM errors);
SCM scm_pyunicode_to_string (SCM pystr, SCM errors);

/* Convert Python string to Guile string, or vice versa; other objects
   are returned unchanged. Codec error checking is the
   default. (FIXME: Should I say `strict'?) */
inline SCM scm_pyunicode_to_scm (SCM obj);
inline SCM scm_scm_to_pyunicode (SCM obj);

SCM scm_python_repr (SCM obj);  /* (python-repr string) →
                                   pybytes-or-pyunicode */
SCM scm_python_str (SCM obj);   /* (python-str string) →
                                   pybytes-or-pyunicode */

bool scm_is_pybool (SCM obj);
SCM scm_pybool_p (SCM obj);
SCM scm_pybool (SCM obj);       /* (pybool obj) */
SCM scm_pyobject_true_p (SCM pyobj);    /* (pyobject-true? pyobj) */

bool scm_is_pyfloat (SCM obj);
bool scm_is_pyfloat_exact (SCM obj);
SCM scm_pyfloat_p (SCM obj);
SCM scm_pyfloat_exact_p (SCM obj);
SCM scm_real_to_pyfloat (SCM real_number);      /* (real->pyfloat x) */
SCM scm_pyfloat_to_real (SCM pyfloat);  /* (pyfloat->real pyobj) */

bool scm_is_pycomplex (SCM obj);
bool scm_is_pycomplex_exact (SCM obj);
SCM scm_pycomplex_p (SCM obj);
SCM scm_pycomplex_exact_p (SCM obj);
SCM scm_number_to_pycomplex (SCM complex_number);       /* (number->pycomplex z) */
SCM scm_pycomplex_to_number (SCM pycomplex);    /* (pycomplex->number pyobj) */

/* `pyinteger' can refer to either of PyInt and PyLong, depending on
   Python version, the value of the integer, and other
   circumstances. */
bool scm_is_pyinteger (SCM obj);
bool scm_is_pyinteger_exact (SCM obj);
SCM scm_pyinteger_p (SCM obj);
SCM scm_pyinteger_exact_p (SCM obj);
SCM scm_integer_to_pyinteger (SCM integer);     /* (integer->pyinteger n) */
SCM scm_pyinteger_to_integer (SCM pyinteger);   /* (pyinteger->integer pyobj) */
SCM scm_pyinteger_to_scm (SCM obj);     /* (pyinteger->scm obj) */

/* `pyreal' can refer to either pyinteger or pyfloat. */
bool scm_is_pyreal (SCM obj);
SCM scm_pyreal_p (SCM obj);
SCM scm_pyreal_to_real (SCM obj);

PyObject *scm_to_py_globals (SCM globals);      /* #t for builtins; #f for
                                                   an empty dict. */
PyObject *scm_to_py_locals (SCM locals);        /* #f for an empty dict. */

SCM rnrs_make_py_exc_info_condition (SCM type, SCM value, SCM traceback);
SCM rnrs_c_make_py_exc_info_condition (PyObject *type, PyObject *value,
                                       PyObject *traceback);
SCM rnrs_fetch_py_exc_info_condition (void);
void scm_c_py_exc_rnrs_raise_condition (const char *who, const char *message,
                                        SCM irritants);
SCM scm_py_exc_rnrs_raise_condition (SCM who, SCM message, SCM irritants);
SCM scm_py_exc_fetch_info (void);

void scm_c_py_exc_check_violation (const char *who, const char *message,
                                   SCM irritants);
SCM scm_py_exc_check_violation (SCM who, SCM message, SCM irritants);

bool scm_c_pyobject_hasattr (SCM obj, SCM attr_name);
SCM scm_pyobject_hasattr_p (SCM obj, SCM attr_name);
SCM scm_pyobject_getattr (SCM obj, SCM attr_name);
SCM scm_pyobject_setattr_x (SCM obj, SCM attr_name, SCM v);
SCM scm_pyobject_delattr_x (SCM obj, SCM attr_name);
bool scm_pyobject_is_less (SCM obj1, SCM obj2);
bool scm_pyobject_is_leq (SCM obj1, SCM obj2);
bool scm_pyobject_is_eq (SCM obj1, SCM obj2);
bool scm_pyobject_is_neq (SCM obj1, SCM obj2);
bool scm_pyobject_is_gr (SCM obj1, SCM obj2);
bool scm_pyobject_is_geq (SCM obj1, SCM obj2);
SCM scm_pyobject_less_p (SCM obj1, SCM obj2);
SCM scm_pyobject_leq_p (SCM obj1, SCM obj2);
SCM scm_pyobject_eq_p (SCM obj1, SCM obj2);
SCM scm_pyobject_neq_p (SCM obj1, SCM obj2);
SCM scm_pyobject_gr_p (SCM obj1, SCM obj2);
SCM scm_pyobject_geq_p (SCM obj1, SCM obj2);
SCM scm_pyobject_repr (SCM obj);
SCM scm_pyobject_unicode (SCM obj);
SCM scm_pyobject_ascii (SCM obj);
SCM scm_pyobject_bytes (SCM obj);
bool scm_c_pyobject_isinstance (SCM inst, SCM cls);
SCM scm_pyobject_isinstance_p (SCM inst, SCM cls);
bool scm_c_pyobject_issubclass (SCM inst, SCM cls);
SCM scm_pyobject_issubclass_p (SCM inst, SCM cls);
bool scm_is_pycallable (SCM obj);
SCM scm_pycallable_p (SCM obj);
SCM scm_pyobject_call (SCM callable_object, SCM args, SCM kw);
SCM scm_pyobject_callobject (SCM callable_object, SCM args);
SCM scm_pyobject_callfunctionobjargs (SCM callable, SCM args_lst);
SCM scm_pyobject_callmethodobjargs (SCM obj, SCM method_name, SCM args_lst);
SCM scm_pyobject_to_procedure (SCM callable);
SCM scm_pyobject_method_to_procedure (SCM obj, SCM method_name);
SCM scm_pyobject_hash (SCM obj);
SCM scm_pyobject_not (SCM obj);
SCM scm_pyobject_type (SCM obj);
ssize_t scm_c_pyobject_length (SCM obj);
SCM scm_pyobject_length (SCM obj);
SCM scm_pyobject_getitem (SCM obj, SCM key);
SCM scm_pyobject_setitem_x (SCM obj, SCM key, SCM v);
SCM scm_pyobject_delitem_x (SCM obj, SCM key);
int scm_c_pyobject_asfiledescriptor (SCM obj);
SCM scm_pyobject_asfiledescriptor (SCM obj);
SCM scm_pyobject_dir (SCM obj);
SCM scm_pyobject_getiter (SCM obj);

bool scm_is_pysequence (SCM obj);
SCM scm_pysequence_p (SCM obj);
bool scm_is_pylist (SCM obj);
bool scm_is_pylist_exact (SCM obj);
SCM scm_pylist_p (SCM obj);
SCM scm_pylist_exact_p (SCM obj);
bool scm_is_pytuple (SCM obj);
bool scm_is_pytuple_exact (SCM obj);
SCM scm_pytuple_p (SCM obj);
SCM scm_pytuple_exact_p (SCM obj);
ssize_t scm_c_pysequence_length (SCM obj);
SCM scm_pysequence_length (SCM obj);
SCM scm_pysequence_concat (SCM obj1, SCM obj2);
SCM scm_pysequence_concat_list (SCM obj_list);
SCM scm_c_pysequence_repeat (SCM obj, ssize_t count);
SCM scm_pysequence_repeat (SCM obj, SCM count);
SCM scm_pysequence_inplaceconcat_x (SCM obj1, SCM obj2);
SCM scm_pysequence_inplaceconcat_list_x (SCM obj_list);
SCM scm_c_pysequence_inplacerepeat (SCM obj, ssize_t count);
SCM scm_pysequence_inplacerepeat_x (SCM obj, SCM count);
SCM scm_c_pysequence_getitem (SCM obj, ssize_t i);
SCM scm_pysequence_getitem (SCM obj, SCM i);
SCM scm_c_pysequence_getslice (SCM obj, ssize_t i1, ssize_t i2);
SCM scm_pysequence_getslice (SCM obj, SCM i1, SCM i2);
void scm_c_pysequence_setitem (SCM obj, ssize_t i, SCM v);
SCM scm_pysequence_setitem_x (SCM obj, SCM i, SCM v);
void scm_c_pysequence_delitem (SCM obj, ssize_t i);
SCM scm_pysequence_delitem_x (SCM obj, SCM i);
void scm_c_pysequence_setslice (SCM obj, ssize_t i1, ssize_t i2, SCM v);
SCM scm_pysequence_setslice_x (SCM obj, SCM i1, SCM i2, SCM v);
void scm_c_pysequence_delslice (SCM obj, ssize_t i1, ssize_t i2);
SCM scm_pysequence_delslice_x (SCM obj, SCM i1, SCM i2);
ssize_t scm_c_pysequence_count (SCM obj, SCM v);
SCM scm_pysequence_count (SCM obj, SCM v);
bool scm_pysequence_contains (SCM obj, SCM v);
SCM scm_pysequence_contains_p (SCM obj, SCM v);
ssize_t scm_c_pysequence_index (SCM obj, SCM v);
SCM scm_pysequence_index (SCM obj, SCM v);
SCM scm_pysequence_list (SCM obj);
SCM scm_pysequence_tuple (SCM obj);
//
SCM scm_c_pysequence_to_list_with_tail (PyObject *_obj, SCM tail);
SCM scm_c_pysequence_to_list (PyObject *_obj);
SCM scm_pysequence_to_list_with_tail (SCM obj, SCM tail);
SCM scm_pysequence_to_list (SCM obj);
SCM scm_pysequence_reverse_to_list (SCM obj);
inline SCM scm_pysequence_reverse_to_pylist (SCM obj);
inline SCM scm_pysequence_reverse_to_pytuple (SCM obj);
SCM scm_pysequence_map_to_list (SCM proc, SCM obj_lst);
//
SCM scm_c_pysequence_to_vector (PyObject *_obj);
SCM scm_pysequence_to_vector (SCM obj);
SCM scm_pysequence_map_to_vector (SCM proc, SCM obj_lst);
SCM scm_c_pysequence_reverse_to_vector (PyObject *_obj);
SCM scm_pysequence_reverse_to_vector (SCM obj);
SCM scm_pysequence_map_to_vector (SCM proc, SCM obj_lst);
//
SCM scm_pysequence_for_each (SCM proc, SCM obj_lst);
SCM scm_pysequence_fold_left (SCM proc, SCM init, SCM obj_lst);
SCM scm_pysequence_fold_right (SCM proc, SCM init, SCM obj_lst);
SCM scm_pysequence_for_all (SCM proc, SCM obj_lst);
SCM scm_pysequence_exists (SCM proc, SCM obj_lst);
//
SCM scm_list_to_pylist (SCM lst);
SCM scm_list_to_pytuple (SCM lst);
SCM scm_list_reverse_to_pylist (SCM lst);
SCM scm_list_reverse_to_pytuple (SCM lst);
SCM scm_vector_to_pylist (SCM v);
SCM scm_vector_to_pytuple (SCM v);
SCM scm_vector_reverse_to_pylist (SCM v);
SCM scm_vector_reverse_to_pytuple (SCM v);
//
SCM scm_list_map_to_pylist (SCM proc, SCM obj_lst);
SCM scm_list_map_to_pytuple (SCM proc, SCM obj_lst);
SCM scm_vector_map_to_pylist (SCM proc, SCM obj_lst);
SCM scm_vector_map_to_pytuple (SCM proc, SCM obj_lst);
//
SCM scm_c_pysequence_to_s8vector (PyObject *_obj);
SCM scm_pysequence_to_s8vector (SCM obj);
SCM scm_c_pysequence_to_s16vector (PyObject *_obj);
SCM scm_pysequence_to_s16vector (SCM obj);
SCM scm_c_pysequence_to_s32vector (PyObject *_obj);
SCM scm_pysequence_to_s32vector (SCM obj);
SCM scm_c_pysequence_to_u8vector (PyObject *_obj);
SCM scm_pysequence_to_u8vector (SCM obj);
SCM scm_c_pysequence_to_u16vector (PyObject *_obj);
SCM scm_pysequence_to_u16vector (SCM obj);
SCM scm_c_pysequence_to_u32vector (PyObject *_obj);
SCM scm_pysequence_to_u32vector (SCM obj);
SCM scm_c_pysequence_to_f32vector (PyObject *_obj);
SCM scm_pysequence_to_f32vector (SCM obj);
SCM scm_c_pysequence_to_f64vector (PyObject *_obj);
SCM scm_pysequence_to_f64vector (SCM obj);
//
SCM scm_c_pysequence_reverse_to_s8vector (PyObject *_obj);
SCM scm_pysequence_reverse_to_s8vector (SCM obj);
SCM scm_c_pysequence_reverse_to_s16vector (PyObject *_obj);
SCM scm_pysequence_reverse_to_s16vector (SCM obj);
SCM scm_c_pysequence_reverse_to_s32vector (PyObject *_obj);
SCM scm_pysequence_reverse_to_s32vector (SCM obj);
SCM scm_c_pysequence_reverse_to_u8vector (PyObject *_obj);
SCM scm_pysequence_reverse_to_u8vector (SCM obj);
SCM scm_c_pysequence_reverse_to_u16vector (PyObject *_obj);
SCM scm_pysequence_reverse_to_u16vector (SCM obj);
SCM scm_c_pysequence_reverse_to_u32vector (PyObject *_obj);
SCM scm_pysequence_reverse_to_u32vector (SCM obj);
SCM scm_c_pysequence_reverse_to_f32vector (PyObject *_obj);
SCM scm_pysequence_reverse_to_f32vector (SCM obj);
SCM scm_c_pysequence_reverse_to_f64vector (PyObject *_obj);
SCM scm_pysequence_reverse_to_f64vector (SCM obj);
//
SCM scm_s8vector_to_pylist (SCM v);
SCM scm_s16vector_to_pylist (SCM v);
SCM scm_s32vector_to_pylist (SCM v);
SCM scm_u8vector_to_pylist (SCM v);
SCM scm_u16vector_to_pylist (SCM v);
SCM scm_u32vector_to_pylist (SCM v);
SCM scm_f32vector_to_pylist (SCM v);
SCM scm_f64vector_to_pylist (SCM v);
//
SCM scm_s8vector_reverse_to_pylist (SCM v);
SCM scm_s16vector_reverse_to_pylist (SCM v);
SCM scm_s32vector_reverse_to_pylist (SCM v);
SCM scm_u8vector_reverse_to_pylist (SCM v);
SCM scm_u16vector_reverse_to_pylist (SCM v);
SCM scm_u32vector_reverse_to_pylist (SCM v);
SCM scm_f32vector_reverse_to_pylist (SCM v);
SCM scm_f64vector_reverse_to_pylist (SCM v);
//
SCM scm_s8vector_to_pytuple (SCM v);
SCM scm_s16vector_to_pytuple (SCM v);
SCM scm_s32vector_to_pytuple (SCM v);
SCM scm_u8vector_to_pytuple (SCM v);
SCM scm_u16vector_to_pytuple (SCM v);
SCM scm_u32vector_to_pytuple (SCM v);
SCM scm_f32vector_to_pytuple (SCM v);
SCM scm_f64vector_to_pytuple (SCM v);
//
SCM scm_s8vector_reverse_to_pytuple (SCM v);
SCM scm_s16vector_reverse_to_pytuple (SCM v);
SCM scm_s32vector_reverse_to_pytuple (SCM v);
SCM scm_u8vector_reverse_to_pytuple (SCM v);
SCM scm_u16vector_reverse_to_pytuple (SCM v);
SCM scm_u32vector_reverse_to_pytuple (SCM v);
SCM scm_f32vector_reverse_to_pytuple (SCM v);
SCM scm_f64vector_reverse_to_pytuple (SCM v);
//
SCM scm_pysequence_map_to_s8vector (SCM proc, SCM obj_lst);
SCM scm_pysequence_map_to_s16vector (SCM proc, SCM obj_lst);
SCM scm_pysequence_map_to_s32vector (SCM proc, SCM obj_lst);
SCM scm_pysequence_map_to_s64vector (SCM proc, SCM obj_lst);
SCM scm_pysequence_map_to_u8vector (SCM proc, SCM obj_lst);
SCM scm_pysequence_map_to_u16vector (SCM proc, SCM obj_lst);
SCM scm_pysequence_map_to_u32vector (SCM proc, SCM obj_lst);
SCM scm_pysequence_map_to_u64vector (SCM proc, SCM obj_lst);
SCM scm_pysequence_map_to_f32vector (SCM proc, SCM obj_lst);
SCM scm_pysequence_map_to_f64vector (SCM proc, SCM obj_lst);
//
SCM scm_c_pysequence_take (SCM obj, ssize_t n);
SCM scm_pysequence_take (SCM obj, SCM n);
SCM scm_c_pysequence_drop (SCM obj, ssize_t n);
SCM scm_pysequence_drop (SCM obj, SCM n);
SCM scm_c_pysequence_take_right (SCM obj, ssize_t n);
SCM scm_pysequence_take_right (SCM obj, SCM n);
SCM scm_c_pysequence_drop_right (SCM obj, ssize_t n);
SCM scm_pysequence_drop_right (SCM obj, SCM n);
inline SCM scm_pysequence_car (SCM obj);
inline SCM scm_pysequence_cdr (SCM obj);
inline SCM scm_pysequence_caar (SCM obj);
inline SCM scm_pysequence_cadr (SCM obj);
inline SCM scm_pysequence_cdar (SCM obj);
inline SCM scm_pysequence_cddr (SCM obj);
//
SCM scm_pylist_insert_x (SCM pylist, SCM i, SCM item);
SCM scm_pylist_append_x (SCM pylist, SCM obj);
SCM scm_pylist_sort_x (SCM pylist);
SCM scm_pylist_reverse_x (SCM pylist);

bool scm_is_pymapping (SCM obj);
SCM scm_pymapping_p (SCM obj);
bool scm_is_pydict (SCM obj);
bool scm_is_pydict_exact (SCM obj);
SCM scm_pydict_p (SCM obj);
SCM scm_pydict_exact_p (SCM obj);
ssize_t scm_c_pymapping_length (SCM obj);
SCM scm_pymapping_length (SCM obj);
void scm_c_pymapping_delitemstring (SCM obj, const char *key);
SCM scm_pymapping_delitemstring_x (SCM obj, SCM key);
bool scm_c_pymapping_haskeystring (SCM obj, const char *key);
SCM scm_pymapping_haskeystring_p (SCM obj, SCM key);
bool scm_c_pymapping_haskey (SCM obj, SCM key);
SCM scm_pymapping_haskey_p (SCM obj, SCM key);
SCM scm_pymapping_keys (SCM obj);
SCM scm_pymapping_values (SCM obj);
SCM scm_pymapping_items (SCM obj);
SCM scm_c_pymapping_getitemstring (SCM obj, const char *key);
SCM scm_pymapping_getitemstring (SCM obj, SCM key);
void scm_c_pymapping_setitemstring (SCM obj, const char *key, SCM v);
SCM scm_pymapping_setitemstring_x (SCM obj, SCM key, SCM v);
SCM scm_pymapping_to_alist (SCM obj);
SCM scm_alist_to_pydict (SCM lst);
SCM scm_pydict_clear_x (SCM obj);
SCM scm_pydictproxy_new (SCM obj);
SCM scm_pydict_copy (SCM obj);
void scm_c_pydict_merge (SCM a, SCM b, bool override);
SCM scm_pydict_merge_x (SCM a, SCM b, SCM override);
void scm_c_pydict_mergefromseq2 (SCM a, SCM seq2, bool override);
SCM scm_pydict_mergefromseq2_x (SCM a, SCM seq2, SCM override);
SCM scm_pyseq2_to_pydict (SCM obj);
SCM scm_pyobject_to_pydict (SCM obj);
SCM scm_c_pymapping_to_keyworded_list (PyObject *_obj);
SCM scm_pymapping_to_keyworded_list (SCM obj);
SCM scm_keyworded_list_to_pydict (SCM lst);
SCM scm_keyworded_list_to_py_arguments (SCM lst);

bool scm_is_pyset (SCM obj);
SCM scm_pyset_p (SCM obj);
bool scm_is_pyfrozenset (SCM obj);
SCM scm_pyfrozenset_p (SCM obj);
bool scm_is_pyfrozenset_exact (SCM obj);
SCM scm_pyfrozenset_exact_p (SCM obj);
bool scm_is_pyanyset (SCM obj);
SCM scm_pyanyset_p (SCM obj);
bool scm_is_pyanyset_exact (SCM obj);
SCM scm_pyanyset_exact_p (SCM obj);
SCM scm_pyset_new (SCM pyiterable);
SCM scm_pyfrozenset_new (SCM pyiterable);
SCM scm_list_to_pyset (SCM lst);
SCM scm_list_to_pyfrozenset (SCM lst);
SCM scm_pyset_size (SCM obj);
bool scm_c_pyset_contains (SCM anyset, SCM key);
SCM scm_pyset_contains_p (SCM anyset, SCM key);
SCM scm_pyset_add_x (SCM anyset, SCM key);
SCM scm_pyset_discard_x (SCM set, SCM key);
SCM scm_pyset_pop_x (SCM set);
SCM scm_pyset_clear_x (SCM set);

bool scm_is_pynumber (SCM obj);
SCM scm_pynumber_p (SCM obj);
bool scm_is_pyindex (SCM obj);
SCM scm_pyindex_p (SCM obj);
SCM scm_pynumber_add (SCM obj1, SCM obj2);
SCM scm_pynumber_add_list (SCM obj1, SCM obj_lst);
SCM scm_pynumber_subtract (SCM obj1, SCM obj2);
SCM scm_pynumber_minus_list (SCM obj1, SCM obj_lst);
SCM scm_pynumber_multiply (SCM obj1, SCM obj2);
SCM scm_pynumber_multiply_list (SCM obj1, SCM obj_lst);
SCM scm_pynumber_floordivide (SCM obj1, SCM obj2);
SCM scm_pynumber_truedivide (SCM obj1, SCM obj2);
SCM scm_pynumber_truedivide_list (SCM obj1, SCM obj2, SCM obj_lst);
SCM scm_pynumber_remainder (SCM obj1, SCM obj2);
SCM scm_pynumber_divmod (SCM obj1, SCM obj2);
SCM scm_pynumber_power (SCM obj1, SCM obj2, SCM obj3);
SCM scm_pynumber_negative (SCM obj);
SCM scm_pynumber_positive (SCM obj);
SCM scm_pynumber_absolute (SCM obj);
SCM scm_pynumber_invert (SCM obj);
SCM scm_pynumber_lshift (SCM obj1, SCM obj2);
SCM scm_pynumber_rshift (SCM obj1, SCM obj2);
SCM scm_pynumber_and (SCM obj1, SCM obj2);
SCM scm_pynumber_and_list (SCM obj1, SCM obj_lst);
SCM scm_pynumber_xor (SCM obj1, SCM obj2);
SCM scm_pynumber_xor_list (SCM obj1, SCM obj_lst);
SCM scm_pynumber_or (SCM obj1, SCM obj2);
SCM scm_pynumber_or_list (SCM obj1, SCM obj_lst);
SCM scm_pynumber_inplaceadd (SCM obj1, SCM obj2);
SCM scm_pynumber_inplaceadd_list (SCM obj1, SCM obj2, SCM obj_lst);
SCM scm_pynumber_inplacesubtract (SCM obj1, SCM obj2);
SCM scm_pynumber_inplacesubtract_list (SCM obj1, SCM obj2, SCM obj_lst);
SCM scm_pynumber_inplacemultiply (SCM obj1, SCM obj2);
SCM scm_pynumber_inplacemultiply_list (SCM obj1, SCM obj2, SCM obj_lst);
SCM scm_pynumber_inplacefloordivide (SCM obj1, SCM obj2);
SCM scm_pynumber_inplacetruedivide (SCM obj1, SCM obj2);
SCM scm_pynumber_inplacetruedivide_list (SCM obj1, SCM obj2, SCM obj_lst);
SCM scm_pynumber_inplaceremainder (SCM obj1, SCM obj2);
SCM scm_pynumber_inplacedivmod (SCM obj1, SCM obj2);
SCM scm_pynumber_inplacepower (SCM obj1, SCM obj2, SCM obj3);
SCM scm_pynumber_inplacelshift (SCM obj1, SCM obj2);
SCM scm_pynumber_inplacershift (SCM obj1, SCM obj2);
SCM scm_pynumber_inplaceand (SCM obj1, SCM obj2);
SCM scm_pynumber_inplaceand_list (SCM obj1, SCM obj2, SCM obj_lst);
SCM scm_pynumber_inplacexor (SCM obj1, SCM obj2);
SCM scm_pynumber_inplacexor_list (SCM obj1, SCM obj2, SCM obj_lst);
SCM scm_pynumber_inplaceor (SCM obj1, SCM obj2);
SCM scm_pynumber_inplaceor_list (SCM obj1, SCM obj2, SCM obj_lst);
SCM scm_pynumber_integer (SCM obj);
SCM scm_pynumber_float (SCM obj);
SCM scm_pynumber_index (SCM obj);
SCM scm_c_pynumber_tobase (SCM obj, int base);
SCM scm_pynumber_tobase (SCM obj, SCM base);

bool scm_is_pymodule (SCM obj);
SCM scm_pymodule_p (SCM obj);
SCM scm_c_pymodule_new (const char *name);
SCM scm_pymodule_new (SCM name);
SCM scm_pymodule_getdict (SCM module);
const char *scm_c_pymodule_getname (SCM module);
SCM scm_pymodule_getname (SCM module);
SCM scm_pymodule_getfilename (SCM module);
void scm_c_pymodule_addobject (SCM module, const char *name, SCM v);
SCM scm_pymodule_addobject_x (SCM module, SCM name, SCM v);

SCM scm_c_pyimport_importmodule (const char *name, SCM globals, SCM locals,
                                 SCM fromlist, int level);
SCM scm_pyimport_importmodule (SCM name, SCM globals, SCM locals, SCM fromlist,
                               SCM level);
SCM scm_pyimport_import (SCM name);
SCM scm_pyimport_reloadmodule (SCM module);
SCM scm_c_pyimport_addmodule (const char *name);
SCM scm_pyimport_addmodule_x (SCM name);
SCM scm_pyimport_getmoduledict (void);

/* Convert a Python module name to the name of a Guile module. For
   example: @code{"a.b.c"} yields @code{'(a b c)}. */
SCM scm_pymodule_name_to_module_name (SCM pymodule_name);

/* Convert a Python module name to the name of a Guile initialization
   procedure. For example: @code{"a.b.c"} yields
   @code{'PyInit-a.b.c}. */
SCM scm_pyinit_name (SCM pymodule_name);

/* Return the variable that contains the Guile initialization
   procedure for a Python module; but return @code{#f} if the variable
   cannot be found. */
SCM scm_pyinit_variable (SCM module_name, SCM pyinit_name);
SCM scm_pymodule_pyinit_variable (SCM pymodule_name);

/* Return the Guile initialization procedure for a Python module;
   but return @code{#f} if the procedure cannot be found. */
SCM scm_pyinit_procedure (SCM module_name, SCM pyinit_name);
SCM scm_pymodule_pyinit_procedure (SCM pymodule_name);

/* The following four are defined only if libguile is built with
   scm_procedure_to_pointer. */
SCM scm_procedure_to_PyNoArgsFunction (SCM proc);
SCM scm_procedure_to_PyCFunction (SCM proc);
SCM scm_procedure_to_PyCFunctionWithKeywords (SCM proc);
SCM scm_procedure_to_pycallable (SCM proc, SCM method_p, SCM args_p,
                                 SCM keywords_p);

/* Guile objects wrapped for use in Python. These are meant to be
   created only by C or Guile code, not by Python code directly. */
extern PyTypeObject PySCM_Type;
inline int PySCM_Check (PyObject *pyobj);
PyObject *PySCM_FromSCM (SCM obj);
SCM PySCM_AsSCM (PyObject *pyobj);
bool scm_is_pyscm (SCM obj);
SCM scm_pyscm_p (SCM obj);
SCM scm_scm_to_pyscm (SCM obj);
SCM scm_pyscm_to_scm (SCM obj);

SCM scm_pyerr_print_x (SCM set_sys_last_vars_p);
SCM scm_pyerr_occurred_p (void);
SCM scm_pyerr_exceptionmatches_p (SCM exc);
SCM scm_pyerr_givenexceptionmatches_p (SCM given, SCM exc);
SCM scm_pyerr_normalizeexception_x (SCM type, SCM value, SCM traceback);
SCM scm_pyerr_clear_x (void);
SCM scm_pyerr_fetch_x (void);
SCM scm_pyerr_restore_x (SCM type, SCM value, SCM traceback);
SCM scm_pyerr_set_x (SCM exc, SCM obj);

bool scm_is_pytype (SCM obj);
bool scm_is_pytype_exact (SCM obj);
SCM scm_pytype_p (SCM obj);
SCM scm_pytype_exact_p (SCM obj);
SCM scm_pytype_clearcache_x (void);
SCM scm_pytype_getflags (SCM obj);
SCM scm_pytype_modified_x (SCM obj);
SCM scm_tpflags_symbols_to_integer (SCM symbol_lst);
SCM scm_integer_to_tpflags_symbols (SCM flag_bits);
SCM scm_pytype_hasfeature_p (SCM pytype, SCM features);
bool scm_pytype_is_issubtype (SCM a, SCM b);
SCM scm_pytype_issubtype_p (SCM a, SCM b);

bool scm_is_pymethod (SCM obj);
SCM scm_pymethod_p (SCM obj);
bool scm_is_pyinstancemethod (SCM obj);
SCM scm_pyinstancemethod_p (SCM obj);
SCM scm_pyinstancemethod_new (SCM func, SCM class);

bool scm_is_pyiter (SCM obj);
SCM scm_pyiter_p (SCM obj);
SCM scm_pyiter_next_x (SCM obj);
SCM scm_stream_to_pyiter (SCM stream);
SCM scm_thunk_to_pyiter (SCM thunk);

SCM scm_py_local_ref (SCM key);
SCM scm_py_global_ref (SCM key);
SCM scm_py_builtin_ref (SCM key);
SCM scm_py_local_ref_p (SCM key);
SCM scm_py_global_ref_p (SCM key);
SCM scm_py_builtin_ref_p (SCM key);
SCM scm_py_var_ref (SCM key);
SCM scm_py_var_ref_p (SCM key);
SCM scm_py_module_var_ref (SCM module_key, SCM variable_key);
SCM scm_py_item_ref (SCM obj, SCM key);
SCM scm_py_item_set_x (SCM obj, SCM key, SCM value);
SCM scm_py_item_del_x (SCM obj, SCM key);

inline void
scm_c_assert_py_initialized (void)
{
  if (!Py_IsInitialized ())
    scm_raise_py_uninitialized ();
}

/* PySCM_Check() is `really' a Py*_CheckExact() function, because the
   type cannot be inherited. Purposely it is declared `int' instead of
   `bool', for consistency with the documentation of CPython's own
   Py*_CheckExact() `functions'; though in fact CPython declares these
   as macros. */
inline int
PySCM_Check (PyObject *pyobj)
{
  return (Py_TYPE (pyobj) == &PySCM_Type);
}

inline SCM
scm_pysequence_reverse_to_pylist (SCM obj)
{
  return scm_list_to_pylist (scm_pysequence_reverse_to_list (obj));
}

inline SCM
scm_pysequence_reverse_to_pytuple (SCM obj)
{
  return scm_list_to_pytuple (scm_pysequence_reverse_to_list (obj));
}

inline SCM
scm_pysequence_car (SCM obj)
{
  return scm_c_pysequence_getitem (obj, 0);
}

inline SCM
scm_pysequence_cdr (SCM obj)
{
  return scm_c_pysequence_drop (obj, 1);
}

inline SCM
scm_pysequence_caar (SCM obj)
{
  return scm_pysequence_car (scm_pysequence_car (obj));
}

inline SCM
scm_pysequence_cadr (SCM obj)
{
  return scm_c_pysequence_getitem (obj, 1);
}

inline SCM
scm_pysequence_cdar (SCM obj)
{
  return scm_pysequence_cdr (scm_pysequence_car (obj));
}

inline SCM
scm_pysequence_cddr (SCM obj)
{
  return scm_pysequence_cdr (scm_pysequence_cdr (obj));
}

inline SCM
scm_pyunicode_to_scm (SCM obj)
{
  return (scm_is_pyunicode (obj)) ?
    scm_pyunicode_to_string (obj, SCM_UNDEFINED) : obj;
}

inline SCM
scm_scm_to_pyunicode (SCM obj)
{
  return (scm_is_string (obj)) ?
    scm_string_to_pyunicode (obj, SCM_UNDEFINED) : obj;
}

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* _SORTSMILL_GUILE_CORE_PYTHON_H */
