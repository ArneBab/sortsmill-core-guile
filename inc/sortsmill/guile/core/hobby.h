/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Guile.
 * 
 * Sorts Mill Core Guile is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Guile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SORTSMILL_GUILE_CORE_HOBBY_H
#define _SORTSMILL_GUILE_CORE_HOBBY_H

#include <libguile.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

SCM scm_hobby_tensions_to_control_points (SCM perhaps_increase_tensions_p,
                                          SCM p0, SCM dir0, SCM tension0,
                                          SCM tension3, SCM dir3, SCM p3);
SCM scm_hobby_guess_directions (SCM start_dir, SCM end_dir,
                                SCM start_curl, SCM end_curl,
                                SCM a_tensions, SCM b_tensions, SCM points);
SCM scm_hobby_periodic_guess_directions (SCM a_tensions, SCM b_tensions,
                                         SCM points);

SCM scm_solve_hobby_guide_tokens (SCM input_tokens);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* _SORTSMILL_GUILE_CORE_HOBBY_H */
