/*
 * Copyright (C) 2012 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Guile.
 * 
 * Sorts Mill Core Guile is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Guile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SORTSMILL_GUILE_FORMAT_H
#define _SORTSMILL_GUILE_FORMAT_H

#include <libguile.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

/* ‘format’ from (ice-9 format). */
SCM scm_format (SCM destination, SCM message, SCM args);
SCM scm_c_utf8_format (SCM destination, const char *message, SCM args);
SCM scm_c_locale_format (SCM destination, const char *message, SCM args);

/* ‘format’ with ‘destination’ set to #f. */
SCM scm_sformat (SCM message, SCM args);
SCM scm_c_utf8_sformat (const char *message, SCM args);
SCM scm_c_locale_sformat (const char *message, SCM args);

/* ‘format’ with ‘destination’ set to #t. */
SCM scm_oformat (SCM message, SCM args);
SCM scm_c_utf8_oformat (const char *message, SCM args);
SCM scm_c_locale_oformat (const char *message, SCM args);

/* ‘format’ with ‘destination’ set to the current error port. */
SCM scm_eformat (SCM message, SCM args);
SCM scm_c_utf8_eformat (const char *message, SCM args);
SCM scm_c_locale_eformat (const char *message, SCM args);

/* ‘format’ with a malloc'd C string as return value. */
char *scm_utf8_sformat_to_c (const char *message, SCM args);
char *scm_locale_sformat_to_c (const char *message, SCM args);

/* ‘format’ with a garbage-collected C string as return value. */
char *scm_utf8_sformat_to_c_gc (const char *message, SCM args);
char *scm_locale_sformat_to_c_gc (const char *message, SCM args);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* _SORTSMILL_GUILE_FORMAT_H */
