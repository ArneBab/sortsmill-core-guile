/*
 * Copyright (C) 2014, 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Guile.
 * 
 * Sorts Mill Core Guile is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Guile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SORTSMILL_GUILE_CORE_UNIFORM_VECTORS_H
#define _SORTSMILL_GUILE_CORE_UNIFORM_VECTORS_H

#include <libguile.h>
#include <sortsmill/core.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

SCM scm_list_map_to_s8vector (SCM proc, SCM obj_lst);
SCM scm_list_map_to_s16vector (SCM proc, SCM obj_lst);
SCM scm_list_map_to_s32vector (SCM proc, SCM obj_lst);
SCM scm_list_map_to_s64vector (SCM proc, SCM obj_lst);
SCM scm_list_map_to_u8vector (SCM proc, SCM obj_lst);
SCM scm_list_map_to_u16vector (SCM proc, SCM obj_lst);
SCM scm_list_map_to_u32vector (SCM proc, SCM obj_lst);
SCM scm_list_map_to_u64vector (SCM proc, SCM obj_lst);
SCM scm_list_map_to_f32vector (SCM proc, SCM obj_lst);
SCM scm_list_map_to_f64vector (SCM proc, SCM obj_lst);

SCM scm_s8vector_concatenate (SCM vec_lst);
SCM scm_s16vector_concatenate (SCM vec_lst);
SCM scm_s32vector_concatenate (SCM vec_lst);
SCM scm_s64vector_concatenate (SCM vec_lst);
SCM scm_u8vector_concatenate (SCM vec_lst);
SCM scm_u16vector_concatenate (SCM vec_lst);
SCM scm_u32vector_concatenate (SCM vec_lst);
SCM scm_u64vector_concatenate (SCM vec_lst);
SCM scm_f32vector_concatenate (SCM vec_lst);
SCM scm_f64vector_concatenate (SCM vec_lst);
SCM scm_c32vector_concatenate (SCM vec_lst);
SCM scm_c64vector_concatenate (SCM vec_lst);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* _SORTSMILL_GUILE_CORE_UNIFORM_VECTORS_H */
