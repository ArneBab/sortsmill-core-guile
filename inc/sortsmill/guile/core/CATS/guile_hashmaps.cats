/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SMCOREGUILE_GUILE_HASHMAPS__CATS_
#define SMCOREGUILE_GUILE_HASHMAPS__CATS_

#include <sortsmill/guile/core/hashmaps.h>

/*#define _smcoreguile_guile_hashmaps__inline ATSinline()*/
#define _smcoreguile_guile_hashmaps__inline static inline

#define _smcoreguile_guile_hashmaps__scm_hashmap_p scm_hashmap_p
#define _smcoreguile_guile_hashmaps__scm_to_scm_t_hashmap scm_to_scm_t_hashmap
#define _smcoreguile_guile_hashmaps__scm_from_scm_t_hashmap scm_from_scm_t_hashmap

_smcoreguile_guile_hashmaps__inline atstype_bool
_smcoreguile_guile_hashmaps__scm_is_hashmap (SCM obj)
{
  return (atstype_bool) scm_is_hashmap (obj);
}

#endif /* SMCOREGUILE_GUILE_HASHMAPS__CATS_ */
