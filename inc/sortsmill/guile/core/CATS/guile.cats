/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Library.
 * 
 * Sorts Mill Core Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SMCOREGUILE_GUILE__CATS_
#define SMCOREGUILE_GUILE__CATS_

#include <libguile.h>

/*#define _smcoreguile_guile__inline ATSinline()*/
/*#define _smcoreguile_guile__inline static inline*/

#define _smcoreguile_guile__SCM_UNDEFINED__() SCM_UNDEFINED
#define _smcoreguile_guile__SCM_UNSPECIFIED__() SCM_UNSPECIFIED
#define _smcoreguile_guile__SCM_EOL__() SCM_EOL
#define _smcoreguile_guile__SCM_BOOL_T__() SCM_BOOL_T
#define _smcoreguile_guile__SCM_BOOL_F__() SCM_BOOL_F

#define _smcoreguile_guile__SCM_UNBNDP(x) ((atstype_bool) SCM_UNBNDP ((x)))
#define _smcoreguile_guile__scm_is_eq(x, y) ((atstype_bool) scm_is_eq ((x), (y)))
#define _smcoreguile_guile__scm_is_true(x) ((atstype_bool) scm_is_true ((x)))
#define _smcoreguile_guile__scm_is_false(x) ((atstype_bool) scm_is_false ((x)))

#define _smcoreguile_guile__SCM_UNPACK SCM_UNPACK
#define _smcoreguile_guile__SCM_PACK SCM_PACK
#define _smcoreguile_guile__scm_eq_p scm_eq_p
#define _smcoreguile_guile__scm_eqv_p scm_eqv_p
#define _smcoreguile_guile__scm_equal_p scm_equal_p
#define _smcoreguile_guile__scm_hash scm_hash
#define _smcoreguile_guile__scm_hashq scm_hashq
#define _smcoreguile_guile__scm_hashv scm_hashv
#define _smcoreguile_guile__scm_call_0 scm_call_0
#define _smcoreguile_guile__scm_call_1 scm_call_1
#define _smcoreguile_guile__scm_call_2 scm_call_2
#define _smcoreguile_guile__scm_call_3 scm_call_3
#define _smcoreguile_guile__scm_call_4 scm_call_4
#define _smcoreguile_guile__scm_call_5 scm_call_5
#define _smcoreguile_guile__scm_call_6 scm_call_6
#define _smcoreguile_guile__scm_call_7 scm_call_7
#define _smcoreguile_guile__scm_call_8 scm_call_8
#define _smcoreguile_guile__scm_call_9 scm_call_9

#define _smcoreguile_guile__scm_apply_0 scm_apply_0
#define _smcoreguile_guile__scm_apply_1 scm_apply_1
#define _smcoreguile_guile__scm_apply_2 scm_apply_2
#define _smcoreguile_guile__scm_apply_3 scm_apply_3
#define _smcoreguile_guile__scm_apply scm_apply

#define _smcoreguile_guile__scm_list_1 scm_list_1
#define _smcoreguile_guile__scm_list_2 scm_list_2
#define _smcoreguile_guile__scm_list_3 scm_list_3
#define _smcoreguile_guile__scm_list_4 scm_list_4
#define _smcoreguile_guile__scm_list_5 scm_list_5

#define _smcoreguile_guile__scm_list_copy scm_list_copy

#define _smcoreguile_guile__scm_from_bool scm_from_bool
#define _smcoreguile_guile__scm_to_bool(x) ((atstype_bool) scm_to_bool ((x)))

#define _smcoreguile_guile__scm_from_char scm_from_char
#define _smcoreguile_guile__scm_to_char scm_to_char
#define _smcoreguile_guile__scm_from_schar scm_from_schar
#define _smcoreguile_guile__scm_to_schar scm_to_schar
#define _smcoreguile_guile__scm_from_uchar scm_from_uchar
#define _smcoreguile_guile__scm_to_uchar scm_to_uchar

#define _smcoreguile_guile__scm_from_short scm_from_short
#define _smcoreguile_guile__scm_to_short scm_to_short
#define _smcoreguile_guile__scm_from_ushort scm_from_ushort
#define _smcoreguile_guile__scm_to_ushort scm_to_ushort

#define _smcoreguile_guile__scm_from_int scm_from_int
#define _smcoreguile_guile__scm_to_int scm_to_int
#define _smcoreguile_guile__scm_from_uint scm_from_uint
#define _smcoreguile_guile__scm_to_uint scm_to_uint

#define _smcoreguile_guile__scm_from_long scm_from_long
#define _smcoreguile_guile__scm_to_long scm_to_long
#define _smcoreguile_guile__scm_from_ulong scm_from_ulong
#define _smcoreguile_guile__scm_to_ulong scm_to_ulong

#define _smcoreguile_guile__scm_from_long_long scm_from_long_long
#define _smcoreguile_guile__scm_to_long_long scm_to_long_long
#define _smcoreguile_guile__scm_from_ulong_long scm_from_ulong_long
#define _smcoreguile_guile__scm_to_ulong_long scm_to_ulong_long

#define _smcoreguile_guile__scm_from_ssize_t scm_from_ssize_t
#define _smcoreguile_guile__scm_to_ssize_t scm_to_ssize_t
#define _smcoreguile_guile__scm_from_size_t scm_from_size_t
#define _smcoreguile_guile__scm_to_size_t scm_to_size_t

#define _smcoreguile_guile__scm_from_ptrdiff_t scm_from_ptrdiff_t
#define _smcoreguile_guile__scm_to_ptrdiff_t scm_to_ptrdiff_t

#define _smcoreguile_guile__scm_from_int8 scm_from_int8
#define _smcoreguile_guile__scm_to_int8 scm_to_int8
#define _smcoreguile_guile__scm_from_uint8 scm_from_uint8
#define _smcoreguile_guile__scm_to_uint8 scm_to_uint8

#define _smcoreguile_guile__scm_from_int16 scm_from_int16
#define _smcoreguile_guile__scm_to_int16 scm_to_int16
#define _smcoreguile_guile__scm_from_uint16 scm_from_uint16
#define _smcoreguile_guile__scm_to_uint16 scm_to_uint16

#define _smcoreguile_guile__scm_from_int32 scm_from_int32
#define _smcoreguile_guile__scm_to_int32 scm_to_int32
#define _smcoreguile_guile__scm_from_uint32 scm_from_uint32
#define _smcoreguile_guile__scm_to_uint32 scm_to_uint32

#define _smcoreguile_guile__scm_from_int64 scm_from_int64
#define _smcoreguile_guile__scm_to_int64 scm_to_int64
#define _smcoreguile_guile__scm_from_uint64 scm_from_uint64
#define _smcoreguile_guile__scm_to_uint64 scm_to_uint64

#define _smcoreguile_guile__scm_error scm_error
#define _smcoreguile_guile__scm_syserror scm_syserror
#define _smcoreguile_guile__scm_syserror_msg scm_syserror_msg
#define _smcoreguile_guile__scm_num_overflow scm_num_overflow
#define _smcoreguile_guile__scm_out_of_range scm_out_of_range
#define _smcoreguile_guile__scm_wrong_num_args scm_wrong_num_args
#define _smcoreguile_guile__scm_wrong_type_arg scm_wrong_type_arg
#define _smcoreguile_guile__scm_wrong_type_arg_msg scm_wrong_type_arg_msg
#define _smcoreguile_guile__scm_memory_error scm_memory_error
#define _smcoreguile_guile__scm_misc_error scm_misc_error

#endif /* SMCOREGUILE_GUILE__CATS_ */
