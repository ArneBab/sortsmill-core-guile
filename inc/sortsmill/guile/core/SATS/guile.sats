// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

//--------------------------------------------------------------------

#define ATS_PACKNAME "SMCOREGUILE.guile"
#define ATS_EXTERN_PREFIX "_smcoreguile_guile__"

%{#
#include "sortsmill/guile/core/CATS/guile.cats"
%}

staload "libc/SATS/stddef.sats"

typedef SCM = $extype"SCM"
typedef scm_t_bits = $extype"scm_t_bits"
typedef scm_t_signed_bits = $extype"scm_t_signed_bits"

(* Define constants this way instead of by `val',
   to avoid having to dynload the module.
   Use the #defines instead of the functions. *)
fn SCM_UNDEFINED__ : () -<> SCM = "mac#%"
fn SCM_UNSPECIFIED__ : () -<> SCM = "mac#%"
fn SCM_EOL__ : () -<> SCM = "mac#%"
fn SCM_BOOL_T__ : () -<> SCM = "mac#%"
fn SCM_BOOL_F__ : () -<> SCM = "mac#%"
#define SCM_UNDEFINED (SCM_UNDEFINED__ ())
#define SCM_UNSPECIFIED (SCM_UNSPECIFIED__ ())
#define SCM_EOL (SCM_EOL__ ())
#define SCM_BOOL_T (SCM_BOOL_T__ ())
#define SCM_BOOL_F (SCM_BOOL_F__ ())

fn SCM_UNBNDP : SCM -<> bool = "mac#%"

fn SCM_UNPACK : SCM -<> scm_t_bits = "mac#%"
fn SCM_PACK : scm_t_bits -<> SCM = "mac#%"
fn scm_is_eq : (SCM, SCM) -<> bool = "mac#%"
fn scm_is_true : SCM -> bool = "mac#%"
fn scm_is_false : SCM -> bool = "mac#%"

fn scm_eq_p : (SCM, SCM) -> SCM = "mac#%"
fn scm_eqv_p : (SCM, SCM) -> SCM = "mac#%"
fn scm_equal_p : (SCM, SCM) -> SCM = "mac#%"

fn scm_hash : (SCM, SCM) -> SCM = "mac#%"
fn scm_hashq : (SCM, SCM) -> SCM = "mac#%"
fn scm_hashv : (SCM, SCM) -> SCM = "mac#%"

fn scm_call_0 : (SCM) -> SCM = "mac#%"
fn scm_call_1 : (SCM, SCM) -> SCM = "mac#%"
fn scm_call_2 : (SCM, SCM, SCM) -> SCM = "mac#%"
fn scm_call_3 : (SCM, SCM, SCM, SCM) -> SCM = "mac#%"
fn scm_call_4 : (SCM, SCM, SCM, SCM, SCM) -> SCM = "mac#%"
fn scm_call_5 : (SCM, SCM, SCM, SCM, SCM, SCM) -> SCM = "mac#%"
fn scm_call_6 : (SCM, SCM, SCM, SCM, SCM, SCM, SCM) -> SCM = "mac#%"
fn scm_call_7 : (SCM, SCM, SCM, SCM, SCM, SCM, SCM, SCM) -> SCM = "mac#%"
fn scm_call_8 : (SCM, SCM, SCM, SCM, SCM, SCM, SCM, SCM, SCM) -> SCM = "mac#%"
fn scm_call_9 : (SCM, SCM, SCM, SCM, SCM, SCM, SCM, SCM, SCM, SCM) -> SCM = "mac#%"

fn scm_apply_0 : (SCM, SCM) -> SCM = "mac#%"
fn scm_apply_1 : (SCM, SCM, SCM) -> SCM = "mac#%"
fn scm_apply_2 : (SCM, SCM, SCM, SCM) -> SCM = "mac#%"
fn scm_apply_3 : (SCM, SCM, SCM, SCM, SCM) -> SCM = "mac#%"
fn scm_apply : (SCM, SCM, SCM) -> SCM = "mac#%"

fn scm_list_1 : (SCM) -> SCM = "mac#%"
fn scm_list_2 : (SCM, SCM) -> SCM = "mac#%"
fn scm_list_3 : (SCM, SCM, SCM) -> SCM = "mac#%"
fn scm_list_4 : (SCM, SCM, SCM, SCM) -> SCM = "mac#%"
fn scm_list_5 : (SCM, SCM, SCM, SCM, SCM) -> SCM = "mac#%"

fn scm_list_copy : SCM -> SCM = "mac#%"

fn scm_from_bool : bool -> SCM = "mac#%"
fn scm_to_bool : SCM -> bool = "mac#%"

fn scm_from_char : char -> SCM = "mac#%"
fn scm_to_char : SCM -> char = "mac#%"
fn scm_from_schar : schar -> SCM = "mac#%"
fn scm_to_schar : SCM -> schar = "mac#%"
fn scm_from_uchar : uchar -> SCM = "mac#%"
fn scm_to_uchar : SCM -> uchar = "mac#%"

fn scm_from_short : sint -> SCM = "mac#%"
fn scm_to_short : SCM -> sint = "mac#%"
fn scm_from_ushort : usint -> SCM = "mac#%"
fn scm_to_ushort : SCM -> usint = "mac#%"

fn scm_from_int : int -> SCM = "mac#%"
fn scm_to_int : SCM -> int = "mac#%"
fn scm_from_uint : uint -> SCM = "mac#%"
fn scm_to_uint : SCM -> uint = "mac#%"

fn scm_from_long : lint -> SCM = "mac#%"
fn scm_to_long : SCM -> lint = "mac#%"
fn scm_from_ulong : ulint -> SCM = "mac#%"
fn scm_to_ulong : SCM -> ulint = "mac#%"

fn scm_from_long_long : llint -> SCM = "mac#%"
fn scm_to_long_long : SCM -> llint = "mac#%"
fn scm_from_ulong_long : ullint -> SCM = "mac#%"
fn scm_to_ulong_long : SCM -> ullint = "mac#%"

fn scm_from_ssize_t : ssize_t -> SCM = "mac#%"
fn scm_to_ssize_t : SCM -> ssize_t = "mac#%"
fn scm_from_size_t : size_t -> SCM = "mac#%"
fn scm_to_size_t : SCM -> size_t = "mac#%"

fn scm_from_ptrdiff_t : ptrdiff_t -> SCM = "mac#%"
fn scm_to_ptrdiff_t : SCM -> ptrdiff_t = "mac#%"

fn scm_from_int8 : int8 -> SCM = "mac#%"
fn scm_to_int8 : SCM -> int8 = "mac#%"
fn scm_from_uint8 : uint8 -> SCM = "mac#%"
fn scm_to_uint8 : SCM -> uint8 = "mac#%"

fn scm_from_int16 : int16 -> SCM = "mac#%"
fn scm_to_int16 : SCM -> int16 = "mac#%"
fn scm_from_uint16 : uint16 -> SCM = "mac#%"
fn scm_to_uint16 : SCM -> uint16 = "mac#%"

fn scm_from_int32 : int32 -> SCM = "mac#%"
fn scm_to_int32 : SCM -> int32 = "mac#%"
fn scm_from_uint32 : uint32 -> SCM = "mac#%"
fn scm_to_uint32 : SCM -> uint32 = "mac#%"

fn scm_from_int64 : int64 -> SCM = "mac#%"
fn scm_to_int64 : SCM -> int64 = "mac#%"
fn scm_from_uint64 : uint64 -> SCM = "mac#%"
fn scm_to_uint64 : SCM -> uint64 = "mac#%"

fn scm_error : (SCM, string, string, SCM, SCM) -> SCM = "mac#%"
fn scm_syserror : string -> void = "mac#%"
fn scm_syserror_msg : (string, string, SCM) -> void = "mac#%"
fn scm_num_overflow : string -> void = "mac#%"
fn scm_out_of_range : (string, SCM) -> void = "mac#%"
fn scm_wrong_num_args : SCM -> void = "mac#%"
fn scm_wrong_type_arg : (string, int, SCM) -> void = "mac#%"
fn scm_wrong_type_arg_msg : (string, int, SCM, string) -> void = "mac#%"
fn scm_memory_error : string -> void = "mac#%"
fn scm_misc_error : (string, string, SCM) -> void = "mac#%"

//--------------------------------------------------------------------
