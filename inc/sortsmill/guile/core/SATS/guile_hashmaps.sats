// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

//--------------------------------------------------------------------
//
// Immutable hash maps for Guile.
//

#define ATS_PACKNAME "SMCOREGUILE.guile_hashmaps"
#define ATS_EXTERN_PREFIX "_smcoreguile_guile_hashmaps__"

%{#
#include "sortsmill/guile/core/CATS/guile_hashmaps.cats"
%}

staload "sortsmill/guile/core/SATS/guile.sats"

//--------------------------------------------------------------------

typedef scm_t_hashmap = $extype"scm_t_hashmap"

fn scm_is_hashmap : SCM -> bool = "mac#%"
fn scm_hashmap_p : SCM -> SCM = "mac#%"
fn scm_to_scm_t_hashmap : SCM -> scm_t_hashmap = "mac#%"
fn scm_from_scm_t_hashmap : scm_t_hashmap -> SCM = "mac#%"

// The first optional argument is a hash procedure.
// The second is an equality or equivalence procedure
// that takes the already-stored key as the second argument.
fn scm_c_make_hashmap0 : () -> scm_t_hashmap
fn scm_c_make_hashmap1 : (SCM) -> scm_t_hashmap
fn scm_c_make_hashmap2 : (SCM, SCM) -> scm_t_hashmap
overload scm_c_make_hashmap with scm_c_make_hashmap0
overload scm_c_make_hashmap with scm_c_make_hashmap1
overload scm_c_make_hashmap with scm_c_make_hashmap2

// The first optional argument is a hash procedure.
// The second is an equality or equivalence procedure
// that takes the already-stored key as the second argument.
fn scm_make_hashmap0 : () -> SCM
fn scm_make_hashmap1 : (SCM) -> SCM
fn scm_make_hashmap2 : (SCM, SCM) -> SCM
overload scm_make_hashmap with scm_make_hashmap0
overload scm_make_hashmap with scm_make_hashmap1
overload scm_make_hashmap with scm_make_hashmap2

fn scm_hashmap_is_null : scm_t_hashmap -> bool
fn scm_hashmap_null_p : SCM -> SCM

fn scm_c_hashmap_size : scm_t_hashmap -> size_t
fn scm_hashmap_size : SCM -> SCM

fn scm_c_hashmap_contains : (scm_t_hashmap, SCM) -> bool
fn scm_hashmap_contains_p : (SCM, SCM) -> SCM

fn scm_c_hashmap_ref : (scm_t_hashmap, SCM, SCM) -> SCM
fn scm_hashmap_ref : (SCM, SCM, SCM) -> SCM

fn scm_c_hashmap_set : (scm_t_hashmap, SCM, SCM) -> scm_t_hashmap
fn scm_hashmap_set : (SCM, SCM, SCM) -> SCM

fn scm_c_hashmap_remove : (scm_t_hashmap, SCM) -> scm_t_hashmap
fn scm_hashmap_remove : (SCM, SCM) -> SCM

//--------------------------------------------------------------------
