// -*- coding: utf-8 -*-

// Copyright (C) 2013 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile
// 
// Sorts Mill Core Guile is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with Sorts Mill Core Guile.  If not, see
// <http://www.gnu.org/licenses/>.

#ifndef _SMCOREGUILE_MPZ_PYLONG_H
#define _SMCOREGUILE_MPZ_PYLONG_H 1

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

#include <Python.h>
#include <gmp.h>

#if PY_MAJOR_VERSION <= 2
#define Py2or3String_FromString     PyString_FromString
#define Py2or3String_FromFormat     PyString_FromFormat
#define Py2or3String_Check          PyString_Check
#define Py2or3String_CheckExact     PyString_CheckExact
#define Py2or3String_Format         PyString_Format
#define Py2or3String_AsString       PyString_AsString
#define PyStrOrUnicode_Check(op)    (PyString_Check(op) || PyUnicode_Check(op))
#define PyStrOrUnicode_CheckExact(op) (PyString_CheckExact(op) || PyUnicode_CheckExact(op))
#define PyIntOrLong_FromLong        PyInt_FromLong
#define PyIntOrLong_Check(op)       (PyInt_Check(op) || PyLong_Check(op))
#define PyIntOrLong_CheckExact(op)  (PyInt_CheckExact(op) || PyLong_CheckExact(op))
#define PyIntOrLong_FromSize_t      PyInt_FromSize_t
#define PyIntOrLong_FromSsize_t     PyInt_FromSsize_t
#define PyIntOrLong_AsSsize_t       PyInt_AsSsize_t
#define PyIntOrLong_AsLong          PyInt_AsLong
#else
#define Py2or3String_FromString     PyUnicode_FromString
#define Py2or3String_FromFormat     PyUnicode_FromFormat
#define Py2or3String_Check          PyUnicode_Check
#define Py2or3String_CheckExact     PyUnicode_CheckExact
#define Py2or3String_Format         PyUnicode_Format
#define Py2or3String_AsString       PyUnicode_AS_DATA
#define PyStrOrUnicode_Check(op)    (PyBytes_Check(op) || PyUnicode_Check(op))
#define PyStrOrUnicode_CheckExact(op) (PyBytes_CheckExact(op) || PyUnicode_CheckExact(op))
#define PyIntOrLong_FromLong        PyLong_FromLong
#define PyIntOrLong_Check(op)       PyLong_Check(op)
#define PyIntOrLong_CheckExact(op)  PyLong_CheckExact(op)
#define PyIntOrLong_FromSize_t      PyLong_FromSize_t
#define PyIntOrLong_FromSsize_t     PyLong_FromSsize_t
#define PyIntOrLong_AsSsize_t       PyLong_AsSsize_t
#define PyIntOrLong_AsLong          PyLong_AsLong
#endif

// Avoid polluting the namespace too badly: try not to duplicate
// symbols exported by gmpy or gmpy2.
#define mpz_get_PyLong _smcoreguile_mpz_get_PyLong
#define mpz_set_PyIntOrLong _smcoreguile_mpz_set_PyIntOrLong

PyObject *mpz_get_PyLong (mpz_srcptr z);
int mpz_set_PyIntOrLong (mpz_ptr z, PyObject *lsrc);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* _SMCOREGUILE_MPZ_PYLONG_H */
