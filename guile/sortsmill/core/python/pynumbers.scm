;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core python pynumbers)

  (export
   pynumber?           ;; (pynumber? obj) → boolean
   pyindex?            ;; (pyindex? obj) → boolean
   py+                 ;; (py+ obj1 obj2 ...) → obj
   py-                 ;; (py- obj1 obj2 ...) → obj
   py*                 ;; (py* obj1 obj2 ...) → obj
   py-floor/           ;; (py-floor/ obj1 obj2) → obj
   py/                 ;; (py/ obj1 obj2 ...) → obj
   py%                 ;; (py% obj1 obj2) → obj
   py-divmod           ;; (py-divmod obj1 obj2) → obj
   py-power            ;; (py-power obj1 obj2 [obj3]) → obj
   py-positive         ;; (py-positive obj) → obj
   py-abs              ;; (py-abs obj) → obj
   py-bitwise-not      ;; (py-bitwise-not obj) → obj
   py-lshift           ;; (py-lshift obj1 obj2) → obj
   py-rshift           ;; (py-rshift obj1 obj2) → obj
   py-bitwise-and      ;; (py-bitwise-and obj1 obj2 ...) → obj
   py-bitwise-xor      ;; (py-bitwise-xor obj1 obj2 ...) → obj
   py-bitwise-ior      ;; (py-bitwise-ior obj1 obj2 ...) → obj
   py+!                ;; (py+! obj1 obj2 ...) → obj
   py-!                ;; (py-! obj1 obj2 ...) → obj
   py*!                ;; (py*! obj1 obj2 ...) → obj
   py-floor/!          ;; (py-floor/! obj1 obj2) → obj
   py/!                ;; (py/! obj1 obj2 ...) → obj
   py%!                ;; (py%! obj1 obj2) → obj
   py-power!           ;; (py-power! obj1 obj2 [obj3]) → obj
   py-lshift!          ;; (py-lshift! obj1 obj2) → obj
   py-rshift!          ;; (py-rshift! obj1 obj2) → obj
   py-bitwise-and!     ;; (py-bitwise-and! obj1 obj2 ...) → obj
   py-bitwise-xor!     ;; (py-bitwise-xor! obj1 obj2 ...) → obj
   py-bitwise-ior!     ;; (py-bitwise-ior! obj1 obj2 ...) → obj
   pynumber->pyinteger ;; (pynumber->pyinteger obj) → obj
   pynumber->pyfloat   ;; (pynumber->pyfloat obj) → obj
   pynumber->pyindex   ;; (pynumber->pyindex obj) → obj
   pynumber-tobase     ;; (pynumber-tobase obj) → obj
   )

  (import (rnrs)
          (except (guile) error)
          (system foreign)
          (sortsmill core helpers)
          (sortsmill core scm-procedures)
          )

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile-python)
                          scm_pynumber_p
                          scm_pyindex_p
                          scm_pynumber_add_list
                          scm_pynumber_minus_list
                          scm_pynumber_multiply_list
                          scm_pynumber_floordivide
                          scm_pynumber_truedivide_list
                          scm_pynumber_remainder
                          scm_pynumber_divmod
                          scm_pynumber_power
                          scm_pynumber_positive
                          scm_pynumber_absolute
                          scm_pynumber_invert
                          scm_pynumber_lshift
                          scm_pynumber_rshift
                          scm_pynumber_and_list
                          scm_pynumber_xor_list
                          scm_pynumber_or_list
                          scm_pynumber_inplaceadd_list
                          scm_pynumber_inplacesubtract_list
                          scm_pynumber_inplacemultiply_list
                          scm_pynumber_inplacefloordivide
                          scm_pynumber_inplacetruedivide_list
                          scm_pynumber_inplaceremainder
                          scm_pynumber_inplacepower
                          scm_pynumber_inplacelshift
                          scm_pynumber_inplacershift
                          scm_pynumber_inplaceand_list
                          scm_pynumber_inplacexor_list
                          scm_pynumber_inplaceor_list
                          scm_pynumber_integer
                          scm_pynumber_float
                          scm_pynumber_index
                          scm_pynumber_tobase
                          ))

  (define-scm-procedure (pynumber? obj)
    "Is the argument a Guile-wrapped Python object supporting Python's
number protocol?"
    scm_pynumber_p)

  (define-scm-procedure (pyindex? obj)
    "A wrapper around @code{PyIndex_Check}."
    scm_pyindex_p)

  (define-scm-procedure (py+ obj1 . obj_lst)
    "A wrapper around @code{PyNumber_Add}. This procedure accepts any
positive number of arguments, but (unlike @code{+}) it cannot be
called without arguments. (We would not know what type of object to
return, because @code{py+} can deal with numbers, strings, sets, and
so forth.)"
    scm_pynumber_add_list)

  (define-scm-procedure (py- obj1 . obj_lst)
    "A wrapper around @code{PyNumber_Negative} and
@code{PyNumber_Subtract}. This procedure accepts any positive number
of arguments. When called with one argument, it calls
@code{PyNumber_Negative}; otherwise it calls
@code{PyNumber_Subtract}."
    scm_pynumber_minus_list)

  (define-scm-procedure (py* obj1 . obj_lst)
    "A wrapper around @code{PyNumber_Multiply}. This procedure accepts
any positive number of arguments, but (unlike @code{*}) it cannot be
called without arguments. (We would not know what type of object to
return; for instance, @code{py*} can mean a string repeat operation.)"
    scm_pynumber_multiply_list)

  (define-scm-procedure (py-floor/ obj1 obj2)
    "A wrapper around @code{PyNumber_FloorDivide}."
    scm_pynumber_floordivide)

  (define-scm-procedure (py/ obj1 obj2 . obj_lst)
    "A wrapper around @code{PyNumber_TrueDivide}. This procedure
accepts any number of arguments greater than or equal to two,
but (unlike @code{/}) it cannot be called with only one
argument. (Side note: if the Python number protocol had a `reciprocal'
function, we could have used that.)"
    scm_pynumber_truedivide_list)

  (define-scm-procedure (py% obj1 obj2)
    "A wrapper around @code{PyNumber_Remainder}."
    scm_pynumber_remainder)

  (define-scm-procedure (py-divmod obj1 obj2)
    "A wrapper around @code{PyNumber_Divmod}."
    scm_pynumber_divmod)

  (define*-scm-procedure (py-power obj1 obj2 #:optional obj3)
    "A wrapper around @code{PyNumber_Power}. The third argument is
optional; leaving it out is equivalent to passing the Python
@code{None} object."
    scm_pynumber_power)

  (define-scm-procedure (py-positive obj)
    "A wrapper around @code{PyNumber_Positive}."
    scm_pynumber_positive)

  (define-scm-procedure (py-abs obj)
    "A wrapper around @code{PyNumber_Absolute}."
    scm_pynumber_absolute)

  (define-scm-procedure (py-bitwise-not obj)
    "A wrapper around @code{PyNumber_Invert}."
    scm_pynumber_invert)

  (define-scm-procedure (py-lshift obj1 obj2)
    "A wrapper around @code{PyNumber_Lshift}."
    scm_pynumber_lshift)

  (define-scm-procedure (py-rshift obj1 obj2)
    "A wrapper around @code{PyNumber_Rshift}."
    scm_pynumber_rshift)

  (define-scm-procedure (py-bitwise-and obj1 . obj_lst)
    "A wrapper around @code{PyNumber_And}. This procedure accepts any
positive number of arguments."
    scm_pynumber_and_list)

  (define-scm-procedure (py-bitwise-xor obj1 . obj_lst)
    "A wrapper around @code{PyNumber_Xor}. This procedure accepts any
positive number of arguments."
    scm_pynumber_xor_list)

  (define-scm-procedure (py-bitwise-ior obj1 . obj_lst)
    "A wrapper around @code{PyNumber_Ior}. This procedure accepts any
positive number of arguments."
    scm_pynumber_or_list)

  (define-scm-procedure (py+! obj1 obj2 . obj_lst)
    "A wrapper around @code{PyNumber_InPlaceAdd}. This procedure
accepts any positive number of arguments greater than or equal to
two."
    scm_pynumber_inplaceadd_list)

  (define-scm-procedure (py-! obj1 obj2 . obj_lst)
    "A wrapper around @code{PyNumber_InPlaceSubtract}. This procedure
accepts any positive number of arguments greater than or equal to
two."
    scm_pynumber_inplacesubtract_list)

  (define-scm-procedure (py*! obj1 obj2 . obj_lst)
    "A wrapper around @code{PyNumber_InPlaceMultiply}. This procedure
accepts any positive number of arguments greater than or equal to
two."
    scm_pynumber_inplacemultiply_list)

  (define-scm-procedure (py-floor/! obj1 obj2)
    "A wrapper around @code{PyNumber_InPlaceFloorDivide}."
    scm_pynumber_inplacefloordivide)

  (define-scm-procedure (py/! obj1 obj2 . obj_lst)
    "A wrapper around @code{PyNumber_InPlaceTrueDivide}. This
procedure accepts any positive number of arguments greater than or
equal to two."
    scm_pynumber_inplacetruedivide_list)

  (define-scm-procedure (py%! obj1 obj2)
    "A wrapper around @code{PyNumber_InPlaceRemainder}."
    scm_pynumber_inplaceremainder)

  (define*-scm-procedure (py-power! obj1 obj2 #:optional obj3)
    "A wrapper around @code{PyNumber_InPlacePower}. The third argument
is optional; leaving it out is equivalent to passing the Python
@code{None} object."
    scm_pynumber_inplacepower)

  (define-scm-procedure (py-lshift! obj1 obj2)
    "A wrapper around @code{PyNumber_InPlaceLshift}."
    scm_pynumber_inplacelshift)

  (define-scm-procedure (py-rshift! obj1 obj2)
    "A wrapper around @code{PyNumber_InPlaceRshift}."
    scm_pynumber_inplacershift)

  (define-scm-procedure (py-bitwise-and! obj1 obj2 . obj_lst)
    "A wrapper around @code{PyNumber_InPlaceAnd}. This procedure
accepts any positive number of arguments greater than or equal to
two."
    scm_pynumber_inplaceand_list)

  (define-scm-procedure (py-bitwise-xor! obj1 obj2 . obj_lst)
    "A wrapper around @code{PyNumber_InPlaceXor}. This procedure
accepts any positive number of arguments greater than or equal to
two."
    scm_pynumber_inplacexor_list)

  (define-scm-procedure (py-bitwise-ior! obj1 obj2 . obj_lst)
    "A wrapper around @code{PyNumber_InPlaceOr}. This procedure
accepts any positive number of arguments greater than or equal to
two."
    scm_pynumber_inplaceor_list)

  (define-scm-procedure (pynumber->pyinteger obj)
    "Currently this is a wrapper around @code{PyNumber_Long}, but it
is not precluded from also calling @code{PyNumber_Int} in the future."
    scm_pynumber_integer)

  (define-scm-procedure (pynumber->pyfloat obj)
    "A wrapper around @code{PyNumber_Float}."
    scm_pynumber_float)

  (define-scm-procedure (pynumber->pyindex obj)
    "A wrapper around @code{PyNumber_Index}."
    scm_pynumber_index)

  (define-scm-procedure (pynumber-tobase obj base)
    "A wrapper around @code{PyNumber_ToBase}. The @var{base} argument
may be either a Python or a Guile integer."
    scm_pynumber_tobase)

  ) ;; end of library.
