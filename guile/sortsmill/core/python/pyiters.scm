;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2014 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core python pyiters)

  (export
   pyiter?        ;; (pyiter? obj) → boolean
   pyiter-next!   ;; (pyiter-next! obj) → next-obj-or-#f
   stream->pyiter ;; (stream->pyiter stream) → pyiter
   thunk->pyiter  ;; (thunk->pyiter thunk) → pyiter
   )

  (import (rnrs)
          (except (guile) error)
          (system foreign)
          (sortsmill core python version)
          (sortsmill core kwargs)
          (sortsmill core helpers)
          (sortsmill core scm-procedures))

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile-python)
                          scm_pyiter_p
                          scm_pyiter_next_x
                          scm_stream_to_pyiter
                          scm_thunk_to_pyiter))

  (define-scm-procedure (pyiter? obj)
    "Is the argument a Guile-wrapped Python iterator?"
    scm_pyiter_p)

  (define-scm-procedure (pyiter-next! obj)
    "A wrapper around @code{PyIter_Next}. Returns @code{#f} when the
iteration is finished."
    scm_pyiter_next_x)

  (define-scm-procedure (stream->pyiter stream)
    "Convert a SRFI-41 stream of pyobjects to a Python iterator."
    scm_stream_to_pyiter)

  (define-scm-procedure (thunk->pyiter thunk)
    "Convert a thunk to a Python iterator. The thunk should return a
pyobject except at the end of iteration, whereupon it should return
@code{#f}."
    scm_thunk_to_pyiter)

  ) ;; end of library.
