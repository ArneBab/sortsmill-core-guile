;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core python version)

  (export
   python-version-and-abi ;; Reëxported from (sortsmill core helpers).
   PY_VERSION_HEX
   PY_MAJOR_VERSION
   PY_MINOR_VERSION
   PY_MICRO_VERSION
   PY_RELEASE_LEVEL
   PY_RELEASE_SERIAL
   )

  (import (rnrs)
          (except (guile) error)
          (system foreign)
          (sortsmill core helpers))

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile-python)
                          init_guile_pyversion)
    ((pointer->procedure void init_guile_pyversion '())))

  ) ;; end of library.
