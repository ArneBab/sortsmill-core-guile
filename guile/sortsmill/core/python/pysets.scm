;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013, 2014 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core python pysets)

  (export
   pyset?                   ;; (pyset? obj) → boolean
   pyfrozenset?             ;; (pyfrozenset? obj) → boolean
   pyfrozenset-exact?       ;; (pyfrozenset-exact? obj) → boolean
   pyanyset?                ;; (pyanyset? obj) → boolean
   pyanyset-exact?          ;; (pyanyset-exact? obj) → boolean
   pyset-new                ;; (pyset-new [pyiterable]) → obj
   pyfrozenset-new          ;; (pyfrozenset-new [pyiterable]) → obj
   list->pyset              ;; (list->pyset list) → obj
   list->pyfrozenset        ;; (list->pyfrozenset list) → obj
   pyset-size               ;; (pyset-size obj) → integer
   pyset-contains?          ;; (pyset-contains? obj key) → boolean
   pyset-add!               ;; (pyset-add! obj key) → *unspecified*
   pyset-discard!           ;; (pyset-discard! obj key) → *unspecified*
   pyset-pop!               ;; (pyset-pop! obj) → obj
   pyset-clear!             ;; (pyset-clear! obj) → *unspecified*
   )

  (import (rnrs)
          (except (guile) error)
          (system foreign)
          (sortsmill core helpers)
          (sortsmill core scm-procedures))

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile-python)
                          scm_pyset_p
                          scm_pyfrozenset_p
                          scm_pyfrozenset_exact_p
                          scm_pyanyset_p
                          scm_pyanyset_exact_p
                          scm_pyset_new
                          scm_pyfrozenset_new
                          scm_list_to_pyset
                          scm_list_to_pyfrozenset
                          scm_pyset_size
                          scm_pyset_contains_p
                          scm_pyset_add_x
                          scm_pyset_discard_x
                          scm_pyset_pop_x
                          scm_pyset_clear_x
                          ))

  (define-scm-procedure (pyset? obj)
    "Is the argument a Guile-wrapped Python set?"
    scm_pyset_p)

  (define-scm-procedure (pyfrozenset? obj)
    "Is the argument a @code{pyfrozenset}?"
    scm_pyfrozenset_p)

  (define-scm-procedure (pyfrozenset-exact? obj)
    "Is the argument a @code{pyfrozenset} but not a member of a subtype of
@code{pyfrozenset}?"
    scm_pyfrozenset_exact_p)

  (define-scm-procedure (pyanyset? obj)
    "Is the argument a @code{pyanyset}?"
    scm_pyanyset_p)

  (define-scm-procedure (pyanyset-exact? obj)
    "Is the argument a @code{pyanyset} but not a member of a subtype of
@code{pyanyset}?"
    scm_pyanyset_exact_p)

  (define*-scm-procedure (pyset-new #:optional pyiterable)
    "A wrapper around @code{PySet_New}. If the argument is left out,
the procedure returns an empty set."
    scm_pyset_new)

  (define*-scm-procedure (pyfrozenset-new #:optional pyiterable)
    "A wrapper around @code{PyFrozenSet_New}. If the argument is left
out, the procedure returns an empty set."
    scm_pyfrozenset_new)

  (define-scm-procedure (list->pyset lst)
    "Convert a list of Guile-wrapped Python objects to a Guile-wrapped
Python set."
    scm_list_to_pyset)

  (define-scm-procedure (list->pyfrozenset lst)
    "Convert a list of Guile-wrapped Python objects to a Guile-wrapped
Python frozenset."
    scm_list_to_pyfrozenset)

  (define-scm-procedure (pyset-size obj)
    "A wrapper around @code{PySet_Size}."
    scm_pyset_size)

  (define-scm-procedure (pyset-contains? obj key)
    "A wrapper around @code{PySet_Contains}."
    scm_pyset_contains_p)

  (define-scm-procedure (pyset-add! obj key)
    "A wrapper around @code{PySet_Add}."
    scm_pyset_add_x)

  (define-scm-procedure (pyset-discard! obj key)
    "A wrapper around @code{PySet_Discard}."
    scm_pyset_discard_x)

  (define-scm-procedure (pyset-pop! obj)
    "A wrapper around @code{PySet_Pop}."
    scm_pyset_pop_x)

  (define-scm-procedure (pyset-clear! obj)
    "A wrapper around @code{PySet_Clear}."
    scm_pyset_clear_x)

  ) ;; end of library.
