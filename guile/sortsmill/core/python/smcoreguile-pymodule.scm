;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core python smcoreguile-pymodule)

  (export)

  (import (rnrs)
          (except (guile) error)
          (ice-9 eval-string)
          (ice-9 format)
          (sortsmill core kwargs)
          (sortsmill core helpers)
          (sortsmill core scm-procedures)
          (sortsmill core python shorthand)
          (sortsmill core python pyobjects)
          (sortsmill core python pysequences)
          (sortsmill core python pymodules)
          (sortsmill core python reflection)
          (sortsmill core python pyscms)
          (sortsmill core python pyexcs))

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-ltdlopened
                           (string-append "libguile-" (effective-version)))
                          scm_define))

  ;;----------------------------------------------------------------------

  (define-scm-procedure (scm-define symb var) scm_define)

  (define (string->module-name s)
    (map string->symbol (remove "" (string-split s #\space))))

  (define* (python->guile-module m #:key [interface? #f])
    "Convert a Python object to a Guile module or its public
interface. Currently an already resolved module (or interface), a
string, tuple of strings, or list of strings is accepted."
    (cond [(pyscm? m) (pyscm->scm m)]
          [(or (pytuple? m) (pylist? m))
           ((if interface? resolve-interface resolve-module)
            (map (compose string->symbol pyunicode->string)
                 (pysequence->list m)))]
          [else ((if interface? resolve-interface resolve-module)
                 (string->module-name (pyunicode->string m)))]))

  (define (python->symbol s)
    (string->symbol (pyunicode->string s)))

  ;;----------------------------------------------------------------------

  (define py-sortsmill_core_guile-hooks
    (make-parameter
     '() (lambda (v)
           (assert (and (list? v) (for-all procedure? v)))
           v)))

  (define (cons-hook! hook-proc)
    (py-sortsmill_core_guile-hooks
     (cons hook-proc (py-sortsmill_core_guile-hooks))))

  (define (add-function-to-module! key proc)
    (cons-hook!
     (lambda (m) (pymodule-addobject! m key (scm->pyscm proc)))))

  (add-function-to-module!
   "resolve_module"
   (lambda/kwargs (module)
     "Return a Guile module, given its name or the module itself."
     (assert module)
     (scm->pyscm (python->guile-module module))))

  (add-function-to-module!
   "current_module"
   (lambda/kwargs ()
     "Return the current Guile module."
     (scm->pyscm (current-module))))

  (add-function-to-module!
   "set_current_module"
   (lambda/kwargs (module)
     "Set the current Guile module, and return it."
     (set-current-module (pyscm->scm module))
     module))

  (add-function-to-module!
   "public_variable"
   (lambda/kwargs (module name)
     "Return the variable that is bound to @var{name} in the
public (exported) interface of the given @var{module}. The module may
be given directly or specified by name. If the variable is not found,
return @code{None}."
     (assert (and module name))
     (let ([var (module-variable (python->guile-module module #:interface? #t)
                                 (python->symbol name))])
       (if var (scm->pyscm var) (py None)))))

  (add-function-to-module!
   "private_variable"
   (lambda/kwargs (module name)
     "Return the variable that is bound to @var{name} in the given
@var{module}. The module may be given directly or specified by
name. The variable need not be exported. If the variable is not found,
return @code{None}."
     (assert (and module name))
     (let ([var (module-variable (python->guile-module module)
                                 (python->symbol name))])
       (if var (scm->pyscm var) (py None)))))

  (add-function-to-module!
   "public_lookup"
   (lambda/kwargs (module name)
     "Return the variable that is bound to @var{name} in the given
@var{module}. The module may be given directly or specified by
name. The variable need not be exported. If the variable is not found,
raise a Python exception."
     (assert (and module name))
     (let ([var (module-variable (python->guile-module module #:interface? #t)
                                 (python->symbol name))])
       (if var
           (scm->pyscm var)
           (let ([message (format #f "Guile variable ~s not found in module ~s"
                                  name module)])
             (pyerr-set! pyexc-runtimeerror message)
             (error "public_lookup" message))))))

  (add-function-to-module!
   "private_lookup"
   (lambda/kwargs (module name)
     "Return the variable that is bound to @var{name} in the given
@var{module}. The module may be given directly or specified by
name. The variable need not be exported. If the variable is not found,
raise a Python exception."
     (assert (and module name))
     (let ([var (module-variable (python->guile-module module)
                                 (python->symbol name))])
       (if var
           (scm->pyscm var)
           (let ([message (format #f "Guile variable ~s not found in module ~s"
                                  name module)])
             (pyerr-set! pyexc-runtimeerror message)
             (error "private_lookup" message))))))

  (add-function-to-module!
   "variable_ref"
   (lambda/kwargs (variable convert)
     "Return the value that is bound to a given variable. Optionally
convert the value to a Python object by calling @code{scm->pyscm}."
     (assert variable)
     (let ([var (pyscm->scm variable)])
       (if (and convert (pyobject-true? convert))
           (scm->pyscm (variable-ref var))
           (variable-ref var)))))

  (add-function-to-module!
   "variable_set"
   (lambda/kwargs (variable value convert)
     "Set the value that is bound to a given variable, and return the
value. Optionally convert the stored value (but not the returned
value) to a Guile object by calling @code{pyscm->scm}."
     (assert (and variable value))
     (let ([var (pyscm->scm variable)])
       (if (and convert (pyobject-true? convert))
           (variable-set! var (pyscm->scm value))
           (variable-set! var value)))
     value))

  (add-function-to-module!
   "variable_define"
   (lambda/kwargs (module name value convert)
     "Set the contents of a Guile variable, if necessary first
defining the variable and binding it to a symbol (the variable's name)
within the given module. The module must already exist. Return the
given value. Optionally convert the stored value (though not the
returned value) from a Python @code{scm} object to a Guile
object. FIXME: This description is written very badly."
     (assert (and module name value))
     (let ([module^ (python->guile-module module)]
           [name^ (python->symbol name)]
           [convert^ (and convert (pyobject-true? convert))])
       (save-module-excursion
        (lambda ()
          (set-current-module module^)
          (scm-define name^ (if convert^ (pyscm->scm value) value)))))
     value))

  (add-function-to-module!
   "eval_string"
   (lambda/kwargs (string [language (py None)]
                          [compile (py None)])
     "Evaluate a Python string as Guile code, optionally compiling. If
the return value is a pyobject, return it unchanged; otherwise wrap
the return value in a pyscm."
     (let ([string^ (pyunicode->string string)]
           [lang^ (if (pynone? language)
                      (current-language)
                      (string->symbol (pyunicode->string language)))]
           [compile^ (pyobject-true? compile)])
       ((lambda (obj) (if (pyobject? obj) obj (scm->pyscm obj)))
        (if compile^
            (let* ([frame (pyeval-getframe)]
                   [lineno (and frame (pyframe-getlinenumber frame))]
                   [file-name (and frame (pyframe-getfilename frame))])
              (eval-string string^ #:lang lang^ #:compile? compile^
                           #:file file-name #:line lineno))
            (eval-string string^ #:lang lang^ #:compile? compile^))))))

  (cons-hook!
   (lambda (m)
     ;; FIXME: How can we get rid of the ‘__main__’ in messages like
     ;; ‘__main__.GuileException: ERROR: ERROR: R6RS exception:’?
     (pymodule-addobject! m "GuileException"
                          (py (type (#:scm (python-str "GuileException"))
                                    (#:tuple exceptions.Exception)
                                    (#:dict))))))

  ) ;; end of library.
