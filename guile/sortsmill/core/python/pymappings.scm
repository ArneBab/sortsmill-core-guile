;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013, 2014 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core python pymappings)

  (export
   pymapping?               ;; (pymapping? obj) → boolean
   pydict?                  ;; (pydict? obj) → boolean
   pydict-exact?            ;; (pydict-exact? obj) → boolean
   pymapping-length         ;; (pymapping-length obj) → integer
   pymapping-delitemstring! ;; (pymapping-delitemstring! obj string) → *unspecified*
   pymapping-haskeystring?  ;; (pymapping-haskeystring? obj string) → boolean
   pymapping-haskey?        ;; (pymapping-haskey? obj key) → boolean
   pymapping-keys           ;; (pymapping-keys obj) → pylist
   pymapping-values         ;; (pymapping-values obj) → pylist
   pymapping-items          ;; (pymapping-items obj) → pylist
   pymapping-getitemstring  ;; (pymapping-getitemstring obj string) → obj
   pymapping-setitemstring! ;; (pymapping-setitemstring! obj string v) → *unspecified*
   pymapping->alist         ;; (pymapping->alist obj) → alist
   alist->pydict            ;; (alist->pydict alist) → obj
   pydict-clear!            ;; (pydict-clear! obj) → *unspecified*
   pydictproxy-new          ;; (pydictproxy-new obj) → obj
   pydict-copy              ;; (pydict-copy obj) → obj
   pydict-merge!            ;; (pydict-merge! pydict pydict [boolean]) → *unspecified*
   pydict-mergefromseq2!    ;; (pydict-mergefromseq2! pydict pyseq [boolean]) → *unspecified*
   pyseq2->pydict           ;; (pyseq2->pydict obj) → obj
   pyobject->pydict         ;; (pyobject->pydict obj) → obj
   pymapping->keyworded-list ;; (pymapping->keyworded-list obj) → list
   keyworded-list->pydict    ;; (keyworded-list->pydict list) → obj
   keyworded-list->py-arguments ;; (keyworded-list->py-arguments list) → (args . kw)
   )

  (import (rnrs)
          (except (guile) error)
          (system foreign)
          (sortsmill core helpers)
          (sortsmill core scm-procedures)
          )

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile-python)
                          scm_pymapping_p
                          scm_pydict_p
                          scm_pydict_exact_p
                          scm_pymapping_length
                          scm_pymapping_delitemstring_x
                          scm_pymapping_haskeystring_p
                          scm_pymapping_haskey_p
                          scm_pymapping_keys
                          scm_pymapping_values
                          scm_pymapping_items
                          scm_pymapping_getitemstring
                          scm_pymapping_setitemstring_x
                          scm_pymapping_to_alist
                          scm_alist_to_pydict
                          scm_pydict_clear_x
                          scm_pydictproxy_new
                          scm_pydict_copy
                          scm_pydict_merge_x
                          scm_pydict_mergefromseq2_x
                          scm_pyseq2_to_pydict
                          scm_pyobject_to_pydict
                          scm_pymapping_to_keyworded_list
                          scm_keyworded_list_to_pydict
                          scm_keyworded_list_to_py_arguments
                          ))

  (define-scm-procedure (pymapping? obj)
    "Is the argument a Guile-wrapped Python object supporting Python's
mapping protocol?"
    scm_pymapping_p)

  (define-scm-procedure (pydict? obj)
    "Is the argument a @code{pydict}?"
    scm_pydict_p)

  (define-scm-procedure (pydict-exact? obj)
    "Is the argument a @code{pydict} but not a member of a subtype of
@code{pydict}?"
    scm_pydict_exact_p)

  (define-scm-procedure (pymapping-length obj)
    "A wrapper around @code{PyMapping_Length}."
    scm_pymapping_length)

  (define-scm-procedure (pymapping-delitemstring! obj key)
    "A wrapper around @code{PyMapping_DelItemString}. The @var{key}
must be a Guile string."
    scm_pymapping_delitemstring_x)

  (define-scm-procedure (pymapping-haskeystring? obj key)
    "A wrapper around @code{PyMapping_HasKeyString}. The @var{key}
must be a Guile string."
    scm_pymapping_haskeystring_p)

  (define-scm-procedure (pymapping-haskey? obj key)
    "A wrapper around @code{PyMapping_HasKey}."
    scm_pymapping_haskey_p)

  (define-scm-procedure (pymapping-keys obj)
    "A wrapper around @code{PyMapping_Keys}."
    scm_pymapping_keys)

  (define-scm-procedure (pymapping-values obj)
    "A wrapper around @code{PyMapping_Values}."
    scm_pymapping_values)

  (define-scm-procedure (pymapping-items obj)
    "A wrapper around @code{PyMapping_Items}."
    scm_pymapping_items)

  (define-scm-procedure (pymapping-getitemstring obj key)
    "A wrapper around @code{PyMapping_GetItemString}. The @var{key}
must be a Guile string."
    scm_pymapping_getitemstring)

  (define-scm-procedure (pymapping-setitemstring! obj key v)
    "A wrapper around @code{PyMapping_SetItemString}. The @var{key}
must be a Guile string."
    scm_pymapping_setitemstring_x)

  (define-scm-procedure (pymapping->alist obj)
    "Convert a Guile-wrapped Python object, that has support for the
mapping protocol, to a Guile association list of Guile-wrapped Python
objects."
    scm_pymapping_to_alist)

  (define-scm-procedure (alist->pydict obj)
    "Convert an association list of Guile-wrapped Python objects to a
Guile-wrapped Python dictionary."
    scm_alist_to_pydict)

  (define-scm-procedure (pydict-clear! obj)
    "A wrapper around @code{PyDict_Clear}."
    scm_pydict_clear_x)

  (define-scm-procedure (pydictproxy-new obj)
    "A wrapper around @code{PyDictProxy_new}."
    scm_pydictproxy_new)

  (define-scm-procedure (pydict-copy obj)
    "A wrapper around @code{PyDict_Copy}."
    scm_pydict_copy)

  (define*-scm-procedure (pydict-merge! a b #:optional override?)
    "A wrapper around @code{PyDict_Merge}. The optional
@var{override?} argument is regarded as a @emph{Guile} boolean (so
that @code{0}, for instance, is regarded as `true')."
    scm_pydict_merge_x)

  (define*-scm-procedure (pydict-mergefromseq2! a seq2 #:optional override?)
    "A wrapper around @code{PyDict_MergeFromSeq2}. The optional
@var{override?} argument is regarded as a @emph{Guile} boolean (so
that @code{0}, for instance, is regarded as `true')."
    scm_pydict_mergefromseq2_x)

  (define-scm-procedure (pyseq2->pydict obj)
    "Convert a Guile-wrapped generalized Python equivalent of an
association list to a Guile-wrapped Python dictionary. FIXME: Write
better documentation for this."
    scm_pyseq2_to_pydict)

  (define-scm-procedure (pyobject->pydict obj)
    "Equivalent to @code{pydict-copy} or @code{pyseq2->pydict},
depending on the type of the argument."
    scm_pyobject_to_pydict)

  (define-scm-procedure (pymapping->keyworded-list obj)
    "Convert a Guile-wrapped Python object, that has support for the
mapping protocol and has only unicode and bytes objects as keys, to a
Guile list of alternating keywords and Guile-wrapped Python
objects. FIXME: Write better documentation."
    scm_pymapping_to_keyworded_list)

  (define-scm-procedure (keyworded-list->pydict lst)
    "This procedure takes, as argument, a Guile list of alternating
keywords and Guile-wrapped Python objects. It returns a Guile-wrapped
Python @code{dict} object of the specified bindings. FIXME: Write
better documentation."
    scm_keyworded_list_to_pydict)

  (define-scm-procedure (keyworded-list->py-arguments lst)
    "Convert a Guile-style argument list (for example, @code{(list (py
1) (py 2) #:key1 (py 3) #:key2 (py 4))} to two values: a
pytuple (in this case @code{(py (1 2))}); and either
@code{#f} or a pydict (in this case @code{(py (#:dict (\"key1\"
. 3) (\"key2\" . 4)))}. FIXME: Write better documentation."
    scm_keyworded_list_to_py_arguments)

  ) ;; end of library.
