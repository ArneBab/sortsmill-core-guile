;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2015 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(define-module (sortsmill core srfi-42)

  #:use-module ((rnrs) #:select (for-all))
  #:use-module (srfi srfi-42)
  #:use-module (ice-9 match)
  #:use-module (sortsmill core vlists)
  #:use-module (sortsmill core immutable-vectors)

  #:export (:vlst
            :ivect
            :u8ivect :u16ivect :u32ivect :u64ivect
            :s8ivect :s16ivect :s32ivect :s64ivect
            :f32ivect :f64ivect :c32ivect :c64ivect
            vlst-ec
            vlst-append-ec
            ivect-ec
            u8ivect-ec u16ivect-ec u32ivect-ec u64ivect-ec
            s8ivect-ec s16ivect-ec s32ivect-ec s64ivect-ec
            f32ivect-ec f64ivect-ec c32ivect-ec c64ivect-ec
            ivect-append-ec
            u8ivect-append-ec u16ivect-append-ec u32ivect-append-ec u64ivect-append-ec
            s8ivect-append-ec s16ivect-append-ec s32ivect-append-ec s64ivect-append-ec
            f32ivect-append-ec f64ivect-append-ec c32ivect-append-ec c64ivect-append-ec)

  #:re-export (:
               :-dispatch-ref
               :-dispatch-set!
               :char-range
               :dispatched
               :do
               :generator-proc
               :integers
               :let
               :list
               :parallel
               :port
               :range
               :real-range
               :string
               :until
               :vector
               :while
               any?-ec
               append-ec
               dispatch-union
               do-ec
               every?-ec
               first-ec
               fold-ec
               fold3-ec
               last-ec
               list-ec
               make-initial-:-dispatch
               max-ec
               min-ec
               product-ec
               string-append-ec
               string-ec
               sum-ec
               vector-ec
               vector-of-length-ec)

  ) ;; end of define-module.

;;--------------------------------------------------------------------
;;
;; The :vlst generator.
;;

(define-syntax :vlst
  ;; Like :list except for vlst.
  (syntax-rules (index)
    [(_ cc var (index i) arg ...)
     (:parallel cc (:vlst var arg ...) (:integers i))]
    [(_ cc var arg1 arg2 arg ...)
     (:vlst cc var (vlst-append arg1 arg2 arg ...))]
    [(_ cc var arg)
     (:do cc
          (let [])
          ((t arg))
          (not (vlst-null? t))
          (let ([var (vlst-car t)]))
          #t
          ((vlst-cdr t)))]))

;;--------------------------------------------------------------------
;;
;; The :ivect, :u8ivect, :u16ivect, etc., generators.
;;

(define-syntax :ivect^^
  (syntax-rules ()
    [(_ cc var (index i) my-length my-ref arg)
     (:do cc
          (let ([v arg]
                [len 0])
            (set! len (my-length v)))
          ((i 0))
          (< i len)
          (let ([var (my-ref v i)]))
          #t
          ((1+ i)))]))

(eval-when (load compile eval)
  (define (:ivect^ my-length my-ref my-append)
    ;; Like :string except for ivect. (FIXME: It _might_ be advisable
    ;; to do an implementation more similar to that of :vector, to
    ;; avoid the ivect-append.)
    (lambda (stx)
      (syntax-case stx (index)
        [(_ cc var (index i) arg)
         #`(:ivect^^ cc var (index i) #,my-length #,my-ref arg)]
        [(_ cc var (index i) arg1 arg2 arg ...)
         #`(:ivect^^ cc var (index i) #,my-length #,my-ref
                     (#,my-append arg1 arg2 arg ...))]
        [(_ cc var arg)
         #`(:ivect^^ cc var (index i) #,my-length #,my-ref arg)]
        [(_ cc var arg1 arg2 arg ...)
         #`(:ivect^^ cc var (index i) #,my-length #,my-ref
                     (#,my-append arg1 arg2 arg ...))]))))

(define-syntax :ivect (:ivect^ #'ivect-length #'ivect-ref #'ivect-append))
(define-syntax :u8ivect (:ivect^ #'u8ivect-length #'u8ivect-ref #'u8ivect-append))
(define-syntax :u16ivect (:ivect^ #'u16ivect-length #'u16ivect-ref #'u16ivect-append))
(define-syntax :u32ivect (:ivect^ #'u32ivect-length #'u32ivect-ref #'u32ivect-append))
(define-syntax :u64ivect (:ivect^ #'u64ivect-length #'u64ivect-ref #'u64ivect-append))
(define-syntax :s8ivect (:ivect^ #'s8ivect-length #'s8ivect-ref #'s8ivect-append))
(define-syntax :s16ivect (:ivect^ #'s16ivect-length #'s16ivect-ref #'s16ivect-append))
(define-syntax :s32ivect (:ivect^ #'s32ivect-length #'s32ivect-ref #'s32ivect-append))
(define-syntax :s64ivect (:ivect^ #'s64ivect-length #'s64ivect-ref #'s64ivect-append))
(define-syntax :f32ivect (:ivect^ #'f32ivect-length #'f32ivect-ref #'f32ivect-append))
(define-syntax :f64ivect (:ivect^ #'f64ivect-length #'f64ivect-ref #'f64ivect-append))
(define-syntax :c32ivect (:ivect^ #'c32ivect-length #'c32ivect-ref #'c32ivect-append))
(define-syntax :c64ivect (:ivect^ #'c64ivect-length #'c64ivect-ref #'c64ivect-append))

;;--------------------------------------------------------------------
;;
;; Extensions to the dispatching ‘:’ generator.
;;

(define extra-dispatch
  (match-lambda
   [() 'sortsmill-core-srfi-42]
   [(arg)
    (cond [(vlst? arg) (:generator-proc (:vlst arg))]
          [(any-ivect? arg)
           (cond [(ivect? arg) (:generator-proc (:ivect arg))]
                 [(u8ivect? arg) (:generator-proc (:u8ivect arg))]
                 [(u16ivect? arg) (:generator-proc (:u16ivect arg))]
                 [(u32ivect? arg) (:generator-proc (:u32ivect arg))]
                 [(u64ivect? arg) (:generator-proc (:u64ivect arg))]
                 [(s8ivect? arg) (:generator-proc (:s8ivect arg))]
                 [(s16ivect? arg) (:generator-proc (:s16ivect arg))]
                 [(s32ivect? arg) (:generator-proc (:s32ivect arg))]
                 [(s64ivect? arg) (:generator-proc (:s64ivect arg))]
                 [(f32ivect? arg) (:generator-proc (:f32ivect arg))]
                 [(f64ivect? arg) (:generator-proc (:f64ivect arg))]
                 [(c32ivect? arg) (:generator-proc (:c32ivect arg))]
                 [(c64ivect? arg) (:generator-proc (:c64ivect arg))]
                 [else #f])]
          [else #f])]
   [(. args)
    (cond [(for-all vlst? args)
           (:generator-proc (:vlst (vlst-concatenate args)))]
          [(any-ivect? (car args))
           (cond [(for-all ivect? args)
                  (:generator-proc (:ivect (ivect-concatenate args)))]
                 [(for-all u8ivect? args)
                  (:generator-proc (:u8ivect (u8ivect-concatenate args)))]
                 [(for-all u16ivect? args)
                  (:generator-proc (:u16ivect (u16ivect-concatenate args)))]
                 [(for-all u32ivect? args)
                  (:generator-proc (:u32ivect (u32ivect-concatenate args)))]
                 [(for-all u64ivect? args)
                  (:generator-proc (:u64ivect (u64ivect-concatenate args)))]
                 [(for-all s8ivect? args)
                  (:generator-proc (:s8ivect (s8ivect-concatenate args)))]
                 [(for-all s16ivect? args)
                  (:generator-proc (:s16ivect (s16ivect-concatenate args)))]
                 [(for-all s32ivect? args)
                  (:generator-proc (:s32ivect (s32ivect-concatenate args)))]
                 [(for-all s64ivect? args)
                  (:generator-proc (:s64ivect (s64ivect-concatenate args)))]
                 [(for-all f32ivect? args)
                  (:generator-proc (:f32ivect (f32ivect-concatenate args)))]
                 [(for-all f64ivect? args)
                  (:generator-proc (:f64ivect (f64ivect-concatenate args)))]
                 [(for-all c32ivect? args)
                  (:generator-proc (:c32ivect (c32ivect-concatenate args)))]
                 [(for-all c64ivect? args)
                  (:generator-proc (:c64ivect (c64ivect-concatenate args)))]
                 [else #f])]
          [else #f])]))

(:-dispatch-set! (dispatch-union (:-dispatch-ref) extra-dispatch))

;;--------------------------------------------------------------------
;;
;; Comprehensions returning vlst.
;;

(define-syntax vlst-ec
  (syntax-rules ()
    [(_ a a* ...)
     (vlst-reverse (fold-ec vlst-null a a* ... vlst-cons))]))

(define-syntax vlst-append-ec
  (syntax-rules ()
    [(_ a a* ...)
     (vlst-concatenate (vlst-ec a a* ...))]))

;;--------------------------------------------------------------------
;;
;; Comprehensions returning ivect, u8ivect, u16ivect, etc.
;;

(eval-when (compile load eval)
  (define (ivect-ec^ my-null my-push)
    (lambda (stx)
      (syntax-case stx ()
        [(_ a a* ...)
         #`(fold-ec #,my-null a a* ... (lambda (x v) (#,my-push v x)))]))))

(define-syntax ivect-ec (ivect-ec^ #'ivect-null #'ivect-push))
(define-syntax u8ivect-ec (ivect-ec^ #'u8ivect-null #'u8ivect-push))
(define-syntax u16ivect-ec (ivect-ec^ #'u16ivect-null #'u16ivect-push))
(define-syntax u32ivect-ec (ivect-ec^ #'u32ivect-null #'u32ivect-push))
(define-syntax u64ivect-ec (ivect-ec^ #'u64ivect-null #'u64ivect-push))
(define-syntax s8ivect-ec (ivect-ec^ #'s8ivect-null #'s8ivect-push))
(define-syntax s16ivect-ec (ivect-ec^ #'s16ivect-null #'s16ivect-push))
(define-syntax s32ivect-ec (ivect-ec^ #'s32ivect-null #'s32ivect-push))
(define-syntax s64ivect-ec (ivect-ec^ #'s64ivect-null #'s64ivect-push))
(define-syntax f32ivect-ec (ivect-ec^ #'f32ivect-null #'f32ivect-push))
(define-syntax f64ivect-ec (ivect-ec^ #'f64ivect-null #'f64ivect-push))
(define-syntax c32ivect-ec (ivect-ec^ #'c32ivect-null #'c32ivect-push))
(define-syntax c64ivect-ec (ivect-ec^ #'c64ivect-null #'c64ivect-push))

(eval-when (compile load eval)
  (define (ivect-append-ec^ my-concatenate)
    (lambda (stx)
      (syntax-case stx ()
        [(_ a a* ...)
         #`(#,my-concatenate (list-ec a a* ...))]))))

(define-syntax ivect-append-ec (ivect-append-ec^ #'ivect-concatenate))
(define-syntax u8ivect-append-ec (ivect-append-ec^ #'u8ivect-concatenate))
(define-syntax u16ivect-append-ec (ivect-append-ec^ #'u16ivect-concatenate))
(define-syntax u32ivect-append-ec (ivect-append-ec^ #'u32ivect-concatenate))
(define-syntax u64ivect-append-ec (ivect-append-ec^ #'u64ivect-concatenate))
(define-syntax s8ivect-append-ec (ivect-append-ec^ #'s8ivect-concatenate))
(define-syntax s16ivect-append-ec (ivect-append-ec^ #'s16ivect-concatenate))
(define-syntax s32ivect-append-ec (ivect-append-ec^ #'s32ivect-concatenate))
(define-syntax s64ivect-append-ec (ivect-append-ec^ #'s64ivect-concatenate))
(define-syntax f32ivect-append-ec (ivect-append-ec^ #'f32ivect-concatenate))
(define-syntax f64ivect-append-ec (ivect-append-ec^ #'f64ivect-concatenate))
(define-syntax c32ivect-append-ec (ivect-append-ec^ #'c32ivect-concatenate))
(define-syntax c64ivect-append-ec (ivect-append-ec^ #'c64ivect-concatenate))

;;--------------------------------------------------------------------
