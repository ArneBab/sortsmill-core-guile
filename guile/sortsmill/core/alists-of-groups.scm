;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2014 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core alists-of-groups)

  (export
   alist-of-groups:make ;; (alist-of-groups:make aog-or-pairs) → aog
   alist-of-groups:reverse ;; (alist-of-groups:reverse aog-or-pairs) → aog

   alist-of-groups:ref  ;; (alist-of-group:ref aog key) → vector
   alist-of-groups:set! ;; (alist-of-group:set! aog key element1 element2 ...) → aog
   alist-of-groups:cons-set! ;; (alist-of-group:cons-set! aog key element1 element2 ...) → aog
   alist-of-groups:add! ;; (alist-of-group:add! aog key element1 element2 ...) → aog
   alist-of-groups:cons-add! ;; (alist-of-group:cons-add! aog key element1 element2 ...) → aog

   alist-of-groups:find-group ;; (alist-of-groups:find-group aog element) → key, value, aog

   alist-of-groups:keys ;; (alist-of-groups:keys aog) → list
   alist-of-groups:values ;; (alist-of-groups:values aog) → list-of-vectors
   alist-of-groups:pairs ;; (alist-of-groups:pairs aog) → list-of-pairs

   alist-of-groups:element-count ;; (alist-of-groups:element-count aog) → integer

   alist-of-groups:duplicate-elements ;; (alist-of-groups:duplicate-elements aog) → alist
   )

  (import (rnrs)
          (except (guile) error)
          (system foreign)
          (sortsmill core helpers)
          (sortsmill core scm-procedures)
          (sortsmill smcoreguile-pkginfo))

  (define (_ string) (gettext string SMCOREGUILE_PACKAGE))

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile)
                          scm_alist_of_groups_make
                          scm_alist_of_groups_reverse
                          scm_alist_of_groups_ref
                          scm_alist_of_groups_set_x
                          scm_alist_of_groups_cons_set_x
                          scm_alist_of_groups_add_x
                          scm_alist_of_groups_cons_add_x
                          scm_alist_of_groups_find_group
                          scm_alist_of_groups_keys
                          scm_alist_of_groups_values
                          scm_alist_of_groups_pairs
                          scm_alist_of_groups_element_count
                          scm_alist_of_groups_duplicate_elements
                          ))

  (define-scm-procedure (alist-of-groups:make aog-or-pairs)
    "FIXME: Document this."
    scm_alist_of_groups_make)

  (define-scm-procedure (alist-of-groups:reverse aog-or-pairs)
    "Equivalent to @code{(reverse (alist-of-groups:make aog-or-pairs))},
 except slightly faster."
    scm_alist_of_groups_reverse)

  (define-scm-procedure (alist-of-groups:ref aog key)
    "FIXME: Document this."
    scm_alist_of_groups_ref)

  (define-scm-procedure (alist-of-groups:set! aog key . elements)
    "FIXME: Document this."
    scm_alist_of_groups_set_x)

  (define-scm-procedure (alist-of-groups:cons-set! aog key . elements)
    "FIXME: Document this."
    scm_alist_of_groups_cons_set_x)

  (define-scm-procedure (alist-of-groups:add! aog key . elements)
    "FIXME: Document this."
    scm_alist_of_groups_add_x)

  (define-scm-procedure (alist-of-groups:cons-add! aog key . elements)
    "FIXME: Document this."
    scm_alist_of_groups_cons_add_x)

  (define-scm-procedure (alist-of-groups:find-group aog element)
    "FIXME: Document this."
    scm_alist_of_groups_find_group)

  (define-scm-procedure (alist-of-groups:keys aog)
    "FIXME: Document this."
    scm_alist_of_groups_keys)

  (define-scm-procedure (alist-of-groups:values aog)
    "FIXME: Document this."
    scm_alist_of_groups_values)

  (define-scm-procedure (alist-of-groups:pairs aog)
    "FIXME: Document this."
    scm_alist_of_groups_pairs)

  (define-scm-procedure (alist-of-groups:element-count aog)
    "The number of (possibly non-unique) elements in the
@code{alist-of-groups}."
    scm_alist_of_groups_element_count)

  (define-scm-procedure (alist-of-groups:duplicate-elements aog)
    "FIXME: Document this."
    scm_alist_of_groups_duplicate_elements)

  ) ;; end of library.
