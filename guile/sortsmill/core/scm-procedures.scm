;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013, 2014 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core scm-procedures)

  ;; Macros for wrapping ‘SCM scm_something_something(SCM, SCM, etc)’
  ;; C functions for use in Guile code.

  ;; FIXME: Document these macros.

  (export lambda*-scm-procedure
          define*-scm-procedure
          lambda-scm-procedure
          define-scm-procedure)

  (import (rnrs)
          (except (guile) error)
          (ice-9 match)
          (ice-9 format)
          (system foreign)
          (sortsmill core helpers)
          (sortsmill core i18n)
          (sortsmill smcoreguile-pkginfo))

  (define-gettext-for-domain _ SMCOREGUILE_PACKAGE)

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-ltdlopened
                           (string-append "libguile-" (effective-version)))
                          scm_c_make_gsubr)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile)
                          init_sortsmill_core_guile_scm_procedures)
    ((pointer->procedure void init_sortsmill_core_guile_scm_procedures '())))

  (define gsubr-max-arg-count SCM_GSUBR_MAX)

  (define scm-c-make-gsubr
    (pointer->procedure '* scm_c_make_gsubr `(* ,int ,int ,int *)))

  (define (make-gsubr name req opt rst fcn)
    (pointer->scm (scm-c-make-gsubr (string->pointer name) req opt rst fcn)))

  (define (split-lambda*-scm-procedure-arguments arguments%)
    ;;
    (define (get-rest-argument args% required optional)
      (syntax-case args% ()
        [rest-arg (identifier? #'rest-arg)
                  (values (reverse required) (reverse optional)
                          (list #'rest-arg))]
        [else
         (syntax-violation 'lambda*-scm-procedure
                           (_ "invalid rest argument")
                           (syntax->datum arguments%)
                           (syntax->datum args%))]))
    ;;
    (define (get-optional-arguments args% required optional)
      (syntax-case args% ()
        [()
         (values (reverse required) (reverse optional) '())]
        [(opt-arg . more-args) (identifier? #'opt-arg)
         (get-optional-arguments #'more-args required (cons #'opt-arg optional))]
        [(#:rest rest-arg)
         (get-rest-argument #'rest-arg required optional)]
        [rest-arg (identifier? #'rest-arg)
                  (get-rest-argument #'rest-arg required optional)]
        [else
         (syntax-violation 'lambda*-scm-procedure
                           (_ "invalid optional argument list")
                           (syntax->datum arguments%)
                           (syntax->datum args%))]))
    ;;
    (define (get-required-arguments args% required)
      (syntax-case args% ()
        [()
         (values (reverse required) '() '())]
        [(req-arg . more-args) (identifier? #'req-arg)
         (get-required-arguments #'more-args (cons #'req-arg required))]
        [(#:optional . more-args)
         (get-optional-arguments #'more-args required '())]
        [(#:rest rest-arg)
         (get-rest-argument #'rest-arg required '())]
        [rest-arg (identifier? #'rest-arg)
                  (get-rest-argument #'rest-arg required '())]
        [else
         (syntax-violation 'lambda*-scm-procedure
                           (_ "invalid argument list")
                           (syntax->datum arguments%)
                           (syntax->datum args%))]))
    ;;
    (get-required-arguments arguments% '()))

  (define (split-lambda-scm-procedure-arguments arguments%)
    ;;
    (define (get-rest-argument args% regular-args)
      (syntax-case args% ()
        [rest-arg (identifier? #'rest-arg)
                  (values (reverse regular-args) (list #'rest-arg))]
        [else
         (syntax-violation 'lambda-scm-procedure
                           (_ "invalid rest argument")
                           (syntax->datum arguments%)
                           (syntax->datum args%))]))
    ;;
    ;;
    (define (get-arguments args% regular-args)
      (syntax-case args% ()
        [()
         (values (reverse regular-args) '())]
        [(req-arg . more-args) (identifier? #'req-arg)
         (get-arguments #'more-args (cons #'req-arg regular-args))]
        [rest-arg (identifier? #'rest-arg)
                  (get-rest-argument #'rest-arg regular-args)]
        [else
         (syntax-violation 'lambda-scm-procedure
                           (_ "invalid argument list")
                           (syntax->datum arguments%)
                           (syntax->datum args%))]))
    ;;
    (get-arguments arguments% '()))

  (define (lambda*-scm-procedure-arglist required-args optional-args rest-args)
    `(,@required-args
      ,@(if (null? optional-args)
            '()
            (cons (datum->syntax (car optional-args) #:optional) optional-args))
      ,@(if (null? rest-args)
            '()
            (cons (datum->syntax (car rest-args) #:rest) rest-args))))

  (define (lambda*-scm-procedure-arglist-with-defaults
           required-args optional-args rest-args)
    `(,@required-args
      ,@(if (null? optional-args)
            '()
            (cons (datum->syntax (car optional-args) #:optional)
                  (map (lambda (a) #`(#,a *missing-argument*))
                       optional-args)))
      ,@(if (null? rest-args)
            '()
            (cons (datum->syntax (car rest-args) #:rest) rest-args))))

  (define (lambda-scm-procedure-arglist regular-args rest-args)
    (if (null? rest-args)
        regular-args
        (begin
          (assert (null? (cdr rest-args)))
          `(,@regular-args . ,(car rest-args)))))

  (define-syntax lambda*-scm-procedure
    (lambda (stx)
      (syntax-case stx ()
        [(_ arguments% doc-string% func%)
         ;; Procedure with documentation string (or #f) but without
         ;; name.
         #'(lambda*-scm-procedure arguments% doc-string% func% #f)]

        [(_ arguments% func%)
         ;; Procedure without documentation string or name.
         #'(lambda*-scm-procedure arguments% #f func% #f)]

        [(_ arguments% doc-string% func% name%)
         (let-values ([(required-args optional-args rest-args)
                       (split-lambda*-scm-procedure-arguments #'arguments%)])
           (if (<= (+ (length required-args)
                      (length optional-args)
                      (length rest-args))
                   gsubr-max-arg-count)
               #`(let* ([nm name%]
                        [name-str (if nm (symbol->string nm) "#")])
                   (let ([doc-str doc-string%]
                         [gsubr
                          (make-gsubr name-str
                                      #,(datum->syntax stx (length required-args))
                                      #,(datum->syntax stx (length optional-args))
                                      #,(datum->syntax stx (length rest-args))
                                      func%)])
                     (when doc-str
                       (set-procedure-property! gsubr 'documentation doc-str))
                     (unless nm
                       (set-procedure-property! gsubr 'name #f))
                     gsubr))
               (with-syntax ([star-list%
                              (datum->syntax stx (map (const '*)
                                                      (append required-args
                                                              optional-args
                                                              rest-args)))])
                 #`(let ([doc-str doc-string%]
                         [name-symbol name%]
                         [gsubr
                          (let ([proc (pointer->procedure '* func% 'star-list%)])
                            (lambda* #,(lambda*-scm-procedure-arglist-with-defaults
                                        required-args optional-args rest-args)
                              (pointer->scm
                               (proc #,@(map (lambda (a) #`(scm->pointer #,a))
                                             required-args)
                                     #,@(map (lambda (a)
                                               #`(prepare-optional-argument-for-c
                                                  #,a))
                                             optional-args)
                                     #,@(map (lambda (a) #`(scm->pointer #,a))
                                             rest-args)))))])
                     (when doc-str
                       (set-procedure-property! gsubr 'documentation doc-str))
                     (when name-symbol
                       (set-procedure-property! gsubr 'name name-symbol))
                     gsubr))))] )))

  (define-syntax define*-scm-procedure
    (lambda (stx)
      (syntax-case stx ()
        [(_ (name% . arguments%) doc-string% func%) (identifier? #'name%)
         ;; Procedure with documentation string, and with name same as
         ;; that of the variable being defined.
         #'(define*-scm-procedure (name% . arguments%)
             doc-string% func% 'name%)]

        [(_ (name% . arguments%) func%) (identifier? #'name%)
         ;; Procedure without documentation string, and with name same
         ;; as that of the variable being defined.
         #'(define*-scm-procedure (name% . arguments%)
             #f func% 'name%)]

        [(_ (name% . arguments%) doc-string% func% name%%) (identifier? #'name%)
         ;; Procedure with documentation string and an explicitly
         ;; specified name.
         (let-values ([(required-args optional-args rest-args)
                       (split-lambda*-scm-procedure-arguments #'arguments%)])
           (with-syntax ([arg-list% (lambda*-scm-procedure-arglist
                                     required-args optional-args rest-args)])
             #'(define name%
                 (lambda*-scm-procedure arg-list%
                   doc-string% func% name%%))))] )))

  (define-syntax lambda-scm-procedure
    (lambda (stx)
      (syntax-case stx ()
        [(_ arguments% doc-string% func%)
         ;; Procedure with documentation string (or #f) but without
         ;; name.
         #'(lambda-scm-procedure arguments% doc-string% func% #f)]

        [(_ arguments% func%)
         ;; Procedure without documentation string or name.
         #'(lambda-scm-procedure arguments% #f func% #f)]

        [(_ arguments% doc-string% func% name%)
         (let-values ([(regular-args rest-args)
                       (split-lambda-scm-procedure-arguments #'arguments%)])
           (if (<= (+ (length regular-args) (length rest-args))
                   gsubr-max-arg-count)
               #`(let* ([nm name%]
                        [name-str (if nm (symbol->string nm) "#")])
                   (let ([doc-str doc-string%]
                         [gsubr
                          (make-gsubr name-str
                                      #,(datum->syntax stx (length regular-args))
                                      0
                                      #,(datum->syntax stx (length rest-args))
                                      func%)])
                     (when doc-str
                       (set-procedure-property! gsubr 'documentation doc-str))
                     (unless nm
                       (set-procedure-property! gsubr 'name #f))
                     gsubr))
               (with-syntax ([star-list%
                              (datum->syntax stx (map (const '*)
                                                      (append regular-args
                                                              rest-args)))])
                 #`(let ([doc-str doc-string%]
                         [name-symbol name%]
                         [gsubr
                          (let ([proc (pointer->procedure '* func% 'star-list%)])
                            (lambda #,(lambda-scm-procedure-arglist
                                       regular-args rest-args)
                              (pointer->scm
                               (proc #,@(map (lambda (a) #`(scm->pointer #,a))
                                             (append regular-args rest-args))))))])
                     (when doc-str
                       (set-procedure-property! gsubr 'documentation doc-str))
                     (when name-symbol
                       (set-procedure-property! gsubr 'name name-symbol))
                     gsubr))))] )))

  (define-syntax define-scm-procedure
    (lambda (stx)
      (syntax-case stx ()
        [(_ (name% . arguments%) doc-string% func%) (identifier? #'name%)
         ;; Procedure with documentation string, and with name same as
         ;; that of the variable being defined.
         #'(define-scm-procedure (name% . arguments%)
             doc-string% func% 'name%)]

        [(_ (name% . arguments%) func%) (identifier? #'name%)
         ;; Procedure without documentation string, and with name same
         ;; as that of the variable being defined.
         #'(define-scm-procedure (name% . arguments%)
             #f func% 'name%)]

        [(_ (name% . arguments%) doc-string% func% name%%) (identifier? #'name%)
         ;; Procedure with documentation string and an explicitly
         ;; specified name.
         (let-values ([(regular-args rest-args)
                       (split-lambda-scm-procedure-arguments #'arguments%)])
           (with-syntax ([arg-list% (lambda-scm-procedure-arglist
                                     regular-args rest-args)])
             #'(define name%
                 (lambda-scm-procedure arg-list%
                   doc-string% func% name%%))))] )))

  ) ;; end of library.
