;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2015 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core hashmaps)

  (export
   hashmap?
   make-hashmap
   hashmap-null?
   hashmap-size
   hashmap-contains?
   hashmap-ref
   hashmap-set
   hashmap-remove

   hashmap-cursor?
   )

  (import (except (rnrs)
                  ;; We want to redefine the following with GOOPS;
                  ;; importing them from (rnrs) might cause trouble.
                  write equal?)
          (except (guile) error)
          (system foreign)
          (sortsmill core helpers)
          (sortsmill core scm-procedures)
          (sortsmill core i18n)
          (sortsmill smcoreguile-pkginfo))

  (define-gettext-for-domain _ SMCOREGUILE_PACKAGE)

  (eval-when (compile load eval)
    (expose-foreign-funcs
     (dynamic-link-sortsmill-core-guile)

     scm_hashmap_p
     scm_make_hashmap
     scm_hashmap_null_p
     scm_hashmap_size
     scm_hashmap_contains_p
     scm_hashmap_ref
     scm_hashmap_set
     scm_hashmap_remove

     scm_hashmap_cursor_p

     guile_init_smcoreguile_hashmaps)

    ((pointer->procedure void guile_init_smcoreguile_hashmaps '())))

  (define-syntax when-using-foreign-objects
    (if using-foreign-objects?
        (syntax-rules () [(_ body body* ...) (begin body body* ...)])
        (syntax-rules () [(_ body body* ...) *unspecified*])))

  (when-using-foreign-objects

   (use-modules (oop goops)
                (system foreign-object))

;;;;;   (expose-foreign-funcs (dynamic-link-sortsmill-core-guile)
;;;;;                         guile_ivect_write__)
;;;;;
;;;;;   (define-scm-procedure (internal-ivect-write obj port)
;;;;;     guile_ivect_write__)
;;;;;
;;;;;   (define-generic write)
;;;;;   (define-method (write [obj <ivect>] port)
;;;;;     (internal-ivect-write obj port))
;;;;;
;;;;;   (expose-foreign-funcs (dynamic-link-sortsmill-core-guile)
;;;;;                         guile_ivect_equal_p__)
;;;;;
;;;;;   (define-scm-procedure (internal-ivect-equal? a b)
;;;;;     guile_ivect_equal_p__)
;;;;;
;;;;;   (define-generic equal?)
;;;;;   (define-method (equal? [a <ivect>] [b <ivect>])
;;;;;     (internal-ivect-equal? a b))

   ) ;; end of if-using-foreign-objects.

  ;;------------------------------------------------------------

  (define-scm-procedure (hashmap? obj)
    scm_hashmap_p)

  (define*-scm-procedure (make-hashmap #:optional hash-proc equal-proc)
    scm_make_hashmap)

  (define-scm-procedure (hashmap-null? obj)
    scm_hashmap_null_p)

  (define-scm-procedure (hashmap-size obj)
    scm_hashmap_size)

  (define-scm-procedure (hashmap-contains? map key)
    scm_hashmap_contains_p)

  (define*-scm-procedure (hashmap-ref map key #:optional default-value)
    scm_hashmap_ref)

  (define-scm-procedure (hashmap-set map key value)
    scm_hashmap_set)

  (define-scm-procedure (hashmap-remove map key)
    scm_hashmap_remove)

  ;;------------------------------------------------------------

  (define-scm-procedure (hashmap-cursor? obj)
    scm_hashmap_cursor_p)

  ;;------------------------------------------------------------

  ) ;; end of library.
