;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2014 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core procs-with-setters)

  (export define-procedures-with-setters)

  (import (rnrs)
          (except (guile) error)
          (ice-9 format))

  (define-syntax define-procedure-with-setter
    (lambda (stx)
      (let ([derived-name
             (lambda (fmt^ v^)
               (let ([fmt (syntax->datum fmt^)]
                     [v (syntax->datum v^)])
                 (datum->syntax stx (string->symbol (format #f fmt v)))))])
        (syntax-case stx ()
          [(_ ref-fmt set-fmt v)
           (and (string? (syntax->datum #'ref-fmt))
                (string? (syntax->datum #'set-fmt))
                (identifier? #'v))
           #`(define v
               (make-procedure-with-setter #,(derived-name #'ref-fmt #'v)
                                           #,(derived-name #'set-fmt #'v)))]
          [(_ v) (identifier? #'v)
           ;; The default component procedures are ‘IDENTIFIER-ref’
           ;; and ‘IDENTIFIER-set!’.
           #'(define-procedure-with-setter "~a-ref" "~a-set!" v)]))))

  (define-syntax define-procedures-with-setters
    (lambda (stx)
      (syntax-case stx ()
        [(_ ref-fmt set-fmt v ...)
           (and (string? (syntax->datum #'ref-fmt))
                (string? (syntax->datum #'set-fmt))
                (for-all identifier? #'(v ...)))
         #'(begin (define-procedure-with-setter ref-fmt set-fmt v) ...)]
        [(_ v ...) (for-all identifier? #'(v ...))
         #'(begin (define-procedure-with-setter v) ...)])))

  ) ;; end of library.
