;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core x-alloc) ;;; Guile interface to
                                  ;;; <sortsmill/x_alloc.h>

  (export x-alloc-die         ; (x-alloc-die) → terminates the program
          x-alloc-die-on-null ; (x-alloc-die-on-null p) → p, or terminates the program
          x-free              ; (x-free ptr) → *unspecified*
          x-malloc            ; (x-malloc size) → pointer
          x-zalloc            ; (x-zalloc size) → pointer
          x-calloc            ; (x-calloc n size) → pointer
          x-realloc           ; (x-realloc ptr size) → pointer
          x-2realloc          ; (x-2realloc ptr size-ptr) → pointer
          x-memdup            ; (x-memdup ptr size) → pointer
          x-strdup            ; (x-strdup ptr) → pointer
          x-nmalloc           ; (x-nmalloc n size) → pointer
          x-nrealloc          ; (x-nrealloc ptr n size) → pointer
          x-2nrealloc         ; (x-nrealloc ptr n-ptr size) → pointer
          x-charalloc         ; (x-charalloc size) → pointer
          )

  (import (sortsmill core helpers)
          (rnrs)
          (except (guile) error)
          (system foreign))

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-ltdlopened "libsortsmill-core")
                          x_alloc_die
                          x_free
                          x_malloc
                          x_zalloc
                          x_calloc
                          x_realloc
                          x_2realloc
                          x_memdup
                          x_strdup
                          x_nmalloc
                          x_nrealloc
                          x_2nrealloc
                          x_charalloc))

  (define x-alloc-die
    (let ([proc (pointer->procedure void x_alloc_die '())])
      (lambda ()
        "@code{(x-alloc-die)} terminates the program with a `memory
exhausted' message."
        (proc))))

  (define (x-alloc-die-on-null p)
    "If its argument is a null pointer, @code{(x-alloc-die-on-null}
terminates the program with a `memory exhausted' message. Otherwise,
it returns the argument."
    (when (null-pointer? p) (x-alloc-die))
    p)

  (define x-free
    (let ([proc (pointer->procedure void x_free '(*))])
      (lambda (p)
        "@code{(x-free p)} frees the malloced block at pointer
@var{p}."
        (proc p))))

  (define x-malloc
    (let ([proc (pointer->procedure '* x_malloc `(,size_t))])
      (lambda (size)
        "@code{(x-malloc size)} allocates a block of memory, using x_malloc(3),
and returns a pointer."
        (proc size))))

  (define x-zalloc
    (let ([proc (pointer->procedure '* x_zalloc `(,size_t))])
      (lambda (size)
        "@code{(x-zalloc size)} allocates a block of memory, using x_zalloc(3),
and returns a pointer."
        (proc size))))

  (define x-calloc
    (let ([proc (pointer->procedure '* x_zalloc `(,size_t ,size_t))])
      (lambda (n size)
        "@code{(x-calloc n size)} allocates a block of memory,
using x_calloc(3), and returns a pointer."
        (proc n size))))

  (define x-realloc
    (let ([proc (pointer->procedure '* x_realloc `(* ,size_t))])
      (lambda (p size)
        "@code{(x-realloc p size)} reallocates a block of memory,
using x_realloc(3), and returns a pointer."
        (proc p size))))

  (define x-2realloc
    (let ([proc (pointer->procedure '* x_2realloc '(* *))])
      (lambda (p size-ptr)
        "@code{(x-2realloc p size-ptr)} reallocates a block of memory,
using x_2realloc(3), and returns a pointer. The memory pointed to by
@var{size-ptr} might be changed as a side effect."
        (proc p size-ptr))))

  (define x-memdup
    (let ([proc (pointer->procedure '* x_memdup `(* ,size_t))])
      (lambda (p size)
        "@code{(x-memdup p size)} copies a block of memory to freshly
malloced memory, using x_memdup(3), and returns a pointer."
        (proc p size))))

  (define x-strdup
    (let ([proc (pointer->procedure '* x_strdup '(*))])
      (lambda (p)
        "@code{(x-strdup p)} copies the null-terminated string pointed
to by @var{p}, using x_strdup(3), and returns a pointer."
        (proc p))))

  (define x-nmalloc
    (let ([proc (pointer->procedure '* x_nmalloc `(,size_t ,size_t))])
      (lambda (n size)
        "@code{(x-nmalloc n size)} allocates a block of memory,
using x_nmalloc(3), and returns a pointer."
        (proc n size))))

  (define x-nrealloc
    (let ([proc (pointer->procedure '* x_nrealloc `(* ,size_t ,size_t))])
      (lambda (p n size)
        "@code{(x-nrealloc n size)} reallocates a block of memory,
using x_nrealloc(3), and returns a pointer."
        (proc p n size))))

  (define x-2nrealloc
    (let ([proc (pointer->procedure '* x_2nrealloc `(* * ,size_t))])
      (lambda (p n-ptr size)
        "@code{(x-2nrealloc n size)} reallocates a block of memory,
using x_2nrealloc(3), and returns a pointer. The memory pointed to by
@var{n-ptr} might be changed as a side effect."
        (proc p n-ptr size))))

  (define x-charalloc
    (let ([proc (pointer->procedure '* x_charalloc `(,size_t))])
      (lambda (size)
        "@code{(x-charalloc size)} allocates a block of memory, using x_charalloc(3),
and returns a pointer. (Because we treat the pointer as a void
pointer, calling @code{x-charalloc} is effectively equivalent to
calling the @code{x-malloc} procedure. We include it for
completeness.)"
        (proc size))))

  ) ;; end of library.
