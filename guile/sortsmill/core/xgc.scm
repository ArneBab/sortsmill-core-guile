;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core xgc) ;;; Guile interface to
                              ;;; <sortsmill/xgc.h>

  (export x-gc-free              ; (x-gc-free ptr) → *unspecified*
          x-gc-malloc            ; (x-gc-malloc size) → pointer
          x-gc-malloc-atomic     ; (x-gc-malloc-atomic size) → pointer
          x-gc-malloc-uncollectable ; (x-gc-malloc-uncollectable size) → pointer
          x-gc-malloc-uncollectible ; (x-gc-malloc-uncollectible size) → pointer
          x-gc-realloc              ; (x-gc-realloc ptr size) → pointer
          x-gc-malloc-ignore-off-page ; (x-gc-malloc-ignore-off-page size) → pointer
          x-gc-malloc-atomic-ignore-off-page ; (x-gc-malloc-atomic-ignore-off-page size) → pointer
          x-gc-malloc-stubborn ; (x-gc-malloc-stubborn size) → pointer
          x-gc-strdup          ; (x-gc-strdup ptr) → pointer
          x-gc-strndup         ; (x-gc-strndup ptr n) → pointer
          x-gc-grabstr         ; (x-gc-grabstr ptr) → pointer
          x-gc-u8-grabstr      ; (x-gc-u8-grabstr ptr) → pointer
          x-gc-u16-grabstr     ; (x-gc-u16-grabstr ptr) → pointer
          x-gc-u32-grabstr     ; (x-gc-u32-grabstr ptr) → pointer
          )

  (import (sortsmill core helpers)
          (rnrs)
          (except (guile) error)
          (system foreign))

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-ltdlopened "libsortsmill-core")
                          x_gc_free
                          x_gc_malloc
                          x_gc_malloc_atomic
                          x_gc_malloc_uncollectable
                          x_gc_malloc_uncollectible
                          x_gc_realloc
                          x_gc_malloc_ignore_off_page
                          x_gc_malloc_atomic_ignore_off_page
                          x_gc_malloc_stubborn
                          x_gc_strdup
                          x_gc_strndup
                          x_gc_grabstr
                          x_gc_u8_grabstr
                          x_gc_u16_grabstr
                          x_gc_u32_grabstr))

  (define x-gc-free
    (let ([proc (pointer->procedure void x_gc_free '(*))])
      (lambda (p)
        "@code{(x-gc-free p)} frees the garbage-collectible block at
pointer @var{p}, using x_gc_free(3)."
        (proc p))))

  (define x-gc-malloc
    (let ([proc (pointer->procedure '* x_gc_malloc `(,size_t))])
      (lambda (size)
        "@code{(x-gc-malloc size)} allocates a block of memory,
using x_gc_malloc(3), and returns a pointer."
        (proc size))))

  (define x-gc-malloc-atomic
    (let ([proc (pointer->procedure '* x_gc_malloc_atomic `(,size_t))])
      (lambda (size)
        "@code{(x-gc-malloc-atomic size)} allocates a block of memory,
using x_gc_malloc_atomic(3), and returns a pointer."
        (proc size))))

  (define x-gc-malloc-uncollectable
    (let ([proc (pointer->procedure '* x_gc_malloc_uncollectable `(,size_t))])
      (lambda (size)
        "@code{(x-gc-malloc-uncollectable size)} allocates a block of memory,
using x_gc_malloc_uncollectable(3), and returns a pointer."
        (proc size))))

  (define x-gc-malloc-uncollectible
    (let ([proc (pointer->procedure '* x_gc_malloc_uncollectible `(,size_t))])
      (lambda (size)
        "@code{(x-gc-malloc-uncollectible size)} allocates a block of memory,
using x_gc_malloc_uncollectible(3), and returns a pointer."
        (proc size))))

  (define x-gc-realloc
    (let ([proc (pointer->procedure '* x_gc_malloc `(* ,size_t))])
      (lambda (p size)
        "@code{(x-gc-realloc size)} reallocates a block of memory,
using x_gc_realloc(3), and returns a pointer."
        (proc p size))))

  (define x-gc-malloc-ignore-off-page
    (let ([proc (pointer->procedure '* x_gc_malloc_ignore_off_page `(,size_t))])
      (lambda (size)
        "@code{(x-gc-malloc-ignore-off-page size)} allocates a block of memory,
using x_gc_malloc_ignore_off_page(3), and returns a pointer."
        (proc size))))

  (define x-gc-malloc-atomic-ignore-off-page
    (let ([proc (pointer->procedure '* x_gc_malloc_atomic_ignore_off_page `(,size_t))])
      (lambda (size)
        "@code{(x-gc-malloc-atomic-ignore-off-page size)} allocates a block of memory,
using x_gc_malloc_atomic_ignore_off_page(3), and returns a pointer."
        (proc size))))

  (define x-gc-malloc-stubborn
    (let ([proc (pointer->procedure '* x_gc_malloc_stubborn `(,size_t))])
      (lambda (size)
        "@code{(x-gc-malloc-stubborn size)} allocates a block of memory,
using x_gc_malloc_stubborn(3), and returns a pointer."
        (proc size))))

  (define x-gc-strdup
    (let ([proc (pointer->procedure '* x_gc_strdup '(*))])
      (lambda (p)
        "@code{(x-gc-strdup p)} copies the null-terminated string pointed to
  by @var{p}, using x_gc_strdup(3), and returns a pointer."
        (proc p))))

  (define x-gc-strndup
    (let ([proc (pointer->procedure '* x_gc_strndup `(* ,size_t))])
      (lambda (p n)
        "@code{(x-gc-strndup p)} copies from the string pointed to
  by @var{p}, using x_gc_strndup(3), and returns a pointer."
        (proc p n))))

  (define x-gc-grabstr
    (let ([proc (pointer->procedure '* x_gc_grabstr '(*))])
      (lambda (p)
        "@code{(x-gc-grabstr p)} copies and then frees the
null-terminated, malloced string pointed to by @var{p}, using
x_gc_grabstr(3), and returns a pointer."
        (proc p))))

  (define x-gc-u8-grabstr
    (let ([proc (pointer->procedure '* x_gc_u8_grabstr '(*))])
      (lambda (p)
        "@code{(x-gc-u8_grabstr p)} copies and then frees the
null-terminated, malloced UTF-8 string pointed to by @var{p}, using
x_gc_u8_grabstr(3), and returns a pointer."
        (proc p))))

  (define x-gc-u16-grabstr
    (let ([proc (pointer->procedure '* x_gc_u16_grabstr '(*))])
      (lambda (p)
        "@code{(x-gc-u16_grabstr p)} copies and then frees the
null-terminated, malloced UTF-16 string pointed to by @var{p}, using
x_gc_u16_grabstr(3), and returns a pointer."
        (proc p))))

  (define x-gc-u32-grabstr
    (let ([proc (pointer->procedure '* x_gc_u32_grabstr '(*))])
      (lambda (p)
        "@code{(x-gc-u32_grabstr p)} copies and then frees the
null-terminated, malloced UTF-32 string pointed to by @var{p}, using
x_gc_u32_grabstr(3), and returns a pointer."
        (proc p))))

  ) ;; end of library.
