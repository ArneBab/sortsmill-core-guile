include(`m4citrus.m4')dnl -*- mode: scheme; coding: utf-8 -*-
;; Copyright (C) 2013, 2014 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.
m4_divert([-1])

m4_changecom([;;])

m4_define([rbmap_exports],[dnl
   make-$1       ; (make-$1) → $1
   $1?           ; (rbmap? obj) → boolean
   $1-set!       ; ($1-set! $1 key value) → *unspecified*
   $1-delete!    ; ($1-delete! $1 key) → *unspecified*
   $1-ref        ; ($1-ref $1 key [[default-value]]) → value
   $1            ; Procedure with setter
   $1-fold-left  ; ($1-fold-left proc init map [[start-key]]) → result
   $1-fold-right ; ($1-fold-right proc init map [[start-key]]) → result
   alist->$1     ; (alist->$1 alist) → $1
   $1->alist     ; ($1->alist $1) → alist
   plist->$1     ; (alist->$1 plist) → $1
   $1->plist     ; ($1->alist $1) → plist
   $1-keys       ; ($1-keys $1) → list
   $1-values     ; ($1-values $1) → list
   $1-map->list  ; ($1-map->list proc map) → list
   $1-for-each   ; ($1-for-each proc map) → *unspecified*
   $1-count      ; ($1-count pred map) → integer
   $1-size       ; ($1-size map) → integer
   $1-null?      ; ($1-null? map) → boolean
])

m4_define([rbmap_foreign_funcs],[dnl
                          scm_make_$1
                          scm_$1_set_x
                          scm_$1_delete_x
                          scm_$1_ref
                          scm_$1_fold_left
                          scm_$1_fold_right
                          scm_alist_to_$1
                          scm_$1_to_alist
                          scm_plist_to_$1
                          scm_$1_to_plist
                          scm_$1_keys
                          scm_$1_values
                          scm_$1_map_to_list
                          scm_$1_for_each
                          scm_$1_count
                          scm_$1_size
                          scm_$1_null_p
])

m4_define([rbmap_procedures],[dnl
  (define make-$1
    (let ([[make-T (lambda-scm-procedure () scm_make_$1)]]
          [[plist->T (lambda-scm-procedure (plist) scm_plist_to_$1)]])
      (case-lambda
        [[() (make-T)]]
        [[(. arguments) (plist->T arguments)]])))
  (set-procedure-property! make-$1 'documentation "Create a new `$1',
an integer-keyed map using type @code{$2} internally for the
keys. Accepts alternating key-value arguments. See also the C
functions scm_make_$1(3) and scm_plist_to_$1(3).")

  (define-wrapped-pointer-type $1
    $1? pointer->$1 $1->pointer
    [[lambda (obj port)
      (format port "#<$1 0x~x>"
              (pointer-address ($1->pointer obj)))]] )
  (set-procedure-property! $1? 'documentation "Return @code{#t} if the
argument is an $1. Otherwise return @code{#f}.")

  (define-scm-procedure ($1-set! map key value)
    "Bind @var{key} to @var{value} in the $1 @var{map}. The return
value is unspecified. See also the C function scm_$1_set_x(3)."
    scm_$1_set_x)

  (define-scm-procedure ($1-delete! map key)
    "Delete the binding of @var{key} in the $1 @var{map}, if there
is such a binding. Otherwise make no change. The return value is
unspecified. See also the C function scm_$1_delete_x(3)."
    scm_$1_delete_x)

  (define*-scm-procedure ($1-ref map key #:optional default-value)
    "Return the value bound to @var{key} in the $1 @var{map},
or return @var{default-value} if no such binding exists. If
@var{default-value} is not provided, return @code{#f} instead. See
also the C function scm_$1_ref(3)."
    scm_$1_ref)

  (define rbmap (make-procedure-with-setter $1-ref $1-set!))

  (define*-scm-procedure ($1-fold-left proc init map #:optional start-key)
    "Fold the $1 @var{map}, in order of increasing keys. Folding
starts at the first key greater than or equal to @{start-key}, if it
is provided; otherwise folding starts with the least key. The
procedure @var{proc} takes arguments in the order @code{(prior key
value)}. See also the C function scm_$1_fold_left(3)."
    scm_$1_fold_left)

  (define*-scm-procedure ($1-fold-right proc init map #:optional start-key)
    "Fold the $1 @var{map}, in order of decreasing keys. Folding
starts at the first key less than or equal to @{start-key}, if it is
provided; otherwise folding starts with the greatest key. The
procedure @var{proc} takes arguments in the order @code{(key value
prior)}. See also the C function scm_$1_fold_right(3)."
    scm_$1_fold_right)

  (define-scm-procedure (alist->$1 alist)
    "Convert an integer-keyed association list to a map using type
@code{$2} internally for the keys. See also the C function
scm_alist_to_$1(3)."
    scm_alist_to_$1)

  (define-scm-procedure ($1->alist map)
    "Return an association list that has the same bindings as the
$1 @var{map}, in key order. See also the C function
scm_$1_to_alist(3)."
    scm_$1_to_alist)

  (define-scm-procedure (plist->$1 plist)
    "Convert an integer-keyed `property list' to a map using type
@code{$2} internally for the keys. See also the C function
scm_plist_to_$1(3)."
    scm_plist_to_$1)

  (define-scm-procedure ($1->plist map)
    "Return a `property list' that has the same bindings as the $1
@var{map}, in key order. See also the C function scm_$1_to_plist(3)."
    scm_$1_to_plist)

  (define-scm-procedure ($1-keys map)
    "Return a list that has the same keys as the $1 @var{map}, in
key order. See also the C function scm_$1_keys(3)."
    scm_$1_keys)

  (define-scm-procedure ($1-values map)
    "Return a list that has the same values as the $1 @var{map},
in key order. See also the C function scm_$1_values(3)."
    scm_$1_values)

  (define-scm-procedure ($1-map->list proc map)
    "Apply the procedure @var{proc}, taking arguments @code{(key
value)}, to the entries of $1 @var{map}, producing a list of the
individual results. The list order corresponds to increasing order of
the map's keys. The order of application is unspecified. See also the
C function scm_$1_map_to_list(3)."
    scm_$1_map_to_list)

  (define-scm-procedure ($1-for-each proc map)
    "Apply the procedure @var{proc}, taking arguments @code{(key
value)}, to the entries of $1 @var{map}, throwing away the individual
results. Application is in order of increasing keys. Return value are
unspecified. See also the C function scm_$1_for_each(3)."
scm_$1_for_each)

  (define-scm-procedure ($1-count pred map)
    "Return the number of bindings in $1 @var{map} that satisfy
the predicate @code{(pred key value)}. See also the C function
scm_$1_count(3)."
    scm_$1_count)

  (define-scm-procedure ($1-size map)
    "Return the number of bindings in the $1 @var{map}. See also
the C function scm_$1_size(3)."
    scm_$1_size)

  (define-scm-procedure ($1-null? map)
    "Is the $1 @var{map} empty? See also the C functions
scm_$1_null_p(3) and scm_$1_is_null(3)."
    scm_$1_null_p)

  ;;-----------------------------------------------------------------------
])

m4_divert([])dnl

(library (sortsmill core rbmap generic)

  ;; Integer-keyed maps based on the left-leaning 2-3 red-black trees
  ;; implementation of <sortsmill/core/rb.h>.

  ;; FIXME: Write the manpages to which the doc-strings refer.
  ;;
  ;; FIXME: Provide a full repertoire of integer types.
  ;;
  ;; FIXME: Come up with better names than ‘rbmapi’, ‘rbmaps’, and ‘rbmapz’.
  ;; (Probably ‘rbmapu32’ is an okay name.)

  (export

rbmap_exports([rbmapu8])
rbmap_exports([rbmapu16])
rbmap_exports([rbmapu32])
rbmap_exports([rbmapu64])
rbmap_exports([rbmapui])
rbmap_exports([rbmapul])
rbmap_exports([rbmapusz])
rbmap_exports([rbmapumx])

rbmap_exports([rbmaps8])
rbmap_exports([rbmaps16])
rbmap_exports([rbmaps32])
rbmap_exports([rbmaps64])
rbmap_exports([rbmapsi])
rbmap_exports([rbmapsl])
rbmap_exports([rbmapssz])
rbmap_exports([rbmapsmx])

rbmap_exports([rbmapq])
   )

  (import (rnrs)
          (except (guile) error)
          (system foreign)
          (ice-9 format)
          (sortsmill core helpers)
          (sortsmill core scm-procedures))

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile)

rbmap_foreign_funcs([rbmapu8])
rbmap_foreign_funcs([rbmapu16])
rbmap_foreign_funcs([rbmapu32])
rbmap_foreign_funcs([rbmapu64])
rbmap_foreign_funcs([rbmapui])
rbmap_foreign_funcs([rbmapul])
rbmap_foreign_funcs([rbmapusz])
rbmap_foreign_funcs([rbmapumx])

rbmap_foreign_funcs([rbmaps8])
rbmap_foreign_funcs([rbmaps16])
rbmap_foreign_funcs([rbmaps32])
rbmap_foreign_funcs([rbmaps64])
rbmap_foreign_funcs([rbmapsi])
rbmap_foreign_funcs([rbmapsl])
rbmap_foreign_funcs([rbmapssz])
rbmap_foreign_funcs([rbmapsmx])

rbmap_foreign_funcs([rbmapq])
                          ))

rbmap_procedures([rbmapu8], [uint8_t])
rbmap_procedures([rbmapu16], [uint16_t])
rbmap_procedures([rbmapu32], [uint32_t])
rbmap_procedures([rbmapu64], [uint64_t])
rbmap_procedures([rbmapui], [unsigned int])
rbmap_procedures([rbmapul], [unsigned long int])
rbmap_procedures([rbmapusz], [size_t])
rbmap_procedures([rbmapumx], [uintmax_t])

rbmap_procedures([rbmaps8], [int8_t])
rbmap_procedures([rbmaps16], [int16_t])
rbmap_procedures([rbmaps32], [int32_t])
rbmap_procedures([rbmaps64], [int64_t])
rbmap_procedures([rbmapsi], [int])
rbmap_procedures([rbmapsl], [long int])
rbmap_procedures([rbmapssz], [ssize_t])
rbmap_procedures([rbmapsmx], [intmax_t])

rbmap_procedures([rbmapq], [intptr_t])

  ) ;; end of library.
