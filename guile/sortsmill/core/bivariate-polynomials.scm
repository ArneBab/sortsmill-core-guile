;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2015 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core bivariate-polynomials)

;;; Bivariate polynomials represented as square rank-2 arrays.

  (export exact-bipoly-add
          exact-bipoly-sub
          exact-bipoly-mul
          exact-bipoly-scalarmul
          exact-bipoly-partial-deriv-wrt-x
          exact-bipoly-partial-deriv-wrt-y

          f64bipoly-add dbipoly-add
          f64bipoly-sub dbipoly-sub
          f64bipoly-mul dbipoly-mul
          f64bipoly-scalarmul dbipoly-scalarmul

          bipoly->pretty-string)

  (import (rnrs)
          (only (srfi :1) iota list-tabulate zip)
          (ice-9 match)
          (ice-9 format)
          (except (guile) error)
          (system foreign)
          (sortsmill core helpers)
          (sortsmill core scm-procedures))

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile)

                          scm_exact_bipoly_add
                          scm_exact_bipoly_sub
                          scm_exact_bipoly_mul
                          scm_exact_bipoly_scalarmul
                          scm_exact_bipoly_partial_deriv_wrt_x
                          scm_exact_bipoly_partial_deriv_wrt_y

                          scm_dbipoly_add
                          scm_dbipoly_sub
                          scm_dbipoly_mul
                          scm_dbipoly_scalarmul))

  (define-scm-procedure (exact-bipoly-add a b)
    "FIXME: Document this."
    scm_exact_bipoly_add)

  (define-scm-procedure (exact-bipoly-sub a b)
    "FIXME: Document this."
    scm_exact_bipoly_sub)

  (define-scm-procedure (exact-bipoly-mul a b)
    "FIXME: Document this."
    scm_exact_bipoly_mul)

  (define-scm-procedure (exact-bipoly-scalarmul a r)
    "FIXME: Document this."
    scm_exact_bipoly_scalarmul)

  (define-scm-procedure (exact-bipoly-partial-deriv-wrt-x a)
    "FIXME: Document this."
    scm_exact_bipoly_partial_deriv_wrt_x)

  (define-scm-procedure (exact-bipoly-partial-deriv-wrt-y a)
    "FIXME: Document this."
    scm_exact_bipoly_partial_deriv_wrt_y)

  (define-scm-procedure (dbipoly-add a b)
    "FIXME: Document this."
    scm_dbipoly_add)

  (define-scm-procedure (dbipoly-sub a b)
    "FIXME: Document this."
    scm_dbipoly_sub)

  (define-scm-procedure (dbipoly-mul a b)
    "FIXME: Document this."
    scm_dbipoly_mul)

  (define-scm-procedure (dbipoly-scalarmul a r)
    "FIXME: Document this."
    scm_dbipoly_scalarmul)

  (define f64bipoly-add dbipoly-add)
  (define f64bipoly-sub dbipoly-sub)
  (define f64bipoly-mul dbipoly-mul)
  (define f64bipoly-scalarmul dbipoly-scalarmul)

  (define (bipoly->pretty-string poly)
    "Pretty-print a bivariate polynomial, returning a
string. (Printing the string may require a UTF-8 locale or similar.)"
    (assert (= 2 (array-rank poly)))
    (multipoly:pretty-print poly))

  ;;--------------------------------------------------------------------
  ;;
  ;; Implementation of pretty-printing for multivariate polynomials
  ;; represented as approximately-half-populated hypercube-shaped
  ;; arrays.

  (define (multipoly:by-degrees poly)
    (if poly
        [begin
          (assert (apply = (array-dimensions poly)))
          (let ([n (car (array-dimensions poly))])
            (fold-left
             (lambda (prior degree)
               (cons (fold-left
                      (lambda (prior^ i)
                        (let ([j (- degree i)])
                          (cons (array-ref poly i j) prior^)))
                      '()
                      (iota (+ degree 1) 0))
                     prior))
             '()
             (iota n (- n 1) -1)))]
        #f))

  (define (multipoly:pretty-print poly)
    (let ([poly (if (array? poly) (multipoly:by-degrees poly) poly)])
      (if poly
          [let ([terms (apply append (map terms-for-degree poly))])
            (if (null? terms)
                "0"
                (string-join terms " + "))]
          #f)))

  (define (terms-for-degree coefs)
    (let ([p (- (length coefs) 1)])
      (map format-term
           (filter (lambda (e) (not (= (car e) 0)))
                   (zip coefs (basis-for-degree p))))))

  (define (format-term term)
    (cond
     [(string=? (cadr term) "1") (format #f "~a" (car term))]
     [(= (car term) 1) (format #f "~a" (cadr term))]
     [(= (car term) -1) (format #f "-~a" (cadr term))]
     [else (format #f "~a~a" (car term) (cadr term))] ))

  (define (basis-for-degree p)
    (case p
      [(0) '("1")]
      [else
       (let ([x-count (lambda (i) (- p i))]
             [y-count (lambda (i) i)]
             [show-x  (lambda (n)
                        (case n
                          [(0) ""]
                          [(1) "x"]
                          [else (format #f "x~a"
                                        (integer->superscript n))]))]
             [show-y  (lambda (n)
                        (case n
                          [(0) ""]
                          [(1) "y"]
                          [else (format #f "y~a"
                                        (integer->superscript n))]))])
         (list-tabulate (+ p 1)
                        (lambda (i)
                          (format #f "~a~a"
                                  (show-x (x-count i))
                                  (show-y (y-count i))))))] ))

  (define (integer->superscript n)
    (string-map char->superscript (format #f "~d" n)))

  (define (char->superscript d)
    ;; Note that to display these superscripts you will have to switch
    ;; to a compatible locale. Guile’s default locale is inadequate.
    (match d
      [#\0 #\⁰]
      [#\1 #\¹]
      [#\2 #\²]
      [#\3 #\³]
      [#\4 #\⁴]
      [#\5 #\⁵]
      [#\6 #\⁶]
      [#\7 #\⁷]
      [#\8 #\⁸]
      [#\9 #\⁹]
      [#\- #\⁻]
      [other other]))

  ;;--------------------------------------------------------------------

  ) ;; end of library.
