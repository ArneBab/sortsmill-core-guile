;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2014 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core lists)

  ;;
  ;; Procedures for working with lists.
  ;;
  ;; Some of these may have been written as diversions rather than out
  ;; of need.
  ;;

  (export
   reverse-until ;; (reverse-until lst n) → lst1, lst2
   split-before  ;; (split-before lst n) → lst1, lst2
   )

  (import (rnrs)
          (except (guile) error)
          (system foreign)
          (sortsmill core helpers)
          (sortsmill core scm-procedures))

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile)
                          scm_reverse_until
                          scm_split_before))

  (define-scm-procedure (reverse-until lst n)
    "Return two lists; the former comprises the initial @code{n}
elements of @code{lst}, in reverse order, and the latter comprises the
remaining elements in their original order."
    scm_reverse_until)

  (define-scm-procedure (split-before lst n)
    "Return two lists; the former comprises the initial @code{n}
elements of @code{lst}, and the latter comprises the remaining
elements.

This procedure is synonymous with @code{split-at} of SRFI-1, but is a
different implementation."
    scm_split_before)

  ) ;; end of library.
