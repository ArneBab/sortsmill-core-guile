;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill_core_guile psMat)

  ;;
  ;; PostScript matrices as Python six-tuples.
  ;;

  (export)

  (import (rnrs base)
          (sortsmill core python py-psmat))

  (define (PyInit-sortsmill_core_guile.psMat . ignored-arguments)
    "Create the sortsmill_core_guile.psMat module."
    (make-py-psmat-module "sortsmill_core_guile.psMat"))

  ) ;; end of library.
