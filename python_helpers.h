/* *INDENT-OFF*  Indent misbehaves on this file. */
/*
 * Copyright (C) 2013 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Guile.
 * 
 * Sorts Mill Core Guile is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Guile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SMCOREGUILE_PYTHON_HELPERS_H
#define _SMCOREGUILE_PYTHON_HELPERS_H

#include <Python.h>
#include <stdbool.h>
#include <libguile.h>
#include <sortsmill/core.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

/*-----------------------------------------------------------------------*/

#if PY_MAJOR_VERSION < 3

PyMODINIT_FUNC initsortsmill_core_guile (void);

static inline PyMODINIT_FUNC
PyInit_sortsmill_core_guile (void)
{
  initsortsmill_core_guile ();
}

#else                           // !(PY_MAJOR_VERSION < 3)

PyMODINIT_FUNC PyInit_sortsmill_core_guile (void);

#endif // !(PY_MAJOR_VERSION < 3)

int PySCM_Ready (void);

/*-----------------------------------------------------------------------*/

//
// FIXME: Use these in more places where they ought to be used.
//

void py_exc_check_violation (const char *name, SCM irritants);

#define py_exc_check(assertion, name, irritants)        \
  do                                                    \
    {                                                   \
      if (!(assertion))                                 \
        py_exc_check_violation ((name), (irritants));   \
    }                                                   \
  while (false);

/*-----------------------------------------------------------------------*/

/* Macros for easy definition of Guile-Python interface functions. */

#define SCM_PYOBJECT_UNARY_OP(PYNAME, CNAME, APIFUNC)           \
  SCM                                                           \
  CNAME (SCM obj)                                               \
  {                                                             \
    return _scm_pyobject_unary_op (obj, (PYNAME), (APIFUNC));   \
  }                                                             \

#define SCM_PYOBJECT_BINARY_OP(PYNAME, CNAME, APIFUNC)                  \
  SCM                                                                   \
  CNAME (SCM obj1, SCM obj2)                                            \
  {                                                                     \
    return _scm_pyobject_binary_op (obj1, obj2, (PYNAME), (APIFUNC));   \
  }                                                                     \

#define SCM_PYOBJECT_TERNARY_OP(PYNAME, CNAME, APIFUNC)         \
  SCM                                                           \
  CNAME (SCM obj1, SCM obj2, SCM obj3)                          \
  {                                                             \
    return _scm_pyobject_ternary_op (obj1, obj2, obj3,          \
                                     (PYNAME), (APIFUNC));      \
  }                                                             \

#define SCM_PYOBJECT_UNARY_INTTYPE(INTTYPE, PYNAME, CNAME, APIFUNC)     \
  INTTYPE                                                               \
  CNAME (SCM obj)                                                       \
  {                                                                     \
    return _scm_pyobject_unary_##INTTYPE (obj, (PYNAME), (APIFUNC));    \
  }                                                                     \

#define SCM_PYOBJECT_BINARY_INTTYPE(INTTYPE, PYNAME, CNAME, APIFUNC)    \
  INTTYPE                                                               \
  CNAME (SCM obj1, SCM obj2)                                            \
  {                                                                     \
    return _scm_pyobject_binary_##INTTYPE (obj1, obj2, (PYNAME),        \
                                           (APIFUNC));                  \
  }                                                                     \

#define SCM_PYOBJECT_TERNARY_INTTYPE(INTTYPE, PYNAME, CNAME, APIFUNC)   \
  INTTYPE                                                               \
  CNAME (SCM obj1, SCM obj2, SCM obj3)                                  \
  {                                                                     \
    return _scm_pyobject_ternary_##INTTYPE (obj1, obj2, obj3, (PYNAME), \
                                            (APIFUNC));                 \
  }                                                                     \

#define SCM_PYOBJECT_UNARY_BOOL(PYNAME, CNAME, APIFUNC)         \
  bool                                                          \
  CNAME (SCM obj)                                               \
  {                                                             \
    return _scm_pyobject_unary_bool (obj, (PYNAME), (APIFUNC)); \
  }                                                             \

#define SCM_PYOBJECT_BINARY_BOOL(PYNAME, CNAME, APIFUNC)                \
  bool                                                                  \
  CNAME (SCM obj1, SCM obj2)                                            \
  {                                                                     \
    return _scm_pyobject_binary_bool (obj1, obj2, (PYNAME), (APIFUNC)); \
  }                                                                     \

#define SCM_PYOBJECT_TERNARY_BOOL(PYNAME, CNAME, APIFUNC)               \
  bool                                                                  \
  CNAME (SCM obj1, SCM obj2, SCM obj3)                                  \
  {                                                                     \
    return _scm_pyobject_ternary_bool (obj1, obj2, obj3, (PYNAME),      \
                                       (APIFUNC));                      \
  }                                                                     \

#define SCM_PYOBJECT_UNARY_UNSPEC(PYNAME, CNAME, APIFUNC)               \
  SCM                                                                   \
  CNAME (SCM obj)                                                       \
  {                                                                     \
    return _scm_pyobject_unary_unspec (obj, (PYNAME), (APIFUNC));       \
  }                                                                     \

#define SCM_PYOBJECT_BINARY_UNSPEC(PYNAME, CNAME, APIFUNC)      \
  SCM                                                           \
  CNAME (SCM obj1, SCM obj2)                                    \
  {                                                             \
    return _scm_pyobject_binary_unspec (obj1, obj2, (PYNAME),   \
                                        (APIFUNC));             \
  }                                                             \

#define SCM_PYOBJECT_TERNARY_UNSPEC(PYNAME, CNAME, APIFUNC)             \
  SCM                                                                   \
  CNAME (SCM obj1, SCM obj2, SCM obj3)                                  \
  {                                                                     \
    return _scm_pyobject_ternary_unspec (obj1, obj2, obj3, (PYNAME),    \
                                         (APIFUNC));                    \
  }                                                                     \

// Mainly for wrapping PyWhatever_Check() and PyWhatever_CheckExact()
// macros
#define SCM_PYOBJECT_UNARY_BOOLCHECK(CNAME, APIFUNC)            \
  bool                                                          \
  CNAME (SCM obj)                                               \
  {                                                             \
    bool result;                                                \
    if (scm_is_pyobject (obj))                                  \
      {                                                         \
        PyObject *_obj = scm_to_PyObject_ptr (obj);             \
        result = (_obj != NULL && APIFUNC (_obj));              \
      }                                                         \
    else                                                        \
      result = false;                                           \
    return result;                                              \
  }

// Wrap PyWhatever_Check() and PyWhatever_CheckExact() macros.
#define SCM_PYOBJECT_UNARY_WRAPCHECKS(LOWERCASE, CAPITALIZED)           \
  VISIBLE SCM_PYOBJECT_UNARY_BOOLCHECK (scm_is_##LOWERCASE,             \
                                        CAPITALIZED##_Check);           \
  VISIBLE SCM_PYOBJECT_UNARY_BOOLCHECK (scm_is_##LOWERCASE##_exact,     \
                                        CAPITALIZED##_CheckExact);      \
  VISIBLE SCM                                                           \
  scm_##LOWERCASE##_p (SCM obj)                                         \
  {                                                                     \
    return (scm_is_##LOWERCASE (obj)) ? SCM_BOOL_T : SCM_BOOL_F;        \
  }                                                                     \
  VISIBLE SCM                                                           \
  scm_##LOWERCASE##_exact_p (SCM obj)                                   \
  {                                                                     \
    return                                                              \
      (scm_is_##LOWERCASE##_exact (obj)) ? SCM_BOOL_T : SCM_BOOL_F;     \
  }                                                                     \

// Wrap only the PyWhatever_Check() macro.
#define SCM_PYOBJECT_UNARY_WRAPCHECK_ONLY(LOWERCASE, CAPITALIZED)       \
  VISIBLE SCM_PYOBJECT_UNARY_BOOLCHECK (scm_is_##LOWERCASE,             \
                                        CAPITALIZED##_Check);           \
  VISIBLE SCM                                                           \
  scm_##LOWERCASE##_p (SCM obj)                                         \
  {                                                                     \
    return (scm_is_##LOWERCASE (obj)) ? SCM_BOOL_T : SCM_BOOL_F;        \
  }                                                                     \

SCM _scm_pyobject_unary_op (SCM obj, const char *name,
                            PyObject *(*api_func) (PyObject *));

SCM _scm_pyobject_binary_op (SCM obj1, SCM obj2, const char *name,
                             PyObject *(*api_func) (PyObject *, PyObject *));

SCM _scm_pyobject_ternary_op (SCM obj1, SCM obj2, SCM obj3,
                              const char *name,
                              PyObject *(*api_func) (PyObject *, PyObject *,
                                                     PyObject *));

#define _SCM_PYOBJECT_INTTYPE_DECL(INTTYPE)                             \
  INTTYPE _scm_pyobject_unary_##INTTYPE (SCM obj,                       \
                                         const char *name,              \
                                         INTTYPE (*api_func)            \
                                         (PyObject *));                 \
                                                                        \
  INTTYPE _scm_pyobject_binary_##INTTYPE (SCM obj1, SCM obj2,           \
                                          const char *name,             \
                                          INTTYPE (*api_func)           \
                                          (PyObject *, PyObject *));    \
                                                                        \
  INTTYPE _scm_pyobject_ternary_##INTTYPE (SCM obj1, SCM obj2,          \
                                           SCM obj3,                    \
                                           const char *name,            \
                                           INTTYPE (*api_func)          \
                                           (PyObject *, PyObject *,     \
                                            PyObject *));

_SCM_PYOBJECT_INTTYPE_DECL (int);
_SCM_PYOBJECT_INTTYPE_DECL (long);
_SCM_PYOBJECT_INTTYPE_DECL (ssize_t);

bool _scm_pyobject_unary_bool (SCM obj, const char *name,
                               int (*api_func) (PyObject *));

bool _scm_pyobject_binary_bool (SCM obj1, SCM obj2, const char *name,
                                int (*api_func) (PyObject *, PyObject *));

bool _scm_pyobject_ternary_bool (SCM obj1, SCM obj2, SCM obj3,
                                 const char *name,
                                 int (*api_func) (PyObject *, PyObject *,
                                                  PyObject *));

SCM _scm_pyobject_unary_unspec (SCM obj, const char *name,
                                int (*api_func) (PyObject *));

SCM _scm_pyobject_binary_unspec (SCM obj1, SCM obj2, const char *name,
                                 int (*api_func) (PyObject *, PyObject *));

SCM _scm_pyobject_ternary_unspec (SCM obj1, SCM obj2, SCM obj3,
                                  const char *name,
                                  int (*api_func) (PyObject *, PyObject *,
                                                   PyObject *));

/*-----------------------------------------------------------------------*/

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* _SMCOREGUILE_PYTHON_HELPERS_H */
