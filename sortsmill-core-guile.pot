# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Khaled Hosny and Barry Schwartz
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: sortsmill-core-guile 1.0.0\n"
"Report-Msgid-Bugs-To: http://bitbucket.org/sortsmill/sortsmill-core-guile/"
"issues\n"
"POT-Creation-Date: 2015-09-30 13:43-0500\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../guile_bincoef.c:63
msgid ""
"scm_bincoef(n,k) is not implemented for n or k negative or greater than ~a"
msgstr ""

#: ../guile_bivariate_polynomials.c:76 ../guile_polynomials.c:2052
msgid "square rank-2 array"
msgstr ""

#: ../guile_brentroot.c:72 ../guile_brentroot.c:105
msgid "the function does not evaluate to opposite signs at these points"
msgstr ""

#: ../guile_brentroot.c:79 ../guile_brentroot.c:112
msgid "incorrect call to the C function"
msgstr ""

#: ../guile_hashmap_objects.c:178
msgid "expected a hashmap"
msgstr ""

#: ../guile_hashmap_objects.c:188
msgid "expected a hashmap cursor"
msgstr ""

#: ../guile_hobby.c:69 ../guile_hobby.c:182
msgid "too few points"
msgstr ""

#: ../guile_hobby.c:76 ../guile_hobby.c:189
msgid "wrong number of first tensions"
msgstr ""

#: ../guile_hobby.c:83 ../guile_hobby.c:196
msgid "wrong number of second tensions"
msgstr ""

#: ../guile_immutable_vectors.c:148
msgid "unexpected value for ivect type"
msgstr ""

#: ../guile_immutable_vectors.c:165 ../guile_immutable_vectors.c:1103
#: ../guile_immutable_vectors.c:1145 ../guile_immutable_vectors.c:1585
#: ../guile_immutable_vectors.c:1654
msgid "the end index must not be less than the start index"
msgstr ""

#: ../guile_immutable_vectors.c:178
msgid "the ivect is not long enough"
msgstr ""

#: ../guile_immutable_vectors.c:234
msgid "expected an untyped ivect"
msgstr ""

#: ../guile_immutable_vectors.c:245
msgid "expected a ~sivect"
msgstr ""

#: ../guile_immutable_vectors.c:1012 ../guile_immutable_vectors.c:1048
msgid "an attempt to pop more entries than there are"
msgstr ""

#: ../guile_immutable_vectors.c:1201
msgid "there are not enough entries for the new values"
msgstr ""

#: ../guile_kwargs.c:49
msgid "too many positional arguments"
msgstr ""

#: ../guile_linear_systems.c:48
msgid "the leading dimensions do not match"
msgstr ""

#: ../guile_linear_systems.c:55
msgid "this matrix is not square"
msgstr ""

#: ../guile_linear_systems.c:63 ../guile_linear_systems.c:127
msgid "the array dimensions are too great"
msgstr ""

#: ../guile_linear_systems.c:119
msgid "the dimensions are inconsistent"
msgstr ""

#: ../guile_linear_systems.c:146
msgid "the number of right hand sides is too great"
msgstr ""

#: ../guile_linear_systems.c:154
msgid "the system dimension must be at least 3x3"
msgstr ""

#: ../guile_lists.c:54
msgid "the list is too short"
msgstr ""

#: ../guile_polynomials.c:129
msgid "non-empty vector or list"
msgstr ""

#: ../guile_polynomials.c:236
msgid "polynomials of degrees that must be but are not equal"
msgstr ""

#: ../guile_polynomials.c:830
msgid "division by zero"
msgstr ""

#: ../guile_polynomials.c:1501
msgid "maximum number of points exceeded"
msgstr ""

#: ../guile_polynomials.c:1887 ../guile_polynomials.c:1934
msgid "the polynomial is identically zero"
msgstr ""

#: ../guile_polynomials.c:2016 ../guile_polynomials.c:2152
msgid "this polynomial degree is not supported"
msgstr ""

#: ../guile_polynomials.c:2024 ../guile_polynomials.c:2163
msgid "an error occurred during implicitization"
msgstr ""

#: ../guile_polynomials.c:2304 ../guile_polynomials.c:2316
msgid "degree greater than one"
msgstr ""

#: ../guile_polynomials.c:2346 ../guile_polynomials.c:2429
msgid "there are infinitely many solutions"
msgstr ""

#: ../guile_ps_number.c:78
msgid "not a valid PostScript numeral"
msgstr ""

#: ../guile_rbmap.c:507
msgid "illegal key"
msgstr ""

#: ../guile_rbmap.c:529
msgid "expected an alist"
msgstr ""

#: ../guile_rbmap.c:547
msgid "expected a list of even length"
msgstr ""

#: ../guile_rbmap.c:565
msgid "expected a list"
msgstr ""

#: ../guile_uniform_vectors.c:51
msgid "too few arguments"
msgstr ""

#: ../guile_vlists.c:1401 ../guile_vlists.c:1438
msgid "the vlst is not long enough to drop this many"
msgstr ""

#: ../guile_vlists.c:1419 ../guile_vlists.c:1457 ../guile_vlists.c:1477
msgid "the vlst is not long enough to take this many"
msgstr ""

#: ../guile_vlists.c:1503 ../guile_vlists.c:1528
msgid "the vlst is null"
msgstr ""

#: ../guile/sortsmill/core/rbmap/specialized.scm:60
#, scheme-format
msgid "unrecognized `only' elements: ~s"
msgstr ""

#: ../guile/sortsmill/core/rbmap/specialized.scm:69
#, scheme-format
msgid "unrecognized `except' elements: ~s"
msgstr ""

#: ../guile/sortsmill/core/rbmap/specialized.scm:187
#: ../guile/sortsmill/core/rbmap/specialized.scm:481
#: ../guile/sortsmill/core/rbmap/specialized.scm:753
msgid "unrecognized name in rename"
msgstr ""

#: ../guile/sortsmill/core/rbmap/specialized.scm:194
#: ../guile/sortsmill/core/rbmap/specialized.scm:488
#: ../guile/sortsmill/core/rbmap/specialized.scm:760
msgid "multiple rename of the same object"
msgstr ""

#: ../guile/sortsmill/core/rbmap/specialized.scm:215
#: ../guile/sortsmill/core/rbmap/specialized.scm:510
msgid "multiple key preprocessors specified"
msgstr ""

#: ../guile/sortsmill/core/rbmap/specialized.scm:229
#: ../guile/sortsmill/core/rbmap/specialized.scm:524
#: ../guile/sortsmill/core/rbmap/specialized.scm:801
msgid "multiple printers specified"
msgstr ""

#: ../guile/sortsmill/core/rbmap/specialized.scm:426
#: ../guile/sortsmill/core/rbmap/specialized.scm:700
#: ../guile/sortsmill/core/rbmap/specialized.scm:1016
msgid "unrecognized clause prefix"
msgstr ""

#: ../guile/sortsmill/core/rbmap/specialized.scm:431
#: ../guile/sortsmill/core/rbmap/specialized.scm:705
#: ../guile/sortsmill/core/rbmap/specialized.scm:1021
msgid "bad clause"
msgstr ""

#: ../guile/sortsmill/core/rbmap/specialized.scm:809
msgid "no keys specified"
msgstr ""

#: ../guile/sortsmill/core/brent-root-finder.scm:183
msgid "the root is not bracketed between these points"
msgstr ""

#: ../guile/sortsmill/core/delayed-evaluation.scm:113
msgid "attempt to apply a non-applicable delayed object"
msgstr ""

#: ../guile/sortsmill/core/helpers.scm:102
msgid ""
"Please set the environment variable SORTSMILL_PYTHON_VERSION to one of "
"\"2.6\", \"2.7\", \"3.2\", \"3.3\", etc."
msgstr ""

#: ../guile/sortsmill/core/helpers.scm:146
msgid "foreign function not found"
msgstr ""

#: ../guile/sortsmill/core/higher-order-procedures.scm:57
#: ../guile/sortsmill/core/higher-order-procedures.scm:127
#, scheme-format
msgid "expected at least this many arguments: ~d"
msgstr ""

#: ../guile/sortsmill/core/hobby.scm:153 ../guile/sortsmill/core/hobby.scm:161
msgid "a tension is less than 0.75 and bounds are being enforced"
msgstr ""

#: ../guile/sortsmill/core/hobby.scm:183
msgid "a curl is less than zero and bounds are being enforced"
msgstr ""

#: ../guile/sortsmill/core/hobby.scm:233
msgid "unrecognized Hobby guide entry"
msgstr ""

#: ../guile/sortsmill/core/hobby.scm:343
msgid "control point not in a pair"
msgstr ""

#: ../guile/sortsmill/core/hobby.scm:347
msgid "unrecognized token value"
msgstr ""

#: ../guile/sortsmill/core/kwargs.scm:138
msgid "malformed lambda/kwargs procedure"
msgstr ""

#: ../guile/sortsmill/core/kwargs.scm:223
msgid "expected `ARG-NAME' or `(ARG-NAME DEFAULT-VALUE)'"
msgstr ""

#: ../guile/sortsmill/core/libunicodenames.scm:81
msgid "failed to find a names db"
msgstr ""

#: ../guile/sortsmill/core/libunicodenames.scm:97
msgid "failed to open a names db"
msgstr ""

#: ../guile/sortsmill/core/libunicodenames.scm:114
#: ../guile/sortsmill/core/libunicodenames.scm:167
#: ../guile/sortsmill/core/libunicodenames.scm:195
msgid "expected a uninm-names-db handle"
msgstr ""

#: ../guile/sortsmill/core/libunicodenames.scm:164
#: ../guile/sortsmill/core/libunicodenames.scm:192
msgid "expected a character or integer between 0 and #xFFFFFFFF"
msgstr ""

#: ../guile/sortsmill/core/machine.scm:104
msgid "unrecognized size"
msgstr ""

#: ../guile/sortsmill/core/extensible-procedures.scm:97
msgid "not an extensible procedure"
msgstr ""

#: ../guile/sortsmill/core/extensible-procedures.scm:104
msgid "not an extensible procedure part"
msgstr ""

#: ../guile/sortsmill/core/extensible-procedures.scm:190
msgid "arguments not matched"
msgstr ""

#: ../guile/sortsmill/core/extensible-procedures.scm:256
msgid "conflicting fallback procedures and setters"
msgstr ""

#: ../guile/sortsmill/core/extensible-procedures.scm:258
msgid "conflicting fallback procedures"
msgstr ""

#: ../guile/sortsmill/core/extensible-procedures.scm:261
msgid "conflicting fallback setters"
msgstr ""

#: ../guile/sortsmill/core/po-files.scm:167
msgid "failed to read the specified PO file"
msgstr ""

#: ../guile/sortsmill/core/po-files.scm:204
msgid "illegal initializer"
msgstr ""

#: ../guile/sortsmill/core/po-files.scm:256
msgid "failed to create an iterator"
msgstr ""

#: ../guile/sortsmill/core/psmat.scm:75
msgid "second argument: expected a six-element vector"
msgstr ""

#: ../guile/sortsmill/core/psmat.scm:78
msgid "first argument: expected a six-element vector"
msgstr ""

#: ../guile/sortsmill/core/psmat.scm:130
msgid "expected a six-element vector"
msgstr ""

#: ../guile/sortsmill/core/scm-procedures.scm:68
#: ../guile/sortsmill/core/scm-procedures.scm:116
msgid "invalid rest argument"
msgstr ""

#: ../guile/sortsmill/core/scm-procedures.scm:84
msgid "invalid optional argument list"
msgstr ""

#: ../guile/sortsmill/core/scm-procedures.scm:102
#: ../guile/sortsmill/core/scm-procedures.scm:131
msgid "invalid argument list"
msgstr ""

#: ../guile/sortsmill/core/python/friendlier.scm:253
msgid "illegal del! form"
msgstr ""

#: ../guile/sortsmill/core/python/friendlier.scm:268
msgid "bases must be a list or pysequence"
msgstr ""

#: ../guile/sortsmill/core/python/friendlier.scm:278
msgid "unexpected type"
msgstr ""

#: ../guile/sortsmill/core/python/friendlier.scm:316
msgid "invalid arguments to py-call"
msgstr ""

#: ../guile/sortsmill/core/python/py-psmat.scm:90
msgid "second argument: expected a Python six-tuple"
msgstr ""

#: ../guile/sortsmill/core/python/py-psmat.scm:93
msgid "first argument: expected a Python six-tuple"
msgstr ""

#: ../guile/sortsmill/core/python/py-psmat.scm:149
msgid "expected a Python six-tuple"
msgstr ""

#: ../guile/sortsmill/core/python/pyexcs.scm:186
msgid "Python exception not supported"
msgstr ""

#: ../guile/sortsmill/core/python/pytype-maker.scm:46
msgid "expected a string, symbol, pyunicode, or pybytes"
msgstr ""

#: ../guile/sortsmill/core/python/pytype-maker.scm:53
msgid "expected a procedure or pycallable"
msgstr ""

#: ../guile/sortsmill/core/python/pytype-maker.scm:69
msgid "expected a base class"
msgstr ""

#: ../guile/sortsmill/core/python/pytype-maker.scm:77
msgid "expected a pydict"
msgstr ""

#: ../guile/sortsmill/core/python/pytype-maker.scm:168
msgid "unexpected arguments"
msgstr ""

#: ../guile/sortsmill/core/python/pytypes.scm:166
#, scheme-format
msgid "expected symbols from ~a"
msgstr ""

#: ../guile/sortsmill/core/python/shorthand.scm:69
msgid "py expression syntax error"
msgstr ""

#: ../guile/sortsmill/core/python/shorthand.scm:76
#, scheme-format
msgid "~0@*~a expects exactly this many arguments: ~1@*~a"
msgstr ""

#: ../guile/sortsmill/core/python/shorthand.scm:84
#, scheme-format
msgid "~0@*~a expects at least this many arguments: ~1@*~a"
msgstr ""

#: ../guile/sortsmill/core/python/shorthand.scm:92
#, scheme-format
msgid "~0@*~a expects at most this many arguments: ~1@*~a"
msgstr ""

#: ../guile/sortsmill/core/python/shorthand.scm:100
#, scheme-format
msgid "~0@*~a expects a number of arguments within this range: [~1@*~a,~2@*~a]"
msgstr ""

#: ../guile/sortsmill/core/python/shorthand.scm:105
#, scheme-format
msgid "~0@*~a: expected a Python identifier"
msgstr ""

#: ../guile/sortsmill/core/python/shorthand.scm:110
#, scheme-format
msgid "~0@*~a: expected a key-value pair"
msgstr ""

#: ../guile/sortsmill/core/python/shorthand.scm:115
#, scheme-format
msgid "~0@*~a: bad bytes elements: ~1@*~a"
msgstr ""

#: ../guile/sortsmill/core/python/shorthand.scm:120
#, scheme-format
msgid "~0@*~a: expected a string"
msgstr ""
