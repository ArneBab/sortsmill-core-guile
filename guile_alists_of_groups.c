#include <config.h>

// Copyright (C) 2014 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <stdbool.h>
#include <libintl.h>
#include <sortsmill/guile/core.h>

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

static inline bool
list_is_length_1 (SCM lst)
{
  return (!scm_is_null (lst) && scm_is_null (SCM_CDR (lst)));
}

static SCM
new_group_with_quick_look (const char *who, SCM elements,
                           SCM (*hash_ref) (SCM, SCM, SCM),
                           SCM (*hash_set_x) (SCM, SCM, SCM))
{
  SCM elems =
    (scm_is_vector (elements)) ? (scm_vector_to_list (elements)) : elements;

  SCM group = SCM_EOL;
  SCM quick_look = scm_make_hash_table (SCM_UNDEFINED);

  for (SCM p = elems; !scm_is_null (p); p = SCM_CDR (p))
    {
      SCM_ASSERT_TYPE ((scm_is_pair (p)), elements, SCM_ARGn, who,
                       "list or vector");
      if (scm_is_false (hash_ref (quick_look, SCM_CAR (p), SCM_BOOL_F)))
        {
          group = scm_cons (SCM_CAR (p), group);
          hash_set_x (quick_look, SCM_CAR (p), SCM_BOOL_T);
        }
    }

  return
    scm_list_2 (scm_vector (scm_reverse_x_without_checking (group, SCM_EOL)),
                quick_look);
}

static SCM
extend_group_with_quick_look (const char *who, SCM elements, SCM grp,
                              SCM (*hash_ref) (SCM, SCM, SCM),
                              SCM (*hash_set_x) (SCM, SCM, SCM))
{
  SCM elems =
    (scm_is_vector (elements)) ? (scm_vector_to_list (elements)) : elements;

  SCM group = SCM_EOL;
  SCM quick_look = scm_cadr (grp);

  for (SCM p = elems; !scm_is_null (p); p = SCM_CDR (p))
    {
      SCM_ASSERT_TYPE ((scm_is_pair (p)), elements, SCM_ARGn, who,
                       "list or vector");
      if (scm_is_false (hash_ref (quick_look, SCM_CAR (p), SCM_BOOL_F)))
        {
          group = scm_cons (SCM_CAR (p), group);
          hash_set_x (quick_look, SCM_CAR (p), SCM_BOOL_T);
        }
    }
  group = scm_append_x (scm_list_2 (scm_vector_to_list (SCM_CAR (grp)),
                                    scm_reverse_x_without_checking (group,
                                                                    SCM_EOL)));

  return scm_list_2 (scm_vector (group), quick_look);
}

static SCM
add_new_group_entry (SCM aog, SCM new_entry, bool cons_rather_than_append)
{
  SCM new_aog;
  if (cons_rather_than_append)
    new_aog = scm_cons (new_entry, aog);
  else
    new_aog = scm_append_x (scm_list_2 (aog, scm_list_1 (new_entry)));
  return new_aog;
}

static SCM
prepare_group (SCM group)
{
  if (!scm_is_null (group))
    {
      SCM first_elem = scm_car (group);
      if (scm_is_vector (first_elem))
        // Because of this branch, group elements cannot be
        // vectors. (We intend, however, to use alist-of-groups with
        // strings as elements.)
        group = scm_vector_to_list (first_elem);
    }
  return group;
}

static SCM
general_alist_of_groups_make (const char *who,
                              SCM aog_or_pairs, bool reversed,
                              SCM cons_add_x (SCM, SCM, SCM))
{
  SCM aog = SCM_EOL;
  for (SCM p = aog_or_pairs; !scm_is_null (p); p = SCM_CDR (p))
    {
      SCM_ASSERT_TYPE ((scm_is_pair (p)), aog_or_pairs, SCM_ARG1, who, "list");
      SCM entry = SCM_CAR (p);
      SCM_ASSERT_TYPE ((scm_is_pair (entry)), aog_or_pairs, SCM_ARG1, who,
                       "alist");
      SCM group = prepare_group (SCM_CDR (entry));
      aog = scm_alist_of_groups_cons_add_x (aog, SCM_CAR (entry), group);
    }
  return (reversed) ? aog : scm_reverse_x_without_checking (aog, SCM_EOL);
}

VISIBLE SCM
scm_alist_of_groups_make (SCM aog_or_pairs)
{
  return general_alist_of_groups_make ("alist-of-groups:make",
                                       aog_or_pairs, false,
                                       scm_alist_of_groups_cons_add_x);
}


VISIBLE SCM
scm_alist_of_groups_reverse (SCM aog_or_pairs)
{
  return general_alist_of_groups_make ("alist-of-groups:reverse",
                                       aog_or_pairs, true,
                                       scm_alist_of_groups_cons_add_x);
}

static SCM
general_alist_of_groups_ref (SCM aog, SCM key, SCM (*assoc) (SCM, SCM))
{
  SCM entry = assoc (key, aog);
  return (scm_is_true (entry)) ? scm_cadr (entry) : SCM_BOOL_F;
}

VISIBLE SCM
scm_alist_of_groups_ref (SCM aog, SCM key)
{
  return general_alist_of_groups_ref (aog, key, scm_assoc);
}

static SCM
general_alist_of_groups_set_x (const char *who,
                               SCM aog, SCM key, SCM value,
                               bool cons_rather_than_append,
                               SCM (*assoc) (SCM, SCM),
                               SCM (*hash_ref) (SCM, SCM, SCM),
                               SCM (*hash_set_x) (SCM, SCM, SCM))
{
  SCM group = prepare_group (value);
  SCM grp = new_group_with_quick_look (who, group, hash_ref, hash_set_x);
  SCM entry = assoc (key, aog);
  if (scm_is_true (entry))
    scm_set_cdr_x (entry, grp);
  else
    aog = add_new_group_entry (aog, scm_cons (key, grp),
                               cons_rather_than_append);
  return aog;
}

VISIBLE SCM
scm_alist_of_groups_set_x (SCM aog, SCM key, SCM value)
{
  return general_alist_of_groups_set_x ("alist-of-groups:set!",
                                        aog, key, value, false, scm_assoc,
                                        scm_hash_ref, scm_hash_set_x);
}

VISIBLE SCM
scm_alist_of_groups_cons_set_x (SCM aog, SCM key, SCM value)
{
  return general_alist_of_groups_set_x ("alist-of-groups:cons-set!",
                                        aog, key, value, true, scm_assoc,
                                        scm_hash_ref, scm_hash_set_x);
}

static SCM
general_alist_of_groups_add_x (const char *who,
                               SCM aog, SCM key, SCM elements,
                               bool cons_rather_than_append,
                               SCM (*assoc) (SCM, SCM),
                               SCM (*hash_ref) (SCM, SCM, SCM),
                               SCM (*hash_set_x) (SCM, SCM, SCM))
{
  SCM group = prepare_group (elements);
  if (!scm_is_null (group))
    {
      SCM entry = assoc (key, aog);
      if (scm_is_true (entry))
        {
          SCM grp = extend_group_with_quick_look (who, group, scm_cdr (entry),
                                                  hash_ref, hash_set_x);
          scm_set_cdr_x (entry, grp);
        }
      else
        {
          SCM grp =
            new_group_with_quick_look (who, group, hash_ref, hash_set_x);
          aog = add_new_group_entry (aog, scm_cons (key, grp),
                                     cons_rather_than_append);
        }
    }
  return aog;
}

VISIBLE SCM
scm_alist_of_groups_add_x (SCM aog, SCM key, SCM elements)
{
  return general_alist_of_groups_add_x ("alist-of-groups:add!",
                                        aog, key, elements, false, scm_assoc,
                                        scm_hash_ref, scm_hash_set_x);
}

VISIBLE SCM
scm_alist_of_groups_cons_add_x (SCM aog, SCM key, SCM elements)
{
  return general_alist_of_groups_add_x ("alist-of-groups:cons-add!",
                                        aog, key, elements, true, scm_assoc,
                                        scm_hash_ref, scm_hash_set_x);
}

static SCM
general_alist_of_groups_find_group (SCM aog, SCM element,
                                    SCM (*hash_ref) (SCM, SCM, SCM))
{
  SCM results[3];
  while (!scm_is_null (aog) &&
         scm_is_false (hash_ref (scm_caddar (aog), element, SCM_BOOL_F)))
    aog = scm_cdr (aog);
  if (scm_is_null (aog))
    {
      results[0] = SCM_BOOL_F;
      results[1] = SCM_BOOL_F;
      results[2] = SCM_EOL;     // Parts of aog not yet searched.
    }
  else
    {
      results[0] = scm_caar (aog);      // The group’s key.
      results[1] = scm_cadar (aog);     // The group’ value vector.
      results[2] = scm_cdr (aog);       // Parts of aog not yet searched.
    }
  return scm_c_values (results, 3);
}

VISIBLE SCM
scm_alist_of_groups_find_group (SCM aog, SCM element)
{
  return general_alist_of_groups_find_group (aog, element, scm_hash_ref);
}

VISIBLE SCM
scm_alist_of_groups_keys (SCM aog)
{
  SCM lst = SCM_EOL;
  while (!scm_is_null (aog))
    {
      SCM_ASSERT_TYPE ((scm_is_pair (aog)), aog, SCM_ARG1,
                       "alist-of-groups:keys", "list");
      lst = scm_cons (scm_car (SCM_CAR (aog)), lst);
      aog = SCM_CDR (aog);
    }
  return scm_reverse_x_without_checking (lst, SCM_EOL);
}

VISIBLE SCM
scm_alist_of_groups_values (SCM aog)
{
  SCM lst = SCM_EOL;
  while (!scm_is_null (aog))
    {
      SCM_ASSERT_TYPE ((scm_is_pair (aog)), aog, SCM_ARG1,
                       "alist-of-groups:values", "list");
      lst = scm_cons (scm_cadr (SCM_CAR (aog)), lst);
      aog = SCM_CDR (aog);
    }
  return scm_reverse_x_without_checking (lst, SCM_EOL);
}

VISIBLE SCM
scm_alist_of_groups_pairs (SCM aog)
{
  SCM lst = SCM_EOL;
  while (!scm_is_null (aog))
    {
      SCM_ASSERT_TYPE ((scm_is_pair (aog)), aog, SCM_ARG1,
                       "alist-of-groups:pairs", "list");
      SCM entry = SCM_CAR (aog);
      lst = scm_acons (scm_car (entry), scm_cadr (entry), lst);
      aog = SCM_CDR (aog);
    }
  return scm_reverse_x_without_checking (lst, SCM_EOL);
}

VISIBLE size_t
scm_c_alist_of_groups_element_count (SCM aog)
{
  size_t n = 0;
  for (SCM p = aog; !scm_is_null (p); p = SCM_CDR (p))
    {
      SCM_ASSERT_TYPE ((scm_is_pair (aog)), aog, SCM_ARG1,
                       "alist-of-groups:element-count", "list");
      n += scm_c_vector_length (scm_cadr (SCM_CAR (p)));
    }
  return n;
}

VISIBLE SCM
scm_alist_of_groups_element_count (SCM aog)
{
  return scm_from_size_t (scm_c_alist_of_groups_element_count (aog));
}

static SCM
general_alist_of_groups_duplicate_elements (SCM aog,
                                            SCM (*hash_ref) (SCM, SCM, SCM),
                                            SCM (*hash_set_x) (SCM, SCM, SCM))
{
  scm_t_array_handle handle;
  size_t n;
  ssize_t stride;

  SCM ht = scm_make_hash_table (SCM_UNDEFINED);
  SCM duplicated_elements = SCM_EOL;

  for (SCM p = aog; !scm_is_null (p); p = SCM_CDR (p))
    {
      SCM_ASSERT_TYPE ((scm_is_pair (aog)), aog, SCM_ARG1,
                       "alist-of-groups:find-duplicates", "list");
      scm_dynwind_begin (0);
      SCM entry = SCM_CAR (p);
      SCM key = scm_car (entry);
      const SCM *v =
        scm_vector_elements (scm_cadr (entry), &handle, &n, &stride);
      scm_dynwind_array_handle_release (&handle);
      for (size_t i = 0; i < n; i++)
        {
          SCM ht_entry = hash_ref (ht, *v, SCM_EOL);
          if (list_is_length_1 (ht_entry))
            duplicated_elements = scm_cons (*v, duplicated_elements);
          hash_set_x (ht, *v, scm_cons (key, ht_entry));
          v += stride;
        }
      scm_dynwind_end ();
    }

  SCM duplicates = SCM_EOL;
  for (SCM p = duplicated_elements; !scm_is_null (p); p = SCM_CDR (p))
    {
      SCM element = SCM_CAR (p);
      SCM keys = scm_reverse_x (hash_ref (ht, element, SCM_EOL), SCM_EOL);
      assert (!scm_is_null (keys));
      duplicates = scm_acons (element, keys, duplicates);
    }

  return duplicates;
}

VISIBLE SCM
scm_alist_of_groups_duplicate_elements (SCM aog)
{
  return general_alist_of_groups_duplicate_elements (aog, scm_hash_ref,
                                                     scm_hash_set_x);
}
