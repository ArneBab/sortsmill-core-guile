// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.


VISIBLE SCM
SCM_GAUSSELIMSOLVE (SCM A, SCM B)
{
  const char *who = GAUSSELIMSOLVE_STR;

  scm_dynwind_begin (0);

  scm_t_array_handle handle_A;
  scm_t_array_handle handle_B;

  scm_array_get_handle (A, &handle_A);
  scm_dynwind_array_handle_release (&handle_A);
  scm_array_get_handle (B, &handle_B);
  scm_dynwind_array_handle_release (&handle_B);

  SCM_ASSERT_TYPE ((scm_array_handle_rank (&handle_A) == 2),
                   A, SCM_ARG1, who, _("rank-2 array"));
  SCM_ASSERT_TYPE ((scm_array_handle_rank (&handle_B) == 2),
                   B, SCM_ARG2, who, _("rank-2 array"));
  const scm_t_array_dim *dims_A = scm_array_handle_dims (&handle_A);
  const scm_t_array_dim *dims_B = scm_array_handle_dims (&handle_B);
  const ssize_t m_A = dims_A[0].ubnd - dims_A[0].lbnd + 1;
  const ssize_t n_A = dims_A[1].ubnd - dims_A[1].lbnd + 1;
  const ssize_t m_B = dims_B[0].ubnd - dims_B[0].lbnd + 1;
  const ssize_t n_B = dims_B[1].ubnd - dims_B[1].lbnd + 1;

  check_augmented_matrix_dimensions (who, A, B, m_A, n_A, m_B, n_B);
  const int m = m_A;
  const int n = n_A + n_B;

  const REAL *A_data = READONLY_ELEMENTS (&handle_A);
  const REAL *B_data = READONLY_ELEMENTS (&handle_B);
  REAL *augmented_matrix = scm_malloc (m * n * sizeof (REAL));
  scm_dynwind_free (augmented_matrix);
  REAL **rows = scm_malloc (m * sizeof (REAL *));
  scm_dynwind_free (rows);
  for (int i = 0; i < m; i++)
    {
      rows[i] = &augmented_matrix[i * n];
      for (int j = 0; j < m; j++)
        rows[i][j] =
          A_data[(ssize_t) i * dims_A[0].inc + (ssize_t) j * dims_A[1].inc];
      for (int j = 0; j < n - m; j++)
        rows[i][j + m] =
          B_data[(ssize_t) i * dims_B[0].inc + (ssize_t) j * dims_B[1].inc];
    }

  int info;
  GAUSSELIMSOLVE (m, n, rows, NULL, &info);

  SCM X;
  if (info != 0)
    X = SCM_BOOL_F;
  else
    {
      X = scm_make_typed_array (SYMBOL_Fxx (), SCM_UNSPECIFIED,
                                scm_list_2 (scm_from_int (m),
                                            scm_from_int (n - m)));
      scm_t_array_handle handle;
      scm_array_get_handle (X, &handle);
      REAL *data = WRITABLE_ELEMENTS (&handle);
      for (int i = 0; i < m; i++)
        for (int j = 0; j < n - m; j++)
          {
            *data = rows[i][j + m];
            data++;
          }
      scm_array_handle_release (&handle);
    }

  scm_dynwind_end ();

  SCM values[2] = { X, scm_from_int (info) };
  return scm_c_values (values, 2);
}
