#include <config.h>

// Copyright (C) 2013 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <Python.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <locale.h>
#include <libguile.h>

#include <xalloc.h>
#include <sortsmill/guile/core.h>
#include <sortsmill/guile/core/python.h>
#include <sortsmill/smcoreguile-dirlayout.h>
#include "python_helpers.h"

static const unsigned char guiledir_in_utf8[] = SMCOREGUILE_GUILEDIR_IN_UTF8;
static const unsigned char guileobjdir_in_utf8[] =
  SMCOREGUILE_GUILEOBJDIR_IN_UTF8;

// FIXME: Our directories are added at the end to avoid interfering
// with settings of GUILE_LOAD_PATH, GUILE_LOAD_COMPILED_PATH,
// etc. However, it would be better to have our directories come
// before Guile’s built-in directories.
static void
append_to_path (const char *module, const char *path_variable,
                const char *dir)
{
  SCM load_path = scm_c_public_lookup (module, path_variable);
  SCM lst = scm_append (scm_list_2 (scm_variable_ref (load_path),
                                    scm_list_1 (scm_from_utf8_string (dir))));
  scm_variable_set_x (load_path, lst);
}

static void
append_to_load_path (const char *dir)
{
  append_to_path ("guile", "%load-path", dir);
}

static void
append_to_load_compiled_path (const char *dir)
{
  append_to_path ("guile", "%load-compiled-path", dir);
}

static void
main_func (void *data, int argc, char **argv)
{
  append_to_load_path ((const char *) guiledir_in_utf8);
  append_to_load_compiled_path ((const char *) guileobjdir_in_utf8);

  int errval = PyImport_AppendInittab ("sortsmill_core_guile",
                                       PyInit_sortsmill_core_guile);
  if (errval == -1)
    xalloc_die ();

  Py_Initialize();

  SCM program_params = SCM_EOL;
  for (int i = argc - 1; 0 <= i; i--)
    program_params = scm_cons (scm_from_locale_string (argv[i]),
                               program_params);

  const int exit_status = scm_to_int (scm_py_main (program_params));

  Py_Finalize ();
  exit (exit_status);
}

void
main (int argc, char **argv)
{
  setlocale (LC_ALL, "");
  
  int phony_data;
  scm_boot_guile (argc, argv, main_func, &phony_data);
}
