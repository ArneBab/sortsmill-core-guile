#include <config.h>

// Copyright (C) 2014 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <sortsmill/core.h>
#include <sortsmill/guile/core.h>

#define _AO_MODULE "sortsmill core atomic-ops"

//-------------------------------------------------------------------------

#if HAVE_ATOMIC_OPS_H

#include <atomic_ops.h>

#else // !HAVE_ATOMIC_OPS_H

typedef size_t AO_t;

#define AO_load_acquire_read(p) (*(p))
#define AO_store_release_write(p, value) (void (*(p) = (value)))

#endif // !HAVE_ATOMIC_OPS_H

static inline void
_scm_assert_ao (SCM ao, unsigned int arg_no, const char *who)
{
  SCM_ASSERT_TYPE ((scm_is_bytevector (ao)
                    && sizeof (AO_t) <= SCM_BYTEVECTOR_LENGTH (ao)),
                   ao, arg_no, who, "bytevector of length >= sizeof(AO_t)");
}

static AO_t
scm_to_AO_t (SCM value)
{
  AO_t v;
  switch (sizeof (AO_t))
    {
    case 8:
      v = scm_to_uint64 (value);
      break;
    case 4:
      v = scm_to_uint32 (value);
      break;
    case 2:
      v = scm_to_uint16 (value);
      break;
    case 1:
      v = scm_to_uint8 (value);
      break;
    default:
      v = scm_to_uintmax (value);
      break;
    }
  return v;
}

static SCM
scm_from_AO_t (AO_t v)
{
  SCM value;
  switch (sizeof (AO_t))
    {
    case 8:
      value = scm_from_uint64 (v);
      break;
    case 4:
      value = scm_from_uint32 (v);
      break;
    case 2:
      value = scm_from_uint16 (v);
      break;
    case 1:
      value = scm_from_uint8 (v);
      break;
    default:
      value = scm_from_uintmax (v);
      break;
    }
  return value;
}

//-------------------------------------------------------------------------

VISIBLE SCM
scm_make_AO (SCM value)
{
  SCM ao = scm_c_make_bytevector (sizeof (AO_t));
  volatile AO_t *p = (volatile AO_t *) SCM_BYTEVECTOR_CONTENTS (ao);
  AO_store_release_write (p, scm_to_AO_t (value));
  return ao;
}

// FIXME: Maybe we should write C or m4 macros for many different
// atomic operations. For now, just define what we intend to use.
VISIBLE SCM
scm_AO_load (SCM ao)
{
  _scm_assert_ao (ao, SCM_ARG1, "AO-load");
  volatile AO_t *p = (volatile AO_t *) SCM_BYTEVECTOR_CONTENTS (ao);
  return scm_from_AO_t (AO_load (p));
}

// FIXME: Maybe we should write C or m4 macros for many different
// atomic operations. For now, just define what we intend to use.
VISIBLE SCM
scm_AO_load_acquire_read (SCM ao)
{
  _scm_assert_ao (ao, SCM_ARG1, "AO-load-acquire-read");
  volatile AO_t *p = (volatile AO_t *) SCM_BYTEVECTOR_CONTENTS (ao);
  return scm_from_AO_t (AO_load_acquire_read (p));
}

// FIXME: Maybe we should write C or m4 macros for many different
// atomic operations. For now, just define what we intend to use.
VISIBLE SCM
scm_AO_store_release_write_x (SCM ao, SCM value)
{
  _scm_assert_ao (ao, SCM_ARG1, "AO-store-release-write!");
  volatile AO_t *p = (volatile AO_t *) SCM_BYTEVECTOR_CONTENTS (ao);
  AO_store_release_write (p, scm_to_AO_t (value));
  return SCM_UNSPECIFIED;
}

//-------------------------------------------------------------------------
