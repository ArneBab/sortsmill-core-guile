#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2013, 2014 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile
// 
// Sorts Mill Core Guile is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with Sorts Mill Core Guile.  If not, see
// <http://www.gnu.org/licenses/>.

#include <Python.h>
#include <assert.h>
#include <stdbool.h>
#include <limits.h>
#include <libintl.h>
#include <libguile.h>
#include <sortsmill/guile/core.h>
#include <sortsmill/guile/core/python.h>
#include "python_helpers.h"
#include "mpz_pylong.h"

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

static inline ssize_t
ssz_min (ssize_t a, ssize_t b)
{
  return (a < b) ? a : b;
}

//-------------------------------------------------------------------------
//
// Referenceable copies of inline functions.

// FIXME: Write more of these Lisp-style convenience routines. Also
// check their correctness more carefully.
VISIBLE SCM scm_pysequence_car (SCM obj);
VISIBLE SCM scm_pysequence_cdr (SCM obj);
VISIBLE SCM scm_pysequence_caar (SCM obj);
VISIBLE SCM scm_pysequence_cadr (SCM obj);
VISIBLE SCM scm_pysequence_cdar (SCM obj);
VISIBLE SCM scm_pysequence_cddr (SCM obj);

VISIBLE SCM scm_pysequence_reverse_to_pylist (SCM obj);
VISIBLE SCM scm_pysequence_reverse_to_pytuple (SCM obj);

//-------------------------------------------------------------------------

SCM_PYOBJECT_UNARY_WRAPCHECK_ONLY (pysequence, PySequence);
SCM_PYOBJECT_UNARY_WRAPCHECKS (pylist, PyList);
SCM_PYOBJECT_UNARY_WRAPCHECKS (pytuple, PyTuple);

VISIBLE SCM_PYOBJECT_UNARY_INTTYPE (ssize_t, "pysequence-length",
                                    scm_c_pysequence_length, PySequence_Length);

VISIBLE SCM
scm_pysequence_length (SCM obj)
{
  return scm_from_size_t (scm_c_pysequence_length (obj));
}

VISIBLE SCM_PYOBJECT_BINARY_OP ("pysequence-concat",
                                scm_pysequence_concat, PySequence_Concat);

VISIBLE SCM
scm_pysequence_concat_list (SCM obj_list)
{
  SCM_ASSERT_TYPE ((scm_is_pair (obj_list)), obj_list, SCM_ARG1,
                   "pysequence-concat", "nonempty list");
  SCM result = SCM_CAR (obj_list);
  for (SCM p = SCM_CDR (obj_list); !scm_is_null (p); p = SCM_CDR (p))
    {
      SCM_ASSERT_TYPE ((scm_is_pair (p)), obj_list, SCM_ARG1,
                       "pysequence-concat", "nonempty list");
      result = scm_pysequence_concat (result, SCM_CAR (p));
    }
  return result;
}

VISIBLE SCM
scm_c_pysequence_repeat (SCM obj, ssize_t count)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);

  PyObject *_result = PySequence_Repeat (_obj, count);
  if (_result == NULL)
    scm_c_py_exc_check_violation
      ("pysequence-repeat", _("PySequence_Repeat raised a Python exception"),
       scm_list_2 (obj, scm_from_ssize_t (count)));

  return scm_c_make_pyobject (_result);
}

VISIBLE SCM
scm_pysequence_repeat (SCM obj, SCM count)
{
  return scm_c_pysequence_repeat
    (obj, scm_to_ssize_t (scm_pyinteger_to_scm (count)));
}

VISIBLE SCM_PYOBJECT_BINARY_OP ("pysequence-inplaceconcat!",
                                scm_pysequence_inplaceconcat_x,
                                PySequence_InPlaceConcat);

VISIBLE SCM
scm_pysequence_inplaceconcat_list_x (SCM obj_list)
{
  SCM_ASSERT_TYPE ((scm_is_pair (obj_list)), obj_list, SCM_ARG1,
                   "pysequence-inplaceconcat!", "nonempty list");
  SCM result = SCM_CAR (obj_list);
  for (SCM p = SCM_CDR (obj_list); !scm_is_null (p); p = SCM_CDR (p))
    {
      SCM_ASSERT_TYPE ((scm_is_pair (p)), obj_list, SCM_ARG1,
                       "pysequence-inplaceconcat!", "nonempty list");
      result = scm_pysequence_inplaceconcat_x (result, SCM_CAR (p));
    }
  return result;
}

VISIBLE SCM
scm_c_pysequence_inplacerepeat (SCM obj, ssize_t count)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);

  PyObject *_result = PySequence_InPlaceRepeat (_obj, count);
  if (_result == NULL)
    scm_c_py_exc_check_violation
      ("pysequence-inplacerepeat!",
       _("PySequence_InPlaceRepeat raised a Python exception"),
       scm_list_2 (obj, scm_from_ssize_t (count)));

  return scm_c_make_pyobject (_result);
}

VISIBLE SCM
scm_pysequence_inplacerepeat_x (SCM obj, SCM count)
{
  return scm_c_pysequence_inplacerepeat
    (obj, scm_to_ssize_t (scm_pyinteger_to_scm (count)));
}

VISIBLE SCM
scm_c_pysequence_getitem (SCM obj, ssize_t i)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);

  PyObject *_result = PySequence_GetItem (_obj, i);
  if (_result == NULL)
    scm_c_py_exc_check_violation
      ("pysequence-getitem",
       _("PySequence_GetItem raised a Python exception"),
       scm_list_2 (obj, scm_from_ssize_t (i)));

  return scm_c_make_pyobject (_result);
}

VISIBLE SCM
scm_pysequence_getitem (SCM obj, SCM i)
{
  return scm_c_pysequence_getitem
    (obj, scm_to_ssize_t (scm_pyinteger_to_scm (i)));
}

VISIBLE SCM
scm_c_pysequence_getslice (SCM obj, ssize_t i1, ssize_t i2)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);

  PyObject *_result = PySequence_GetSlice (_obj, i1, i2);
  if (_result == NULL)
    scm_c_py_exc_check_violation
      ("pysequence-getslice",
       _("PySequence_GetSlice raised a Python exception"),
       scm_list_3 (obj, scm_from_ssize_t (i1), scm_from_ssize_t (i2)));

  return scm_c_make_pyobject (_result);
}

VISIBLE SCM
scm_pysequence_getslice (SCM obj, SCM i1, SCM i2)
{
  return scm_c_pysequence_getslice
    (obj, scm_to_ssize_t (scm_pyinteger_to_scm (i1)),
     scm_to_ssize_t (scm_pyinteger_to_scm (i2)));
}

VISIBLE void
scm_c_pysequence_setitem (SCM obj, ssize_t i, SCM v)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);

  scm_assert_pyobject (v);
  PyObject *_v = scm_to_PyObject_ptr (v);

  int errval = PySequence_SetItem (_obj, i, _v);
  if (errval == -1)
    scm_c_py_exc_check_violation
      ("pysequence-setitem!",
       _("PySequence_SetItem raised a Python exception"),
       scm_list_3 (obj, scm_from_ssize_t (i), v));
}

VISIBLE SCM
scm_pysequence_setitem_x (SCM obj, SCM i, SCM v)
{
  scm_c_pysequence_setitem (obj, scm_to_ssize_t (scm_pyinteger_to_scm (i)), v);
  return SCM_UNSPECIFIED;
}

VISIBLE void
scm_c_pysequence_delitem (SCM obj, ssize_t i)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);

  int errval = PySequence_DelItem (_obj, i);
  if (errval == -1)
    scm_c_py_exc_check_violation
      ("pysequence-delitem!",
       _("PySequence_DelItem raised a Python exception"),
       scm_list_2 (obj, scm_from_ssize_t (i)));
}

VISIBLE SCM
scm_pysequence_delitem_x (SCM obj, SCM i)
{
  scm_c_pysequence_delitem (obj, scm_to_ssize_t (scm_pyinteger_to_scm (i)));
  return SCM_UNSPECIFIED;
}

VISIBLE void
scm_c_pysequence_setslice (SCM obj, ssize_t i1, ssize_t i2, SCM v)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);

  scm_assert_pyobject (v);
  PyObject *_v = scm_to_PyObject_ptr (v);

  int errval = PySequence_SetSlice (_obj, i1, i2, _v);
  if (errval == -1)
    scm_c_py_exc_check_violation
      ("pysequence-setslice!",
       _("PySequence_SetSlice raised a Python exception"),
       scm_list_4 (obj, scm_from_ssize_t (i1), scm_from_ssize_t (i2), v));
}

VISIBLE SCM
scm_pysequence_setslice_x (SCM obj, SCM i1, SCM i2, SCM v)
{
  scm_c_pysequence_setslice (obj, scm_to_ssize_t (scm_pyinteger_to_scm (i1)),
                             scm_to_ssize_t (scm_pyinteger_to_scm (i2)), v);
  return SCM_UNSPECIFIED;
}

VISIBLE void
scm_c_pysequence_delslice (SCM obj, ssize_t i1, ssize_t i2)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);

  int errval = PySequence_DelSlice (_obj, i1, i2);
  if (errval == -1)
    scm_c_py_exc_check_violation
      ("pysequence-delslice!",
       _("PySequence_DelSlice raised a Python exception"),
       scm_list_3 (obj, scm_from_ssize_t (i1), scm_from_ssize_t (i2)));
}

VISIBLE SCM
scm_pysequence_delslice_x (SCM obj, SCM i1, SCM i2)
{
  scm_c_pysequence_delslice (obj, scm_to_ssize_t (scm_pyinteger_to_scm (i1)),
                             scm_to_ssize_t (scm_pyinteger_to_scm (i2)));
  return SCM_UNSPECIFIED;
}

VISIBLE SCM_PYOBJECT_BINARY_INTTYPE (ssize_t, "pysequence-count",
                                     scm_c_pysequence_count, PySequence_Count);

VISIBLE SCM
scm_pysequence_count (SCM obj, SCM v)
{
  return scm_from_ssize_t (scm_c_pysequence_count (obj, v));
}

VISIBLE SCM_PYOBJECT_BINARY_BOOL ("pysequence-contains",
                                  scm_pysequence_contains, PySequence_Contains);

VISIBLE SCM
scm_pysequence_contains_p (SCM obj, SCM v)
{
  return (scm_pysequence_contains (obj, v)) ? SCM_BOOL_T : SCM_BOOL_F;
}

VISIBLE SCM_PYOBJECT_BINARY_INTTYPE (ssize_t, "pysequence-index",
                                     scm_c_pysequence_index, PySequence_Index);

VISIBLE SCM
scm_pysequence_index (SCM obj, SCM v)
{
  return scm_from_ssize_t (scm_c_pysequence_index (obj, v));
}

VISIBLE SCM
scm_pysequence_list (SCM obj)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);

  PyObject *_result = PySequence_List (_obj);
  if (_result == NULL)
    scm_c_py_exc_check_violation
      ("pysequence-list", _("PySequence_List raised a Python exception"),
       scm_list_1 (obj));

  return scm_c_make_pyobject (_result);
}

VISIBLE SCM
scm_pysequence_tuple (SCM obj)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);

  PyObject *_result = PySequence_Tuple (_obj);
  if (_result == NULL)
    scm_c_py_exc_check_violation
      ("pysequence-tuple", _("PySequence_Tuple raised a Python exception"),
       scm_list_1 (obj));

  return scm_c_make_pyobject (_result);
}

VISIBLE SCM
scm_c_pysequence_to_list_with_tail (PyObject *_obj, SCM tail)
{
  if (!PySequence_Check (_obj))
    SCM_ASSERT_TYPE (false, scm_c_make_pyobject (_obj), SCM_ARG1,
                     "pysequence->list", "pysequence");

  PyObject *fast = PySequence_Fast (_obj, "");
  py_exc_check ((fast != NULL), "pysequence->list",
                scm_list_1 (scm_c_make_pyobject (_obj)));

  scm_dynwind_begin (0);
  scm_dynwind_decref (fast);

  ssize_t n = PySequence_Fast_GET_SIZE (fast);
  PyObject **items = PySequence_Fast_ITEMS (fast);
  SCM lst = (SCM_UNBNDP (tail)) ? SCM_EOL : tail;
  for (ssize_t i = n - 1; 0 <= i; i--)
    lst = scm_cons (scm_c_incref_make_pyobject (items[i]), lst);

  scm_dynwind_end ();

  return lst;
}

VISIBLE SCM
scm_c_pysequence_to_list (PyObject *_obj)
{
  return scm_c_pysequence_to_list_with_tail (_obj, SCM_EOL);
}

VISIBLE SCM
scm_pysequence_to_list_with_tail (SCM obj, SCM tail)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);
  return scm_c_pysequence_to_list_with_tail (_obj, tail);
}

VISIBLE SCM
scm_pysequence_to_list (SCM obj)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);
  return scm_c_pysequence_to_list (_obj);
}

VISIBLE SCM
scm_pysequence_reverse_to_list (SCM obj)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);

  PyObject *fast = PySequence_Fast (_obj, "");
  if (fast == NULL)
    scm_c_py_exc_check_violation
      ("pysequence-reverse->list",
       _("PySequence_Fast raised a Python exception"), scm_list_1 (obj));

  scm_dynwind_begin (0);
  scm_dynwind_decref (fast);

  ssize_t n = PySequence_Fast_GET_SIZE (fast);
  PyObject **items = PySequence_Fast_ITEMS (fast);
  SCM lst = SCM_EOL;
  for (ssize_t i = 0; i < n; i++)
    lst = scm_cons (scm_c_incref_make_pyobject (items[i]), lst);

  scm_dynwind_end ();

  return lst;
}

VISIBLE SCM
scm_pysequence_map_to_list (SCM proc, SCM obj_lst)
{
  size_t obj_count = scm_to_size_t (scm_length (obj_lst));
  if (obj_count == 0)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("pysequence-map->list"),
        rnrs_c_make_message_condition (_("too few arguments")),
        rnrs_make_irritants_condition (SCM_EOL)));

  scm_dynwind_begin (0);

  PyObject **fast = scm_malloc (obj_count * sizeof (PyObject *));
  scm_dynwind_free (fast);

  SCM p = obj_lst;
  for (size_t i = 0; i < obj_count; i++)
    {
      scm_assert_pyobject (SCM_CAR (p));
      fast[i] = PySequence_Fast (scm_to_PyObject_ptr (SCM_CAR (p)), "");
      if (fast[i] == NULL)
        scm_c_py_exc_check_violation
          ("pysequence-map->list",
           _("PySequence_Fast raised a Python exception"),
           scm_list_1 (SCM_CAR (p)));
      scm_dynwind_decref (fast[i]);
      p = SCM_CDR (p);
    }

  ssize_t n = SSIZE_MAX;
  PyObject ***items = scm_malloc (obj_count * sizeof (PyObject **));
  scm_dynwind_free (items);
  for (size_t i = 0; i < obj_count; i++)
    {
      n = ssz_min (n, PySequence_Fast_GET_SIZE (fast[i]));
      items[i] = PySequence_Fast_ITEMS (fast[i]);
    }

  SCM lst = SCM_EOL;
  for (ssize_t j = n - 1; 0 <= j; j--)
    {
      SCM args = SCM_EOL;
      for (size_t i = 0; i < obj_count; i++)
        {
          PyObject *item = items[obj_count - i - 1][j];
          args = scm_cons (scm_c_incref_make_pyobject (item), args);
        }
      lst = scm_cons (scm_apply_0 (proc, args), lst);
    }

  scm_dynwind_end ();

  return lst;
}

VISIBLE SCM
scm_c_pysequence_to_vector (PyObject *_obj)
{
  if (!PySequence_Check (_obj))
    SCM_ASSERT_TYPE (false, scm_c_make_pyobject (_obj), SCM_ARG1,
                     "pysequence->vector", "pysequence");

  PyObject *fast = PySequence_Fast (_obj, "");
  py_exc_check ((fast != NULL), "pysequence->vector",
                scm_list_1 (scm_c_make_pyobject (_obj)));

  scm_dynwind_begin (0);
  scm_dynwind_decref (fast);

  ssize_t n = PySequence_Fast_GET_SIZE (fast);
  PyObject **items = PySequence_Fast_ITEMS (fast);

  SCM v = scm_c_make_vector (n, SCM_UNDEFINED);
  for (ssize_t i = 0; i < n; i++)
    SCM_SIMPLE_VECTOR_SET (v, i, scm_c_incref_make_pyobject (items[i]));

  scm_dynwind_end ();

  return v;
}

VISIBLE SCM
scm_pysequence_to_vector (SCM obj)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);
  return scm_c_pysequence_to_vector (_obj);
}

VISIBLE SCM
scm_c_pysequence_reverse_to_vector (PyObject *_obj)
{
  if (!PySequence_Check (_obj))
    SCM_ASSERT_TYPE (false, scm_c_make_pyobject (_obj), SCM_ARG1,
                     "pysequence-reverse->vector", "pysequence");

  PyObject *fast = PySequence_Fast (_obj, "");
  py_exc_check ((fast != NULL), "pysequence-reverse->vector",
                scm_list_1 (scm_c_make_pyobject (_obj)));

  scm_dynwind_begin (0);
  scm_dynwind_decref (fast);

  ssize_t n = PySequence_Fast_GET_SIZE (fast);
  PyObject **items = PySequence_Fast_ITEMS (fast);

  SCM v = scm_c_make_vector (n, SCM_UNDEFINED);
  for (ssize_t i = 0; i < n; i++)
    SCM_SIMPLE_VECTOR_SET (v, (n - 1) - i,
                           scm_c_incref_make_pyobject (items[i]));

  scm_dynwind_end ();

  return v;
}

VISIBLE SCM
scm_pysequence_reverse_to_vector (SCM obj)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);
  return scm_c_pysequence_reverse_to_vector (_obj);
}

VISIBLE SCM
scm_pysequence_map_to_vector (SCM proc, SCM obj_lst)
{
  size_t obj_count = scm_to_size_t (scm_length (obj_lst));
  if (obj_count == 0)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("pysequence-map->vector"),
        rnrs_c_make_message_condition (_("too few arguments")),
        rnrs_make_irritants_condition (SCM_EOL)));

  scm_dynwind_begin (0);

  PyObject **fast = scm_malloc (obj_count * sizeof (PyObject *));
  scm_dynwind_free (fast);

  SCM p = obj_lst;
  for (size_t i = 0; i < obj_count; i++)
    {
      scm_assert_pyobject (SCM_CAR (p));
      fast[i] = PySequence_Fast (scm_to_PyObject_ptr (SCM_CAR (p)), "");
      if (fast[i] == NULL)
        scm_c_py_exc_check_violation
          ("pysequence-map->vector",
           _("PySequence_Fast raised a Python exception"),
           scm_list_1 (SCM_CAR (p)));
      scm_dynwind_decref (fast[i]);
      p = SCM_CDR (p);
    }

  ssize_t n = SSIZE_MAX;
  PyObject ***items = scm_malloc (obj_count * sizeof (PyObject **));
  scm_dynwind_free (items);
  for (size_t i = 0; i < obj_count; i++)
    {
      n = ssz_min (n, PySequence_Fast_GET_SIZE (fast[i]));
      items[i] = PySequence_Fast_ITEMS (fast[i]);
    }

  SCM v = scm_c_make_vector (n, SCM_UNDEFINED);
  for (ssize_t j = 0; j < n; j++)
    {
      SCM args = SCM_EOL;
      for (size_t i = 0; i < obj_count; i++)
        {
          PyObject *item = items[obj_count - i - 1][j];
          args = scm_cons (scm_c_incref_make_pyobject (item), args);
        }
      SCM_SIMPLE_VECTOR_SET (v, j, scm_apply_0 (proc, args));
    }

  scm_dynwind_end ();

  return v;
}

VISIBLE SCM
scm_pysequence_for_each (SCM proc, SCM obj_lst)
{
  size_t obj_count = scm_to_size_t (scm_length (obj_lst));
  if (obj_count == 0)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("pysequence-for-each"),
        rnrs_c_make_message_condition (_("too few arguments")),
        rnrs_make_irritants_condition (SCM_EOL)));

  scm_dynwind_begin (0);

  PyObject **fast = scm_malloc (obj_count * sizeof (PyObject *));
  scm_dynwind_free (fast);

  SCM p = obj_lst;
  for (size_t i = 0; i < obj_count; i++)
    {
      scm_assert_pyobject (SCM_CAR (p));
      fast[i] = PySequence_Fast (scm_to_PyObject_ptr (SCM_CAR (p)), "");
      if (fast[i] == NULL)
        scm_c_py_exc_check_violation
          ("pysequence-for-each",
           _("PySequence_Fast raised a Python exception"),
           scm_list_1 (SCM_CAR (p)));
      scm_dynwind_decref (fast[i]);
      p = SCM_CDR (p);
    }

  ssize_t n = SSIZE_MAX;
  PyObject ***items = scm_malloc (obj_count * sizeof (PyObject **));
  scm_dynwind_free (items);
  for (size_t i = 0; i < obj_count; i++)
    {
      n = ssz_min (n, PySequence_Fast_GET_SIZE (fast[i]));
      items[i] = PySequence_Fast_ITEMS (fast[i]);
    }

  for (ssize_t j = 0; j < n; j++)
    {
      SCM args = SCM_EOL;
      for (size_t i = 0; i < obj_count; i++)
        {
          PyObject *item = items[obj_count - i - 1][j];
          args = scm_cons (scm_c_incref_make_pyobject (item), args);
        }
      scm_apply_0 (proc, args);
    }

  scm_dynwind_end ();

  return SCM_UNSPECIFIED;
}

VISIBLE SCM
scm_pysequence_fold_left (SCM proc, SCM init, SCM obj_lst)
{
  size_t obj_count = scm_to_size_t (scm_length (obj_lst));
  if (obj_count == 0)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("pysequence-fold-left"),
        rnrs_c_make_message_condition (_("too few arguments")),
        rnrs_make_irritants_condition (SCM_EOL)));

  scm_dynwind_begin (0);

  PyObject **fast = scm_malloc (obj_count * sizeof (PyObject *));
  scm_dynwind_free (fast);

  SCM p = obj_lst;
  for (size_t i = 0; i < obj_count; i++)
    {
      scm_assert_pyobject (SCM_CAR (p));
      fast[i] = PySequence_Fast (scm_to_PyObject_ptr (SCM_CAR (p)), "");
      if (fast[i] == NULL)
        scm_c_py_exc_check_violation
          ("pysequence-fold-left",
           _("PySequence_Fast raised a Python exception"),
           scm_list_1 (SCM_CAR (p)));
      scm_dynwind_decref (fast[i]);
      p = SCM_CDR (p);
    }

  ssize_t n = SSIZE_MAX;
  PyObject ***items = scm_malloc (obj_count * sizeof (PyObject **));
  scm_dynwind_free (items);
  for (size_t i = 0; i < obj_count; i++)
    {
      n = ssz_min (n, PySequence_Fast_GET_SIZE (fast[i]));
      items[i] = PySequence_Fast_ITEMS (fast[i]);
    }

  for (ssize_t j = 0; j < n; j++)
    {
      SCM args = SCM_EOL;
      for (size_t i = 0; i < obj_count; i++)
        {
          PyObject *item = items[obj_count - i - 1][j];
          args = scm_cons (scm_c_incref_make_pyobject (item), args);
        }
      init = scm_apply_1 (proc, init, args);
    }

  scm_dynwind_end ();

  return init;
}

VISIBLE SCM
scm_pysequence_fold_right (SCM proc, SCM init, SCM obj_lst)
{
  size_t obj_count = scm_to_size_t (scm_length (obj_lst));
  if (obj_count == 0)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("pysequence-fold-right"),
        rnrs_c_make_message_condition (_("too few arguments")),
        rnrs_make_irritants_condition (SCM_EOL)));

  scm_dynwind_begin (0);

  PyObject **fast = scm_malloc (obj_count * sizeof (PyObject *));
  scm_dynwind_free (fast);

  SCM p = obj_lst;
  for (size_t i = 0; i < obj_count; i++)
    {
      scm_assert_pyobject (SCM_CAR (p));
      fast[i] = PySequence_Fast (scm_to_PyObject_ptr (SCM_CAR (p)), "");
      if (fast[i] == NULL)
        scm_c_py_exc_check_violation
          ("pysequence-fold-right",
           _("PySequence_Fast raised a Python exception"),
           scm_list_1 (SCM_CAR (p)));
      scm_dynwind_decref (fast[i]);
      p = SCM_CDR (p);
    }

  ssize_t n = SSIZE_MAX;
  PyObject ***items = scm_malloc (obj_count * sizeof (PyObject **));
  scm_dynwind_free (items);
  for (size_t i = 0; i < obj_count; i++)
    {
      n = ssz_min (n, PySequence_Fast_GET_SIZE (fast[i]));
      items[i] = PySequence_Fast_ITEMS (fast[i]);
    }

  for (ssize_t j = n - 1; 0 <= j; j--)
    {
      SCM args = scm_list_1 (init);
      for (size_t i = 0; i < obj_count; i++)
        {
          PyObject *item = items[obj_count - i - 1][j];
          args = scm_cons (scm_c_incref_make_pyobject (item), args);
        }
      init = scm_apply_0 (proc, args);
    }

  scm_dynwind_end ();

  return init;
}

VISIBLE SCM
scm_pysequence_for_all (SCM proc, SCM obj_lst)
{
  size_t obj_count = scm_to_size_t (scm_length (obj_lst));
  if (obj_count == 0)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("pysequence-for-all"),
        rnrs_c_make_message_condition (_("too few arguments")),
        rnrs_make_irritants_condition (SCM_EOL)));

  scm_dynwind_begin (0);

  PyObject **fast = scm_malloc (obj_count * sizeof (PyObject *));
  scm_dynwind_free (fast);

  SCM p = obj_lst;
  for (size_t i = 0; i < obj_count; i++)
    {
      scm_assert_pyobject (SCM_CAR (p));
      fast[i] = PySequence_Fast (scm_to_PyObject_ptr (SCM_CAR (p)), "");
      if (fast[i] == NULL)
        scm_c_py_exc_check_violation
          ("pysequence-for-all",
           _("PySequence_Fast raised a Python exception"),
           scm_list_1 (SCM_CAR (p)));
      scm_dynwind_decref (fast[i]);
      p = SCM_CDR (p);
    }

  ssize_t n = SSIZE_MAX;
  PyObject ***items = scm_malloc (obj_count * sizeof (PyObject **));
  scm_dynwind_free (items);
  for (size_t i = 0; i < obj_count; i++)
    {
      n = ssz_min (n, PySequence_Fast_GET_SIZE (fast[i]));
      items[i] = PySequence_Fast_ITEMS (fast[i]);
    }

  SCM result = SCM_BOOL_T;
  ssize_t j = 0;
  while (scm_is_true (result) && j < n)
    {
      SCM args = SCM_EOL;
      for (size_t i = 0; i < obj_count; i++)
        {
          PyObject *item = items[obj_count - i - 1][j];
          args = scm_cons (scm_c_incref_make_pyobject (item), args);
        }
      result = scm_apply_0 (proc, args);
      j++;
    }

  scm_dynwind_end ();

  return result;
}

VISIBLE SCM
scm_pysequence_exists (SCM proc, SCM obj_lst)
{
  size_t obj_count = scm_to_size_t (scm_length (obj_lst));
  if (obj_count == 0)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("pysequence-exists"),
        rnrs_c_make_message_condition (_("too few arguments")),
        rnrs_make_irritants_condition (SCM_EOL)));

  scm_dynwind_begin (0);

  PyObject **fast = scm_malloc (obj_count * sizeof (PyObject *));
  scm_dynwind_free (fast);

  SCM p = obj_lst;
  for (size_t i = 0; i < obj_count; i++)
    {
      scm_assert_pyobject (SCM_CAR (p));
      fast[i] = PySequence_Fast (scm_to_PyObject_ptr (SCM_CAR (p)), "");
      if (fast[i] == NULL)
        scm_c_py_exc_check_violation
          ("pysequence-exists",
           _("PySequence_Fast raised a Python exception"),
           scm_list_1 (SCM_CAR (p)));
      scm_dynwind_decref (fast[i]);
      p = SCM_CDR (p);
    }

  ssize_t n = SSIZE_MAX;
  PyObject ***items = scm_malloc (obj_count * sizeof (PyObject **));
  scm_dynwind_free (items);
  for (size_t i = 0; i < obj_count; i++)
    {
      n = ssz_min (n, PySequence_Fast_GET_SIZE (fast[i]));
      items[i] = PySequence_Fast_ITEMS (fast[i]);
    }

  SCM result = SCM_BOOL_F;
  ssize_t j = 0;
  while (scm_is_false (result) && j < n)
    {
      SCM args = SCM_EOL;
      for (size_t i = 0; i < obj_count; i++)
        {
          PyObject *item = items[obj_count - i - 1][j];
          args = scm_cons (scm_c_incref_make_pyobject (item), args);
        }
      result = scm_apply_0 (proc, args);
      j++;
    }

  scm_dynwind_end ();

  return result;
}

//-------------------------------------------------------------------------

#define SCM_LIST_REINDEXED_TO_PYSEQUENCE(NAME, SCM_NAME, PyType,        \
                                         REINDEX)                       \
  SCM                                                                   \
  NAME (SCM lst)                                                        \
  {                                                                     \
    /* The following will not work if @var{lst} is cyclic. */           \
    ssize_t n = 0;                                                      \
    for (SCM p = lst; !scm_is_null (p); p = SCM_CDR (p))                \
      {                                                                 \
        SCM_ASSERT_TYPE ((scm_is_pair (p)                               \
                          && scm_is_pyobject (SCM_CAR (p))),            \
                         lst, SCM_ARG1, SCM_NAME,                       \
                         "list of pyobject items");                     \
        n++;                                                            \
      }                                                                 \
                                                                        \
    PyObject *pyseq = PyType##_New (n);                                 \
    if (pyseq == NULL)                                                  \
      scm_c_py_exc_check_violation                                      \
        (SCM_NAME, _(#PyType "_New raised a Python exception"),         \
         scm_list_1 (scm_from_ssize_t (n)));                            \
                                                                        \
    SCM p = lst;                                                        \
    for (ssize_t i = 0; i < n; i++)                                     \
      {                                                                 \
        PyObject *pyobj = scm_to_PyObject_ptr (SCM_CAR (p));            \
        Py_XINCREF (pyobj);                                             \
        PyType##_SET_ITEM (pyseq, REINDEX, pyobj);                      \
        p = SCM_CDR (p);                                                \
      }                                                                 \
                                                                        \
    return scm_c_make_pyobject (pyseq);                                 \
  }

VISIBLE SCM_LIST_REINDEXED_TO_PYSEQUENCE (scm_list_to_pylist, "list->pylist",
                                          PyList, (i));

VISIBLE SCM_LIST_REINDEXED_TO_PYSEQUENCE (scm_list_reverse_to_pylist,
                                          "list-reverse->pylist", PyList,
                                          (n - i - 1));

VISIBLE SCM_LIST_REINDEXED_TO_PYSEQUENCE (scm_list_to_pytuple, "list->pytuple",
                                          PyTuple, (i));

VISIBLE SCM_LIST_REINDEXED_TO_PYSEQUENCE (scm_list_reverse_to_pytuple,
                                          "list-reverse->pytuple", PyTuple,
                                          (n - i - 1));

//-------------------------------------------------------------------------

#define SCM_VECTOR_REINDEXED_TO_PYSEQUENCE(NAME, SCM_NAME, PyType,      \
                                           REINDEX)                     \
  VISIBLE SCM                                                           \
  NAME (SCM v)                                                          \
  {                                                                     \
    scm_dynwind_begin (0);                                              \
                                                                        \
    scm_t_array_handle v_handle;                                        \
    size_t v_length;                                                    \
    ssize_t stride;                                                     \
    const SCM *elems =                                                  \
      scm_vector_elements (v, &v_handle, &v_length, &stride);           \
    scm_dynwind_array_handle_release (&v_handle);                       \
                                                                        \
    const ssize_t n = v_length;                                         \
                                                                        \
    for (ssize_t i = 0; i < n; i++)                                     \
      SCM_ASSERT_TYPE ((scm_is_pyobject (elems[stride * i])),           \
                       v, SCM_ARG1, SCM_NAME,                           \
                       "vector of pyobject items");                     \
                                                                        \
    PyObject *pyseq = PyType##_New (n);                                 \
    py_exc_check ((pyseq != NULL), #PyType "_New",                      \
                  scm_list_1 (scm_from_ssize_t (n)));                   \
                                                                        \
    for (ssize_t i = 0; i < n; i++)                                     \
      {                                                                 \
        PyObject *pyobj =                                               \
          scm_to_PyObject_ptr (elems[stride * i]);                      \
        Py_XINCREF (pyobj);                                             \
        PyType##_SET_ITEM (pyseq, REINDEX, pyobj);                      \
      }                                                                 \
                                                                        \
    scm_dynwind_end ();                                                 \
                                                                        \
    return scm_c_make_pyobject (pyseq);                                 \
  }

VISIBLE SCM_VECTOR_REINDEXED_TO_PYSEQUENCE (scm_vector_to_pylist,
                                            "vector->pylist", PyList, (i));

VISIBLE SCM_VECTOR_REINDEXED_TO_PYSEQUENCE (scm_vector_reverse_to_pylist,
                                            "vector-reverse->pylist", PyList,
                                            (n - i - 1));

VISIBLE SCM_VECTOR_REINDEXED_TO_PYSEQUENCE (scm_vector_to_pytuple,
                                            "vector->pytuple", PyTuple, (i));

VISIBLE SCM_VECTOR_REINDEXED_TO_PYSEQUENCE (scm_vector_reverse_to_pytuple,
                                            "vector-reverse->pytuple", PyTuple,
                                            (n - i - 1));

//-------------------------------------------------------------------------

#define SCM_LIST_MAP_REINDEXED_TO_PYSEQUENCE(NAME, SCM_NAME, PyType,    \
                                             REINDEX)                   \
  VISIBLE SCM                                                           \
  NAME (SCM proc, SCM obj_lst)                                          \
  {                                                                     \
    size_t obj_count = scm_to_size_t (scm_length (obj_lst));            \
    if (obj_count == 0)                                                 \
      rnrs_raise_condition                                              \
        (scm_list_4                                                     \
         (rnrs_make_assertion_violation (),                             \
          rnrs_c_make_who_condition (SCM_NAME),                         \
          rnrs_c_make_message_condition (_("too few arguments")),       \
          rnrs_make_irritants_condition (SCM_EOL)));                    \
                                                                        \
    scm_dynwind_begin (0);                                              \
                                                                        \
    SCM *lists = scm_malloc (obj_count * sizeof (SCM));                 \
    scm_dynwind_free (lists);                                           \
                                                                        \
    SCM p = obj_lst;                                                    \
    for (size_t i = 0; i < obj_count; i++)                              \
      {                                                                 \
        lists[i] = SCM_CAR (p);                                         \
        p = SCM_CDR (p);                                                \
      }                                                                 \
                                                                        \
    ssize_t n = scm_to_ssize_t (scm_length (lists[0]));                 \
    for (ssize_t i = 1; i < obj_count; i++)                             \
      n = ssz_min (n, scm_to_ssize_t (scm_length (lists[i])));          \
                                                                        \
    PyObject *pyseq = PyType##_New (n);                                 \
    py_exc_check ((pyseq != NULL), #PyType "_New",                      \
                  scm_list_1 (scm_from_ssize_t (n)));                   \
                                                                        \
    for (ssize_t j = 0; j < n; j++)                                     \
      {                                                                 \
        SCM args = SCM_EOL;                                             \
        for (size_t i = 0; i < obj_count; i++)                          \
          {                                                             \
            const size_t k = obj_count - i - 1;                         \
            args = scm_cons (SCM_CAR (lists[k]), args);                 \
            lists[k] = SCM_CDR (lists[k]);                              \
          }                                                             \
        SCM element = scm_apply_0 (proc, args);                         \
        scm_assert_pyobject (element);                                  \
        PyObject *pyobj = scm_to_PyObject_ptr (element);                \
        Py_XINCREF (pyobj);                                             \
        PyType##_SET_ITEM (pyseq, REINDEX, pyobj);                      \
      }                                                                 \
                                                                        \
    scm_dynwind_end ();                                                 \
                                                                        \
    return scm_c_make_pyobject (pyseq);                                 \
  }

VISIBLE SCM_LIST_MAP_REINDEXED_TO_PYSEQUENCE (scm_list_map_to_pylist,
                                              "list-map->pylist", PyList, (j));

VISIBLE SCM_LIST_MAP_REINDEXED_TO_PYSEQUENCE (scm_list_map_to_pytuple,
                                              "list-map->pytuple", PyTuple,
                                              (j));

//-------------------------------------------------------------------------

#define SCM_VECTOR_MAP_REINDEXED_TO_PYSEQUENCE(NAME, SCM_NAME, PyType,  \
                                               REINDEX)                 \
  VISIBLE SCM                                                           \
  NAME (SCM proc, SCM obj_lst)                                          \
  {                                                                     \
    size_t obj_count = scm_to_size_t (scm_length (obj_lst));            \
    if (obj_count == 0)                                                 \
      rnrs_raise_condition                                              \
        (scm_list_4                                                     \
         (rnrs_make_assertion_violation (),                             \
          rnrs_c_make_who_condition (SCM_NAME),                         \
          rnrs_c_make_message_condition (_("too few arguments")),       \
          rnrs_make_irritants_condition (SCM_EOL)));                    \
                                                                        \
    scm_dynwind_begin (0);                                              \
                                                                        \
    SCM *vectors = scm_malloc (obj_count * sizeof (SCM));               \
    scm_dynwind_free (vectors);                                         \
                                                                        \
    SCM p = obj_lst;                                                    \
    for (size_t i = 0; i < obj_count; i++)                              \
      {                                                                 \
        vectors[i] = SCM_CAR (p);                                       \
        p = SCM_CDR (p);                                                \
      }                                                                 \
                                                                        \
    const size_t m = scm_c_vector_length (vectors[0]);                  \
    if (SSIZE_MAX < m)                                                  \
      /* This branch is extremely unlikely to be run. */                \
      rnrs_raise_condition                                              \
        (scm_list_4                                                     \
         (rnrs_make_assertion_violation (),                             \
          rnrs_c_make_who_condition (SCM_NAME),                         \
          rnrs_c_make_message_condition (_("vector too long")),         \
          rnrs_make_irritants_condition (SCM_EOL)));                    \
    ssize_t n = (ssize_t) m;                                            \
    for (ssize_t i = 1; i < obj_count; i++)                             \
      {                                                                 \
        const size_t m = scm_c_vector_length (vectors[i]);              \
        if (SSIZE_MAX < m)                                              \
          /* This branch is extremely unlikely to be run. */            \
          rnrs_raise_condition                                          \
            (scm_list_4                                                 \
             (rnrs_make_assertion_violation (),                         \
              rnrs_c_make_who_condition (SCM_NAME),                     \
              rnrs_c_make_message_condition (_("vector too long")),     \
              rnrs_make_irritants_condition (SCM_EOL)));                \
        n = ssz_min (n, (ssize_t) m);                                   \
      }                                                                 \
                                                                        \
    PyObject *pyseq = PyType##_New (n);                                 \
    py_exc_check ((pyseq != NULL), #PyType "_New",                      \
                  scm_list_1 (scm_from_ssize_t (n)));                   \
                                                                        \
    for (ssize_t j = 0; j < n; j++)                                     \
      {                                                                 \
        SCM args = SCM_EOL;                                             \
        for (size_t i = 0; i < obj_count; i++)                          \
          args = scm_cons (scm_c_vector_ref (vectors[i], j), args);     \
        SCM element = scm_apply_0 (proc, args);                         \
        scm_assert_pyobject (element);                                  \
        PyObject *pyobj = scm_to_PyObject_ptr (element);                \
        Py_XINCREF (pyobj);                                             \
        PyType##_SET_ITEM (pyseq, REINDEX, pyobj);                      \
      }                                                                 \
                                                                        \
    scm_dynwind_end ();                                                 \
                                                                        \
    return scm_c_make_pyobject (pyseq);                                 \
  }

VISIBLE SCM_VECTOR_MAP_REINDEXED_TO_PYSEQUENCE (scm_vector_map_to_pylist,
                                                "vector-map->pylist", PyList,
                                                (j));

VISIBLE SCM_VECTOR_MAP_REINDEXED_TO_PYSEQUENCE (scm_vector_map_to_pytuple,
                                                "vector-map->pytuple", PyTuple,
                                                (j));

//-------------------------------------------------------------------------

#define SCM_C_PYSEQ_REINDEXED_TO_UNIFORM_VECTOR(NAME, T, TXX, CONVERT,  \
                                                REINDEX)                \
  SCM                                                                   \
  NAME (PyObject *_obj)                                                 \
  {                                                                     \
    if (!PySequence_Check (_obj))                                       \
      SCM_ASSERT_TYPE (false, scm_c_make_pyobject (_obj), SCM_ARG1,     \
                       #NAME, "pysequence");                            \
                                                                        \
    PyObject *fast = PySequence_Fast (_obj, "");                        \
    py_exc_check ((fast != NULL), #NAME,                                \
                  scm_list_1 (scm_c_make_pyobject (_obj)));             \
                                                                        \
    scm_dynwind_begin (0);                                              \
    scm_dynwind_decref (fast);                                          \
                                                                        \
    ssize_t n = PySequence_Fast_GET_SIZE (fast);                        \
    PyObject **items = PySequence_Fast_ITEMS (fast);                    \
                                                                        \
    T *data = (T *) scm_malloc (n * sizeof (T));                        \
    SCM v = scm_take_##TXX##vector (data, n);                           \
    for (ssize_t i = 0; i < n; i++)                                     \
      data[REINDEX] = (T) CONVERT (items[i]);                           \
                                                                        \
    scm_dynwind_end ();                                                 \
                                                                        \
    return v;                                                           \
  }

#define SCM_C_PYSEQ_TO_UNIFORM_VECTOR(T, TXX, CONVERT)                  \
  SCM_C_PYSEQ_REINDEXED_TO_UNIFORM_VECTOR (scm_c_pysequence_to_##TXX##vector, \
                                           T, TXX, CONVERT, (i))

#define SCM_C_PYSEQ_REVERSE_TO_UNIFORM_VECTOR(T, TXX, CONVERT)          \
  SCM_C_PYSEQ_REINDEXED_TO_UNIFORM_VECTOR (scm_c_pysequence_reverse_to_##TXX##vector, \
                                           T, TXX, CONVERT, ((n - 1) - i))

#define SCM_PYSEQ_REINDEXED_TO_UNIFORM_VECTOR(NAME, CNAME)      \
  SCM                                                           \
  NAME (SCM obj)                                                \
  {                                                             \
    scm_assert_pyobject (obj);                                  \
    PyObject *_obj = scm_to_PyObject_ptr (obj);                 \
    return CNAME (_obj);                                        \
  }

#define SCM_PYSEQ_TO_UNIFORM_VECTOR(TXX)                                \
  SCM_PYSEQ_REINDEXED_TO_UNIFORM_VECTOR (scm_pysequence_to_##TXX##vector, \
                                         scm_c_pysequence_to_##TXX##vector)

#define SCM_PYSEQ_REVERSE_TO_UNIFORM_VECTOR(TXX)                        \
  SCM_PYSEQ_REINDEXED_TO_UNIFORM_VECTOR (scm_pysequence_reverse_to_##TXX##vector, \
                                         scm_c_pysequence_reverse_to_##TXX##vector)

#if SIZEOF_LONG < 4             // This is unlikely to be satisfied.
#error long int must be at least 4 bytes long.
#endif

#if SIZEOF_SIZE_T < 4           // This is unlikely to be satisfied.
#error size_t must be at least 4 bytes long.
#endif

VISIBLE SCM_C_PYSEQ_TO_UNIFORM_VECTOR (int8_t, s8, PyIntOrLong_AsSsize_t);
VISIBLE SCM_PYSEQ_TO_UNIFORM_VECTOR (s8);
VISIBLE SCM_C_PYSEQ_TO_UNIFORM_VECTOR (int16_t, s16, PyIntOrLong_AsSsize_t);
VISIBLE SCM_PYSEQ_TO_UNIFORM_VECTOR (s16);
VISIBLE SCM_C_PYSEQ_TO_UNIFORM_VECTOR (int32_t, s32, PyIntOrLong_AsSsize_t);
VISIBLE SCM_PYSEQ_TO_UNIFORM_VECTOR (s32);

VISIBLE SCM_C_PYSEQ_TO_UNIFORM_VECTOR (uint8_t, u8, PyIntOrLong_AsSsize_t);
VISIBLE SCM_PYSEQ_TO_UNIFORM_VECTOR (u8);
VISIBLE SCM_C_PYSEQ_TO_UNIFORM_VECTOR (uint16_t, u16, PyIntOrLong_AsSsize_t);
VISIBLE SCM_PYSEQ_TO_UNIFORM_VECTOR (u16);
#if PY_MAJOR_VERSION <= 2
VISIBLE SCM_C_PYSEQ_TO_UNIFORM_VECTOR (uint32_t, u32, PyInt_AsUnsignedLongMask);
VISIBLE SCM_PYSEQ_TO_UNIFORM_VECTOR (u32);
#else
VISIBLE SCM_C_PYSEQ_TO_UNIFORM_VECTOR (uint32_t, u32, PyLong_AsUnsignedLong);
VISIBLE SCM_PYSEQ_TO_UNIFORM_VECTOR (u32);
#endif

VISIBLE SCM_C_PYSEQ_TO_UNIFORM_VECTOR (float, f32, PyFloat_AsDouble);
VISIBLE SCM_PYSEQ_TO_UNIFORM_VECTOR (f32);
VISIBLE SCM_C_PYSEQ_TO_UNIFORM_VECTOR (double, f64, PyFloat_AsDouble);
VISIBLE SCM_PYSEQ_TO_UNIFORM_VECTOR (f64);

VISIBLE SCM_C_PYSEQ_REVERSE_TO_UNIFORM_VECTOR (int8_t, s8,
                                               PyIntOrLong_AsSsize_t);
VISIBLE SCM_PYSEQ_REVERSE_TO_UNIFORM_VECTOR (s8);
VISIBLE SCM_C_PYSEQ_REVERSE_TO_UNIFORM_VECTOR (int16_t, s16,
                                               PyIntOrLong_AsSsize_t);
VISIBLE SCM_PYSEQ_REVERSE_TO_UNIFORM_VECTOR (s16);
VISIBLE SCM_C_PYSEQ_REVERSE_TO_UNIFORM_VECTOR (int32_t, s32,
                                               PyIntOrLong_AsSsize_t);
VISIBLE SCM_PYSEQ_REVERSE_TO_UNIFORM_VECTOR (s32);

VISIBLE SCM_C_PYSEQ_REVERSE_TO_UNIFORM_VECTOR (uint8_t, u8,
                                               PyIntOrLong_AsSsize_t);
VISIBLE SCM_PYSEQ_REVERSE_TO_UNIFORM_VECTOR (u8);
VISIBLE SCM_C_PYSEQ_REVERSE_TO_UNIFORM_VECTOR (uint16_t, u16,
                                               PyIntOrLong_AsSsize_t);
VISIBLE SCM_PYSEQ_REVERSE_TO_UNIFORM_VECTOR (u16);
#if PY_MAJOR_VERSION <= 2
VISIBLE SCM_C_PYSEQ_REVERSE_TO_UNIFORM_VECTOR (uint32_t, u32,
                                               PyInt_AsUnsignedLongMask);
VISIBLE SCM_PYSEQ_REVERSE_TO_UNIFORM_VECTOR (u32);
#else
VISIBLE SCM_C_PYSEQ_REVERSE_TO_UNIFORM_VECTOR (uint32_t, u32,
                                               PyLong_AsUnsignedLong);
VISIBLE SCM_PYSEQ_REVERSE_TO_UNIFORM_VECTOR (u32);
#endif

VISIBLE SCM_C_PYSEQ_REVERSE_TO_UNIFORM_VECTOR (float, f32, PyFloat_AsDouble);
VISIBLE SCM_PYSEQ_REVERSE_TO_UNIFORM_VECTOR (f32);
VISIBLE SCM_C_PYSEQ_REVERSE_TO_UNIFORM_VECTOR (double, f64, PyFloat_AsDouble);
VISIBLE SCM_PYSEQ_REVERSE_TO_UNIFORM_VECTOR (f64);

//-------------------------------------------------------------------------

#define SCM_UNIFORM_VECTOR_REINDEXED_TO_PYSEQ(SEQT, NAME, T, TXX,       \
                                              CONVERT, REINDEX)         \
  SCM                                                                   \
  NAME (SCM v)                                                          \
  {                                                                     \
    scm_dynwind_begin (0);                                              \
                                                                        \
    scm_t_array_handle v_handle;                                        \
    size_t v_length;                                                    \
    ssize_t stride;                                                     \
    const T *elems =                                                    \
      scm_##TXX##vector_elements (v, &v_handle, &v_length, &stride);    \
    scm_dynwind_array_handle_release (&v_handle);                       \
                                                                        \
    const ssize_t n = v_length;                                         \
                                                                        \
    PyObject *pyseq = SEQT##_New (n);                                   \
    py_exc_check ((pyseq != NULL), #SEQT "_New",                        \
                  scm_list_1 (scm_from_ssize_t (n)));                   \
                                                                        \
    for (ssize_t i = 0; i < n; i++)                                     \
      {                                                                 \
        PyObject *pyobj = CONVERT (elems[stride * i]);                  \
        if (pyobj == NULL)                                              \
          {                                                             \
            PyObject *type;                                             \
            PyObject *value;                                            \
            PyObject *traceback;                                        \
            PyErr_Fetch (&type, &value, &traceback);                    \
            for (ssize_t j = 0; j < i; j++)                             \
              Py_DECREF (SEQT##_GET_ITEM (pyseq, j));                   \
            Py_DECREF (pyseq);                                          \
            PyErr_Restore (type, value, traceback);                     \
            py_exc_check (false, #CONVERT, SCM_EOL);                    \
          }                                                             \
        SEQT##_SET_ITEM (pyseq, REINDEX, pyobj);                        \
      }                                                                 \
                                                                        \
    scm_dynwind_end ();                                                 \
                                                                        \
    return scm_c_make_pyobject (pyseq);                                 \
  }

#define SCM_UNIFORM_VECTOR_REINDEXED_TO_PYLIST(NAME, T, TXX, CONVERT,   \
                                               REINDEX)                 \
  SCM_UNIFORM_VECTOR_REINDEXED_TO_PYSEQ(PyList, NAME, T, TXX,           \
                                        CONVERT, REINDEX)               \

#define SCM_UNIFORM_VECTOR_TO_PYLIST(T, TXX, CONVERT)                   \
  SCM_UNIFORM_VECTOR_REINDEXED_TO_PYLIST (scm_##TXX##vector_to_pylist,  \
                                          T, TXX, CONVERT, (i))

#define SCM_UNIFORM_VECTOR_REVERSE_TO_PYLIST(T, TXX, CONVERT)           \
  SCM_UNIFORM_VECTOR_REINDEXED_TO_PYLIST (scm_##TXX##vector_reverse_to_pylist, \
                                          T, TXX, CONVERT, ((n - 1) - i))

#define SCM_UNIFORM_VECTOR_REINDEXED_TO_PYTUPLE(NAME, T, TXX, CONVERT,  \
                                                REINDEX)                \
  SCM_UNIFORM_VECTOR_REINDEXED_TO_PYSEQ(PyTuple, NAME, T, TXX,          \
                                        CONVERT, REINDEX)               \

#define SCM_UNIFORM_VECTOR_TO_PYTUPLE(T, TXX, CONVERT)                  \
  SCM_UNIFORM_VECTOR_REINDEXED_TO_PYTUPLE (scm_##TXX##vector_to_pytuple, \
                                           T, TXX, CONVERT, (i))

#define SCM_UNIFORM_VECTOR_REVERSE_TO_PYTUPLE(T, TXX, CONVERT)          \
  SCM_UNIFORM_VECTOR_REINDEXED_TO_PYTUPLE (scm_##TXX##vector_reverse_to_pytuple, \
                                           T, TXX, CONVERT, ((n - 1) - i))

#if SIZEOF_LONG < 4             // This is unlikely to be satisfied.
#error long int must be at least 4 bytes long.
#endif

#if SIZEOF_SIZE_T < 4           // This is unlikely to be satisfied.
#error size_t must be at least 4 bytes long.
#endif

VISIBLE SCM_UNIFORM_VECTOR_TO_PYLIST (int8_t, s8, PyIntOrLong_FromLong);
VISIBLE SCM_UNIFORM_VECTOR_TO_PYLIST (int16_t, s16, PyIntOrLong_FromLong);
VISIBLE SCM_UNIFORM_VECTOR_TO_PYLIST (int32_t, s32, PyIntOrLong_FromLong);

VISIBLE SCM_UNIFORM_VECTOR_TO_PYLIST (uint8_t, u8, PyIntOrLong_FromLong);
VISIBLE SCM_UNIFORM_VECTOR_TO_PYLIST (uint16_t, u16, PyIntOrLong_FromLong);
VISIBLE SCM_UNIFORM_VECTOR_TO_PYLIST (uint32_t, u32, PyIntOrLong_FromSize_t);

VISIBLE SCM_UNIFORM_VECTOR_REVERSE_TO_PYLIST (int8_t, s8, PyIntOrLong_FromLong);
VISIBLE SCM_UNIFORM_VECTOR_REVERSE_TO_PYLIST (int16_t, s16,
                                              PyIntOrLong_FromLong);
VISIBLE SCM_UNIFORM_VECTOR_REVERSE_TO_PYLIST (int32_t, s32,
                                              PyIntOrLong_FromLong);

VISIBLE SCM_UNIFORM_VECTOR_REVERSE_TO_PYLIST (uint8_t, u8,
                                              PyIntOrLong_FromLong);
VISIBLE SCM_UNIFORM_VECTOR_REVERSE_TO_PYLIST (uint16_t, u16,
                                              PyIntOrLong_FromLong);
VISIBLE SCM_UNIFORM_VECTOR_REVERSE_TO_PYLIST (uint32_t, u32,
                                              PyIntOrLong_FromSize_t);

VISIBLE SCM_UNIFORM_VECTOR_TO_PYTUPLE (int8_t, s8, PyIntOrLong_FromLong);
VISIBLE SCM_UNIFORM_VECTOR_TO_PYTUPLE (int16_t, s16, PyIntOrLong_FromLong);
VISIBLE SCM_UNIFORM_VECTOR_TO_PYTUPLE (int32_t, s32, PyIntOrLong_FromLong);

VISIBLE SCM_UNIFORM_VECTOR_TO_PYTUPLE (uint8_t, u8, PyIntOrLong_FromLong);
VISIBLE SCM_UNIFORM_VECTOR_TO_PYTUPLE (uint16_t, u16, PyIntOrLong_FromLong);
VISIBLE SCM_UNIFORM_VECTOR_TO_PYTUPLE (uint32_t, u32, PyIntOrLong_FromSize_t);

VISIBLE SCM_UNIFORM_VECTOR_REVERSE_TO_PYTUPLE (int8_t, s8,
                                               PyIntOrLong_FromLong);
VISIBLE SCM_UNIFORM_VECTOR_REVERSE_TO_PYTUPLE (int16_t, s16,
                                               PyIntOrLong_FromLong);
VISIBLE SCM_UNIFORM_VECTOR_REVERSE_TO_PYTUPLE (int32_t, s32,
                                               PyIntOrLong_FromLong);

VISIBLE SCM_UNIFORM_VECTOR_REVERSE_TO_PYTUPLE (uint8_t, u8,
                                               PyIntOrLong_FromLong);
VISIBLE SCM_UNIFORM_VECTOR_REVERSE_TO_PYTUPLE (uint16_t, u16,
                                               PyIntOrLong_FromLong);
VISIBLE SCM_UNIFORM_VECTOR_REVERSE_TO_PYTUPLE (uint32_t, u32,
                                               PyIntOrLong_FromSize_t);

VISIBLE SCM_UNIFORM_VECTOR_TO_PYLIST (float, f32, PyFloat_FromDouble);
VISIBLE SCM_UNIFORM_VECTOR_TO_PYLIST (double, f64, PyFloat_FromDouble);
VISIBLE SCM_UNIFORM_VECTOR_REVERSE_TO_PYLIST (float, f32, PyFloat_FromDouble);
VISIBLE SCM_UNIFORM_VECTOR_REVERSE_TO_PYLIST (double, f64, PyFloat_FromDouble);

VISIBLE SCM_UNIFORM_VECTOR_TO_PYTUPLE (float, f32, PyFloat_FromDouble);
VISIBLE SCM_UNIFORM_VECTOR_TO_PYTUPLE (double, f64, PyFloat_FromDouble);
VISIBLE SCM_UNIFORM_VECTOR_REVERSE_TO_PYTUPLE (float, f32, PyFloat_FromDouble);
VISIBLE SCM_UNIFORM_VECTOR_REVERSE_TO_PYTUPLE (double, f64, PyFloat_FromDouble);

//-------------------------------------------------------------------------

#define SCM_PYSEQ_MAP_REINDEXED_TO_UNIFORM_VECTOR(NAME, SCM_NAME,       \
                                                  T, TXX,               \
                                                  CONVERT, REINDEX)     \
  VISIBLE SCM                                                           \
  NAME (SCM proc, SCM obj_lst)                                          \
  {                                                                     \
    size_t obj_count = scm_to_size_t (scm_length (obj_lst));            \
    if (obj_count == 0)                                                 \
      rnrs_raise_condition                                              \
        (scm_list_4                                                     \
         (rnrs_make_assertion_violation (),                             \
          rnrs_c_make_who_condition (SCM_NAME),                         \
          rnrs_c_make_message_condition (_("too few arguments")),       \
          rnrs_make_irritants_condition (SCM_EOL)));                    \
                                                                        \
    scm_dynwind_begin (0);                                              \
                                                                        \
    PyObject **fast = scm_malloc (obj_count * sizeof (PyObject *));     \
    scm_dynwind_free (fast);                                            \
                                                                        \
    SCM p = obj_lst;                                                    \
    for (size_t i = 0; i < obj_count; i++)                              \
      {                                                                 \
        scm_assert_pyobject (SCM_CAR (p));                              \
        fast[i] =                                                       \
          PySequence_Fast (scm_to_PyObject_ptr (SCM_CAR (p)),           \
                           "");                                         \
        if (fast[i] == NULL)                                            \
          scm_c_py_exc_check_violation                                  \
            (SCM_NAME,                                                  \
             _("PySequence_Fast raised a Python exception"),            \
             scm_list_1 (SCM_CAR (p)));                                 \
        scm_dynwind_decref (fast[i]);                                   \
        p = SCM_CDR (p);                                                \
      }                                                                 \
                                                                        \
    ssize_t n = SSIZE_MAX;                                              \
    PyObject ***items = scm_malloc (obj_count * sizeof (PyObject **));  \
    scm_dynwind_free (items);                                           \
    for (size_t i = 0; i < obj_count; i++)                              \
      {                                                                 \
        n = ssz_min (n, PySequence_Fast_GET_SIZE (fast[i]));            \
        items[i] = PySequence_Fast_ITEMS (fast[i]);                     \
      }                                                                 \
                                                                        \
    T *data = (T *) scm_malloc (n * sizeof (T));                        \
    SCM v = scm_take_##TXX##vector (data, n);                           \
    for (ssize_t j = 0; j < n; j++)                                     \
      {                                                                 \
        SCM args = SCM_EOL;                                             \
        for (size_t i = 0; i < obj_count; i++)                          \
          {                                                             \
            PyObject *item = items[obj_count - i - 1][j];               \
            args = scm_cons (scm_c_incref_make_pyobject (item), args);  \
          }                                                             \
        data[REINDEX] = (T) CONVERT (scm_apply_0 (proc, args));         \
      }                                                                 \
                                                                        \
    scm_dynwind_end ();                                                 \
                                                                        \
    return v;                                                           \
  }

#define SCM_PYSEQ_MAP_TO_UNIFORM_VECTOR(T, TXX, CONVERT)        \
  SCM_PYSEQ_MAP_REINDEXED_TO_UNIFORM_VECTOR                     \
  (scm_pysequence_map_to_##TXX##vector,                         \
   "pysequence-map->" #TXX "vector", T, TXX, CONVERT, (j))

VISIBLE SCM_PYSEQ_MAP_TO_UNIFORM_VECTOR (int8_t, s8, scm_to_int8);
VISIBLE SCM_PYSEQ_MAP_TO_UNIFORM_VECTOR (int16_t, s16, scm_to_int16);
VISIBLE SCM_PYSEQ_MAP_TO_UNIFORM_VECTOR (int32_t, s32, scm_to_int32);
VISIBLE SCM_PYSEQ_MAP_TO_UNIFORM_VECTOR (int64_t, s64, scm_to_int64);

VISIBLE SCM_PYSEQ_MAP_TO_UNIFORM_VECTOR (uint8_t, u8, scm_to_uint8);
VISIBLE SCM_PYSEQ_MAP_TO_UNIFORM_VECTOR (uint16_t, u16, scm_to_uint16);
VISIBLE SCM_PYSEQ_MAP_TO_UNIFORM_VECTOR (uint32_t, u32, scm_to_uint32);
VISIBLE SCM_PYSEQ_MAP_TO_UNIFORM_VECTOR (uint64_t, u64, scm_to_uint64);

VISIBLE SCM_PYSEQ_MAP_TO_UNIFORM_VECTOR (float, f32, scm_to_double);
VISIBLE SCM_PYSEQ_MAP_TO_UNIFORM_VECTOR (double, f64, scm_to_double);

//-------------------------------------------------------------------------

VISIBLE SCM
scm_c_pysequence_take (SCM obj, ssize_t n)
{
  SCM_ASSERT_TYPE ((0 <= n), scm_from_ssize_t (n), SCM_ARG2, "pysequence-take",
                   "nonnegative integer");
  const ssize_t length = scm_c_pysequence_length (obj);
  if (length < n)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("pysequence-take"),
        rnrs_c_make_message_condition (_("item count too great")),
        rnrs_make_irritants_condition
        (scm_list_2 (obj, scm_from_ssize_t (n)))));
  return scm_c_pysequence_getslice (obj, 0, n);
}

VISIBLE SCM
scm_pysequence_take (SCM obj, SCM n)
{
  return scm_c_pysequence_take (obj, scm_to_ssize_t (n));
}

VISIBLE SCM
scm_c_pysequence_drop (SCM obj, ssize_t n)
{
  SCM_ASSERT_TYPE ((0 <= n), scm_from_ssize_t (n), SCM_ARG2, "pysequence-drop",
                   "nonnegative integer");
  const ssize_t length = scm_c_pysequence_length (obj);
  if (length < n)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("pysequence-drop"),
        rnrs_c_make_message_condition (_("item count too great")),
        rnrs_make_irritants_condition
        (scm_list_2 (obj, scm_from_ssize_t (n)))));
  return scm_c_pysequence_getslice (obj, n, length);
}

VISIBLE SCM
scm_pysequence_drop (SCM obj, SCM n)
{
  return scm_c_pysequence_drop (obj, scm_to_ssize_t (n));
}

VISIBLE SCM
scm_c_pysequence_take_right (SCM obj, ssize_t n)
{
  SCM_ASSERT_TYPE ((0 <= n), scm_from_ssize_t (n), SCM_ARG2,
                   "pysequence-take-right", "nonnegative integer");
  const ssize_t length = scm_c_pysequence_length (obj);
  if (length < n)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("pysequence-take-right"),
        rnrs_c_make_message_condition (_("item count too great")),
        rnrs_make_irritants_condition
        (scm_list_2 (obj, scm_from_ssize_t (n)))));
  return scm_c_pysequence_getslice (obj, length - n, length);
}

VISIBLE SCM
scm_pysequence_take_right (SCM obj, SCM n)
{
  return scm_c_pysequence_take_right (obj, scm_to_ssize_t (n));
}

VISIBLE SCM
scm_c_pysequence_drop_right (SCM obj, ssize_t n)
{
  SCM_ASSERT_TYPE ((0 <= n), scm_from_ssize_t (n), SCM_ARG2,
                   "pysequence-drop-right", "nonnegative integer");
  const ssize_t length = scm_c_pysequence_length (obj);
  if (length < n)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("pysequence-drop-right"),
        rnrs_c_make_message_condition (_("item count too great")),
        rnrs_make_irritants_condition
        (scm_list_2 (obj, scm_from_ssize_t (n)))));
  return scm_c_pysequence_getslice (obj, 0, length - n);
}

VISIBLE SCM
scm_pysequence_drop_right (SCM obj, SCM n)
{
  return scm_c_pysequence_drop_right (obj, scm_to_ssize_t (n));
}

//-------------------------------------------------------------------------

VISIBLE SCM
scm_pylist_insert_x (SCM pylist, SCM i, SCM item)
{
  scm_assert_pyobject (pylist);
  PyObject *_pylist = scm_to_PyObject_ptr (pylist);
  SCM_ASSERT_TYPE ((PyList_Check (_pylist)), pylist, SCM_ARG1,
                   "pylist-insert!", "pylist");

  if (scm_is_pyobject (i))
    i = scm_pyinteger_to_integer (i);
  const ssize_t _i = scm_to_ssize_t (i);

  scm_assert_pyobject (item);
  PyObject *_item = scm_to_PyObject_ptr (item);

  int errval = PyList_Insert (_pylist, _i, _item);
  py_exc_check ((errval != -1), "pylist-insert!", scm_list_2 (pylist, item));

  return SCM_UNSPECIFIED;
}

VISIBLE SCM
scm_pylist_append_x (SCM pylist, SCM item)
{
  scm_assert_pyobject (pylist);
  PyObject *_pylist = scm_to_PyObject_ptr (pylist);
  SCM_ASSERT_TYPE ((PyList_Check (_pylist)), pylist, SCM_ARG1,
                   "pylist-append!", "pylist");

  scm_assert_pyobject (item);
  PyObject *_item = scm_to_PyObject_ptr (item);

  int errval = PyList_Append (_pylist, _item);
  py_exc_check ((errval != -1), "pylist-append!", scm_list_2 (pylist, item));

  return SCM_UNSPECIFIED;
}

VISIBLE SCM
scm_pylist_sort_x (SCM pylist)
{
  scm_assert_pyobject (pylist);
  PyObject *_pylist = scm_to_PyObject_ptr (pylist);
  SCM_ASSERT_TYPE ((PyList_Check (_pylist)), pylist, SCM_ARG1,
                   "pylist-sort!", "pylist");

  int errval = PyList_Sort (_pylist);
  py_exc_check ((errval != -1), "pylist-sort!", scm_list_1 (pylist));

  return SCM_UNSPECIFIED;
}

VISIBLE SCM
scm_pylist_reverse_x (SCM pylist)
{
  scm_assert_pyobject (pylist);
  PyObject *_pylist = scm_to_PyObject_ptr (pylist);
  SCM_ASSERT_TYPE ((PyList_Check (_pylist)), pylist, SCM_ARG1,
                   "pylist-reverse!", "pylist");

  int errval = PyList_Reverse (_pylist);
  py_exc_check ((errval != -1), "pylist-reverse!", scm_list_1 (pylist));

  return SCM_UNSPECIFIED;
}

//-------------------------------------------------------------------------
