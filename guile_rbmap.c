#include <config.h>

// Copyright (C) 2013, 2014 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <libintl.h>
#include <sortsmill/guile/core.h>

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

//-------------------------------------------------------------------------

#define _SORTSMILL_CORE_RBMAP_DEFNS(T, scm_from_T, scm_to_T, rbmapX)    \
                                                                        \
  STM_SCM_ONE_TIME_INITIALIZE                                           \
  (STM_ATTRIBUTE_PURE static, SCM, proc_##rbmapX##_p,                   \
   proc_##rbmapX##_p__Value =                                           \
   scm_c_compile_eval_utf8_string                                       \
   ("(@@ (sortsmill core rbmap generic) " #rbmapX "?)"));               \
                                                                        \
  VISIBLE bool                                                          \
  scm_is_##rbmapX (SCM obj)                                             \
  {                                                                     \
    return scm_is_true (scm_call_1 (proc_##rbmapX##_p (), obj));        \
  }                                                                     \
                                                                        \
  VISIBLE SCM                                                           \
  scm_##rbmapX##_p (SCM obj)                                            \
  {                                                                     \
    return scm_call_1 (proc_##rbmapX##_p (), obj);                      \
  }                                                                     \
                                                                        \
  STM_SCM_ONE_TIME_INITIALIZE                                           \
  (STM_ATTRIBUTE_PURE static, SCM, _pointer_to_##rbmapX,                \
   _pointer_to_##rbmapX##__Value =                                      \
   scm_c_compile_eval_utf8_string                                       \
   ("(@@ (sortsmill core rbmap generic) pointer->" #rbmapX ")"));       \
                                                                        \
  STM_SCM_ONE_TIME_INITIALIZE                                           \
  (STM_ATTRIBUTE_PURE static, SCM, _##rbmapX##_to_pointer,              \
   _##rbmapX##_to_pointer__Value =                                      \
   scm_c_compile_eval_utf8_string                                       \
   ("(@@ (sortsmill core rbmap generic)" #rbmapX "->pointer)"));        \
                                                                        \
  typedef struct rbmapX##_node_s rbmapX##_node_t;                       \
                                                                        \
  struct rbmapX##_node_s                                                \
  {                                                                     \
    /* `key' and `value' must appear here, at the  */                   \
    /*  head of the struct, in this order.         */                   \
    T key;                                                              \
    SCM value;                                                          \
    rb_node (rbmapX##_node_t) rbmapX##_link;                            \
  };                                                                    \
                                                                        \
  /* The line-break in the next type typedef helps       */             \
  /* the C mode of GNU Emacs 23 avoid becoming confused. */             \
  typedef rb_tree (rbmapX##_node_t)                                     \
    rbmapX##_t;                                                         \
                                                                        \
  static int                                                            \
  rbmapX##_cmp (rbmapX##_node_t *a_node, rbmapX##_node_t *a_other)      \
  {                                                                     \
    int cmp;                                                            \
    if (a_node->key < a_other->key)                                     \
      cmp = -1;                                                         \
    else if (a_other->key < a_node->key)                                \
      cmp = 1;                                                          \
    else                                                                \
      cmp = 0;                                                          \
    return cmp;                                                         \
  }                                                                     \
                                                                        \
  rb_gen (STM_MAYBE_UNUSED static, rbmapX##_, rbmapX##_t,               \
          rbmapX##_node_t, rbmapX##_link, rbmapX##_cmp);                \
                                                                        \
  static SCM                                                            \
  scm_from_c_##rbmapX (rbmapX##_t *p)                                   \
  {                                                                     \
    return scm_call_1 (_pointer_to_##rbmapX (),                         \
                       scm_from_pointer (p, NULL));                     \
  }                                                                     \
                                                                        \
  static rbmapX##_t *                                                   \
  scm_to_c_##rbmapX (SCM map)                                           \
  {                                                                     \
    return (rbmapX##_t *)                                               \
      scm_to_pointer (scm_call_1 (_##rbmapX##_to_pointer (), map));     \
  }                                                                     \
                                                                        \
  /* Create instances of inline functions. */                           \
  VISIBLE T scm_##rbmapX##_iter_key (scm_t_##rbmapX##_iter iter);       \
  VISIBLE SCM scm_##rbmapX##_iter_value (scm_t_##rbmapX##_iter iter);   \
  VISIBLE void scm_##rbmapX##_iter_set_value (scm_t_##rbmapX##_iter     \
                                              iter,                     \
                                              SCM value);               \
                                                                        \
  VISIBLE scm_t_##rbmapX##_iter                                         \
  scm_c_##rbmapX##_first (SCM map)                                      \
  {                                                                     \
    return (scm_t_##rbmapX##_iter)                                      \
      rbmapX##_first (scm_to_c_##rbmapX (map));                         \
  }                                                                     \
                                                                        \
  VISIBLE scm_t_##rbmapX##_iter                                         \
  scm_c_##rbmapX##_last (SCM map)                                       \
  {                                                                     \
    return (scm_t_##rbmapX##_iter)                                      \
      rbmapX##_last (scm_to_c_##rbmapX (map));                          \
  }                                                                     \
                                                                        \
  VISIBLE scm_t_##rbmapX##_iter                                         \
  scm_c_##rbmapX##_nsearch (SCM map, SCM key)                           \
  {                                                                     \
    rbmapX##_t *tree = scm_to_c_##rbmapX (map);                         \
    scm_t_##rbmapX##_iter iter;                                         \
    if (SCM_UNBNDP (key) || scm_is_false (key))                         \
      iter = (scm_t_##rbmapX##_iter) rbmapX##_first (tree);             \
    else                                                                \
      {                                                                 \
        const T i_key = scm_to_T (key);                                 \
        rbmapX##_node_t key_node = {.key = i_key };                     \
        iter =                                                          \
          (scm_t_##rbmapX##_iter) rbmapX##_nsearch (tree, &key_node);   \
      }                                                                 \
    return iter;                                                        \
  }                                                                     \
                                                                        \
  VISIBLE scm_t_##rbmapX##_iter                                         \
  scm_c_##rbmapX##_psearch (SCM map, SCM key)                           \
  {                                                                     \
    rbmapX##_t *tree = scm_to_c_##rbmapX (map);                         \
    scm_t_##rbmapX##_iter iter;                                         \
    if (SCM_UNBNDP (key) || scm_is_false (key))                         \
      iter = (scm_t_##rbmapX##_iter) rbmapX##_last (tree);              \
    else                                                                \
      {                                                                 \
        const T i_key = scm_to_T (key);                                 \
        rbmapX##_node_t key_node = {.key = i_key };                     \
        iter =                                                          \
          (scm_t_##rbmapX##_iter) rbmapX##_psearch (tree, &key_node);   \
      }                                                                 \
    return iter;                                                        \
  }                                                                     \
                                                                        \
  VISIBLE scm_t_##rbmapX##_iter                                         \
  scm_c_##rbmapX##_next (SCM map, scm_t_##rbmapX##_iter iter)           \
  {                                                                     \
    return (scm_t_##rbmapX##_iter)                                      \
      rbmapX##_next (scm_to_c_##rbmapX (map),                           \
                     (rbmapX##_node_t *) iter);                         \
  }                                                                     \
                                                                        \
  VISIBLE scm_t_##rbmapX##_iter                                         \
  scm_c_##rbmapX##_prev (SCM map, scm_t_##rbmapX##_iter iter)           \
  {                                                                     \
    return (scm_t_##rbmapX##_iter)                                      \
      rbmapX##_prev (scm_to_c_##rbmapX (map),                           \
                     (rbmapX##_node_t *) iter);                         \
  }                                                                     \
                                                                        \
  VISIBLE SCM                                                           \
  scm_make_##rbmapX (void)                                              \
  {                                                                     \
    rbmapX##_t *tree =                                                  \
      scm_gc_malloc (sizeof (rbmapX##_t), #rbmapX "_t");                \
    rbmapX##_new (tree);                                                \
    return scm_from_c_##rbmapX (tree);                                  \
  }                                                                     \
                                                                        \
  static inline void                                                    \
  internal_##rbmapX##_insert (rbmapX##_t *tree, T i_key, SCM value)     \
  {                                                                     \
    rbmapX##_node_t *node =                                             \
      scm_gc_malloc (sizeof (rbmapX##_node_t), #rbmapX "_node_t");      \
    node->key = i_key;                                                  \
    node->value = value;                                                \
    rbmapX##_insert (tree, node);                                       \
  }                                                                     \
                                                                        \
  VISIBLE SCM                                                           \
  scm_##rbmapX##_set_x (SCM map, SCM key, SCM value)                    \
  {                                                                     \
    rbmapX##_t *tree = scm_to_c_##rbmapX (map);                         \
    const T i_key = scm_to_T (key);                                     \
    rbmapX##_node_t key_node = {.key = i_key };                         \
                                                                        \
    rbmapX##_node_t *node = rbmapX##_search (tree, &key_node);          \
    if (node == NULL)                                                   \
      internal_##rbmapX##_insert (tree, i_key, value);                  \
    else                                                                \
      node->value = value;                                              \
                                                                        \
    return SCM_UNSPECIFIED;                                             \
  }                                                                     \
                                                                        \
  VISIBLE SCM                                                           \
  scm_##rbmapX##_delete_x (SCM map, SCM key)                            \
  {                                                                     \
    rbmapX##_t *tree = scm_to_c_##rbmapX (map);                         \
    const T i_key = scm_to_T (key);                                     \
    rbmapX##_node_t key_node = {.key = i_key };                         \
                                                                        \
    rbmapX##_node_t *node = rbmapX##_search (tree, &key_node);          \
                                                                        \
    if (node != NULL)                                                   \
      rbmapX##_remove (tree, node);                                     \
                                                                        \
    return SCM_UNSPECIFIED;                                             \
  }                                                                     \
                                                                        \
  VISIBLE SCM                                                           \
  scm_##rbmapX##_ref (SCM map, SCM key, SCM default_value)              \
  {                                                                     \
    SCM dflt = SCM_UNBNDP (default_value) ? SCM_BOOL_F : default_value; \
                                                                        \
    rbmapX##_t *tree = scm_to_c_##rbmapX (map);                         \
    const T i_key = scm_to_T (key);                                     \
    rbmapX##_node_t key_node = {.key = i_key };                         \
                                                                        \
    const rbmapX##_node_t *node = rbmapX##_search (tree, &key_node);    \
                                                                        \
    return (node == NULL) ? dflt : node->value;                         \
  }                                                                     \
                                                                        \
  VISIBLE SCM                                                           \
  scm_##rbmapX##_fold_left (SCM proc, SCM init, SCM map, SCM start_key) \
  {                                                                     \
    for (scm_t_##rbmapX##_iter p =                                      \
           scm_c_##rbmapX##_nsearch (map, start_key);                   \
         p != NULL;                                                     \
         p = scm_c_##rbmapX##_next (map, p))                            \
      init = scm_call_3 (proc, init,                                    \
                         scm_from_T (scm_##rbmapX##_iter_key (p)),      \
                         scm_##rbmapX##_iter_value (p));                \
    return init;                                                        \
  }                                                                     \
                                                                        \
  VISIBLE SCM                                                           \
  scm_##rbmapX##_fold_right (SCM proc, SCM init, SCM map,               \
                             SCM start_key)                             \
  {                                                                     \
    for (scm_t_##rbmapX##_iter p =                                      \
           scm_c_##rbmapX##_psearch (map, start_key);                   \
         p != NULL;                                                     \
         p = scm_c_##rbmapX##_prev (map, p))                            \
      init = scm_call_3 (proc,                                          \
                         scm_from_T (scm_##rbmapX##_iter_key (p)),      \
                         scm_##rbmapX##_iter_value (p), init);          \
    return init;                                                        \
  }                                                                     \
                                                                        \
  VISIBLE SCM                                                           \
  scm_alist_to_##rbmapX (SCM alist)                                     \
  {                                                                     \
    rbmapX##_node_t key_node;                                           \
    SCM map = scm_make_##rbmapX ();                                     \
    rbmapX##_t *tree = scm_to_c_##rbmapX (map);                         \
    for (SCM p = alist; !scm_is_null (p); p = SCM_CDR (p))              \
      {                                                                 \
        SCM_ASSERT_TYPE ((scm_is_pair (p)                               \
                          && scm_is_pair (SCM_CAR (p))),                \
                         alist, SCM_ARG1, "alist->" #rbmapX,            \
                         "association list");                           \
        const T i_key = scm_to_T (SCM_CAAR (p));                        \
        key_node.key = i_key;                                           \
        rbmapX##_node_t *node = rbmapX##_search (tree, &key_node);      \
        if (node == NULL)                                               \
          internal_##rbmapX##_insert (tree, i_key, SCM_CDAR (p));       \
      }                                                                 \
    return map;                                                         \
  }                                                                     \
                                                                        \
  VISIBLE SCM                                                           \
  scm_##rbmapX##_to_alist (SCM map)                                     \
  {                                                                     \
    SCM alist = SCM_EOL;                                                \
                                                                        \
    /* Go backwards, so consing preserves the order. */                 \
    for (scm_t_##rbmapX##_iter p = scm_c_##rbmapX##_last (map);         \
         p != NULL;                                                     \
         p = scm_c_##rbmapX##_prev (map, p))                            \
      alist = scm_acons (scm_from_T (scm_##rbmapX##_iter_key (p)),      \
                         scm_##rbmapX##_iter_value (p), alist);         \
                                                                        \
    return alist;                                                       \
  }                                                                     \
                                                                        \
  VISIBLE SCM                                                           \
  scm_plist_to_##rbmapX (SCM plist)                                     \
  {                                                                     \
    rbmapX##_node_t key_node;                                           \
    SCM map = scm_make_##rbmapX ();                                     \
    rbmapX##_t *tree = scm_to_c_##rbmapX (map);                         \
    for (SCM p = plist; !scm_is_null (p); p = SCM_CDDR (p))             \
      {                                                                 \
        SCM_ASSERT_TYPE ((scm_is_pair (p)                               \
                          && scm_is_pair (SCM_CDR (p))),                \
                         plist, SCM_ARG1, "plist->" #rbmapX,            \
                         "list of even length");                        \
        const T i_key = scm_to_T (SCM_CAR (p));                         \
        key_node.key = i_key;                                           \
        rbmapX##_node_t *node = rbmapX##_search (tree, &key_node);      \
        if (node == NULL)                                               \
          internal_##rbmapX##_insert (tree, i_key, SCM_CADR (p));       \
      }                                                                 \
    return map;                                                         \
  }                                                                     \
                                                                        \
  VISIBLE SCM                                                           \
  scm_##rbmapX##_to_plist (SCM map)                                     \
  {                                                                     \
    SCM plist = SCM_EOL;                                                \
                                                                        \
    /* Go backwards, so consing preserves the order. */                 \
    for (scm_t_##rbmapX##_iter p = scm_c_##rbmapX##_last (map);         \
         p != NULL;                                                     \
         p = scm_c_##rbmapX##_prev (map, p))                            \
      plist = scm_cons (scm_from_T (scm_##rbmapX##_iter_key (p)),       \
                        scm_cons (scm_##rbmapX##_iter_value (p),        \
                                  plist));                              \
                                                                        \
    return plist;                                                       \
  }                                                                     \
                                                                        \
  VISIBLE SCM                                                           \
  scm_##rbmapX##_keys (SCM map)                                         \
  {                                                                     \
    SCM lst = SCM_EOL;                                                  \
                                                                        \
    /* Go backwards, so consing preserves the order. */                 \
    for (scm_t_##rbmapX##_iter p = scm_c_##rbmapX##_last (map);         \
         p != NULL;                                                     \
         p = scm_c_##rbmapX##_prev (map, p))                            \
      lst = scm_cons (scm_from_T (scm_##rbmapX##_iter_key (p)), lst);   \
                                                                        \
    return lst;                                                         \
  }                                                                     \
                                                                        \
  VISIBLE SCM                                                           \
  scm_##rbmapX##_values (SCM map)                                       \
  {                                                                     \
    SCM lst = SCM_EOL;                                                  \
                                                                        \
    /* Go backwards, so consing preserves the order. */                 \
    for (scm_t_##rbmapX##_iter p = scm_c_##rbmapX##_last (map);         \
         p != NULL;                                                     \
         p = scm_c_##rbmapX##_prev (map, p))                            \
      lst = scm_cons (scm_##rbmapX##_iter_value (p), lst);              \
                                                                        \
    return lst;                                                         \
  }                                                                     \
                                                                        \
  VISIBLE SCM                                                           \
  scm_##rbmapX##_map_to_list (SCM proc, SCM map)                        \
  {                                                                     \
    SCM lst = SCM_EOL;                                                  \
                                                                        \
    /* Go backwards, so consing preserves the order. */                 \
    for (scm_t_##rbmapX##_iter p = scm_c_##rbmapX##_last (map);         \
         p != NULL;                                                     \
         p = scm_c_##rbmapX##_prev (map, p))                            \
      {                                                                 \
        SCM element =                                                   \
          scm_call_2 (proc, scm_from_T (scm_##rbmapX##_iter_key (p)),   \
                      scm_##rbmapX##_iter_value (p));                   \
        lst = scm_cons (element, lst);                                  \
      }                                                                 \
                                                                        \
    return lst;                                                         \
  }                                                                     \
                                                                        \
  VISIBLE SCM                                                           \
  scm_##rbmapX##_for_each (SCM proc, SCM map)                           \
  {                                                                     \
    for (scm_t_##rbmapX##_iter p = scm_c_##rbmapX##_first (map);        \
         p != NULL;                                                     \
         p = scm_c_##rbmapX##_next (map, p))                            \
      scm_call_2 (proc, scm_from_T (scm_##rbmapX##_iter_key (p)),       \
                  scm_##rbmapX##_iter_value (p));                       \
    return SCM_UNSPECIFIED;                                             \
  }                                                                     \
                                                                        \
  VISIBLE SCM                                                           \
  scm_##rbmapX##_count (SCM pred, SCM map)                              \
  {                                                                     \
    SCM count = scm_from_int (0);                                       \
    for (scm_t_##rbmapX##_iter p = scm_c_##rbmapX##_first (map);        \
         p != NULL;                                                     \
         p = scm_c_##rbmapX##_next (map, p))                            \
      if (scm_is_true                                                   \
          (scm_call_2 (pred, scm_from_T (scm_##rbmapX##_iter_key (p)),  \
                       scm_##rbmapX##_iter_value (p))))                 \
        count = scm_oneplus (count);                                    \
    return count;                                                       \
  }                                                                     \
                                                                        \
  VISIBLE SCM                                                           \
  scm_##rbmapX##_size (SCM map)                                         \
  {                                                                     \
    SCM size = scm_from_int (0);                                        \
    for (scm_t_##rbmapX##_iter p = scm_c_##rbmapX##_first (map);        \
         p != NULL;                                                     \
         p = scm_c_##rbmapX##_next (map, p))                            \
      size = scm_oneplus (size);                                        \
    return size;                                                        \
  }                                                                     \
                                                                        \
  VISIBLE bool                                                          \
  scm_##rbmapX##_is_null (SCM map)                                      \
  {                                                                     \
    return (scm_c_##rbmapX##_first (map) == NULL);                      \
  }                                                                     \
                                                                        \
  VISIBLE SCM                                                           \
  scm_##rbmapX##_null_p (SCM map)                                       \
  {                                                                     \
    return scm_from_bool (scm_##rbmapX##_is_null (map));                \
  }

/* Store keys as unsigned integers. */
_SORTSMILL_CORE_RBMAP_DEFNS (uint8_t, scm_from_uint8, scm_to_uint8, rbmapu8);
_SORTSMILL_CORE_RBMAP_DEFNS (uint16_t, scm_from_uint16, scm_to_uint16,
                             rbmapu16);
_SORTSMILL_CORE_RBMAP_DEFNS (uint32_t, scm_from_uint32, scm_to_uint32,
                             rbmapu32);
_SORTSMILL_CORE_RBMAP_DEFNS (uint64_t, scm_from_uint64, scm_to_uint64,
                             rbmapu64);
_SORTSMILL_CORE_RBMAP_DEFNS (unsigned int, scm_from_uint, scm_to_uint, rbmapui);
_SORTSMILL_CORE_RBMAP_DEFNS (unsigned long int, scm_from_ulong, scm_to_ulong,
                             rbmapul);
_SORTSMILL_CORE_RBMAP_DEFNS (size_t, scm_from_size_t, scm_to_size_t, rbmapusz);
_SORTSMILL_CORE_RBMAP_DEFNS (uintmax_t, scm_from_uintmax, scm_to_uintmax,
                             rbmapumx);

/* Store keys as signed integers. */
_SORTSMILL_CORE_RBMAP_DEFNS (int8_t, scm_from_int8, scm_to_int8, rbmaps8);
_SORTSMILL_CORE_RBMAP_DEFNS (int16_t, scm_from_int16, scm_to_int16, rbmaps16);
_SORTSMILL_CORE_RBMAP_DEFNS (int32_t, scm_from_int32, scm_to_int32, rbmaps32);
_SORTSMILL_CORE_RBMAP_DEFNS (int64_t, scm_from_int64, scm_to_int64, rbmaps64);
_SORTSMILL_CORE_RBMAP_DEFNS (int, scm_from_int, scm_to_int, rbmapsi);
_SORTSMILL_CORE_RBMAP_DEFNS (long int, scm_from_long, scm_to_long, rbmapsl);
_SORTSMILL_CORE_RBMAP_DEFNS (ssize_t, scm_from_ssize_t, scm_to_ssize_t,
                             rbmapssz);
_SORTSMILL_CORE_RBMAP_DEFNS (intmax_t, scm_from_intmax, scm_to_intmax,
                             rbmapsmx);

/* Use SCM objects as keys. The name `rbmapq' is supposed to remind
   one of `eq?' */
_SORTSMILL_CORE_RBMAP_DEFNS (scm_t_bits, SCM_PACK, SCM_UNPACK, rbmapq);

//-------------------------------------------------------------------------

static SCM
_scm_map_alist_keys (SCM proc, SCM alist)
{
  SCM result = SCM_EOL;
  for (SCM p = alist; !scm_is_null (p); p = SCM_CDR (p))
    {
      SCM_ASSERT_TYPE ((scm_is_pair (p) && scm_is_pair (SCM_CAR (p))),
                       alist, SCM_ARG2, "map-alist-keys", "alist");
      result = scm_acons (scm_call_1 (proc, SCM_CAAR (p)), SCM_CDAR (p),
                          result);
    }
  return scm_reverse_x_without_checking (result, SCM_EOL);
}

static SCM
_scm_map_plist_keys (SCM proc, SCM plist)
{
  SCM result = SCM_EOL;
  for (SCM p = plist; !scm_is_null (p); p = SCM_CDDR (p))
    {
      SCM_ASSERT_TYPE ((scm_is_pair (p) && scm_is_pair (SCM_CDR (p))),
                       plist, SCM_ARG2, "map-plist-keys",
                       "list of even length");
      result = scm_cons (SCM_CADR (p),
                         scm_cons (scm_call_1 (proc, SCM_CAR (p)), result));
    }
  return scm_reverse_x_without_checking (result, SCM_EOL);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

static void
raise_bad_key (SCM key, SCM who)
{
  rnrs_raise_condition
    (scm_list_4
     (rnrs_make_assertion_violation (),
      rnrs_make_who_condition (who),
      rnrs_c_make_message_condition (_("illegal key")),
      rnrs_make_irritants_condition (scm_list_1 (key))));
}

static SCM
_scm_check_return_key (SCM predicate, SCM key, SCM who)
{
  if (scm_is_false (scm_call_1 (predicate, key)))
    raise_bad_key (key, who);
  return key;
}

static SCM
_scm_check_return_alist (SCM predicate, SCM alist, SCM who)
{
  for (SCM p = alist; !scm_is_null (p); p = SCM_CDR (p))
    {
      if (!scm_is_pair (p) || !scm_is_pair (SCM_CAR (p)))
        rnrs_raise_condition
          (scm_list_4
           (rnrs_make_assertion_violation (),
            rnrs_make_who_condition (who),
            rnrs_c_make_message_condition (_("expected an alist")),
            rnrs_make_irritants_condition (scm_list_1 (alist))));
      if (scm_is_false (scm_call_1 (predicate, SCM_CAAR (p))))
        raise_bad_key (SCM_CAAR (p), who);
    }
  return alist;
}

static SCM
_scm_check_return_plist (SCM predicate, SCM plist, SCM who)
{
  for (SCM p = plist; !scm_is_null (p); p = SCM_CDDR (p))
    {
      if (!scm_is_pair (p) || !scm_is_pair (SCM_CDR (p)))
        rnrs_raise_condition
          (scm_list_4
           (rnrs_make_assertion_violation (),
            rnrs_make_who_condition (who),
            rnrs_c_make_message_condition (_("expected a list of even length")),
            rnrs_make_irritants_condition (scm_list_1 (plist))));
      if (scm_is_false (scm_call_1 (predicate, SCM_CAR (p))))
        raise_bad_key (SCM_CAR (p), who);
    }
  return plist;
}

static SCM
_scm_check_return_list (SCM predicate, SCM list, SCM who)
{
  for (SCM p = list; !scm_is_null (p); p = SCM_CDR (p))
    {
      if (!scm_is_pair (p))
        rnrs_raise_condition
          (scm_list_4
           (rnrs_make_assertion_violation (),
            rnrs_make_who_condition (who),
            rnrs_c_make_message_condition (_("expected a list")),
            rnrs_make_irritants_condition (scm_list_1 (list))));
      if (scm_is_false (scm_call_1 (predicate, SCM_CAR (p))))
        raise_bad_key (SCM_CAR (p), who);
    }
  return list;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

void init_guile_sortsmill_rbmap (void);

VISIBLE void
init_guile_sortsmill_rbmap (void)
{
  scm_c_define_gsubr ("map-alist-keys", 2, 0, 0, _scm_map_alist_keys);
  scm_c_define_gsubr ("map-plist-keys", 2, 0, 0, _scm_map_plist_keys);
  scm_c_define_gsubr ("check-return-key", 3, 0, 0, _scm_check_return_key);
  scm_c_define_gsubr ("check-return-alist", 3, 0, 0, _scm_check_return_alist);
  scm_c_define_gsubr ("check-return-plist", 3, 0, 0, _scm_check_return_plist);
  scm_c_define_gsubr ("check-return-list", 3, 0, 0, _scm_check_return_list);
}

//-------------------------------------------------------------------------
