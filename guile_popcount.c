#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <stdint.h>
#include <limits.h>
#include <gmp.h>
#include <sortsmill/guile/core.h>

VISIBLE SCM
scm_popcount (SCM i)
{
  SCM_ASSERT_TYPE (scm_is_exact_integer (i), i, SCM_ARG1, "popcount",
                   "exact integer");

  SCM popcount;

  if (scm_is_unsigned_integer (i, 0, ULONG_MAX))
    {
      const unsigned long int i_ = scm_to_ulong (i);
#if UINT_MAX < ULONG_MAX
      popcount = scm_from_int ((i_ <= UINT_MAX) ?
                               smcore_popcount ((unsigned int) i_) :
                               smcore_popcountl (i_));
#else
      popcount = scm_from_int (smcore_popcount ((unsigned int) i_));
#endif
    }
  else if (scm_is_unsigned_integer (i, 0, ULLONG_MAX))
    {
      const unsigned long long int i_ = scm_to_ulong_long (i);
      popcount = scm_from_int (smcore_popcountll (i_));
    }
  else
    {
      if (scm_is_true (scm_negative_p (i)))
        scm_out_of_range ("popcount", i);
      mpz_t i_;
      mpz_init (i_);
      scm_to_mpz (i, i_);
      popcount = scm_from_unsigned_integer (mpz_popcount (i_));
      mpz_clear (i_);
    }

  return popcount;
}
